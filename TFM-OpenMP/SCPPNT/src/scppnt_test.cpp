// scppnt_test.cpp
// Example program illustrating some basic features of
// the Simple C++ Numerical Toolkit (SCPPNT).
// SCPPNT is available from http://www.smallwaters.com/programs/cpp/scppnt.html

//#define BOOST_MSVC
//#define SCPPNT_NO_IO

#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

// Make matrix region functions available in Matrix class
#define SCPPNT_USE_REGIONS

//#define SCPPNT_NO_DIR_PREFIX

// Unless SCPPNT_NO_DIR_PREFIX is defined the
// directory prefix scppnt/ is used for SCPPNT
// header file includes. Alternatively, the header
// file directory may be specified in the compiler's
// search path.
  
#ifdef SCPPNT_NO_DIR_PREFIX 
// Include files are in same directory as main code
// or in include search path
#include "vec.h"
#include "cmat.h"
#include "matop.h"
#include "transv.h"
#include "region2d.h"
#else
// Include files are in scppnt subfolder
#include "scppnt/vec.h"
#include "scppnt/cmat.h"
#include "scppnt/matop.h"
#include "scppnt/transv.h"
#include "scppnt/region2d.h"
#endif

using namespace SCPPNT;

int main()
{

  // Construct matrix. Elements are not initialized.
  // To initialize all elements to zero use
  // Matrix<double> a(2, 2, 0.0).
  
  Matrix<double> a(2, 2);
	
  // Assign elements to first row using
  // Fortran-style one-based indexing.
  
  a(1,1) = 0.5; a(1,2) = 1.0;
	
  // Assign elements to second row using
  // C-style zero-based indexing.

  a[1][0] = 1.5; a[1][1] = 2.0;
	
  // Multiply all elements of a by 2.
  // Analogous +=, -=, and /= operators are also available
  // for both matrices and vectors.

  a *= 2.0;

  // Construct 2X3 matrix with initial elements given
  // in row-major order by an iterator.

  double belem[6] = {0.1, 0.2, 0.3, 
				   0.3, 0.2, 0.1};
  Matrix<double> b(belem,2,3);
	
  // Construct vector. Elements are not initialized.
  // To initialize all elements to zero use
  // Vector<double> v(2, 0.0).

  Vector<double> v(2);
	
  // Assign first element using
  // Fortran-style one-based indexing

  v(1) = 2.0;
	
  // Assign second element using
  // C-style zero-based indexing.

  v[1] = 3.0;
	
  // Write a, b, and v

  std::cout << "a: " << a << std::endl;
  std::cout << "b: " << b << std::endl;
  std::cout << "v: " << v << std::endl;
	
  // Create new matrix that is the
  // product of a and b.
    // Analogous + and - operators are also
    // available for both matrices and vectors.

  Matrix<double> c = a * b;

  std::cout << "c = a * b: " << c << std::endl;

  // Matrix product b' * a, without forming the transpose of b

  Matrix<double> e = Transpose_View< Matrix<double> >(b) * a;
    
  std::cout << "b' * a: " << e << std::endl;
    
  // Matrix times vector b' * v, without forming the transpose of b

  Vector<double> u = Transpose_View<Matrix<double> >(b) * v;
    
  std::cout << "b' * v: " << u << std::endl;

  // Create new matrix that is the transpose of c

  Matrix<double> d = transpose(c);
	
  // Upper 2X2 region of d

  Index1D J(1, 2); // Index range of 1 through 2
  Region2D< Matrix<double> > dregion = d(J,J);
	
  std::cout << "d: " << dregion << std::endl;
    
  // Multiply upper 2X2 region of d by a and store result in dregion.
  // Analogous += and -= operators are also
  // available for both matrices and vectors.

  dregion *= a;
   
  // Print product of upper 2X2 submatrix of d and v

  std::cout << "d * a: " << dregion << std::endl;

  return 0;
}

// tcmat.cpp
// Illustrate matrix initialization, basic matrix routines, and simple
// matrix I/O.
// Modifed from TNT example tcmat.cc (available from http://math.nist.gov/tnt/examples.html)

// Unless SCPPNT_NO_DIR_PREFIX is defined the
// directory prefix scppnt/ is used for SCPPNT
// header file includes.

#ifdef SCPPNT_NO_DIR_PREFIX 
// Include files are in same directory as main code
#include "vec.h"
#include "cmat.h"
#else
// Include files are in scppnt subfolder 
#include "scppnt/vec.h"
#include "scppnt/cmat.h"
#endif

using namespace SCPPNT;

int main()
{
    Matrix<double> A(2, 4, 
                                   " 1  2  0  4 "
                                   " 2  0  9  7 ");

    Matrix<double> B(4, 2, 
                                   " 9  4 "
                                   " 1  3 "
                                   " 2  8 "
                                   " 0  1 ");

    Matrix<double> C(2, 4, 
                                   " 3  4  8  1 "
                                   " 7  8  0  1 ");

    std::cout << A*B << std::endl;

    std::cout << A+C << std::endl;

        return 0;

}

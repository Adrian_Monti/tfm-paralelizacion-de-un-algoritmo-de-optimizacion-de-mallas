// Test Cholesky module  
// Modifed from TNT example fchol.cc (available from http://math.nist.gov/tnt/examples.html)

#include <iostream>
#include <cassert>

// Unless SCPPNT_NO_DIR_PREFIX is defined the
// directory prefix scppnt/ is used for SCPPNT
// header file includes.

#ifdef SCPPNT_NO_DIR_PREFIX 
// Include files are in same directory as main code
#include "vec.h"
#include "cmat.h"
#include "cholesky.h"
#include "trisolve.h"
#include "transv.h" 				/* transpose views */
#else
// Include files are in scppnt subfolder 
#include "scppnt/vec.h"
#include "scppnt/cmat.h"
#include "scppnt/cholesky.h"
#include "scppnt/trisolve.h"
#include "scppnt/transv.h" /* transpose views */
#endif

using namespace std;
using namespace SCPPNT;

int main()
{
    Matrix<double> A;

    cin >> A;                   /* A should be symmetric positive definite */

    Subscript N = A.num_rows();
    assert(N == A.num_columns());

    Vector<double> b(N, 1.0);   // b= [1,1,1,...]
    Matrix<double> L(N, N);

    cout << "A: " << A << endl;
    
    if (Cholesky_upper_factorization(A, L) !=0)
    {
        cout << "Cholesky did not work." << endl;
        exit(1);
    }
    

    cout << L << endl;

    // solve Ax =b, as L*L'x =b
    //
    //  let y=L'x, then
    //
    //
    //   solve L y = b;
    //   solve L'x = y;

    Vector<double> y = Lower_triangular_solve(L, b);
    Vector<double> x= Upper_triangular_solve(Transpose_View<Matrix<double> >(L), y);

    cout << "x: " << x << endl;
    Vector<double> residual = A*x;
    residual -= b;
    cout << "Residual A*x-b: " << residual << endl;

        return 0;
}
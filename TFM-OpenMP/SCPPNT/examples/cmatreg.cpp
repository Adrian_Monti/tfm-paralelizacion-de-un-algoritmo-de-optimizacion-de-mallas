//  demonstrate basic matrix regions
// Modifed from TNT example cmatreg.cc (available from http://math.nist.gov/tnt/examples.html)

#define SCPPNT_USE_REGIONS

#include <iostream>

// Unless SCPPNT_NO_DIR_PREFIX is defined the
// directory prefix scppnt/ is used for SCPPNT
// header file includes.

#ifdef SCPPNT_NO_DIR_PREFIX 
// Include files are in same directory as main code
#include "vec.h"
#include "cmat.h"
#else
// Include files are in scppnt subfolder 
#include "scppnt/vec.h"
#include "scppnt/cmat.h"
#endif

using namespace SCPPNT;

int main()
{
    Matrix<double> A(5,7, 0.0);

    Matrix<double> B(4, 4, 
                                   " 3  4  8  1 "
                                   " 7  8  5  1 "
                                   " 1  2  6  4 "
                                   " 2  4  9  7 ");

    Index1D I(2,4);
    Index1D J(1,3);
    Index1D K(1,2);

    A(I,J+1) = B(I,J);

    std::cout << A << std::endl;

    // upper left 2x2 sublock of B(I,I)

    std::cout << B(I,I)(K,K) << std::endl;    

    return 0;
}
    
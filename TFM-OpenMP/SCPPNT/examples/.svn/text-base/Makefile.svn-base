# Compile and run Simple C++ Numeric Toolkit examples (SCPPNT) using gcc under Windows.

# directory containing scppnt test programs
#numdir = /cygdrive/c/programs/SCPPNT/examples
numdir = .

# directory containing SCPPNT include files
# the source for the Simple C++ Numeric Toolkit (SCPPNT)
#scppnt_include = /cygdrive/c/programs/SCPPNT/src/include
scppnt_include = ../src/include

# SCPPNT_MEMBER_COMPARISONS symbol needs to be defined for gcc
# SCPPNT_NO_DIR_PREFIX	symbol defines the location of the SCPPNT include files

DEF = -O3 -DSCPPNT_BOUNDS_CHECK -DSCPPNT_NO_DIR_PREFIX -UBOOST_NO_STDC_NAMESPACE

NooptDEF = -O0 -DSCPPNT_BOUNDS_CHECK -DSCPPNT_NO_DIR_PREFIX -UBOOST_NO_STDC_NAMESPACE

# Compile and run SCPPNT examples and check output.
test: stest tcmat tmult cmatreg ctransp qrtest choltest lutest ctimemm
	./stest >test.out
	diff test.out scppnt_test.out
	./tcmat >test.out
	diff test.out tcmat.out
	./tmult <tmult.in >test.out
	diff test.out tmult.out
	./cmatreg >test.out
	diff test.out cmatreg.out
	./ctransp >test.out
	diff test.out ctransp.out
	./qrtest <matrix.dat >test.out
	diff test.out qr.out
	./choltest <SPD10x10.dat >test.out
	diff test.out chol.out
	./lutest <matrix.dat >test.out
	diff test.out lu.out
	./ctimemm 100
	cat ctimemm.out

# Remove files created with 'make test'
clean:
	rm stest.exe
	rm tcmat.exe
	rm tmult.exe
	rm cmatreg.exe
	rm ctransp.exe
	rm qrtest.exe
	rm choltest.exe
	rm lutest.exe
	rm ctimemm.exe
	rm scppnt_error.o
	rm test.out
	
scppnt_error.o : $(scppnt_include)/scppnt_error.cpp
	gcc -c -oscppnt_error.o $(NooptDEF) -I$(scppnt_include) $(scppnt_include)/scppnt_error.cpp

stest: scppnt_test.cpp scppnt_error.o
	gcc -ostest $(DEF) -I$(scppnt_include) scppnt_test.cpp scppnt_error.o -lstdc++

tcmat: tcmat.cpp scppnt_error.o
	gcc -otcmat $(DEF) -I$(scppnt_include) tcmat.cpp scppnt_error.o -lstdc++

tmult: tmult.cpp scppnt_error.o
	gcc -otmult $(DEF) -I$(scppnt_include) tmult.cpp scppnt_error.o -lstdc++

cmatreg: cmatreg.cpp scppnt_error.o
	gcc -ocmatreg $(DEF) -I$(scppnt_include) cmatreg.cpp scppnt_error.o -lstdc++

ctransp: ctransp.cpp scppnt_error.o
	gcc -octransp $(DEF) -I$(scppnt_include) ctransp.cpp scppnt_error.o -lstdc++

qrtest: qr.cpp scppnt_error.o
	gcc -oqrtest $(DEF) -I$(scppnt_include) qr.cpp scppnt_error.o -lm -lstdc++

choltest: chol.cpp scppnt_error.o
	gcc -ocholtest $(NooptDEF) -I$(scppnt_include) chol.cpp scppnt_error.o -lm -lstdc++

lutest: lu.cpp scppnt_error.o
	gcc -olutest $(NooptDEF) -I$(scppnt_include) lu.cpp scppnt_error.o -lstdc++

ctimemm: ctimemm.cpp scppnt_error.o
	gcc -fstrict-aliasing -octimemm $(DEF) -I$(scppnt_include) ctimemm.cpp scppnt_error.o -lstdc++


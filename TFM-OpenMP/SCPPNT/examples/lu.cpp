// Solve a linear system using LU factorization.
//
// Usage: a.out < matrix.dat
//
// where matrix.dat is an ASCII file consisting of the
// matrix size (M,N) followed by its values.  For example,
//
//  3  2
//  8.1  1.2  4.3
//  1.3  4.3  2.9
//  0.4  1.3  6.1
//
// Modifed from TNT example lu.cc (available from http://math.nist.gov/tnt/examples.html)

#include <iostream>

//#define DOXYGEN

// Unless SCPPNT_NO_DIR_PREFIX is defined the
// directory prefix scppnt/ is used for SCPPNT
// header file includes.

#ifdef SCPPNT_NO_DIR_PREFIX 
// Include files are in same directory as main code
#include "vec.h"
#include "cmat.h"
#include "lu.h"
#else
// Include files are in scppnt subfolder 
#include "scppnt/vec.h"
#include "scppnt/cmat.h"
#include "scppnt/lu.h"
#endif

#include <cassert>

using namespace std;
using namespace SCPPNT;

int main()
{
    Matrix<double> A;

    cin >> A;

    Subscript N = A.dim(1);
    assert(N == A.dim(2));

    Vector<double> b(N, 1.0);   // b= [1,1,1,...]
    Vector<Subscript> index(N);

    cout << "Original Matrix A: " << A << endl;
    
    Matrix<double> T(A);
    if (LU_factor<Matrix<double>,Vector<Subscript> >(T, index) !=0)
    {
        cout << "LU_factor() failed." << endl;
        exit(1);
    }

    Vector<double> x(b);
    if (LU_solve(T, index, x) != 0)
    {
        cout << "LU_Solve() failed." << endl;
        exit(1);
    }
    cout << "Solution x for Ax=b, where b=[1,1,...] " <<endl;
    cout << " x: " << x << endl;

    cout << "A*x should be the vector [1,1,...] "  <<endl;
    Vector<double> residual = A*x;
    residual -= b;
    cout     << "residual [A*x - b]: " << residual << endl;
    
        return 0;
}

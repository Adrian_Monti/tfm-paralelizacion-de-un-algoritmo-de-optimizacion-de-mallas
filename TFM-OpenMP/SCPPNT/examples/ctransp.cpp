// matrix transposes
// Modifed from TNT example ctransp.cc (available from http://math.nist.gov/tnt/examples.html)


// Unless SCPPNT_NO_DIR_PREFIX is defined the
// directory prefix scppnt/ is used for SCPPNT
// header file includes.

#ifdef SCPPNT_NO_DIR_PREFIX 
// Include files are in same directory as main code
#include "cmat.h"
#include "transv.h"
#else
// Include files are in scppnt subfolder 
#include "scppnt/cmat.h"
#include "scppnt/transv.h"
#endif



using namespace std;
using namespace SCPPNT;

int main()
{
    Matrix<double> A(2, 4, 
                          " 1  2  0  4 "
                          " 2  0  9  7 ");

    Matrix<double> B(4, 4, 
                          " 9  4  1  2"
                          " 1  3  4  9"
                          " 2  8  3  1"
                          " 0  1  0  0");

    
    Vector<double> x(4, " 1  2  3  4 ");

    cout << "A: " << A  << endl;
    cout << "Transpose_View(A): " << Transpose_View<Matrix<double> >(A) << endl;

    cout << "B: " << B << endl;

    cout << "B' * [1 2 3 4]'  : " << Transpose_View<Matrix<double> >(B) * x << endl;

        return 0;
}
    
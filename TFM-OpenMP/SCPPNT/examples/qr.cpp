// Solve a linear system using QR factorization.
//
// Usage: a.out < matrix.dat
//
// where matrix.dat is an ASCII file consisting of the
// matrix size (M,N) followed by its values.  For example,
//
//  3  3
//  8.1  1.2  4.3
//  1.3  4.3  2.9
//  0.4  1.3  6.1
//
// 
// Modifed from TNT example qr.cc (available from http://math.nist.gov/tnt/examples.html)

#include <iostream>

// Unless SCPPNT_NO_DIR_PREFIX is defined the
// directory prefix scppnt/ is used for SCPPNT
// header file includes.

#ifdef SCPPNT_NO_DIR_PREFIX 
// Include files are in same directory as main code
#include "vec.h"
#include "cmat.h"
#include "qr.h"
#else
// Include files are in scppnt subfolder 
#include "scppnt/vec.h"
#include "scppnt/cmat.h"
#include "scppnt/qr.h"
#endif

#include <cassert>

using namespace std;
using namespace SCPPNT;

int main()

{
    Matrix<double> A;

    cin >> A;

    Subscript N = A.num_rows();
    assert(N == A.num_columns());

    Vector<double> b(N, 1.0);   // b= [1,1,1,...]
    Vector<double> C(N), D(N);

    cout << "A: " << A << endl;

    Matrix<double> T(A);

    if (QR_factor(T, C, D) !=0)
    {
        cout << "QR failed."  << endl;
        cout << "   returned: \n" << T << endl;
        exit(1);
    }

    Vector<double> x(b);
    if (QR_solve(T, C, D, x) == 1)
    {
        cout << "QR_Solve did not work." << endl;
        exit(1);
    }

    cout << "Solution x for Ax=b, where b=[1,1,...] " <<endl;
    cout << " x: " << x << endl;

	Vector<double> residual = A*x;
	residual -= b;
    cout     << "residual [A*x - b]: " << residual << endl;

        return 0;
}
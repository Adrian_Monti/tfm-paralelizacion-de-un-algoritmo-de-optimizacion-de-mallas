// tmult.cpp
// Several versions of matrix multiply
// Modifed from TNT example tmult.cc (available from http://math.nist.gov/tnt/examples.html)

// Unless SCPPNT_NO_DIR_PREFIX is defined the
// directory prefix scppnt/ is used for SCPPNT
// header file includes.

#ifdef SCPPNT_NO_DIR_PREFIX 
// Include files are in same directory as main code
#include "vec.h"
#include "cmat.h"
#else
// Include files are in scppnt subfolder 
#include "scppnt/vec.h"
#include "scppnt/cmat.h"
#endif

using namespace std;
using namespace SCPPNT;

int main()
{
    Matrix<double> A;
    Matrix<double> B;   

    cin >> A;
    cin >> B;

    Matrix<double> C1;
    Matrix<double> C2;
    Matrix<double> C3;

    C1= A*B;
    C2 = matmult<Matrix<double>,Matrix<double>,Matrix<double> >(A,B);
    matmult(C3, A, B);

    cout << "A*B: " << A*B << endl;;
    cout << "matmult(A,B): " << matmult<Matrix<double>,Matrix<double>,Matrix<double> >(A,B) << endl;
    cout << "matmult(C, A, B): " << C3 << endl;

//     Fortran_Matrix<double> A2( A.num_cols(), A.num_rows(),  &A(1,1));
//     A2 = transpose(A2);
// 
//     Fortran_Matrix<double> B2( B.num_cols(), B.num_rows(),  &B(1,1));
//     B2 = transpose(B2);
// 
//     cout << endl << endl;
//     cout << "C A * B: " << A2 * B2 << endl;
//     cout << "C matmult(A, B) : " << matmult(A2, B2) << endl;
//     
//     Fortran_Matrix<double> C4;
//     matmult(C4, A2, B2);
//     cout << "Fortran matmult(C, A, B) : " << C4 << endl;

        return 0;
}
    

// Example of timing routine
// Modifed from TNT example ctimemm.cc (available from http://math.nist.gov/tnt/examples.html)

#include <iostream>
// Unless SCPPNT_NO_DIR_PREFIX is defined the
// directory prefix scppnt/ is used for SCPPNT
// header file includes.

#ifdef SCPPNT_NO_DIR_PREFIX 
// Include files are in same directory as main code
#include "vec.h"
#include "cmat.h"
#else
// Include files are in scppnt subfolder 
#include "scppnt/vec.h"
#include "scppnt/cmat.h"
#endif

#include "stopwatch.h"

using namespace std;
using namespace SCPPNT;

namespace { // unnamed namespace for local function

template <class MaTRiX>
void timeit(MaTRiX &C, const MaTRiX &A, const MaTRiX &B,
            double resolution_time, double *final_time,
            double *num_cycles)
{
    TNT::stopwatch Q;
    long int cycles=1;

    while(1)
    {

        Q.start();

        for (int r=0; r != cycles; r++)
        {
            matmult(C, A, B);
        }
        Q.stop();

        if (Q.read() >= resolution_time) break;

        cycles *= 2;
        Q.reset();
    }

    *final_time = Q.read();
    *num_cycles = (double) cycles;
}

} // unnamed namespace

int main(int argc, char *argv[])
{

        if (argc < 2)
        {
            cerr << "timemm: time mutliplcation of two NxN matrices.\n";
            cerr << "Usage: N [resolution-time] \n";
            cerr << "  (resolution-time is optional, defaults to 1.0 sec)\n";
            cerr << "Returns: N  actual-time   num-flops   Mflops/sec.\n";
            exit(1);
        }

        Subscript N = atoi(argv[1]);
        double resolution_time = 1.0;    // default to 1 sec timing length

        if (argc > 2)
            resolution_time = atof(argv[2]);

        Matrix<double> A(N,N), B(N,N), C(N,N);
        A = 1.0;
        B = 2.0;

        double num_reps = 0.0;
        double actual_time = 0.0;
        timeit(C, A, B, resolution_time, &actual_time, &num_reps);

        cout << "N: " << N << "    time:  " << actual_time << "  reps: " << num_reps
                <<  "   Mflops: " <<  2e-6*N*N*N*num_reps / actual_time << endl;

                return 0;
}

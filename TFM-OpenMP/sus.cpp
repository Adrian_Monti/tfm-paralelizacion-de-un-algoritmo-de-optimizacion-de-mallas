/* $Id: sus.cpp 48 2011-10-11 19:07:32Z erodriguez $ */

/* -------------------------------------------------------------
SUS code
Simultaneous Untangling and Smoothing
Programa de desenredado y suavizado de mallas 3D

sus.cpp

Copyright (C) 2009, 2010 ULPGC
Universidad de Las Palmas de Gran Canaria
Institute of Intelligent Systems and 
Numerical Applications in Engineering - SIANI
Jose Maria Escobar Sanchez
Eduardo Rodriguez Barrera
Rafael Montenegro Armas
Gustavo Montero Garcia
Jose Maria Gonzalez Yuste

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------*/

#include "Uncmin.h"
#include "vec.h"	// Simple C++ Numeric Toolkit (SCPPNT) vector class
#include "cmat.h"	// Simple C++ Numeric Toolkit (SCPPNT) matrix class
#include "sus.h"
				    //#include "malla.h"      // Para memoria distribuida (Hector)

#include <iostream>     // for cout
#include <cstdio>	// for printf
#include <string.h>     // para strerror
#include <assert.h>     
#include <cfloat>
#include <errno.h>
#include <set>

using namespace std;

#define MASCARA_X (1)
#define MASCARA_Y (2)
#define MASCARA_Z (4)
#define MAX_CARAS_VISTAS 1000

#define BUFFER_SIZE 2048


double W0_inversa[3][3] = { {1.0,  -1.0/rd3 ,  -1.0/rd6} ,
			    {0.0,   2.0/rd3 ,  -1.0/rd6} ,
			    {0.0,   0.0     ,  rd3/rd2} };

double DosTercios = 2.0 / 3.0;
const double Norma = 1.0 ;
double peor_sigma = DBL_MAX;
double deltaglobal;
//T_Parametros params;
//int InvertedElementFlag = 0;  // Flag para indicar que al menos un tetraedro de la estrella local esta invertido
//double FactorDeUmbral = 1.0e-4; // Para calcular el umbral en Calcular_Delta

//---------- Funciones para analizar memoria -----//
#include <sys/types.h>
#include <sys/sysinfo.h>

int label(long long i) {/* generate text labels */
  if      (i<1e3) {printf(" %lluB",i);}
  else if (i<1e6) {printf(" %lluK",i/1024);}
  else if (i<1e9) {printf(" %fM",(double)i/1048576);}
  else            {printf(" %fG",(double)i/1073741824);}
  printf("\n");
  return 0;
}

int parseLine(char* line){
        int i = strlen(line);
        while (*line < '0' || *line > '9') line++;
        line[i-3] = '\0';
        i = atoi(line);
        return i;
}

int getValue(){ //Note: this value is in KB!
        FILE* file = fopen("/proc/self/status", "r");
        int result = -1;
        char line[128];

        while (fgets(line, 128, file) != NULL){
            if (strncmp(line, "VmRSS:", 6) == 0){
                result = parseLine(line);
                break;
            }
        }
        fclose(file);
        return result;
}

void infoMem(int opcion){
   long long info;
   struct sysinfo memInfo;

   sysinfo (&memInfo);

   if(opcion == 1) { // memoria fisca instalada en maquina
        info = memInfo.totalram; // totalPhysMem
        info *= memInfo.mem_unit; // en Bytes
        printf("memoria fisica instalada:");
   } else if (opcion == 2) { // memoria fisca usada en la maquina
        info = memInfo.totalram - memInfo.freeram;
        info *= memInfo.mem_unit;
        printf("memoria fisica usada todosProcesos:");
   } else if (opcion == 3) {
        info = 1024 * (long long)getValue();
        printf("memoria fisica usada esteProceso:");
   }
   label(info);
   printf("\n");
}
//---------- Funciones para analizar memoria -----//

// -------- Some important global variables (see README) ---------
//const double asimptotic_epsilon = 1.0e-10; // Asymptotic value of 
                                     // h(sigma) when sigma tends to -inf
const double EpsSafetyFactor = 1.0e6;      // "Safety" factor for epsilon
//const double meps =  2.220446e-16;         // Machine epsilon
const double meps =  DBL_EPSILON * (double) pow(2.0, 10);  // Machine epsilon
const double EpsilonEfectivo = meps * EpsSafetyFactor; // Effective epsilon

extern double typical_size[3];
int flag_params = 1;


void EscribeT_Parametros(T_Parametros *p)
{
  printf("nodo: %d\n", p->nodo);
  printf("ndim: %d\n", p->ndim); /* dimension de la funcion a optimizar */
  printf("var: %f, %f, %f\n", p->var[0], p->var[1], p->var[2]); /* x,y,z originales del nodo */
  printf("mascara: %d\n", p->mascara); /* mascara de bits indicadora de que dimensiones */
  printf("Delta: %f\n", p->Delta); /* La delta para funcion de Knupp */
  //printf("salir: %d\n", p->salir);   /* 1 -> indica que hay que salir de las iteraciones de Newton */
  printf("MargenEpsilon: %.12g\n", p->MargenEpsilon);
  printf("EpsilonAsintoticoRelativo: %.12g\n", p->EpsilonAsintoticoRelativo); /* Epsilon asintotico relativo a la estrella (antes era global)*/
  printf("deltaRelativa: %.12g\n", p->deltaRelativa);  /* Delta relativa a cada estrella (antes era deltaglobal) */
  printf("FactorDeUmbral: %.12g\n", p->FactorDeUmbral); /* Factor de correccion de calculo del umbral en el calculo de delta */
  printf("----- T_ParProg -----\n");
  fflush(stdout);
}


// Funcion para leer lineas de texto desde un fichero
// Corrige las posibles deficiencias de seguridad de fgets
// Ver siguiente URL para mas informacion
// https://www.securecoding.cert.org/confluence/display/seccode/FIO36-C.+Do+not+assume+a+new-line+character+is+read+when+using+fgets%28%29
char * safe_fgets(char *buf, size_t tam, FILE *f)
{
  char *p;
  
  if (fgets(buf, tam-1, f)) {
    p = strchr(buf, '\n');
    if (p) {
      *p = '\0';
      return(buf);
    }
    else {
      // Linea demasiado larga
      if (!feof(f)) {
	  fprintf(stderr, "safe_gets: Error: linea demasiado larga\n");
	  assert(0);
	}
      else {
	return(buf);
      }
    }
  }
  else {
    return(NULL);
  }
} 

int
LeeLinea(FILE *f, char *buffer)
{
  char *s;
  /* Se salta las lineas vacias o con comentarios */
  /* Los comentarios tienen el caracter # en la columna 0 */
  if (feof(f)) return(0);
  do
    {
      s = safe_fgets(buffer, BUFFER_SIZE, f);
    }
  while ( ((buffer[0]=='#') || (buffer[0]=='\0')) && (!feof(f)) );
  if (feof(f)) return(0);
  else return(1);
}/* LeeLinea */


/* Calcula el numero de caras vistas desde cada nodo */
void
CuentaCarasVistas(T_Malla *Malla)
{
  int i, j;

  for (i=0; i < Malla->Num_nodos; i++) Malla->nodo[i].NCarasVistas = 0;

  for (i=0; i < Malla->Num_tetra; i++)
    for (j=0; j < 4; j++)
      {
	int k = Malla->tetra[i].nodo[j];
	Malla->nodo[k].NCarasVistas++ ;
      }
} /* CuentaCarasVistas */

/* Calcula las caras vistas desde cada nodo */
void
CalculaCarasVistas(T_Malla *Malla)
{
  int i, j;
  int *cont;
  int orden[4][3] = { {1, 2, 3}, {0, 3, 2},
		      {0, 1, 3}, {0, 2, 1} };

  cont = (int *)malloc(Malla->Num_nodos * sizeof(int));
  if (cont == NULL)
    {
      fprintf(stderr, "CalculaCarasVistas: No se puede reservar memoria\n");
      abort();
    }

  for (i=0; i < Malla->Num_nodos; i++) cont[i] = 0;

  for (i=0; i < Malla->Num_tetra; i++)
    {
      for (j=0; j < 4; j++)
	{
	  int n;  
	  int k = Malla->tetra[i].nodo[j];
	  int idx = cont[k];

	  for (n=0; n < 3; n++)
	    {
	      int nodo = Malla->tetra[i].nodo[orden[j][n]];
	      Malla->nodo[k].CarasVistas[n][idx] = nodo;
	    }
	  cont[k]++ ;
	}
    }

  free(cont);
} /* CalculaCarasVistas */

void
CalculaLasConexiones(T_Malla *Malla)
{
  int i;
  int *m;

  /* Calcula el numero de caras vista desde cada nodo (las conexiones) */
  CuentaCarasVistas(Malla);
  /* Reserva memoria para las caras vistas */
  for (i=0; i < Malla->Num_nodos; i++) 
    {
      if ( ( m = (int *)malloc(3 * sizeof(int) * Malla->nodo[i].NCarasVistas)) 
	   == NULL ) {
	fprintf(stderr, 
		"LeerMalla: no puedo reservar memoria para las conexiones: %s\n",
		strerror(errno));
	abort();
      }
      Malla->nodo[i].CarasVistas[0] = m;
      Malla->nodo[i].CarasVistas[1] = m + Malla->nodo[i].NCarasVistas;
      Malla->nodo[i].CarasVistas[2] = m + 2*Malla->nodo[i].NCarasVistas;
    }
  /* Calcula las caras vistas desde cada nodo */
  CalculaCarasVistas(Malla);

} /* CalculaLasConexiones */

/* ----------------------------------------------------------------- */
/* 12/01/2007                                                        */
/*                                                                   */
/* funcion LeerMalla                                                 */
/* Lee los ficheros de coordenadas y elementos de una malla y        */
/* calcula las conexiones, para evitar tener que tener un fichero    */
/* de conexiones.                                                    */
/*                                                                   */
/* Entradas:                                                         */
/*   f_coor: nombre del fichero de coordenadas de los nodos          */
/*   f_elem: nombre del fichero de nodos que componen cada tetraedro */
/* Salidas:                                                          */
/*   Malla: Rellena la estructura con los datos leidos               */
/*   Devuelve el numero de nodos leidos                              */
/* ----------------------------------------------------------------- */
int
LeerMalla(char *f_coor, char *f_elem, T_Malla *Malla)
{
  FILE *f1, *f2;
  char *buffer;
  int i, foo;

  /* Tengo que usar memoria dinamica para el buffer porque si no */
  /* no funciona bien ExtreNumero */

  if ((buffer=(char *)malloc(BUFFER_SIZE * sizeof(char))) == NULL){
    fprintf(stderr, 
	    "LeerMalla: no puedo reservar memoria para el buffer: %s\n",
	    strerror(errno));
    exit(1);
  }

  if ( (f1=fopen(f_coor, "r")) == NULL ){
    fprintf(stderr, "LeerMalla: error abriendo fichero %s: %s\n",
	    f_coor, strerror(errno));
    exit(1); 
  }
  if ( (f2=fopen(f_elem, "r")) == NULL ){
    fprintf(stderr, "LeerMalla: error abriendo fichero %s: %s\n",
	    f_elem, strerror(errno));
    exit(1); 
  }

  /* Reserva memoria para las coordenadas de los nodos y las */
  /* condiciones de contorno */
  LeeLinea(f1, buffer);
  foo = sscanf(buffer, "%d", &Malla->Num_nodos);
  assert(foo);
  assert(foo!=EOF);
  if ((Malla->nodo = (T_Nodo *)malloc(sizeof(T_Nodo) * Malla->Num_nodos)) 
      == NULL){
    fprintf(stderr, 
	    "LeerMalla: no puedo reservar memoria para los nodos: %s\n",
	    strerror(errno) );
    exit(1);
  }
  /* Lectura de las coordenadas de los nodos y sus */
  /* numeros de referencia */
  i = 0;
  while ((LeeLinea(f1, buffer) != 0) && (i < Malla->Num_nodos)){
    sscanf(buffer, "%lf %lf %lf %d", &Malla->nodo[i].x, 
	   &Malla->nodo[i].y, &Malla->nodo[i].z, 
	   &Malla->nodo[i].nr
	   );
    i++;
  }
  fclose(f1);

  if (i < Malla->Num_nodos){
    fprintf(stderr, "LeerMalla: Problema al leer el fichero de nodos:\n");
    fprintf(stderr, "debo leer %d nodos y solo he podido leer %d\n", 
	    Malla->Num_nodos, i-1);
    exit(EXIT_FAILURE);
  }

  /* Reserva memoria para los tetraedros */
  LeeLinea(f2, buffer);
  foo = sscanf(buffer, "%d", &Malla->Num_tetra);
  assert(foo);
  assert(foo!=EOF);
  if ((Malla->tetra = (T_Tetra *)malloc(sizeof(T_Tetra) * Malla->Num_tetra)) 
      == NULL){
    fprintf(stderr, 
	    "LeerMalla: no puedo reservar memoria para los tetraedros: %s\n",
	    strerror(errno));
    exit(1);
  }

  /* Lectura de los nodos que componen cada tetraedro */
  i = 0;
  while ((LeeLinea(f2, buffer) != 0) && (i < Malla->Num_tetra)){
    sscanf(buffer, "%d %d %d %d %d", &foo, &Malla->tetra[i].nodo[0], 
	   &Malla->tetra[i].nodo[1], &Malla->tetra[i].nodo[2], 
	   &Malla->tetra[i].nodo[3]);
    /* OJO : resto 1 porque la malla de escobar empieza en el nodo 1 (FORTRAN)
       mientras que la mia empieza en 0 (C) */
    Malla->tetra[i].nodo[0]--; Malla->tetra[i].nodo[1]--; 
    Malla->tetra[i].nodo[2]--; Malla->tetra[i].nodo[3]--;
    i++;
  }
  fclose(f2);
  if (i < Malla->Num_tetra){
    fprintf(stderr, "LeerMalla: Problema al leer el fichero %s:\n", f_elem);
    fprintf(stderr, "debo leer %d tetraedros y solo he podido leer %d\n", 
	    Malla->Num_tetra, i-1);
    exit(EXIT_FAILURE);
  }

  CalculaLasConexiones(Malla);

  free(buffer);
  return(Malla->Num_nodos);
}/* LeerMalla */



/*
  Conviene que la malla este centrada en el origen y escalada, lo que
  viene siendo una normalizacion, vamos
*/
void
NormalizarLaMalla(T_Malla *Malla)
{
  int i;
  double cx, cy, cz;     // Centro de gravedad de la malla
  double max, max_radio;

  cx = cy = cz = 0.0;
  // Calculo del centro de gravedad
  for (i=0; i < Malla->Num_nodos; i++) 
    {
      cx += Malla->nodo[i].x ;
      cy += Malla->nodo[i].y ;
      cz += Malla->nodo[i].z ;
    }
  cx = cx / Malla->Num_nodos;
  cy = cy / Malla->Num_nodos;
  cz = cz / Malla->Num_nodos;

  // Traslado la malla, restando a cada coordenada la componente
  // correspondiente del centro de gravedad
  for (i=0; i < Malla->Num_nodos; i++) 
    {
      Malla->nodo[i].x = Malla->nodo[i].x - cx;
      Malla->nodo[i].y = Malla->nodo[i].y - cy;
      Malla->nodo[i].z = Malla->nodo[i].z - cz;
    }

  max_radio = 0.0;
  // Calculo del factor de escala (esfera envolvente)
  for (i=0; i < Malla->Num_nodos; i++) 
    {
      max =  Malla->nodo[i].x * Malla->nodo[i].x ;
      max += Malla->nodo[i].y * Malla->nodo[i].y ;
      max += Malla->nodo[i].z * Malla->nodo[i].z ;
      if (max > max_radio)
	max_radio = max;
    }

  // Escalado de la malla
  max_radio = sqrt(max_radio);
  max_radio = max_radio / 10.0;
  for (i=0; i < Malla->Num_nodos; i++) 
    {
      Malla->nodo[i].x = Malla->nodo[i].x / max_radio;
      Malla->nodo[i].y = Malla->nodo[i].y / max_radio;
      Malla->nodo[i].z = Malla->nodo[i].z / max_radio;
    }

  // Guarda la informacion en la malla para poder des-normalizar tras
  // el suavizado
  Malla->cx = cx;    Malla->cy = cy;    Malla->cz = cz;
  Malla->max_radio = max_radio;
}


/*
  Devuelve una malla normalizada a su forma y lugar original
*/
void
DesnormalizarLaMalla(T_Malla *Malla)
{
  int i;
  double cx, cy, cz;     // Centro de gravedad de la malla
  double max_radio;

  // Recupera la informacion necesaria para des-normalizar
  cx = Malla->cx ;    cy = Malla->cy ;    cz = Malla->cz ;
  max_radio = Malla->max_radio ;

  // des-escalado de la malla
  for (i=0; i < Malla->Num_nodos; i++) 
    {
      Malla->nodo[i].x = Malla->nodo[i].x * max_radio;
      Malla->nodo[i].y = Malla->nodo[i].y * max_radio;
      Malla->nodo[i].z = Malla->nodo[i].z * max_radio;
    }

  // des-traslado la malla, sumando a cada coordenada la componente
  // correspondiente del centro de gravedad
  for (i=0; i < Malla->Num_nodos; i++) 
    {
      Malla->nodo[i].x = Malla->nodo[i].x + cx;
      Malla->nodo[i].y = Malla->nodo[i].y + cy;
      Malla->nodo[i].z = Malla->nodo[i].z + cz;
    }
}




/* ----------------------------------------------------------------- */
/* funcion LiberarMalla                                              */
/*                                                                   */
/* Libera la memoria usada por los elementos que componen una malla  */
/*                                                                   */
/* Entrada:                                                          */
/*  Malla: puntero a la malla que hay que liberar                    */
/* ----------------------------------------------------------------- */
void
LiberarMalla(T_Malla *Malla)
{
  int i;

  for (i=0; i < Malla->Num_nodos; i++) 
    free(Malla->nodo[i].CarasVistas[0]);

  free(Malla->nodo);
  free(Malla->tetra);
}/* LiberarMalla */



// -------------------------------------------------------------
// -------------------------------------------------------------
//           DEFINICION DE LAS FUNCION DE KNUPP
// -------------------------------------------------------------
// -------------------------------------------------------------

/* ---------------------------------------------------------------------------
Det
Calcula el determinante de una matriz de 3x3 con pivotacion total
--------------------------------------------------------------------------- */
double
Det(const double m[3][3])
{
  int ncont=0; 
  int ir, ifil, icol, i, j, k;
  double amax, aux;
  double A[3][3];

  memcpy(A, m, 9*sizeof(double));
  /* Determinacion del pivote maximo */
  for (ir=0; ir < 2; ir++){
    amax = fabs(A[ir][ir]);
    ifil = icol = ir;
    for (i=ir; i < 3; i++)
      for (j=ir; j < 3; j++)
	if (fabs(A[i][j]) > amax){
	  ifil = i;
	  icol = j;
	  amax = fabs(A[i][j]);
	}
    /* Intercambio de la fila ir por la fila ifil */
    if (ifil != ir){
      ncont++;
      for (k=ir; k < 3; k++){
	aux = A[ir][k];
	A[ir][k] =  A[ifil][k];
	A[ifil][k] = aux;
      }
    }
    /* Intercambio de columnas */
    if (icol != ir){
      ncont++;
      for (k=0; k < 3; k++){
	aux = A[k][ir];
	A[k][ir] = A[k][icol];
	A[k][icol] = aux;
      }
    }
    
    for (i=ir+1; i < 3; i++)
      for (j=ir+1; j < 3; j++)
	A[i][j] = A[i][j] - A[i][ir] * A[ir][j] / A[ir][ir];
  }

  aux = A[0][0] * A[1][1] * A[2][2];
  if (ncont == 1 || ncont == 3) 
    aux = -aux;
  return(aux);
}/* Det */


// 21/11
double
h_sigma(const double d, const double DeltaCuadrado, const double epsilon_sigma)
{
  double h;
  h = 0.5 * (d + sqrt(((d - 2.0*epsilon_sigma) * (d - 2.0*epsilon_sigma)) 
		      + (4.0 * DeltaCuadrado)) );
  
  return(h);
}


/*
  Funcion objetivo modificada
*/
double
K_Knupp_Modif_directa(const double s[3][3], 
		      const double det, const double Delta, double *nfr,
		      const double epsilon_sigma)
{
  double nf = 0.0;
  double  d = det;
  double DeltaCuadrado = Delta * Delta;

  d = h_sigma(det, DeltaCuadrado, epsilon_sigma);
  d = cbrt(d * d);

  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      {
	nf = nf + (s[i][j] * s[i][j]);
      }
  *nfr = sqrt(nf);

  double aux = nf / (3.0 * d);
  return(aux);
} /* K_Knupp_Modif_directa */

/*
Construye la matriz jacobiana S
Entradas:
   m : malla
   t : tetraedro
Salidas:
   s: matriz jacobiana
*/
void
ConstruyeS(const T_Malla *m, const T_Tetra t, double s[3][3])
{
  double aux[3][3];

  /* Construye la matriz jacobiana */
#ifdef DEBUG
  cout << "tetraedro = " 
	    << t.nodo[0] << " " 
	    << t.nodo[1] << " " << t.nodo[2] << " "
	    << t.nodo[3] << " " << endl;

  for (int i=0; i<4; i++){
    cout << "nodo " << i << "=" 
	      << m->nodo[t.nodo[i]].x << " "  
	      << m->nodo[t.nodo[i]].y << " "
	      << m->nodo[t.nodo[i]].z << " "
	      << endl;
  }
#endif

  for (int i=1; i<4; i++){
    s[0][i-1] = m->nodo[t.nodo[i]].x - m->nodo[t.nodo[0]].x;
    s[1][i-1] = m->nodo[t.nodo[i]].y - m->nodo[t.nodo[0]].y;
    s[2][i-1] = m->nodo[t.nodo[i]].z - m->nodo[t.nodo[0]].z;

  }

#ifdef DEBUG
  cout << "s[]=" << s[0][0] << " " << s[0][1] << " " << s[0][2] << " " << endl;
  cout << "s[]=" << s[1][0] << " " << s[1][1] << " " << s[1][2] << " " << endl;
  cout << "s[]=" << s[2][0] << " " << s[2][1] << " " << s[2][2] << " " << endl;
#endif

  for (int i=0; i<3; i++)
    {
      for (int j=0; j<3; j++)
	{
	  double p = 0.0;
	  for (int k=0; k<3; k++)
	    {
	      p += s[i][k] * W0_inversa[k][j];
	    }
	  aux[i][j] = p;
	}
    }


  for (int i=0; i<3; i++)
    {
      for (int j=0; j<3; j++)
	{
	  s[i][j] = aux[i][j];
	}
    }

  //  Producto(s, W0_inversa);
} /* ConstruyeS */


double
Calcula_Delta(const void *params, const double EpsilonEfectivo, int *InvertedElements)
{
  struct mis_parametros *parametros = (struct mis_parametros *)params;
  double Delta = 1.0e-3;  // delta por defecto
  int j, k, nodo;
  double sigma_min = DBL_MAX ;
  double sigma[1000];
  double s[1000][3][3];
  T_Malla *m;
  T_Tetra t;

  double dmin;

  m = parametros->malla;
  nodo = parametros->nodo;
  *InvertedElements = 0;

  double sigma_medio = 0.0;
  for (j=0 ; j < m->nodo[nodo].NCarasVistas; j++){
    /* El nodo 0 del tetraedro es el nodo i considerado */
    t.nodo[0] = nodo;
    for (k=1; k<4; k++) t.nodo[k] = m->nodo[nodo].CarasVistas[k-1][j];
    ConstruyeS(m, t, s[j]);
    sigma[j] = Det(s[j]);
    if (sigma[j] < 0.0) (*InvertedElements)++ ;
    if (sigma[j] < sigma_min) sigma_min = sigma[j];
    sigma_medio += fabs(sigma[j]);
  }

  sigma_medio = sigma_medio /  m->nodo[nodo].NCarasVistas;
#ifdef DEBUG
  cout << "SIGMA_MIN=" << sigma_min << endl;
#endif

  double radicando = EpsilonEfectivo * (EpsilonEfectivo - sigma_min);
  if (radicando <= 0.0) 
    dmin = 0.0;
  else 
    dmin = sqrt(radicando);

  //  double umbral = 1.0e-4 * sigma_medio;
  double umbral = parametros->FactorDeUmbral * sigma_medio;

  Delta = (dmin >= umbral) ? dmin : umbral ;

  return(Delta);
} 
/* Calcula_Delta */



/* Calcula la F modificada de Knupp, su gradiente y su hessiano 

Entradas: 
  v: coordenadas originales del nodo
  params:
     nodo: nodo central de la estrella de la que se quiere obtener la calidad
     malla: malla completa
     deltaRelativa: parametro delta relativo a la estrella
     EpsilonAsintoticoRelativo: epsilon asintotico relativo
     mascara: mascara de bits para controlar que dimensiones pueden alterarse en la optimizacion
     var[]: x,y,z originales del nodo
     ndim: numero de dimensiones del problema (3)

Salidas:
  f: valor de la funcion
  gradient: vector gradiente
  H: matriz Hessiana
 */
void
new_Fknupp_con_grad_y_hess(const double v[3], void *params, 
			  double *f, double gradient[3], 
			  double H[][3])
{ 
  struct mis_parametros *parametros = (struct mis_parametros *)params;
  T_Malla *m;
  int nodo;
  T_Tetra t;
  double grad[3];
  double s[MAX_CARAS_VISTAS][3][3];
  int j, k;
  double S_S[3];
  double sig_[3];
  double K;
  double K_[3];
  double K__[3][3];
  double sigma[MAX_CARAS_VISTAS];
  double nf= -1.0;
  double sigmacuadrado;
  double aux1, aux2, aux3;
  double acum = 0.0;
  double nf2, LaRaiz, ElFactor, ElOtroFactor;
  int i = 0;
  double sigma_min = DBL_MAX;
  double hess[3][3];
#ifdef DEBUG
  const char *nfun = "new_Fknupp_con_grad_y_hess: ";
#endif

  // Ahora el epsilon asintotico es relativo a la estrella, asi que se
  // calcula en el main y a esta funcion le llega como parametro
  // (originalmente era absoluto y era una variable global)
  double asimptotic_epsilon = parametros->EpsilonAsintoticoRelativo;

  m = parametros->malla;
  nodo = parametros->nodo;
  double Delta = parametros->deltaRelativa;
  double DeltaCuadrado = Delta*Delta;

#ifdef DEBUG
  cout << nfun << endl << "\tDelta=" << Delta << endl;
#endif
  
  for (i=0; i<3; i++) 
    grad[i] = hess[0][i] = hess[1][i] = hess[2][i] = 0.0;
 
  i = 0;
  if (parametros->mascara & MASCARA_X){
    m->nodo[nodo].x = v[i]; i++;
  } else
    m->nodo[nodo].x = parametros->var[0];

  if (parametros->mascara & MASCARA_Y){
    m->nodo[nodo].y = v[i]; i++;
  } else
    m->nodo[nodo].y = parametros->var[1];

  if (parametros->mascara & MASCARA_Z){
    m->nodo[nodo].z = v[i]; i++;
  } else 
    m->nodo[nodo].z = parametros->var[2];
#ifdef DEBUG
  cout <<  
    "\tx = " << m->nodo[nodo].x <<
    "  y = " << m->nodo[nodo].y <<
    "  z = " << m->nodo[nodo].z << endl;
#endif
  acum = 0.0 ;

  for (j=0 ; j < m->nodo[nodo].NCarasVistas; j++){
    /* El nodo 0 del tetraedro es el nodo i considerado */
    t.nodo[0] = nodo;
    for (k=1; k<4; k++) t.nodo[k] = m->nodo[nodo].CarasVistas[k-1][j];
    ConstruyeS(m, t, s[j]);
    sigma[j] = Det(s[j]);
    if (sigma[j] < sigma_min) sigma_min = sigma[j];
  }

  for (j=0 ; j < m->nodo[nodo].NCarasVistas; j++){
    K = K_Knupp_Modif_directa(s[j], sigma[j], Delta, &nf, asimptotic_epsilon);
    acum += K;
    sigmacuadrado = sigma[j] * sigma[j];
    nf *= nf;
    nf2 = nf; /* nf2: norma frob. al cuadrado */

    /* El nodo 0 del tetraedro es el nodo i considerado */
    t.nodo[0] = nodo;
    for (k=1; k<4; k++) t.nodo[k] = m->nodo[nodo].CarasVistas[k-1][j];

    i = 0;
    if (parametros->mascara & MASCARA_X){
      sig_[i] = sigma_x(m->nodo[t.nodo[1]].y, m->nodo[t.nodo[2]].y, 
			m->nodo[t.nodo[3]].y, m->nodo[t.nodo[1]].z,  
			m->nodo[t.nodo[2]].z, m->nodo[t.nodo[3]].z);
      S_S[i] = PES_gS(m->nodo[t.nodo[0]].x, m->nodo[t.nodo[1]].x,
		      m->nodo[t.nodo[2]].x, m->nodo[t.nodo[3]].x);
      i++;
    }
    if (parametros->mascara & MASCARA_Y){
      sig_[i] = sigma_y(m->nodo[t.nodo[1]].x, m->nodo[t.nodo[2]].x, 
			m->nodo[t.nodo[3]].x, m->nodo[t.nodo[1]].z,  
			m->nodo[t.nodo[2]].z, m->nodo[t.nodo[3]].z);
      S_S[i] = PES_gS(m->nodo[t.nodo[0]].y, m->nodo[t.nodo[1]].y,
		      m->nodo[t.nodo[2]].y, m->nodo[t.nodo[3]].y);
      i++;
    }
    if (parametros->mascara & MASCARA_Z){
      sig_[i] = sigma_z(m->nodo[t.nodo[1]].x, m->nodo[t.nodo[2]].x, 
			m->nodo[t.nodo[3]].x, m->nodo[t.nodo[1]].y,  
			m->nodo[t.nodo[2]].y, m->nodo[t.nodo[3]].y);
      S_S[i] = PES_gS(m->nodo[t.nodo[0]].z, m->nodo[t.nodo[1]].z,
		      m->nodo[t.nodo[2]].z, m->nodo[t.nodo[3]].z);
      i++;
    }

    double aux = sigma[j] - 2.0 * asimptotic_epsilon;
    aux = aux * aux;
    LaRaiz = sqrt(aux + 4.0 * DeltaCuadrado);
    double h_sg = h_sigma(sigma[j], DeltaCuadrado, asimptotic_epsilon);
    ElFactor = (DosTercios * (1 - asimptotic_epsilon / h_sg))/ LaRaiz;
    ElOtroFactor = 2.0 / nf;
      
    for (k=0; k < parametros->ndim; k++)
      K_[k] = K * ((ElOtroFactor * S_S[k]) - (ElFactor * sig_[k]));

     /* Calculo del gradiente */
    for(i=0; i < parametros->ndim; i++){
      aux1 = K_[i];
      if (Norma > 1.0)
	aux1 = Norma * pow(K, Norma-1.0) * aux1;
      grad[i] += aux1;
    }

    {
      /* Calculo del Hessiano */
      //double aux = sigma[j] - 2*asimptotic_epsilon;
      //aux = aux*aux;
      nf *= nf; /* norma de Frobenius a la cuarta potencia */
      ElFactor = 4.0 / nf;
      //double h_sg = h_sigma(sigma[j], DeltaCuadrado, asimptotic_epsilon);
      aux1 = aux + 4.0 * DeltaCuadrado;
      aux1 = aux1 * aux1 * aux1;
      ElOtroFactor = DosTercios / sqrt(aux1);
      double termino = (1 -  asimptotic_epsilon / h_sg);
      termino = termino * ((1 +  asimptotic_epsilon / h_sg) * sigma[j] - 4.0 * asimptotic_epsilon);
      ElOtroFactor = ElOtroFactor * termino;

      for(i=0; i<parametros->ndim; i++){
	for(k=i; k<parametros->ndim;k++){ /* la matriz K__ es simetrica */
	  K__[i][k] = K_[i] * K_[k] / K; /* se calcula la triangular superior */
	  if (i!=k)
	    aux1 = 0.0;
	  else 
	    aux1 = 3.0/nf2;
	    
	  aux2 = ElFactor*S_S[i]*S_S[k];
	  aux3 = ElOtroFactor*sig_[i]*sig_[k];
	  K__[i][k] = K__[i][k] + K*(aux1 - aux2 + aux3);
	}
      }
      /* Calculo del Hessiano */
      for(i=0; i<parametros->ndim; i++){
	for(k=i; k<parametros->ndim; k++){
	  aux1 = K__[i][k];
	  if (Norma > 1.0)
	    aux1 = pow(K, Norma-1.0) * aux1;
	  aux2 = 0.0;
	  if (Norma >= 2.0)
	    aux2 = (Norma-1.0) * pow(K, Norma-2.0) * K_[i] * K_[k];
	  hess[i][k] = hess[i][k] + Norma * (aux2 + aux1);
	}
      }
    }
  }

  hess[1][0] = hess[0][1];
  hess[2][0] = hess[0][2];
  hess[2][1] = hess[1][2];

  for (i=0; i < parametros->ndim; i++) 
    {
      gradient[i] = grad[i];
      for (j=0; j< parametros->ndim; j++)
	H[i][j] = hess[i][j];
    }
  *f = acum;


  {
#ifdef DEBUG
    // Solo para DEBUG
    cout << "\tFUNCION DE KNUPP VALE " << acum << endl;

    cout << "\tGRADIENTE VALE "
	      << grad[0] << " "  
	      << grad[1] << " "  
	      << grad[2] << endl;

    cout << "\tHESSIANO VALE " << endl 
	      <<"\t"<< hess[0][0] << "  " << hess[0][1] << "  " << hess[0][2] << endl
	      <<"\t"<< hess[1][0] << "  " << hess[1][1] << "  " << hess[1][2] << endl 
	      <<"\t"<< hess[2][0] << "  " << hess[2][1] << "  " << hess[2][2] << endl ;
#endif
  }
}/* new_Fknupp_con_grad_y_hess */

// -------------------------------------------------------------
// -------------------------------------------------------------
//        FIN DE DEFINICION DE LAS FUNCION DE KNUPP
// -------------------------------------------------------------
// -------------------------------------------------------------

/* ------------------------------------------------------------------- */
/* Calcula la norma de Frobenius de una matriz                         */
/* ------------------------------------------------------------------- */
double 
NFrobenius(double m[3][3])
{
  int i, j;
  double nf = 0.0;

  for (i=0; i<3; i++)
    for (j=0; j<3; j++)
      {
	nf = nf + (m[i][j] * m[i][j]);
      }
  return(sqrt(nf));
}
/* NFrobenius */

double
Q_Knupp(const T_Malla *m, T_Tetra t)
{
  double s[3][3];
  double nf, d, sg;
  int i;

  /* Construye la matriz jacobiana */
  for (i=1; i<4; i++){
    s[0][i-1] = m->nodo[t.nodo[i]].x - m->nodo[t.nodo[0]].x;
    s[1][i-1] = m->nodo[t.nodo[i]].y - m->nodo[t.nodo[0]].y;
    s[2][i-1] = m->nodo[t.nodo[i]].z - m->nodo[t.nodo[0]].z;
  }

  {
    double aux[3][3];

    for (int i=0; i<3; i++)
      {
	for (int j=0; j<3; j++)
	  {
	    double p = 0.0;
	    for (int k=0; k<3; k++)
	      {
		p += s[i][k] * W0_inversa[k][j];
	      }
	    aux[i][j] = p;
	  }
      }

    for (int i=0; i<3; i++)
      {
	for (int j=0; j<3; j++)
	  {
	    s[i][j] = aux[i][j];
	  }
      }
  }

  //  Producto(s, W0_inversa);
  d = sg = Det(s);
  if (sg < peor_sigma) peor_sigma = sg;
  d = d * d;
  d = cbrt(d);
  nf = NFrobenius(s);
  nf = nf * nf;
  if (sg < 0.0) d=-d;
  return( (3.0 * d) / nf );
} /* Q_Knupp */

/* Calcula las calidades de todos los tetraedros de la malla */
/* Si lista es no nulo, almacena en lista las calidades */
/* Devuelve el numero de tetraedros cuya calidad esta por debajo de umbral */
int
CalculaCalidades(const T_Malla *m, double *lista, double umbral)
{
  int i, flag=0;
  //(Domingo)double q;

#pragma omp parallel for shared(flag, lista) //Domingo
  for (i=0; i<m->Num_tetra; i++){
  ////
  //(Domingo)for (int kk=0; kk<4; kk++)
  //(domingo)  assert(m->tetra[23287].nodo[kk] < m->Num_tetra);

    double q;
    q = Q_Knupp(m, m->tetra[i]);

  ////
  //(Domingo)for (int kk=0; kk<4; kk++)
  //(Domingo)  assert(m->tetra[23287].nodo[kk] < m->Num_tetra);

    if (q < umbral) flag++;
    if (lista != NULL) lista[i] = q;
    m->tetra[i].q = q;
  }

  return(flag);
}/* CalculaCalidades */

/* Imprime los tetraedros no validos de la malla y los nodos */
/* que lo forman */
void
ImprimeTetraedrosNoValidos(const T_Malla *m)
{
  int i;
  double q;
  int k=0;

  for (i=0; i<m->Num_tetra; i++){
    q = Q_Knupp(m, m->tetra[i]);
    if (q <= 0.0)
      {
	int n0 = m->tetra[i].nodo[0];
	int n1 = m->tetra[i].nodo[1];
	int n2 = m->tetra[i].nodo[2];
	int n3 = m->tetra[i].nodo[3];
	int r0 = m->nodo[n0].nr;
	int r1 = m->nodo[n1].nr;
	int r2 = m->nodo[n2].nr;
	int r3 = m->nodo[n3].nr;
	k++ ;
	printf("%d Non valid tetrahedron %d: nodes %d %d %d %d   ref. numb. %d %d %d %d\n", k, i, 
	       n0, n1, n2, n3, r0, r1, r2, r3);
      }
  }
  if (k>0)
    printf("Mesh has %d non-valid elements\n", k);
}/* ImprimeTetraedrosNoValidos */



/* Funcion para comparar doubles, usada por qsort */
int
compara_doubles(const void *a, const void *b)
{
  const double *da = (const double *) a;
  const double *db = (const double *) b;

  return (*da > *db) - (*da < *db);
}


void
OrdenaCalidades(const T_Malla *malla, double *buffer)
{
#pragma omp parallel for shared(buffer) //Domingo
  for (int i=0; i < malla->Num_tetra; i++) buffer[i] = malla->tetra[i].q;

  qsort((void *)buffer, malla->Num_tetra, sizeof(double), compara_doubles);
}




// -------------------------------------------------------------
/* Graba un fichero con los nodos de la malla suavizada */
void
GrabaFicheroSalida(const char *nombre, const T_Malla *malla)
{
  int i;
  FILE *FiCa;

  if ((FiCa = fopen(nombre, "w")) == NULL){
    fprintf(stderr, "GrabaFicheroSalida: error en el fichero %s\n%s", 
	    nombre, strerror(errno));
    abort();
  }
  fprintf(FiCa, "# Numero de nodos de la malla\n%d\n", malla->Num_nodos);
  for (i=0; i < malla->Num_nodos; i++) 
    fprintf(FiCa, "%.16f  %.16f  %.16f  %d\n", 
	    malla->nodo[i].x, malla->nodo[i].y, malla->nodo[i].z,
	    malla->nodo[i].nr);
  fclose(FiCa);
}/* GrabaFicheroSalida */ 


/* Graba un fichero con las calidades */
void
GrabaFicheroCalidades(double *lista, T_Malla *mesh, int numtetra, const char *prefijo)
{
  static int numiter = 0;
  int i;
  char nf_calidad[1024]; /* prefijo del nombre de los ficheros de calidades */
  FILE *FiCa;
  
  OrdenaCalidades(mesh, lista);
  sprintf(nf_calidad, "%s%03d", prefijo, numiter++);
  if ((FiCa = fopen(nf_calidad, "w")) == NULL) {
    fprintf(stderr, "GrabaFicheroCalidades: error en el fichero %s\n%s\n", 
	    nf_calidad, strerror(errno));
    abort();
  }
  for(i=0; i<numtetra; i++)
    fprintf(FiCa, "%.04g\n", (lista[i] < 0.0)? 0.0 : lista[i]);
  fclose(FiCa);
} /* GrabaFicheroCalidades */


int 
iGrabaEstadisticas(FILE *f_estadis, T_Malla *mesh, double *lista, int numtetra, int iteraciones, double *CalidadMedia, double *CalidadMinima, double umbral)
{
  //int i;
  double media = 0;
  double max = -2;
  double min = 2;
  int deformes = 0;

  //(Domingo) OrdenaCalidades(mesh, lista);
#pragma omp parallel for shared(deformes, min) //Domingo
//#pragma omp parallel for shared(deformes, media, min, max) //Domingo
//#pragma omp parallel for shared(flag, lista) //Domingo
  for (int i=0; i < numtetra; i++){
    //(Domingo)double aux;

// modificacion Domingo
    double q;
    q = Q_Knupp(mesh, mesh->tetra[i]);

    //(Domingo)if (q < umbral) flag++;
    //(Domingo)if (lista != NULL) lista[i] = q;
    //(Domingo)mesh->tetra[i].q = q;
// modificacion Domingo

    //(Domingo)aux = lista[i];
    //(Domingo)if (aux < 0.0) {
    if (q < 0.0) {
      deformes++;
      q = 0.0;
    }
    //(Domingo)media += aux;
    //media += q;
    if (q < min) min = q;
    //if (q > max) max = q;
  }

  //media = media / numtetra;
  *CalidadMedia = media;
  *CalidadMinima = min;
   fprintf(stdout, "%03d ", iteraciones);
   fprintf(stdout, "%.6e %.6e %.6e %d", min, media, max, deformes);
   //(Domingo)fprintf(f_estadis, "%.6e %.6e %.6e %d", min, media, max, deformes);
   //(Domingo)fflush(f_estadis);

  return deformes;
} /* iGrabaEstadisticas */

void
GrabaEstadisticas(FILE *f_estadis, T_Malla *mesh, double *lista, int numtetra, int iteraciones)
{
  int dumy;
  double CalidadMedia,CalidadMinima;
  dumy = iGrabaEstadisticas(f_estadis, mesh, lista, numtetra, iteraciones, &CalidadMedia, &CalidadMinima, 0.0);
} /* GrabaEstadisticas */





#ifdef BOOST_NO_STDC_NAMESPACE
// Needed for compilers like Visual C++ 6 in which the standard
// C library functions are not in the std namespace.
namespace std {using ::printf;}
#endif

// Vector and Matrix classes from SCPPNT
typedef SCPPNT::Vector<double> dvec;
typedef SCPPNT::Matrix<double> dmat;




// Class to use as function template argument to Uncmin for numeric minimization
class BivarFunctionOnly
{
public:

  BivarFunctionOnly(T_Parametros *ptr);
  ~BivarFunctionOnly();
	
  // Function to minimize
  double f_to_minimize(dvec &p);
	
  // Gradient of function to minimize
  void gradient(dvec &p, dvec &g);
	
  // Hessian of function
  void hessian(dvec &p, dmat &h);
	
  // Indicates analytic gradient is used (0 = do not use)
  int HasAnalyticGradient(); //{return 0;}
	
  // Indicates analytic hessian is not used (0 = do not use)
  int HasAnalyticHessian();// {return 0;}
	
  // Any real vector will contain valid parameter values
  int ValidParameters(dvec &x) {return 1;}
	
  // Dimension of problem
  int dim() {return 3;}

  // Last calculated gradient
  double Gradiente[3]; 

  // Last calculated Hessian
  double Hessiano[3][3];

  // Some parameters used by the quality function
  T_Parametros *ptr_params;
};

// Constructor 
BivarFunctionOnly::BivarFunctionOnly(T_Parametros *ptr)
{
  ptr_params = ptr;
}

// Destructor
BivarFunctionOnly::~BivarFunctionOnly()
{
}

// Function to minimize
double BivarFunctionOnly::f_to_minimize(dvec &p)
{
  double f;
  double v[3];
  double grad[3];
  double H[3][3];

  v[0] = p[0];
  v[1] = p[1];
  v[2] = p[2];

  new_Fknupp_con_grad_y_hess(v, (void *)ptr_params, &f, grad, H);

  // Guardo el resultado del gradiente
  for (int i=0; i<3; i++)
    Gradiente[i] = grad[i];

  // Guardo el resultado del Hessiano (solo la triangular inferior)
  for (int i=0; i<3; i++)
    for (int j=0; j<=i; j++)
      Hessiano[i][j] = H[i][j];

  return  f;
}

// Gradient of function to minimize
void BivarFunctionOnly::gradient(dvec &p, dvec &g)
{
  // Not provided for this function class
}

// Hessian of function to minimize
void BivarFunctionOnly::hessian(dvec &p, dmat &h)
{
  // Not provided for this function class
}

// Indicate whether analytic gradient is used (0 = do not use)
int BivarFunctionOnly::HasAnalyticGradient() {return 0;}

// Indicate whether analytic hessian is used (0 = do not use)
int BivarFunctionOnly::HasAnalyticHessian() {return 0;}


      // Function with analytic gradient

// Class to use as function template argument to Uncmin for minimization with analytic first derivative

class BivarFunctionAndGradient: public BivarFunctionOnly
{
public:
  
  BivarFunctionAndGradient(T_Parametros *ptr);
  ~BivarFunctionAndGradient();
  
  // Gradient of function to minimize
  void gradient(dvec &p, dvec &g);
  
  // Indicates analytic gradient is used (0 = do not use)
  int HasAnalyticGradient();

//private: // No private data structures in this function!
};

// Constructor
BivarFunctionAndGradient::BivarFunctionAndGradient(T_Parametros *ptr):BivarFunctionOnly(ptr)
{
}

// Destructor
BivarFunctionAndGradient::~BivarFunctionAndGradient()
{
}

// Gradient of function to minimize
void BivarFunctionAndGradient::gradient(dvec &p, dvec &g)
{
}

// Indicates whether analytic gradient is used (0 = do not use)
int BivarFunctionAndGradient::HasAnalyticGradient() {return 1;}

// Function with both analytic Gradient and Hessian

// Class to use as function template argument to Uncmin for minimization with analytic first adn second derivatives
class BivarFunctionGradientAndHessian: public BivarFunctionOnly
{
public:
  BivarFunctionGradientAndHessian(T_Parametros *ptr);
  ~BivarFunctionGradientAndHessian();

  // Gradient of function to minimize
  void gradient(dvec &p, dvec &g);
  
  // Indicates analytic gradient is used (0 = do not use)
  int HasAnalyticGradient();
  
  // Hessian of function
  void hessian(dvec &p, dmat &h);
  
  // Indicates analytic hessian is not used (0 = do not use)
  int HasAnalyticHessian();
  
//private: // No private data structures in this function!
};

// Constructor
BivarFunctionGradientAndHessian::BivarFunctionGradientAndHessian(T_Parametros *ptr):BivarFunctionOnly(ptr)
{
  // Pone a 0 la triangular superior del Hessiano
  Hessiano[0][1] = Hessiano[0][2] = Hessiano[1][2] = 0.0;
}

// Destructor
BivarFunctionGradientAndHessian::~BivarFunctionGradientAndHessian()
{
}

// Definicion del gradiente
void BivarFunctionGradientAndHessian::gradient(dvec &p, dvec &g)
{
  // Devuelvo el ultimo gradiente calculado
  g[0] = Gradiente[0];
  g[1] = Gradiente[1];
  g[2] = Gradiente[2];
  
  // OJO: puesto para debug
#ifdef DEBUG
  double f;
  double v[3];
  double grad[3];
  double H[3][3];

  v[0] = p[0];
  v[1] = p[1];
  v[2] = p[2];

  new_Fknupp_con_grad_y_hess(v, (void *)ptr_params, &f, grad, H);

  // Guardo el resultado del gradiente
  for (int i=0; i<3; i++)
    g[i] = Gradiente[i] = grad[i];
  // fin de OJO
#endif

#ifdef DEBUG
  cout << "Gradient is: " << 
    g[0] << " "  << g[1] << " "  << g[2] << endl;
#endif
}

// Hessian of function to minimize
void BivarFunctionGradientAndHessian::hessian(dvec &p, dmat &h)
{
  // Devuelvo el ultimo Hessiano calculado
  for (int i=0; i<3; i++)
    for (int j=0; j<=i; j++)
      h[i][j] = Hessiano[i][j];

  // OJO: para debug
#ifdef DEBUG
  double f;
  double v[3];
  double grad[3];
  double H[3][3];

  v[0] = p[0];
  v[1] = p[1];
  v[2] = p[2];

  new_Fknupp_con_grad_y_hess(v, (void *)ptr_params, &f, grad, H);

  // Guardo el resultado del Hessiano (solo la triangular inferior)
  for (int i=0; i<3; i++)
    for (int j=0; j<=i; j++)
      h[i][j] = Hessiano[i][j] = H[i][j];
#endif
  // fin de OJO


#ifdef DEBUG
  cout << "Hessian is: " <<
    h[0][0] << " " << h[0][1] << " " << h[0][2] << endl <<
    h[1][0] << " " << h[1][1] << " " << h[1][2] << endl <<
    h[2][0] << " " << h[2][1] << " " << h[2][2] << endl ;
#endif
}

// Indicates whether analytic hessian is  used (0 = do not use)
int BivarFunctionGradientAndHessian::HasAnalyticHessian() {return 1;}
int BivarFunctionGradientAndHessian::HasAnalyticGradient() {return 1;}

// OptimizeNode
//
// Node optimization
//
// Input:
//   node: node number
//   m: mesh
//
// Output:
//    0: success
//    1: no se optimizo; quiza intentarlo cambiando el umbral en delta
int OptimizeNode(T_Malla *m, int nodo, int NumeroDeFactorDeUmbral)
{
  //#define DEBUG
  const double VectorDeFactoresDeUmbral[DIMENSION_VECTOR_UMBRAL] = {1.0e-5, 1.0e-4, 1.0e-3, 1.0e-2};
  T_Parametros params;
  int InvertedElements;

  assert(NumeroDeFactorDeUmbral <  DIMENSION_VECTOR_UMBRAL);
  params.FactorDeUmbral = VectorDeFactoresDeUmbral[NumeroDeFactorDeUmbral];

  // minimiza solo los nodos con numero de referencia 0
  if (m->nodo[nodo].nr != 0) return 0;

  // Dimensionality of the problem 
  const int dim = 3;

  // starting values
  double start_values[3] = {0, 0, 0} ;

  // xpls will contain solution, gpls will contain
  // gradient at solution after call to min_<f>.Minimize
  dvec xpls(dim), gpls(dim);
  
  // hpls provides the space for the hessian
  dmat hpls(dim, dim);

  // fpls contains the value of the function at
  // the solution given by xpls.
  double fpls;

  params.mascara = 7;   // minimiza en las 3 dimensiones
  params.ndim = 3 ;
  params.malla = m;
  params.MargenEpsilon = EpsSafetyFactor; // "Safety" factor for epsilon

  double coor_antes[3];
  params.nodo = nodo;
  start_values[0] = params.var[0] = coor_antes[0] = m->nodo[nodo].x; 
  start_values[1] = params.var[1] = coor_antes[1] = m->nodo[nodo].y; 
  start_values[2] = params.var[2] = coor_antes[2] = m->nodo[nodo].z; 

  double deltaRelativa = Calcula_Delta(&params, EpsilonEfectivo, &InvertedElements);

  params.deltaRelativa = deltaRelativa;
  params.EpsilonAsintoticoRelativo = 1.0e-8 * deltaRelativa;
  // El 1.0e-8 se toma porque parece ser un valor típico
  // de la relacion EpsilonAsintotico/delta cuando la
  // malla está enredada

#ifdef DEBUG
  cout << "Minimizando el nodo " << nodo 
       << " (nref " << m->nodo[nodo].nr << ")" << endl;
#endif

  dvec start(start_values, start_values+dim);

  // OJO: solo para debug
#ifdef DEBUG
  if (flag_params)
    { 
      //      flag_params=0;
      cout << "Minimizando el nodo " << nodo 
	   << " (nref " << m->nodo[nodo].nr << ")" << endl;
      EscribeT_Parametros(&params);
    }
#endif
  // fin de OJO

  // Guarda en f_ini el valor inicial de la funcion objetivo
  double f_ini;
  {
    BivarFunctionOnly v(&params);
    f_ini = v.f_to_minimize(start);
  }
#ifdef DEBUG
  cout << "Inicialmente la funcion vale " << f_ini << endl;
#endif



  // Part 2: Load and minimimize the function. Function, first and
  // second-order derivatives supplied
  //
  // Create function object
  BivarFunctionGradientAndHessian FunctionGradientAndHessian(&params); 
  // create Uncmin object
  Uncmin<dvec, dmat, BivarFunctionGradientAndHessian> min_fgh(&FunctionGradientAndHessian); 

  // Set typical magnitude of function values at the minimum
  double magnitude = exp10(round(log10(f_ini)) - 1);
  min_fgh.SetScaleFunc(magnitude);

#ifdef CACA
  MagnitudesDeLaEstrella(m, nodo, typical_size);
#endif

  dvec typ(typical_size, typical_size + dim);
  // Set typical magnitude of argument values at function minimum
  if (min_fgh.SetScaleArg(typ))
    {
      cout << endl << "Error in setting typical value" << endl;
      exit(EXIT_FAILURE);
    }

  // Diagnostic printout piped to stdout (fh, 1, 1)
  // Only results piped to stdout (fh, 1, 0)
  // No info piped to stdout (0, *, *)
  min_fgh.SetPrint(0, 1, 0);
  
  min_fgh.SetTolerances(1.0e-10, 1.0e-10, meps);
  // min_fgh.SetTolerances(-1.0, -1.0, -1.0);

  //Minimize the function
  int iMethod = 1;  // Line Search 
  
  // Set Minimization method 
  min_fgh.SetMethod(iMethod);

  // Minimize function
  min_fgh.Minimize(start, xpls, fpls, gpls, hpls);
      
  // Find status of function minimization.
  // For a description of the values returned by
  // GetMessage see the documentation at the beginning
  // of Uncmin.h.
  int msg = min_fgh.GetMessage();
#ifdef DEBUG  
  cout << endl << endl << "Message returned from Uncmin: " << msg << endl;
  cout << endl << "Function minimum: " << endl << fpls << endl;
  cout << endl << "Parameters: " << xpls << endl;
  cout << "Gradient: " << gpls << endl;

  cout << "Coords Antes: " << coor_antes[0] << "  "
       << coor_antes[1] << "  "
       << coor_antes[2] << endl;

  cout << "Coords Despues: " 
       << m->nodo[nodo].x << "  " 
       << m->nodo[nodo].y << "  " 
       << m->nodo[nodo].z << endl;

  cout << "Coords xpls: " << xpls[0] << "  "
       << xpls[1] << "  "
       << xpls[2] << endl;
#endif
  // Si ha encontrado la solucion, mueve el nodo
  if ((msg >=0) && (msg <= 3))
    {
      if ( (InvertedElements > 0) && ((f_ini <= fpls) || (!finite(fpls))) )
	cout << "OptimizeNode: nodo " << nodo << " Ojo con la optimizacion msg=" << msg
	     << " f_ini=" << f_ini << " fpls=" << fpls
	     << " orig:(" << coor_antes[0] << ", " << coor_antes[1] << ", " << coor_antes[2] 
	     << ") optimiz:(" << m->nodo[nodo].x << ", " << m->nodo[nodo].y << ", " << m->nodo[nodo].z << ")"
	     << " eltos invert=" << InvertedElements
	     << endl;
#ifdef DEBUG
      cout << "OptimizeNode" << endl
	   << "\tEncontrado el optimo (msg=" << msg << ")" << endl
	   << "\tMovido el nodo " << nodo << " de "
	   << start_values[0] << "  " 
	   << start_values[1] << "  " 
	   << start_values[2] << "  a  "
	   << m->nodo[nodo].x << "  " 
	   <<  m->nodo[nodo].y << "  " 
	   <<  m->nodo[nodo].z << endl;
#endif
    }
  else
    {
      // El optimizador no ha encontrado una solucion optima:
      // 
      // Si la posicion obtenida por el optimizador es valida y la
      // nueva calidad es mejor que la de partida, muevo el nodo.
      //
      // Si la nueva posicion no es valida (tipicamente un nan) o la
      // funcion objetivo no mejora con respecto a la de partida,
      // aviso de esta circunstancia devolviendo un 1. Eso permite a
      // OptimizeNode volverlo a intentar con un factor de umbral
      // menos exigente.
      //
      // Optimum not found. Actual node is moved to new
      // (non-optimum) position iif:
      // a) new position is valid and
      // b) local mesh quality is better than original
#ifdef DEBUG
      cout << "\tNo encontrado el optimo (msg=" << msg 
	   << ")" << endl;
#endif	      
      if ( (!(finite(m->nodo[nodo].x) && finite(m->nodo[nodo].y) 
	      && finite(m->nodo[nodo].z))) || (fpls > f_ini) )
	{

	  cout << "\nOptimizeNode: No se mueve el nodo " << nodo 
	       << " de su posicion inicial. f_ini:" << f_ini << " fpls:" << fpls << " orig:("
	       << coor_antes[0] << ", " << coor_antes[1] << ", " << coor_antes[2] << ") optimiz:("
	       << m->nodo[nodo].x << ", " << m->nodo[nodo].y << ", " << m->nodo[nodo].z << ")"
	       << endl;

	  m->nodo[nodo].x = coor_antes[0];
	  m->nodo[nodo].y = coor_antes[1];
	  m->nodo[nodo].z = coor_antes[2];
	  return(1);
	}
      else
	{
#ifdef DEBUG
	  cout << "\tOptimizeNode: No encontrado el optimo. Aun asi, se mueve el nodo " << nodo << " de "
	       << start_values[0] << "  " 
	       << start_values[1] << "  " 
	       << start_values[2] << "  a  "
	       << m->nodo[nodo].x << "  " 
	       << m->nodo[nodo].y << "  " 
	       << m->nodo[nodo].z << endl;
#endif
	}
    }

  return 0;
  //#undef DEBUG
} // OptimizeNode




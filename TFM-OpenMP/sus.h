/* $Id: sus.h 43 2010-10-18 17:44:09Z jcristo $ */
#ifndef __SUS_H_OMP
#define __SUS_H_OMP

/* -------------------------------------------------------------
SUS code
Simultaneous Untangling and Smoothing
Programa de desenredado y suavizado de mallas 3D

sus.h

Copyright (C) 2010 
University of Las Palmas de Gran Canaria - ULPGC
Institute of Intelligent Systems and 
Numerical Applications in Engineering - SIANI
Jose Maria Escobar Sanchez
Eduardo Rodriguez Barrera
Rafael Montenegro Armas
Gustavo Montero Garcia
Jose Maria Gonzalez Yuste
-------------------------------------------------------------*/


/* raiz cuadrada de 2 */
#ifndef rd2
#define rd2 (1.414213562373095633972752693807)
#endif
/* raiz cuadrada de 3 */
#ifndef rd3
#define rd3 (1.732050807568877637265813973499)
#endif
/* raiz cuadrada de 6 */
#ifndef rd6
#define rd6 (2.449489742783178098197284074706)
#endif

#ifndef sigma_x
#define sigma_x(y1, y2, y3, z1, z2, z3) (rd2 * (y3*(z2-z1) + y2*(z1-z3) + y1*(z3-z2)))
#endif
#ifndef sigma_y
#define sigma_y(x1, x2, x3, z1, z2, z3) (rd2 * (x3*(z1-z2) + x1*(z2-z3) + x2*(z3-z1)))
#endif
#ifndef sigma_z
#define sigma_z(x1, x2, x3, y1, y2, y3) (rd2 * (x3*(y2-y1) + x2*(y1-y3) + x1*(y3-y2)))
#endif
#ifndef PES_gS
#define PES_gS(x, x1, x2, x3) (0.5*(3.0*x -x1 -x2 -x3))
#endif

#define DIMENSION_VECTOR_UMBRAL 4

struct
S_Nodo
{
  double x,y,z;      /* Coordenadas del nodo */
  int nr;            /* numero de referencia */
  int NCarasVistas;  /* numero de caras de las que el nodo forma parte */
  int *CarasVistas[3]; /* vector de nodos que forman cada cara (3xNCarasVistas) */
  long int a;          /* numero aleatorio para crear los IS */
  bool ok;           /* indica si el nodo ya ha sido seleccionado */
  bool Adj;          /* indica que el nodo adyacente puede ser procesado */
	bool colored;	// true si el nodo ya ha sido definitivamente coloreado
  int NVecinos;		 /* numero de vecinos sin repetición del nodo */
  int *Vecinos;		 /* vector de vecinos sin repetición del nodo */
  int color;             /* Color asignado al nodo */
  int n_wait;		 /* indica el numero de nodos que debe esperar para poder ser procesado */	
};
typedef struct S_Nodo T_Nodo;

struct
S_Tetra
{
  int nodo[4];       /* nodos que forman el tetraedro */
  double q;          /* medida de calidad del tetraedro */
}; 
typedef struct S_Tetra T_Tetra;

struct               /* Estructura para guadar datos de los nodos */
S_Malla
{
  int Num_nodos;     /* Numero de nodos de la malla */
  int Num_tetra;     /* Numero de tetraedros de la malla */
  T_Nodo *nodo;      /* vector de nodos que forman la malla (Num_nodos)*/
  T_Tetra *tetra;    /* vector de tetraedros que forman la malla (Num_tetra) */
  /// Informacion para el normalizado
  double cx, cy, cz;    // Centro de gravedad de la malla
  double max_radio;     // Radio de la bola envolvente
};
typedef struct S_Malla T_Malla;

typedef double (*T_Func_Knupp)(const T_Malla *m, double s[3][3], 
			       const double det, const double Delta,
			       double *nfr);

struct mis_parametros {
  T_Malla *malla;
  int nodo;
  int ndim; /* dimension de la funcion a optimizar */
  double var[3]; /* x,y,z originales del nodo */
  int mascara; /* mascara de bits indicadora de que dimensiones */
  /* se permite mover en la minimizacion (ver mi_OptimizaNodo) */
  double Delta; /* La delta para funcion de Knupp */
  //  double delta_calculada; /* La delta calculada */
  //  int salir;   /* 1 -> indica que hay que salir de las iteraciones de Newton */
  double MargenEpsilon;
  double EpsilonAsintoticoRelativo; /* Epsilon asintotico relativo a la estrella (antes era global)*/
  double deltaRelativa;  /* Delta relativa a cada estrella (antes era deltaglobal) */
  double FactorDeUmbral; /* Factor de correccion de calculo del umbral en el calculo de delta */
};

typedef struct mis_parametros T_Parametros;

// Functions
int LeerMalla(char *f_coor, char *f_elem, T_Malla *Malla);
void NormalizarLaMalla(T_Malla *Malla);
void CalculaCarasVistas(T_Malla *Malla);
int CalculaCalidades(const T_Malla *m, double *lista, double umbral);
void GrabaFicheroSalida(const char *nombre, const T_Malla *malla);
void GrabaFicheroCalidades(double *lista, T_Malla *mesh, int numtetra, const char *prefijo);
// void GrabaEstadisticas(FILE *f_estadis, T_Malla *mesh, double *lista, int numtetra);
void GrabaEstadisticas(FILE *f_estadis, T_Malla *mesh, double *lista, int numtetra, int iteraciones);
void DesnormalizarLaMalla(T_Malla *Malla);
void ImprimeTetraedrosNoValidos(const T_Malla *m);
void LiberarMalla(T_Malla *Malla);
int OptimizeNode(T_Malla *m, int nodo, int IndiceFactorUmbral);

// Domingo
int iGrabaEstadisticas(FILE *f_estadis, T_Malla *mesh, double *lista, int numtetra, int iteraciones, double * CalidadMedia, double *CalidadMinima, double umbral);
void infoMem(int opcion);


#endif

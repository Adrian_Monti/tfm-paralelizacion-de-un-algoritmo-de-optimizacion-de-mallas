#!/bin/bash
#
#SBATCH --job-name=adrian_B_Conejo_OpenMP_D_11
#SBATCH --output=res_B_Conejo_OpenMP_D_11.txt
#
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=11
#SBATCH --time=40:00
#SBATCH --mem-per-cpu=2000

#export OMP_NUM_THREADS=11
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

./sus_ompDynamic -n ./mallas/conejo/tangled.dat -e ./mallas/conejo/elem.dat > raw_B_Conejo_OpenMP_D_11.txt


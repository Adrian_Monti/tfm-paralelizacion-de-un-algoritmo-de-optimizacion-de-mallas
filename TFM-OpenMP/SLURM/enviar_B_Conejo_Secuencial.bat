#!/bin/bash
#
#SBATCH --job-name=adrian_B_Conejo_Secuencial
#SBATCH --output=res_B_Conejo_Secuencial.txt
#
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=12
#SBATCH --time=40:00
#SBATCH --mem-per-cpu=2000

#export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
#./hello.omp > salida.txt
#./lanzaConejoB.bat
./sus_secuencial -n ./mallas/conejo/tangled.dat -e ./mallas/conejo/elem.dat > raw_B_Conejo_Secuencial.txt


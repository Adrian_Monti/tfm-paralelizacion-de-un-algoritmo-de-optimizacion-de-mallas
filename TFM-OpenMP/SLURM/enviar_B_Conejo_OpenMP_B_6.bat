#!/bin/bash
#
#SBATCH --job-name=adrian_B_Conejo_OpenMP_B_6
#SBATCH --output=res_B_Conejo_OpenMP_B_6.txt
#
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=6
#SBATCH --time=40:00
#SBATCH --mem-per-cpu=2000

#export OMP_NUM_THREADS=6
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

./sus_ompBase -n ./mallas/conejo/tangled.dat -e ./mallas/conejo/elem.dat > raw_B_Conejo_OpenMP_B_6.txt


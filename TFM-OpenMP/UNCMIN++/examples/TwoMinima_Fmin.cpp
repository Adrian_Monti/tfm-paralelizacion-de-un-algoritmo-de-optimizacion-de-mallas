/*
	Fmin example finding the minimum of the function
	
	  f(x) = -1.3*exp(-2*x)/(1 + exp(-2*x))^2 - 0.7*exp(4 - 2*x)/(1 + exp(4 - 2*x) )^2
	  
  The function f has two minima at x = 0.039 and x = 1.818. The two minima 
  satisfy (at least approximately) the conditions g(x) = 0 and h(x) > 0. 
  f also has a local maximum at x = 1.346, characterized by g(x) = 0 and 
  h(x) = -0.196 < 0. 
  
  Outside the range of [-5, 5], f is essentially flat with a value of zero.
  
  This example exercises uncmin++'s minimization capabilities under various
  options of using numerical vs. algebraic derivatives using minimization by
  line search from different starting points. The expectation is that not all
  starting values from within [-5, 5] will lead to a function minimum.
   
	This test uses vector and matrix classes from the Simple C++ Numerical Toolkit 
	(SCPPNT) - http://www.smallwaters.com/software/cpp/scppnt.html.
	
	Fmin is available from http://www.smallwaters.com/programs/cpp/uncmin.html
*/

#include "Fmin.h"         // The code for Brent's methdo
#include "scppnt_math.h"  // Has min and abs function templates
#include <math.h>         // Needed for exp and pow functions
#include <iostream>       // for cout

  double abs(double a);

// Function to minimize
double f_to_minimize(double x)
{
  const double exp_4 = exp(4.0);
  double exp_minus2x = exp(-2.*x);
  
  double fvalue = -1.3*exp_minus2x / ( (1.0 + exp_minus2x)*(1 + exp_minus2x) ) - 
          0.7*exp_4*exp_minus2x / ( (1.0 + exp_4*exp_minus2x) * (1.0 + exp_4*exp_minus2x) );
  return  fvalue;
}

int main()
{	
  //  This driver will exercise Fmin++.
  //  Minimization will be by Brent's method
  
    double LowerBound = -5.0;
    double MidPoint	= 1.0;
    double UpperBound = 5.0;
    
    double tolerance = 3.0e-8;
    
    double fmin_x1value, fmin_x2value, fmin_global_xvalue, fmin_naive_xvalue;
    
    fmin_x1value = Fmin(LowerBound, MidPoint, f_to_minimize, tolerance);
    fmin_x2value = Fmin(MidPoint, UpperBound, f_to_minimize, tolerance);

    fmin_global_xvalue = SCPPNT::min( fmin_x1value, fmin_x2value);
    
		fmin_naive_xvalue = Fmin(LowerBound, UpperBound, f_to_minimize, tolerance);
		  
    std::cout << std::endl << "Segment-wise Function Minima" << std::endl;
    std::cout << "Range [" << LowerBound << "," << MidPoint << "], Xmin = " << fmin_x1value 
    		<< ", F(Xmin) = " << f_to_minimize(fmin_x1value) << std::endl;
    std::cout << "Range [" << MidPoint << "," << UpperBound << "], Xmin = " << fmin_x2value 
    		<< ", F(Xmin) = " << f_to_minimize(fmin_x2value) << std::endl;
    std::cout << std::endl << "Sophisticated Global Minimum" << std::endl;
    std::cout << "Range [" << LowerBound << "," << UpperBound << "], Xmin = " << fmin_global_xvalue 
    		<< ", F(Xmin) = " << f_to_minimize(fmin_global_xvalue) << std::endl;
    std::cout << std::endl << "Naive Global Minimum" << std::endl;
    std::cout << "Range [" << LowerBound << "," << UpperBound << "], Xmin = " << fmin_naive_xvalue 
    		<< ", F(Xmin) = " << f_to_minimize(fmin_naive_xvalue) << std::endl << std::endl;
		if ( SCPPNT::abs(fmin_global_xvalue - fmin_naive_xvalue) < 0.000001 )
			std::cout << "Naive minimum equals global minimum!"	<< std::endl;
		else
			std::cout << "Naive minimum missed global minimum!"	<< std::endl;
    
  return 0;
}

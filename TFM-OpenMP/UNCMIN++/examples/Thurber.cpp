/*
	Uncmin++ example using nonlinear regression with data set Thurber.dat
	from the Statistical Reference Datasets (http://www.itl.nist.gov/div898/strd/).

	This test uses vector and matrix classes from the Simple C++ Numerical Toolkit 
	(SCPPNT) - http://www.smallwaters.com/software/cpp/scppnt.html.
	
	uncmin++ is available from http://www.smallwaters.com/programs/cpp/uncmin.html
*/

#include "Uncmin.h"
#include "vec.h"	// Simple C++ Numeric Toolkit (SCPPNT) vector class
#include "cmat.h"	// Simple C++ Numeric Toolkit (SCPPNT) matrix class
#include <cstdio>	// for printf

#ifdef BOOST_NO_STDC_NAMESPACE
// Needed for compilers like Visual C++ 6 in which the standard
// C library functions are not in the std namespace.
namespace std {using ::printf;}
#endif

// Vector and Matrix classes from SCPPNT
typedef SCPPNT::Vector<double> dvec;
typedef SCPPNT::Matrix<double> dmat;


// Class to use as function template argument to Uncmin for this problem
class ThurberFunc
{

public:

	ThurberFunc();
	~ThurberFunc();
	
	// Function to minimize
	double f_to_minimize(dvec &p);
	
	// Gradient of function to minimize
	void gradient(dvec &p, dvec &g);
	
	// Hessian not used in this example
	void hessian(dvec /* &x */, dmat /* &h */) {}
	
	// Indicates analytic gradient is used
	int HasAnalyticGradient() {return 1;}
	
	// Indicates analytic hessian is not used
	int HasAnalyticHessian() {return 0;}
	
	// Any real vector will contain valid parameter values
	int ValidParameters(dvec &x) {return 1;}
	
	// Dimension of problem (7 parameters in nonlinear function)
	int dim() {return 7;}

private:

	dvec *response; // observed values of the response
	dvec *predictor; // observed values of the predictor

	// Function used in f_to_minimize() and gradient() giving
	// response as function of predictor
	inline double f(double x, dvec &param)
	{
		double x2 = x * x;
		double x3 = x2 * x;
		
		double y = param(1) + param(2) * x + param(3) * x2 + param(4) * x3;
		
		return y / (1.0 + param(5) * x + param(6) * x2 + param(7) * x3);
	}
};

// Constructor - initialize response and predictor vectors.
// Values taken from Thurber.dat.
ThurberFunc::ThurberFunc()
{

	const int n = 37;
	
	double yelem[n] = 
	{80.574E0, 84.248E0, 87.264E0, 87.195E0, 89.076E0, 
	89.608E0, 89.868E0, 90.101E0, 92.405E0, 95.854E0, 
	100.696E0, 101.060E0, 401.672E0, 390.724E0, 567.534E0, 
	635.316E0, 733.054E0, 759.087E0, 894.206E0, 990.785E0, 
	1090.109E0, 1080.914E0, 1122.643E0, 1178.351E0, 1260.531E0, 
	1273.514E0, 1288.339E0, 1327.543E0, 1353.863E0, 1414.509E0, 
	1425.208E0, 1421.384E0, 1442.962E0, 1464.350E0, 1468.705E0, 
	1447.894E0, 1457.628E0};

	double xelem[n] = 
	{-3.067E0, -2.981E0, -2.921E0, -2.912E0, -2.840E0, 
	-2.797E0, -2.702E0, -2.699E0, -2.633E0, -2.481E0, 
	-2.363E0, -2.322E0, -1.501E0, -1.460E0, -1.274E0, 
	-1.212E0, -1.100E0, -1.046E0, -0.915E0, -0.714E0, 
	-0.566E0, -0.545E0, -0.400E0, -0.309E0, -0.109E0, 
	-0.103E0, 0.010E0, 0.119E0, 0.377E0, 0.790E0, 
	0.963E0, 1.006E0, 1.115E0, 1.572E0, 1.841E0, 
	2.047E0, 2.200E0};
	
	response = new dvec(yelem, yelem+n);
	predictor = new dvec(xelem, xelem+n);

}

// Destructor
ThurberFunc::~ThurberFunc()
{
	delete response;
	delete predictor;
}

// Function to minimize
double ThurberFunc::f_to_minimize(dvec &p)
{
	dvec::iterator yi = response->begin();
	dvec::iterator xi = predictor->begin();
	dvec::iterator yend = response->end();
	
	double value = 0.0;
	
	while(yi != yend)
	{
		double y = *yi++;
		
		y -= f(*xi++, p);
		
		value += y * y;
	}
	
	return value;
}

// Gradient of function to minimize
void ThurberFunc::gradient(dvec &p, dvec &g)
{
	dvec::iterator yi = response->begin();
	dvec::iterator xi = predictor->begin();
	dvec::iterator yend = response->end();
	
	g = 0.0;
	
	// double value = 0.0;  (variable not used. ww, 12-22-07)
	
	while(yi != yend)
	{
		double y = *yi++;
		double fhat = f(*xi, p);
		
		y -= fhat;
		y *= -2.0;
		
		double x2 = *xi * *xi;
		double x3 = x2 * *xi;
		double num = p(1) + p(2) * *xi + p(3) * x2 + p(4) * x3;
		
		double den = 1.0 / (1.0 + p(5) * *xi + p(6) * x2 + p(7) * x3);
		
		// first parameter
		g(1) += y * den;
		
		// second parameter
		g(2) += y * (*xi * den);
		
		// third parameter
		g(3) += y * (x2 * den);
		
		g(4) += y * (x3 * den);
		double den2 = den*den;
		g(5) += y * (-*xi * num * den2);
		g(6) += y * (-x2 * num * den2);
		g(7) += y * (-x3 * num * den2);
		
		++xi;
	}
	
}

int main()
{	

	const int dim = 7;
	ThurberFunc thurber; // Create function object
	Uncmin<dvec, dmat, ThurberFunc> min(&thurber); // create Uncmin object

	// Set typical values of function and parameters
	double typical_size[7] = {1000.0, 1000.0, 100.0, 100.0, 1.0, 0.1, 0.01};
	min.SetScaleFunc(5000);
	dvec typ(typical_size, typical_size + dim);
	if (min.SetScaleArg(typ))
	{
		std::printf("Error in setting typical value");
		exit(0);
	}
	
	// For diagnostic printout to stdout, uncomment the next to lines...
	
	// FILE* fh = (FILE*)stdout;
	// min.SetPrint(fh, 1, 1);
	
	// Tighten up the default convergence criterion, so we get the same results as Thurber ...
	
	min.SetTolerances(1.0E-10, 1.0E-10);
	
	// starting values
	double start_values[7] = {1000.0, 1000.0, 400.0, 40.0, 0.7, 0.3, 0.03};
	dvec start(start_values, start_values+dim);


	// xpls will contain solution, gpls will contain
	// gradient at solution after call to min.Minimize
	dvec xpls(dim), gpls(dim);

	// fpls contains the value of the function at
	// the solution given by xpls.
	double fpls;
	
	// Minimize function
	int result = min.Minimize(start, xpls,  fpls,  gpls);
	
	// Find status of function minimization.
	// For a description of the values returned by
	// GetMessage see the documentation at the top
	// of Uncmin.h.
	int msg = min.GetMessage();
	
	std::printf("\nMessage returned from Uncmin: %d\n", msg);
	std::printf("\nFunction minimum: %.10e\n", fpls);
	std::printf("\nParameters:\n");
	
	// Print solution
	for (int i=1; i<=dim; i++)
	{
		std::printf("%.10e\n", xpls(i));
	}


	return 0;
}

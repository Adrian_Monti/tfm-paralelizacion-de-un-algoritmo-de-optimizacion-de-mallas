/*
	Uncmin++ example finding the minimum of the function
	
	  f(x) = -1.3*exp(-2*x)/(1 + exp(-2*x))^2 - 0.7*exp(4 - 2*x)/(1 + exp(4 - 2*x) )^2,
	  
	with first derivative
	  
    g(x) = 2.6*(exp(-2*x) - exp(-4*x))/( 1 + exp(-2*x) )^3 
         
           + 1.4*( exp(4 - 2*x) - exp(8 - 4*x) ) / (1 + exp( 4 - 2*x ) )^3
	           
	and second derivative
	
    h(x) = 2.6*(-2*exp(-2*x) + 4*exp(-4*x))/( 1 + exp(-2*x) )^3 +
         
           15.6*exp(-2*x)(exp(-2*x) - exp(-4*x))/(1+exp(-2*x))^4 -
           
           1.4*(2*exp(4 - 2*x) - 4*exp(8 - 4*x))/(1 + exp(4 - 2*x))^3 -
           
           8.4*(exp(8 - 4*x) - exp(4 - 2*x))*exp(4 - 2*x)/(1 + exp(4 - 2*x))^4
            
  The function f has two minima at x = 0.039 and x = 1.818. The two minima 
  satisfy (at least approximately) the conditions g(x) = 0 and h(x) > 0. 
  f also has a local maximum at x = 1.346, characterized by g(x) = 0 and 
  h(x) = -0.196 < 0. 
  
  Outside the range of [-5, 5], f is essentially flat with a value of zero.
  
  This example exercises uncmin++'s minimization capabilities under various
  options of using numerical vs. algebraic derivatives using minimization by
  line search from different starting points. The expectation is that not all
  starting values from within [-5, 5] will lead to a function minimum.
   
	This test uses vector and matrix classes from the Simple C++ Numerical Toolkit 
	(SCPPNT) - http://www.smallwaters.com/software/cpp/scppnt.html.
	
	uncmin++ is available from http://www.smallwaters.com/programs/cpp/uncmin.html
*/

#include "Uncmin.h"
#include "vec.h"	// Simple C++ Numeric Toolkit (SCPPNT) vector class
#include "cmat.h"	// Simple C++ Numeric Toolkit (SCPPNT) matrix class
#include <math.h>   // Needed for exp and pow functions
#include <iostream> // for cout
#include <iomanip>  // for cout's setprecision() function.
#include <cstdio>	// for printf


#ifdef BOOST_NO_STDC_NAMESPACE
// Needed for compilers like Visual C++ 6 in which the standard
// C library functions are not in the std namespace.
namespace std {using ::printf;}
#endif

// Vector and Matrix classes from SCPPNT
typedef SCPPNT::Vector<double> dvec;
typedef SCPPNT::Matrix<double> dmat;


// Class to use as function template argument to Uncmin for numeric minimization
class FunctionOnly
{
public:

  FunctionOnly();
	~FunctionOnly();
	
	// Function to minimize
	double f_to_minimize(dvec &p);
	
	// Gradient of function to minimize
	void gradient(dvec &p, dvec &g);
	
	// Hessian of function
	void hessian(dvec &p, dmat &h);
	
	// Indicates analytic gradient is used (0 = do not use)
	int HasAnalyticGradient(); //{return 0;}
	
	// Indicates analytic hessian is not used (0 = do not use)
	int HasAnalyticHessian();// {return 0;}
	
	// Any real vector will contain valid parameter values
	int ValidParameters(dvec &x) {return 1;}
	
	// Dimension of problem (1 parameters in two-minima function)
	int dim() {return 1;}

	//private: // No private data structures in this function!
};

// Constructor 
FunctionOnly::FunctionOnly()
{
}

// Destructor
FunctionOnly::~FunctionOnly()
{
}

// Function to minimize
double FunctionOnly::f_to_minimize(dvec &p)
{
  const double exp_4 = exp(4.0);
  double exp_minus2x = exp(-2.*p(1));
  
  double fvalue = -1.3*exp_minus2x / ( (1.0 + exp_minus2x)*(1 + exp_minus2x) ) - 
          0.7*exp_4*exp_minus2x / ( (1.0 + exp_4*exp_minus2x) * (1.0 + exp_4*exp_minus2x) );
  return  fvalue;
}

// Gradient of function to minimize
void FunctionOnly::gradient(dvec &p, dvec &g)
{
  // Not provided for this function class
}

// Hessian of function to minimize
void FunctionOnly::hessian(dvec &p, dmat &h)
{
  // Not provided for this function class
}

// Indicate whether analytic gradient is used (0 = do not use)
int FunctionOnly::HasAnalyticGradient() {return 0;}

// Indicate whether analytic hessian is used (0 = do not use)
int FunctionOnly::HasAnalyticHessian() {return 0;}

// Function with analytic gradient

// Class to use as function template argument to Uncmin for minimization with analytic first derivative

class FunctionAndGradient: public FunctionOnly
{
public:
  
  FunctionAndGradient();
  ~FunctionAndGradient();
  
  // Gradient of function to minimize
  void gradient(dvec &p, dvec &g);
  
  // Indicates analytic gradient is used (0 = do not use)
  int HasAnalyticGradient();

//private: // No private data structures in this function!
};

// Constructor
FunctionAndGradient::FunctionAndGradient()
{
}

// Destructor
FunctionAndGradient::~FunctionAndGradient()
{
}

// Gradient of function to minimize
void FunctionAndGradient::gradient(dvec &p, dvec &g)
{
  
  const double exp_4 = exp(4.0);
  double exp_minus2x = exp(-2.*p(1));
  
 	g(1) =  2.6*(exp_minus2x - exp_minus2x*exp_minus2x) / 
 	            pow(1.0 + exp_minus2x, 3.0)     +
 	        1.4*(exp_minus2x*exp_4 - exp_minus2x*exp_minus2x*exp_4*exp_4) / 
 	            pow(1.0 + exp_minus2x*exp_4, 3.0);
}

// Indicates whether analytic gradient is used (0 = do not use)
int FunctionAndGradient::HasAnalyticGradient() {return 1;}

      // Function with both analytic Gradient and Hessian

// Class to use as function template argument to Uncmin for minimization with analytic first adn second derivatives
class FunctionGradientAndHessian: public FunctionAndGradient
{
public:
  
  FunctionGradientAndHessian();
  ~FunctionGradientAndHessian();
  
  // Hessian of function
  void hessian(dvec &p, dmat &h);
  
  // Indicates analytic hessian is not used (0 = do not use)
  int HasAnalyticHessian();
  
//private: // No private data structures in this function!
};

// Constructor
FunctionGradientAndHessian::FunctionGradientAndHessian()
{
}

// Destructor
FunctionGradientAndHessian::~FunctionGradientAndHessian()
{
}

// Hessian of function to minimize
void FunctionGradientAndHessian::hessian(dvec &p, dmat &h)
{
  const double exp_4 = exp(4.0);
  double exp_minus2x = exp(-2.*p(1));
 
  h(1,1)  =  2.6*(4.0*exp_minus2x*exp_minus2x - 2.0*exp_minus2x) /
                 pow(1.0 + exp_minus2x, 3.0)        +
             15.6*(exp_minus2x - exp_minus2x*exp_minus2x)*exp_minus2x /
                 pow(1.0 + exp_minus2x, 4.0)        -
             1.4*(2.0*exp_minus2x*exp_4 - 4.0*exp_minus2x*exp_minus2x*exp_4*exp_4) /
                 pow(1.0 + exp_minus2x*exp_4, 3.0)  +
             8.4*(exp_minus2x*exp_4 - exp_minus2x*exp_minus2x*exp_4*exp_4)*exp_minus2x*exp_4 /
                 pow(1.0 + exp_minus2x*exp_4, 4.0);
}

// Indicates whether analytic hessian is  used (0 = do not use)
int FunctionGradientAndHessian::HasAnalyticHessian() {return 1;}

int main()
{	
  //  This driver will exercise Uncmin++ 33 times, combining three types of curvature information
  //  with 11 starting values.
  //
  //  Types of curvature information:
  //  1.  Function only, no derivatives
  //  2.  Function and first derivative
  //  3.  Function, plus first and second derivatives
  //  
  //  Starting values are -5, -4, ... , 5
  //
  //  Minimization will be by line search (default method)
    
  //  Part 1, Declaration of Objects Common to all Functions
  
  // Dimensionality of example problem 
  // (all minimization use the same 1-dimensional function)
	const int dim = 1;
	
  // xpls will contain solution, gpls will contain
  // gradient at solution after call to min_<f>.Minimize
  dvec xpls(dim), gpls(dim);
  
  // hpls provides the space for the hessian
  dmat hpls(dim, dim);
  // there are also these derived matrices
  dmat cholesky(dim, dim), Hessian(dim, dim);

  // fpls contains the value of the function at
  // the solution given by xpls.
  double fpls;
  
  // Part 2: Load and minimimize the three functions
  
  // Part 2-1: Only the function itself is supplied, no derivates
  
  std::cout << "Minimize TwoMinima Function, Function Supplied without Derivatives"
    << std::endl;
  
  FunctionOnly FuncOnly; // Create function object
	Uncmin<dvec, dmat, FunctionOnly> min_f(&FuncOnly); // create Uncmin object

  //Minimize the function
  for (int iStart = -5; iStart < 6; iStart++)  //  Try 11 different start values.
  {
 
    std::cout << std::endl << "Start value: " << iStart << std::endl;
    
    // starting values
    double start_values[1] = {iStart};
    
    // Initialize start values
    dvec start(start_values, start_values+dim);
    
    // Minimize function
    min_f.Minimize(start, xpls, fpls, gpls, hpls);
  
    // Find status of function minimization.
    // For a description of the values returned by
    // GetMessage see the documentation at the beginning
    // of Uncmin.h.
    int msg = min_f.GetMessage();
  
    std::cout << std::setprecision(8);
    
    std::cout << std::endl << std::endl << "Message returned from Uncmin: " << msg << std::endl;
    std::cout << std::endl << "Function minimum: " << std::endl << fpls << std::endl;
    std::cout << std::endl << "Parameters: " << xpls << std::endl;
    std::cout << "Gradient: " << gpls << std::endl;
    // Get lower triangle extract of hpls, with upper off-diagonal values set to zero.
    cholesky = hpls;
    for ( int icolumn = 1; icolumn < cholesky.num_columns()  ; icolumn++ )
    {
      for ( int irow = 0; irow < icolumn; irow++)
        cholesky[irow][icolumn] = 0.0;
    }
    Hessian = cholesky * transpose(cholesky);
    std::cout << "Hessian:  " << Hessian << std::endl << std::endl;
  }
	
  // Part 2-2: Function and first derivate supplied, no second-order derivate
  
  std::cout << std::endl << std::endl << 
    "Minimize TwoMinima Function, Function and First Derivative Supplied"
    << std::endl;
  
  FunctionAndGradient FuncAndDerivative1; // Create function object
  Uncmin<dvec, dmat, FunctionAndGradient> min_fg(&FuncAndDerivative1); // create Uncmin object
 
  //Minimize the function
  for (int iStart = -5; iStart < 6; iStart++)  //  Try 11 different start values.
  {

    std::cout << std::endl << "Start value: " << (double) iStart << std::endl;
     
    // starting values
    double start_values[1] = {(double) iStart};

    // Initialize start values
    dvec start(start_values, start_values+dim);
    
    // Minimize function
    min_fg.Minimize(start, xpls, fpls, gpls, hpls);
      
    // Find status of function minimization.
    // For a description of the values returned by
    // GetMessage see the documentation at the beginning
    // of Uncmin.h.
    int msg = min_fg.GetMessage();
  
    std::cout << std::endl << std::endl << "Message returned from Uncmin: " << msg << std::endl;
    std::cout << std::endl << "Function minimum: " << std::endl << fpls << std::endl;
    std::cout << std::endl << "Parameters: " << xpls << std::endl;
    std::cout << "Gradient: " << gpls << std::endl;
    // Get lower triangle extract of hpls, with upper off-diagonal values set to zero.
    cholesky = hpls;
    for ( int icolumn = 1; icolumn < cholesky.num_columns()  ; icolumn++ )
    {
      for ( int irow = 0; irow < icolumn; irow++)
        cholesky[irow][icolumn] = 0.0;
    }
    Hessian = cholesky * transpose(cholesky);
    std::cout << "Hessian:  " << Hessian << std::endl << std::endl;
  }

  // Part 2-3: Function, first and second-order derivates supplied
  
  std::cout << std::endl << std::endl << 
    "Minimize TwoMinima Function, Function, 1st- and 2nd-order Derivatives Supplied"
    << std::endl;
  
  FunctionGradientAndHessian FuncAnd2Derivatives; // Create function object
  Uncmin<dvec, dmat, FunctionGradientAndHessian> min_fgh(&FuncAnd2Derivatives); // create Uncmin object
 
  //Minimize the function
  for (int iStart = -5; iStart < 6; iStart++)  //  Try 11 different start values.
  {

    std::cout << std::endl << "Start value: " << (double) iStart << std::endl;
   
    // starting values
    double start_values[1] = {(double) iStart};
     
    // Initialize start values
    dvec start(start_values, start_values+dim);
  
    // Minimize function
    min_fgh.Minimize(start, xpls, fpls, gpls, hpls);
      
    // Find status of function minimization.
    // For a description of the values returned by
    // GetMessage see the documentation at the beginning
    // of Uncmin.h.
    int msg = min_fgh.GetMessage();
  
    std::cout << std::endl << std::endl << "Message returned from Uncmin: " << msg << std::endl;
    std::cout << std::endl << "Function minimum: " << std::endl << fpls << std::endl;
    std::cout << std::endl << "Parameters: " << xpls << std::endl;
    std::cout << "Gradient: " << gpls << std::endl;
    // Get lower triangle extract of hpls, with upper off-diagonal values set to zero.
    cholesky = hpls;
    for ( int icolumn = 1; icolumn < cholesky.num_columns()  ; icolumn++ )
    {
      for ( int irow = 0; irow < icolumn; irow++)
        cholesky[irow][icolumn] = 0.0;
    }
    Hessian = cholesky * transpose(cholesky);
    std::cout << "Hessian:  " << Hessian << std::endl << std::endl;
  }

  return 0;
}

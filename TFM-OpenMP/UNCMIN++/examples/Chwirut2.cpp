/*
	Uncmin++ example using nonlinear regression with data set Chwirut2.dat
	from the Statistical Reference Datasets (http://www.itl.nist.gov/div898/strd/).
	
	This test uses vector and matrix classes from the Simple C++ Numerical Toolkit 
	(SCPPNT) - http://www.smallwaters.com/software/cpp/scppnt.html.
	
	uncmin++ is available from http://www.smallwaters.com/programs/cpp/uncmin.html
*/

#include "Uncmin.h"
#include "vec.h"	// Simple C++ Numeric Toolkit (SCPPNT) vector class
#include "cmat.h"	// Simple C++ Numeric Toolkit (SCPPNT) matrix class
#include <cstdio>	// for printf
#include <cmath>	// for exp

#ifdef BOOST_NO_STDC_NAMESPACE
// Needed for compilers like Visual C++ 6 in which the standard
// C library functions are not in the std namespace.
namespace std {using ::exp; using ::printf;}
#endif

// Vector and Matrix classes from SCPPNT
typedef SCPPNT::Vector<double> dvec;
typedef SCPPNT::Matrix<double> dmat;

// Class to use as function template argument to Uncmin for this problem
class ChwirutFunc
{

public:

	ChwirutFunc();
	~ChwirutFunc();

	// Function to minimize
	double f_to_minimize(dvec &p);
	
	// Gradient of function to minimize
	void gradient(dvec &p, dvec &g);
	
	// Hessian not used in this example
	void hessian(dvec /* &x */, dmat /* &h */) {}
	
	// Indicates analytic gradient is used
	int HasAnalyticGradient() {return 1;}
	
	// Indicates analytic hessian is not used
	int HasAnalyticHessian() {return 0;}
	
	// Any real vector will contain valid parameter values
	int ValidParameters(dvec &x) {return 1;}
	
	// Dimension of problem (3 parameters in nonlinear function)
	int dim() {return 3;}

private:

	dvec *response; // observed values of the response
	dvec *predictor; // observed values of the predictor
	
	// Function used in f_to_minimize() and gradient() giving
	// response as function of predictor
	inline double f(double x, dvec &param)
	{
		double y = std::exp(-param(1) * x);
		
		return y / (param(2) + param(3) * x);
	}
};

// Constructor - initialize response and predictor vectors.
// Values taken from Chwirut2.dat.
ChwirutFunc::ChwirutFunc()
{

	const int n = 54;
	
	double yelem[n] = 
	{92.9000E0, 57.1000E0, 31.0500E0, 11.5875E0, 8.0250E0, 
	63.6000E0, 21.4000E0, 14.2500E0, 8.4750E0, 63.8000E0, 
	26.8000E0, 16.4625E0, 7.1250E0, 67.3000E0, 41.0000E0, 
	21.1500E0, 8.1750E0, 81.5000E0, 13.1200E0, 59.9000E0, 
	14.6200E0, 32.9000E0, 5.4400E0, 12.5600E0, 5.4400E0, 
	32.0000E0, 13.9500E0, 75.8000E0, 20.0000E0, 10.4200E0, 
	59.5000E0, 21.6700E0, 8.5500E0, 62.0000E0, 20.2000E0, 
	7.7600E0, 3.7500E0, 11.8100E0, 54.7000E0, 23.7000E0, 
	11.5500E0, 61.3000E0, 17.7000E0, 8.7400E0, 59.2000E0, 
	16.3000E0, 8.6200E0, 81.0000E0, 4.8700E0, 14.6200E0, 
	81.7000E0, 17.1700E0, 81.3000E0, 28.9000E0};

	double xelem[n] = 
	{0.500E0, 1.000E0, 1.750E0, 3.750E0, 5.750E0, 
	0.875E0, 2.250E0, 3.250E0, 5.250E0, 0.750E0, 
	1.750E0, 2.750E0, 4.750E0, 0.625E0, 1.250E0, 
	2.250E0, 4.250E0, .500E0, 3.000E0, .750E0, 
	3.000E0, 1.500E0, 6.000E0, 3.000E0, 6.000E0, 
	1.500E0, 3.000E0, .500E0, 2.000E0, 4.000E0, 
	.750E0, 2.000E0, 5.000E0, .750E0, 2.250E0, 
	3.750E0, 5.750E0, 3.000E0, .750E0, 2.500E0, 
	4.000E0, .750E0, 2.500E0, 4.000E0, .750E0, 
	2.500E0, 4.000E0, .500E0, 6.000E0, 3.000E0, 
	.500E0, 2.750E0, .500E0, 1.750E0};
	
	response = new dvec(yelem, yelem+n);
	predictor = new dvec(xelem, xelem+n);

}

// Destructor
ChwirutFunc::~ChwirutFunc()
{
	delete response;
	delete predictor;
}

// Function to minimize
double ChwirutFunc::f_to_minimize(dvec &p)
{
	dvec::iterator yi = response->begin();
	dvec::iterator xi = predictor->begin();
	dvec::iterator yend = response->end();
	
	double value = 0.0;
	
	while(yi != yend)
	{
		double y = *yi++;
		
		y -= f(*xi++, p);
		
		value += y * y;
	}
	
	return value;
}

// Gradient of function to minimize
void ChwirutFunc::gradient(dvec &p, dvec &g)
{
	dvec::iterator yi = response->begin();
	dvec::iterator xi = predictor->begin();
	dvec::iterator yend = response->end();
	
	g = 0.0;
	
	double value = 0.0;
	
	while(yi != yend)
	{
		double y = *yi++;
		double fhat = f(*xi, p);
		
		y -= fhat;
		y *= -2.0;
		
		// first parameter
		g(1) += y * (-fhat * *xi);
		
		// second parameter
		double g2 = -fhat / (p(2) + p(3) * *xi);
		g(2) += y * g2;
		
		// third parameter
		g(3) += y * g2 * *xi;
		
		++xi;
	}
	
}

int main()
{	

	const int dim = 3;
	ChwirutFunc chwirut; // Create function object
	Uncmin<dvec, dmat, ChwirutFunc> min(&chwirut); // create Uncmin object
	
	// starting values
	double start_values[3] = {.10, .01, .02};
	dvec start(start_values, start_values+dim);
	
	// xpls will contain solution, gpls will contain
	// gradient at solution after call to min.Minimize
	dvec xpls(dim), gpls(dim);
	
	// fpls contains the value of the function at
	// the solution given by xpls.
	double fpls;
	
	// Minimize function
	int result = min.Minimize(start, xpls,  fpls,  gpls);
	
	// Find status of function minimization.
	// For a description of the values returned by
	// GetMessage see the documentation at the top
	// of Uncmin.h.
	int msg = min.GetMessage();
	
	std::printf("\nMessage returned from Uncmin: %d\n", msg);
	std::printf("\nFunction minimum: %.10e\n", fpls);
	std::printf("\nParameters:\n");
	
	// Print solution
	for (int i=1; i<=dim; i++)
	{
		std::printf("%.10e\n", xpls(i));
	}


	return 0;
}

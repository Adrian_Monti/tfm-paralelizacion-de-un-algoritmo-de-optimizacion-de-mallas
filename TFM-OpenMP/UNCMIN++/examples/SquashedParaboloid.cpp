/*
	Uncmin++ example finding the minimum of the bivariate function
	
	  F(x, y) = (x - 10)^2 + (y - 10)^2 + (x - 10)*(y - 10),
	  
	with gradient
	  
	           (dF/dx)   (2*x +   y - 30)
	  g(x,y) = (-----) = (--------------) 
	           (dF/dy)   (  x + 2*y - 30)
	           
	and Hessian
	
	           ( 2 | 1 )
	  H(x,y) = (---+---) .
             ( 1 | 1 )
            
  The function has an obvious single and global minimum at (x, y) = (10, 10).
  
  It is used to exercise uncmin++'s minimization capabilities under various
  options of using numerical vs. algebraic derivatives and different search
  algorithms.
   
	This test uses vector and matrix classes from the Simple C++ Numerical Toolkit 
	(SCPPNT) - http://www.smallwaters.com/software/cpp/scppnt.html.
	
	uncmin++ is available from http://www.smallwaters.com/programs/cpp/uncmin.html
*/

#include "Uncmin.h"
#include "vec.h"	// Simple C++ Numeric Toolkit (SCPPNT) vector class
#include "cmat.h"	// Simple C++ Numeric Toolkit (SCPPNT) matrix class
#include <iostream> // for cout
#include <cstdio>	// for printf


#ifdef BOOST_NO_STDC_NAMESPACE
// Needed for compilers like Visual C++ 6 in which the standard
// C library functions are not in the std namespace.
namespace std {using ::printf;}
#endif

// Vector and Matrix classes from SCPPNT
typedef SCPPNT::Vector<double> dvec;
typedef SCPPNT::Matrix<double> dmat;


// Class to use as function template argument to Uncmin for numeric minimization
class BivarFunctionOnly
{
public:

  BivarFunctionOnly();
	~BivarFunctionOnly();
	
	// Function to minimize
	double f_to_minimize(dvec &p);
	
	// Gradient of function to minimize
	void gradient(dvec &p, dvec &g);
	
	// Hessian of function
	void hessian(dvec &p, dmat &h);
	
	// Indicates analytic gradient is used (0 = do not use)
	int HasAnalyticGradient(); //{return 0;}
	
	// Indicates analytic hessian is not used (0 = do not use)
	int HasAnalyticHessian();// {return 0;}
	
	// Any real vector will contain valid parameter values
	int ValidParameters(dvec &x) {return 1;}
	
	// Dimension of problem (2 parameters in quadratic function)
	int dim() {return 2;}

	//private: // No private data structures in this function!
};

// Constructor 
BivarFunctionOnly::BivarFunctionOnly()
{
}

// Destructor
BivarFunctionOnly::~BivarFunctionOnly()
{
}

// Function to minimize
double BivarFunctionOnly::f_to_minimize(dvec &p)
{
  return  
    (p(1) - 1.0e6)*(p(1) - 1.0e6) +   // (x - 10)^2
    (p(2) - 10.0)*(p(2) - 10.0) ;  // (y - 10)^2
    //    (p(1) - 1.0e6)*(p(2) - 10.0);    // (x - 10)*(y - 10)
}

// Gradient of function to minimize
void BivarFunctionOnly::gradient(dvec &p, dvec &g)
{
  // Not provided for this function class
}

// Hessian of function to minimize
void BivarFunctionOnly::hessian(dvec &p, dmat &h)
{
  // Not provided for this function class
}

// Indicate whether analytic gradient is used (0 = do not use)
int BivarFunctionOnly::HasAnalyticGradient() {return 0;}

// Indicate whether analytic hessian is used (0 = do not use)
int BivarFunctionOnly::HasAnalyticHessian() {return 0;}


      // Function with analytic gradient

// Class to use as function template argument to Uncmin for minimization with analytic first derivative

class BivarFunctionAndGradient: public BivarFunctionOnly
{
public:
  
  BivarFunctionAndGradient();
  ~BivarFunctionAndGradient();
  
  // Gradient of function to minimize
  void gradient(dvec &p, dvec &g);
  
  // Indicates analytic gradient is used (0 = do not use)
  int HasAnalyticGradient();

//private: // No private data structures in this function!
};

// Constructor
BivarFunctionAndGradient::BivarFunctionAndGradient()
{
}

// Destructor
BivarFunctionAndGradient::~BivarFunctionAndGradient()
{
}

// Gradient of function to minimize
void BivarFunctionAndGradient::gradient(dvec &p, dvec &g)
{
		// first parameter
		g(1) = 2.0*p(1) - 2.0*1.0e6;
		
		// second parameter
		g(2) = 2.0*p(2) - 20.0;
}

// Indicates whether analytic gradient is used (0 = do not use)
int BivarFunctionAndGradient::HasAnalyticGradient() {return 1;}

      // Function with both analytic Gradient and Hessian

// Class to use as function template argument to Uncmin for minimization with analytic first adn second derivatives
class BivarFunctionGradientAndHessian: public BivarFunctionAndGradient
{
public:
  
  BivarFunctionGradientAndHessian();
  ~BivarFunctionGradientAndHessian();
  
  // Hessian of function
  void hessian(dvec &p, dmat &h);
  
  // Indicates analytic hessian is not used (0 = do not use)
  int HasAnalyticHessian();
  
//private: // No private data structures in this function!
};

// Constructor
BivarFunctionGradientAndHessian::BivarFunctionGradientAndHessian()
{
}

// Destructor
BivarFunctionGradientAndHessian::~BivarFunctionGradientAndHessian()
{
}

// Hessian of function to minimize
void BivarFunctionGradientAndHessian::hessian(dvec &p, dmat &h)
{
  h[0][0] = 2.0;
  h[0][1] = 0.0; // Leave upper off-diagonal entries at 0.0.
  h[1][0] = 0.0;
  h[1][1] = 2.0;
}

// Indicates whether analytic hessian is  used (0 = do not use)
int BivarFunctionGradientAndHessian::HasAnalyticHessian() {return 1;}

int main()
{	
  //  This driver will exercise Uncmin++ 9 times, combining three types of curvature information
  //  with three minimization approaches.
  //  
  //  Types of curvature information:
  //  1.  Function only, no derivatives
  //  2.  Function and first derivative
  //  3.  Function, plus first and second derivatives
  //  
  //  Minimization approaches
  //  a.  Line Search
  //  b.  Double Dogleg
  //  c.  More-Hebdon
    
  //  Part 1, Declaration of Objects Common to all Functions
  
  // Dimensionality of example problem 
  // (all minimization use the same 2-dimensional function)
  const int dim = 2;
  
  // Set typical magnitude of parameter values (off by one in this example)
  const double typical_size[2] = {1.0e3, 10.0}; //
  const dvec typ(typical_size, typical_size + dim);
  
  // starting values
  const double start_values[2] = {0.0, 0.0};

  // Find filehandle of standard output stream (used for intermediate minimization output)
  FILE* fh = (FILE*)stdout;
	
  // xpls will contain solution, gpls will contain
  // gradient at solution after call to min_<f>.Minimize
  dvec xpls(dim), gpls(dim);
  
  // hpls provides the space for the hessian
  dmat hpls(dim, dim);
  // there are also these derived matrices
  dmat cholesky(dim, dim), Hessian(dim, dim);

  // fpls contains the value of the function at
  // the solution given by xpls.
  double fpls;
  

  // Part 2: Load and minimimize the three functions

#ifdef LACOSITA  
  // Part 2-1: Only the function itself is supplied, no derivatives
  
  std::cout << "Minimize 3-dimensional Paraboloid, Function Supplied without Derivatives"
    << std::endl;
  
  BivarFunctionOnly FunctionOnly; // Create function object
	Uncmin<dvec, dmat, BivarFunctionOnly> min_f(&FunctionOnly); // create Uncmin object
	
	// Set typical magnitude of function values at the minimum
	min_f.SetScaleFunc(1);              // Off by an order of magnitude
	//	min_f.SetMaxIter(300); // intenta 300 iteraciones
	// Set typical magnitude of argument values at function minimum
  if (min_f.SetScaleArg(typ))
  {
    std::cout << std::endl << "Error in setting typical value" << std::endl;
    exit(0);
  }
  
  // Diagnostic printout piped to stdout
  min_f.SetPrint(fh, 1, 1);
  
  // Default convergence criteria are tightened up here by two orders of magnitude
  min_f.SetTolerances(1E-4, 1E-4);

  //Minimize the function
  for (int iMethod = 1; iMethod < 4; iMethod++)  //  Cycle through minimization methods
  {
    switch (iMethod)
    {
      case 1:  std::cout << std::endl << "Minimization by Line Search Method" << std::endl;
        break;
      case 2:  std::cout << std::endl << "Minimization by Double Dogleg Method" << std::endl;
        break;
      case 3:  std::cout << std::endl << "Minimization by More-Hebdon Method" << std::endl;
        break;
    }
    // Set Minimization method
    min_f.SetMethod(iMethod);
    
    // Initialize start values
    dvec start(start_values, start_values+dim);
    
    // Minimize function
    min_f.Minimize(start, xpls, fpls, gpls, hpls);
  
    // Find status of function minimization.
    // For a description of the values returned by
    // GetMessage see the documentation at the beginning
    // of Uncmin.h.
    int msg = min_f.GetMessage();
  
    std::cout << std::endl << std::endl << "Message returned from Uncmin: " << msg << std::endl;
    std::cout << std::endl << "Function minimum: " << std::endl << fpls << std::endl;
    std::cout << std::endl << "Parameters: " << xpls << std::endl;
    std::cout << "Gradient: " << gpls << std::endl;
    // Get lower triangle extract of hpls, with upper off-diagonal values set to zero.
    cholesky = hpls;
    for ( int icolumn = 1; icolumn < cholesky.num_columns()  ; icolumn++ )
    {
      for ( int irow = 0; irow < icolumn; irow++)
        cholesky[irow][icolumn] = 0.0;
    }
    Hessian = cholesky * transpose(cholesky);
    std::cout << "Hessian:  " << Hessian << std::endl << std::endl;
  }
	

  // Part 2-2: Function and first derivative supplied, no second-order derivative
  
  std::cout << std::endl << std::endl << 
    "Minimize 2-dimensional Paraboloid, Function and First Derivatives Supplied"
    << std::endl;
  
  BivarFunctionAndGradient FunctionAndGradient; // Create function object
  Uncmin<dvec, dmat, BivarFunctionAndGradient> min_fg(&FunctionAndGradient); // create Uncmin object
 
  // Set typical magnitude of function values at the minimum
  min_fg.SetScaleFunc(10);              // Off by an order of magnitude

  // Set typical magnitude of argument values at function minimum
  if (min_fg.SetScaleArg(typ))
  {
    std::cout << std::endl << "Error in setting typical value" << std::endl;
    exit(0);
  }
  
  // Diagnostic printout piped to stdout
  min_fg.SetPrint(fh, 1, 1);
  
  // Default convergence criteria are tightened up here by two orders of magnitude
  min_fg.SetTolerances(1E-10, 1E-10);

  //Minimize the function
  for (int iMethod = 1; iMethod < 4; iMethod++)  //  Cycle through minimization methods
  {
    switch (iMethod)
    {
      case 1:  std::cout << std::endl << "Minimization by Line Search Method" << std::endl;
        break;
      case 2:  std::cout << std::endl << "Minimization by Double Dogleg Method" << std::endl;
        break;
      case 3:  std::cout << std::endl << "Minimization by More-Hebdon Method" << std::endl;
        break;
    }
    // Set Minimization method
    min_fg.SetMethod(iMethod);
    
    // Initialize start values
    dvec start(start_values, start_values+dim);
    
    // Minimize function
    min_fg.Minimize(start, xpls, fpls, gpls, hpls);
      
    // Find status of function minimization.
    // For a description of the values returned by
    // GetMessage see the documentation at the beginning
    // of Uncmin.h.
    int msg = min_fg.GetMessage();
  
    std::cout << std::endl << std::endl << "Message returned from Uncmin: " << msg << std::endl;
    std::cout << std::endl << "Function minimum: " << std::endl << fpls << std::endl;
    std::cout << std::endl << "Parameters: " << xpls << std::endl;
    std::cout << "Gradient: " << gpls << std::endl;
    // Get lower triangle extract of hpls, with upper off-diagonal values set to zero.
    cholesky = hpls;
    for ( int icolumn = 1; icolumn < cholesky.num_columns()  ; icolumn++ )
    {
      for ( int irow = 0; irow < icolumn; irow++)
        cholesky[irow][icolumn] = 0.0;
    }
    Hessian = cholesky * transpose(cholesky);
    std::cout << "Hessian:  " << Hessian << std::endl << std::endl;
  }


#endif

  // Part 2-3: Function, first and second-order derivatives supplied
  
  std::cout << std::endl << std::endl << 
    "Minimize 2-dimensional Paraboloid, Function, 1st- and 2nd-order Derivatives Supplied"
    << std::endl;
  
  BivarFunctionGradientAndHessian FunctionGradientAndHessian; // Create function object
  Uncmin<dvec, dmat, BivarFunctionGradientAndHessian> min_fgh(&FunctionGradientAndHessian); // create Uncmin object
 
  // Set typical magnitude of function values at the minimum
  min_fgh.SetScaleFunc(10);              // Off by an order of magnitude

  // Set typical magnitude of argument values at function minimum
  if (min_fgh.SetScaleArg(typ))
  {
    std::cout << std::endl << "Error in setting typical value" << std::endl;
    exit(0);
  }
  
  // Diagnostic printout piped to stdout
  min_fgh.SetPrint(fh, 1, 1);
  
  // Default convergence criteria are tightened up here by two orders of magnitude
  min_fgh.SetTolerances(1E-10, 1E-10);

  //Minimize the function
  for (int iMethod = 1; iMethod < 4; iMethod++)  //  Cycle through minimization methods
  {
    switch (iMethod)
    {
      case 1:  std::cout << std::endl << "Minimization by Line Search Method" << std::endl;
        break;
      case 2:  std::cout << std::endl << "Minimization by Double Dogleg Method" << std::endl;
        break;
      case 3:  std::cout << std::endl << "Minimization by More-Hebdon Method" << std::endl;
        break;
    }
    // Set Minimization method
    min_fgh.SetMethod(iMethod);
    
    // Initialize start values
    dvec start(start_values, start_values+dim);
    
    // Minimize function
    min_fgh.Minimize(start, xpls, fpls, gpls, hpls);
      
    // Find status of function minimization.
    // For a description of the values returned by
    // GetMessage see the documentation at the beginning
    // of Uncmin.h.
    int msg = min_fgh.GetMessage();
  
    std::cout << std::endl << std::endl << "Message returned from Uncmin: " << msg << std::endl;
    std::cout << std::endl << "Function minimum: " << std::endl << fpls << std::endl;
    std::cout << std::endl << "Parameters: " << xpls << std::endl;
    std::cout << "Gradient: " << gpls << std::endl;
    // Get lower triangle extract of hpls, with upper off-diagonal values set to zero.
    cholesky = hpls;
    for ( int icolumn = 1; icolumn < cholesky.num_columns()  ; icolumn++ )
    {
      for ( int irow = 0; irow < icolumn; irow++)
        cholesky[irow][icolumn] = 0.0;
    }
    Hessian = cholesky * transpose(cholesky);
    std::cout << "Hessian:  " << Hessian << std::endl << std::endl;
  }


  return 0;
}

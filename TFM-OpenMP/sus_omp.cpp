// --- Included Libraries ---
#include <sched.h>
#include <iostream>    
#include <cstdio>	
#include <string.h>     
#include <assert.h>     
#include <cfloat>
#include <errno.h>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <math.h>
#include <climits>
#include <omp.h>   
#include "sus.h"
#include "sus_papi.h"

// --- /Included Libraries ---
	
// -------------------------------------------------------------------------------------------------------------

// --- Program variables and constants ---	

using namespace std;

struct          miPapiEventosNativos localStructNativos;
struct          miPapiEventosPreset localStructPreset; 

#ifdef miPapi 
long long       PapiResults[MAX_CONTADORES_NATIVOS][5];
long long       PapiThreadResults[NUMEROMAXIMOHILOS][MAX_CONTADORES_NATIVOS][6];
long long       PapiThreadResultsTemp[NUMEROMAXIMOHILOS][3];
long long       values[1], valuesIntermedio[NUMEROMAXIMOHILOS][1];
long long       cyc, us, Time1, Time2, Time3, Time4, Time5, Evento1, Evento2, usTime1, usTime2, usTime3, timeIteracion, usIteracion, timeIteracionTotal, usIteracionTotal;
long long		arrayTime2[NUMEROMAXIMOHILOS];
char            eventName[PAPI_MAX_STR_LEN];
unsigned int    count_hilo[NUMEROMAXIMOHILOS][NUMEROMAXIMOHILOS];
long long  		resultados[1000];

long long tiemposTotales[MAX_CONTADORES_NATIVOS];
long long FP_INS[NUMEROMAXIMOHILOS];

long long L1_TCM[NUMEROMAXIMOHILOS], L2_TCM[NUMEROMAXIMOHILOS], L3_TCM[NUMEROMAXIMOHILOS];
long long LD_INS[NUMEROMAXIMOHILOS], SR_INS[NUMEROMAXIMOHILOS], L2_TCA[NUMEROMAXIMOHILOS], L3_TCA[NUMEROMAXIMOHILOS];



// PAPI counters enabled
// Native
int contadoresPapiNativosActivados[] = 
{
};

// Preset
int contadoresPapiPresetActivados[] = // contadores PAPI preset
{

		4, 5, 6, 23, 24, 25, 46, 47 
		
		// 4: PAPI_L1_TCM	L1 total cache misses
		// 5: PAPI_L2_TCM	L2 total cache misses
		// 6: PAPI_L2_TCM	L2 total cache misses
		// 22: PAPI_TOT_INS
		// 23: PAPI_FP_INS
		// 24: PAPI_LD_INS
		// 25: PAPI_SR_INS
		// 27: PAPI_RES_STL
		// 27: PAPI_TOT_CYC
		// 29: PAPI_LST_INS
		// 46: PAPI_L2_TCA
		// 47: PAPI_L3_TCA
		// 52: PAPI_FP_OPS
};
#endif

double CalidadMedia,CalidadMinima,CalidadMinimaAnterior=0;

char const *version = "1.0X";  // Program version
int MaxNumIter = 50;           // Max. number of iterations allowed
int NumIterSuavizado = 20;     // Number of smoothing iterations 
int NumColors;                 // Number of colors

const int MAX_NUM_IS = 200;
const int MAX_NUM_ELEM_IS = 2600000;
const int MAX_NUM_VECINOS = 50;
int IS[MAX_NUM_IS][MAX_NUM_ELEM_IS]; //Independent Set in rows
int IS_n[MAX_NUM_IS];     // Number of elements in the Independent Set

const int MAX_NODES = 5000000;
vector<int> NodosLibres(MAX_NODES);
vector<int>::iterator it;  
vector<int>::iterator fin;
int nNodosLibres = 0;

// Colouring method selection
#define NOCOLOR -1
#define MAXCOLORS 100

//1. Type B
#ifdef colorB  
	#undef MONTECARLO
	#define TIPOB
	#undef GEBREMEDHIM
	#undef SINCOLOR
#endif

//2. Montecarlo
#ifdef colorM  
	#define MONTECARLO
	#undef TIPOB
	#undef GEBREMEDHIM
	#undef SINCOLOR
#endif

//3. Gebremedhim
#ifdef colorG  
	#undef MONTECARLO
	#undef TIPOB
	#define GEBREMEDHIM
	#undef SINCOLOR
#endif

//4. No Colour
#ifdef SINcolor  
	#undef MONTECARLO
	#undef TIPOB
	#undef GEBREMEDHIM
	#define SINCOLOR
#endif

// Mesh
T_Malla LaMalla;   
// Mesh pointer
T_Malla *m;        

int FirstTime = 1;
double typical_size[3];
extern int flag_params;

int invalidElem = 1000; 
int iteraciones;
int numeroProcesadoresActuales, numeroHilosActuales, cpuActual, hiloActual; //, numeroTotalContadoresActivados;
int NumeroTotalNodos;

// CPU timers: iterations, colours and nodes
long long **tiempoPorNodo;
long long **tiempoPorColor;
long long **overheadPorColor;

bool Normalized = true;
bool SaveQuality = false;
char *fnodes = NULL; // Nodes file (input)
char *felems = NULL; // Element file (input)
char *fstat  = NULL; // Smooth statistics file (output)
char *fsmoot = NULL; // Smoothed mesh file (output). Only nodes are changed during smoothing
char *fquality = NULL; // File(s) quality prefix
int optchar;

// Create a file to save statistics
FILE *f_estadisticas = NULL;
double *lista;

// PAPI method varibles
#ifdef miPapi
int retval;
char errstring[PAPI_MAX_STR_LEN];

static int eventDummy[1];
int        eventset, iP, errorcode, numeroTiposContador, numeroContadores, contadoresAnalizados=0;
int        retvalHilos[NUMEROMAXIMOHILOS], eventsetHilos[NUMEROMAXIMOHILOS];
long long  lldumy, lldumyTotalCiclos, lldumyTotal_us, maxDumy;
unsigned long long  minDumy;
#endif

//Main method variables
int w, z, n, kkP;
int usdumy, usdumy2, ncolor, iter_suav;
int nodo;
double start, end, time1;
long long operacionesFP, operacionesFPantes;

// --- /Program variables and constants ---	

// -------------------------------------------------------------------------------------------------------------

// --- Program methods and functions definitions --- 

// sus_omp uses
void uso(void)
{
	cout << "sus (openMP)" << version << endl;
	cout << "Usage:" << endl;
	cout << "sus -n fnodes -e felems [-o fsmooth] [-s fstat] [-d] [-q qprefix] " 
			<< "[-i max_iter] [-m smooth_iter]" << endl;
	cout << "Mandatory arguments:" << endl;
	cout << "  fnodes  : file of nodes" << endl;
	cout << "  felems  : file of elements" << endl;
	cout << "Optional arguments" << endl;
	cout << "    fsmooth : file of smoothed nodes" << endl;
	cout << "     fstats : file of statistics" << endl;
	cout << "    qprefix : file name prefix for quality files" << endl;
	cout << "   max_iter : maximum number of iterations (default, " 
			<< MaxNumIter << ")" << endl;
	cout << "smooth_iter : number of smoothing iterations (default, " 
			<< NumIterSuavizado << ")" << endl;
	cout << "         -d : do not normalize input mesh" << endl;
}


// Set typical magnitude of parameter values (off by one in this example)
void CalculaMagnitudTipica(T_Malla *m);

// Initialize Independent Set
void Inicializa_IS(void);

// Fills the mesh's Vecinos using CarasVistas
void calculavecinos(T_Malla *Malla);

// Checks for correct execution of method calculavecinos
int CompruebaVecinos(T_Malla *Malla);

//
int CalculaValencia(T_Malla *Malla);

//
void AsignaAleatorios(T_Malla *Malla);

//
void AsignaAleatorios_old(T_Malla *Malla);

// Generate the Independent Set using the Monte Carlo method
int MonteCarloIS(T_Malla *Malla, int *IS);

// [Deprecated] Generate the Independent Set using the Monte Carlo method 
int MonteCarloIS_old(T_Malla *Malla, int *IS);

// Construct a MIS using Luby, returns the number of nodes in the MIS
int ConstruyeMIS (T_Malla *Malla, int *is, int NodosLibres);

// [Deprecated] Construct a MIS using Luby, returns the number of nodes in the MIS
int ConstruyeMIS_old (T_Malla *Malla, int *is, int NodosLibres);

// Generate enough MIS to encompass all the nodes in the mesh, returning the number of colours generated
int ConstruyeColoreado(T_Malla *Malla);

// [Deprecated] Generate enough MIS to encompass all the nodes in the mesh, returning the number of colours generated
int ConstruyeColoreado_old(T_Malla *Malla);

// Plassmann
// Cycles through the entire mesh and assigns n_waits to each node
void AsignaWait (T_Malla *Malla);

// Generate subsets of independent nodes, returning the number of elements
int GeneradorSet (T_Malla *Malla, int *IS);

// 
int ControlaCola (T_Malla *Malla, int *is);

// Generates the graph of necessary colours to encompass all the nodes, returning the number of colours
int ConstruyeGrafo (T_Malla *Malla);

// If colouring is correct, returns the number of colours generated
int CompruebaColoreado(T_Malla *Malla, int ncolores);

// [Deprecated] Generate the Independent Set
int ConstruyeIS_old (T_Malla *Malla);

// Gebremedhim-Manne colouring
// Initialize all colours
void InitColors(T_Malla *m);

// Return the color of neighbor neigh of node node
int GetNeighColor(T_Malla *m, int node, int neigh);

// Return the minimum color used by the neighbors of node or NOCOLOR if none is colored
int GetMinNeighColor(T_Malla *m, int node);

// Return the minimum available color not used by the neighbors
int GetMinAvalaibleColor(T_Malla *m, int node);

// Phase 1: pseudo-color
void Phase1(T_Malla *m);

// Phase 2: detect conflicts
// Return the number of conflicts 
// Output parameter "conflict_list": a pointer to a vector of conflicting nodes
int Phase2(T_Malla *m, int **conflict_list);

// Phase 3: Resolve conflicts (sequentially)
void Phase3(T_Malla *m, int *conflict_list, int nconflicts);

// Postprocess:
// Update data structures IS and IS_n
// Return the number of colors used to colorize the mesh
int PostProc(T_Malla *m);

// Gebremedhim-Manne coloring algorithm
// Return the number of colors
int GebremedhimManneColoring(T_Malla *m);

// Report Coloring summary: color, number of nodes
void ReportColoring(int ncolors);

// Reports the color of each node
void ReportColors(T_Malla *m);

// Indicates if the PAPI counter is enabled
int contadorPapiActivado(int tipoContador, int ordenContador); 

// Main segment methods
// Allocated memory for timers
void TimeMalloc();

// Ininiatilizes the mesh
void InitMesh();

// Colours the mesh
void ColourMesh();

// Starts PAPI segment
void StartPAPI();

// Parses options selected
void ParseOptions(int argc, char **argv);

// Initializes Statistics segment
void InitStats();

// Process a PAPI counter
void ProcessPAPICounter(int kkP);

// Process a node
int ProcessNode(int w, int z);

// Processes all nodes in a colour
int ProcessMesh(int z);

// Record untaglement time statistics and reset timers
void ReportOutputAndReset();

// Record timer statistics
void RecordTimerStats();

// Print PAPI data analysis
void PrintPAPIAnalysis();

// Print PAPI statistics
void PrintPAPIStats();

// Print PAPI results
void PrintPAPIResults();

// --- /Program methods and functions definitions --- 

// -------------------------------------------------------------------------------------------------------------

// --- MAIN ---

int main(int argc, char **argv)
{	
	ParseOptions(argc, argv);

	TimeMalloc();
	
#ifdef miPapi 
	StartPAPI();

	// Iterate through all the PAPI counter types (Native and Preset)
	for(numeroTiposContador = 0; numeroTiposContador < 2; numeroTiposContador++)
	{
		if (numeroTiposContador == 0) numeroContadores = localStructNativos.nNativeEvents;
		else numeroContadores = localStructPreset.nNativeEvents;

		// Reset counters
		for(iP = 0; iP < numeroHilosActuales; iP++){  
			for(int j=0; j< numeroProcesadoresActuales; j++) count_hilo[iP][j] = 0; 
		}
		
		contadoresAnalizados = 0; 
		
		// Repeat the untangling process, iterating through all PAPI counters
		for(kkP = 0; kkP < numeroContadores; kkP++ ) 
		{		

	int contActivado = contadorPapiActivado(numeroTiposContador, kkP); 
			
			// Process the counter if enabled
			if (contActivado)      
			{
				ProcessPAPICounter(kkP);
				
#endif 
			// --- Start Main Iteration ---
				
				InitMesh();

				ColourMesh();		

				InitStats();

				// Repeat the untangling procedure until the end condition is satisfied, or the maximum number of iterations is reached
				for (iteraciones = 1; iteraciones <= MaxNumIter; iteraciones++) { 
				
					int MeshIterationResult= ProcessMesh(iteraciones);
					
					if (MeshIterationResult == 0) break;
					
				} 

				// --- End Main Iteration ---
				
				ReportOutputAndReset();	
			
#ifdef miPapi						
				
				PrintPAPIAnalysis();
					
			} 			
		}

		PrintPAPIStats();
				
	} 
		
	PrintPAPIResults();

#endif 

	return 0;
}
// --- /MAIN ---


// - METHODS ---

// Set typical magnitude of parameter values (off by one in this example)
void CalculaMagnitudTipica(T_Malla *m)
{
	FirstTime = 0;
	// Typical magnitude is obtained as the mean absolute value
	typical_size[0] = typical_size[1] = typical_size[2] = 0.0;
	for (int i=0; i < m->Num_nodos; i++)
	{
		typical_size[0] += fabs(m->nodo[i].x);
		typical_size[1] += fabs(m->nodo[i].y);
		typical_size[2] += fabs(m->nodo[i].z);
	}
	typical_size[0] /= m->Num_nodos;
	typical_size[1] /= m->Num_nodos;
	typical_size[2] /= m->Num_nodos;

} // CalculaMagnitudTipica


//-----------------------------------------------------------------------------------------------------------------------------

void Inicializa_IS(void)
{

	for (int i=0; i < MAX_NUM_IS; i++)
	{
		for (int j=0; j < MAX_NUM_ELEM_IS; j++) IS[i][j] = -999;   // Almacenamiento por filas de los Independent Set
		IS_n[i] = -999;     // Numero de elementos del IS (p.e.: IS_n[3] = 25 significa que el IS numero 3 tiene 25 nodos
	}
}

//-----------------------------------------------------------------------------------------------------------------------------

// calculavecinos
// Rellena el campo Vecinos de la malla, a partir de la informacion de CarasVistas
void calculavecinos(T_Malla *Malla)
{
	int auxtotal[150];
	int i, j, cont, k, d, valor, nvec;

	for (i = 0; i < Malla->Num_nodos; i++)
	{
		Malla->nodo[i].Vecinos = new (std::nothrow) int [MAX_NUM_VECINOS];
		if (Malla->nodo[i].Vecinos == NULL)
		{
			perror("calculavecinos");
			exit(EXIT_FAILURE);
		}
		cont = 0;
		for (j = 0; j < Malla->nodo[i].NCarasVistas; j++)
		{
			for(k = 0; k < 3; k++)
			{
				auxtotal[cont] = Malla->nodo[i].CarasVistas[k][j];
				cont = cont + 1;
			}
		}

		sort (auxtotal, auxtotal + cont);
		valor = auxtotal[0];
		Malla->nodo[i].Vecinos[0] = valor;
		nvec = 1;

		for (d = 1; d < cont; d++)
		{
			if (auxtotal[d] != valor)
			{
				Malla->nodo[i].Vecinos[nvec] = auxtotal[d];
				nvec = nvec + 1;
				valor = auxtotal[d];
			}
		}
		Malla->nodo[i].NVecinos = nvec;
	}
} // calculavecinos



//-----------------------------------------------------------------------------------------------------------------------------


// CompruebaVecinos
// 
// Comprueba que calculavecinos funciona correctamente
// Devuelve 0 si hay un error en el calculo de los vecinos
//          1 en caso contrario
//
int CompruebaVecinos(T_Malla *Malla)
{
	int salida = 1;
	bool aux;

	// Comprueba que todos los vecinos aparecen en CarasVistas
	for (int i=0; i<Malla->Num_nodos; i++)
	{
		assert(Malla->nodo[i].NVecinos > 2);
		for (int j=0; j<Malla->nodo[i].NVecinos; j++)
		{
			int vecino = Malla->nodo[i].Vecinos[j];
			aux = false;
			for (int idx=0; (idx<Malla->nodo[i].NCarasVistas) && !aux; idx++) 
			{
				for (int k=0; k<3; k++)
				{
					if ( Malla->nodo[i].CarasVistas[k][idx] == vecino )
					{
						aux = true;
						break;
					}
				}
			}
			if (aux == false)
			{
				salida = 0;
				cout << "CompruebaVecinos: ERROR: el nodo " << vecino << " dice ser vecino del " 
						<< i << " pero no aparece en CarasVistas" << endl;
			}
		}
	}


	// Comprueba que todos los nodos que figuran en CarasVistas de uno
	// dado tambien estan en los vecinos
	for (int i=0; i<Malla->Num_nodos; i++)
	{
		for (int j=0; j<Malla->nodo[i].NCarasVistas; j++)
		{
			int nodocara, nodovecino;
			for (int k=0; k<3; k++)
			{
				nodocara = Malla->nodo[i].CarasVistas[k][j];
				aux = false;
				for (int idx=0; idx<Malla->nodo[i].NVecinos; idx++)
				{
					nodovecino = Malla->nodo[i].Vecinos[idx];
					if ( nodocara == nodovecino)
					{
						aux = true;
						break;
					}
				}
				if (aux == false)
				{
					salida = 0;
					cout << "CompruebaVecinos: ERROR: el nodo " << nodocara
							<< " que esta en CarasVistas de " << i 
							<< " no aparece entre los vecinos de " << i << endl;
				}
			}
		}
	}

	return(salida);
} //CompruebaVecinos


//-----------------------------------------------------------------------------------------------------------------------------

int CalculaValencia(T_Malla *Malla)
{
	// Obtiene la valencia de la malla: el numero maximo de conexiones 
	int v=0;
	for (int i=0; i<Malla->Num_nodos; i++)
	{
		if (Malla->nodo[i].NVecinos > v)
			v = Malla->nodo[i].NVecinos;
	}

	return(v);  

} // CalculaValencia


//-----------------------------------------------------------------------------------------------------------------------------


void AsignaAleatorios(T_Malla *Malla)
{
	// Asigna nuevos numero aleatorios a los nodos de la malla que aun
	// no pertenecen a ningun IS

	/*
#if defined ompBase || defined ompDynamic
#pragma omp parallel
  {
    struct drand48_data buffer; 
    srand48_r(int(time(NULL)) ^ omp_get_thread_num(), &buffer);

    #pragma omp for
    for (int i=0; i < Malla->Num_nodos; i++)
      {
	if (!Malla->nodo[i].colored)
	  {
	    lrand48_r(&buffer, &Malla->nodo[i].a);
	  } 
      }
  }
#else
	 */
	// Nos aseguramos que la asignacion de numeros aleatorios
	// sea determinista
	for (int i=0; i < Malla->Num_nodos; i++)
		if (!Malla->nodo[i].colored)
			Malla->nodo[i].a = lrand48();
	//#endif

} // AsignaAleatorios

//-----------------------------------------------------------------------------------------------------------------------------

void AsignaAleatorios_old(T_Malla *Malla)
{
	// Asigna nuevos numero aleatorios a los nodos de la malla que aun
	// no pertenecen a ningun IS
	#pragma omp parallel for

	for (int i=0; i < Malla->Num_nodos; i++)
		Malla->nodo[i].a = lrand48();

} //AsignaAleatorios_old


//-----------------------------------------------------------------------------------------------------------------------------

// Construye un IS con el metodo de Monte Carlo
//
// Para evitar usar una sección crítica, se crea un vector de tantos vectores
// como hilos, de forma que cada hilo tiene un vector donde ir insertando los
// nodos que forman parte del IS. Al finalizar, de forma secuencial, se crea
// un solo vector que contenga todos los nodos.
// Se han hecho algunos experimentos (no exhaustivos) que indican que esto
// es ligeramente más rápido que el uso de una sección crítica y un vector
// común a todos los hilos.
// El código que realiza lo expuesto arriba está delimitado por la macro VECTOR.
// Si esta no se define, el código corresponde a la versión que incluye un
// vector compartido por todos los hilos y una sección crítica.
int MonteCarloIS(T_Malla *Malla, int *IS)
{
	int n = 0;  // Numero de nodos del IS
	int nthreads;

#if defined ompBase || defined ompDynamic
	// (coloreado paralelo) #pragma omp parallel
	{
		nthreads=omp_get_num_threads();
	}
	vector <vector<int> > elegidos(nthreads);
#endif

	AsignaAleatorios(Malla);

#if defined ompBase || defined ompDynamic
	// (coloreado paralelo) #pragma omp parallel shared(elegidos)
	{
		int idx = omp_get_thread_num();
		// (coloreado paralelo) #pragma omp for
#endif
		for (int i=0; i < Malla->Num_nodos; i++)
		{
			if (Malla->nodo[i].ok) continue; // El nodo i ya pertenece a un IS
			if (Malla->nodo[i].Adj) continue; // El nodo i es vecino de alguno del IS
			long int ai = Malla->nodo[i].a;
			long int biggest = ai;
			for (int j=0; j < Malla->nodo[i].NVecinos; j++)
			{
				int vecino = Malla->nodo[i].Vecinos[j];
				// Descarto los vecinos que ya tienen color asignado o a los
				// adyacentes de un IS
				if (Malla->nodo[vecino].colored) continue;
				if (Malla->nodo[vecino].Adj) continue;
				long int avecino = Malla->nodo[vecino].a;
				assert (avecino != ai);
				if (avecino > biggest) 
				{
					biggest = avecino; 
					break;
				}
			}
			// Si el nodo actual (i) tiene el mayor de los números
			// aleatorios (considerando el suyo y el de todos sus
			// vecinos), entonces hacemos que pertenezca al IS en curso
			if (biggest == ai)
			{
				// i pertenece al IS
				Malla->nodo[i].ok = true;
#if defined ompBase || defined ompDynamic
				elegidos[idx].push_back(i);
#else
				IS[n] = i;
				n++ ;
#endif
			}
		}
#if defined ompBase || defined ompDynamic
	}
#endif

#if defined ompBase || defined ompDynamic
	// Se almacenan todos los nodos recopilados por cada hilo
	n=0;
	for (size_t i=0; i<elegidos.size(); i++)
	{
		for (size_t j=0; j<elegidos[i].size(); j++)
		{
			IS[n] = elegidos[i][j];
			n++ ;
		}
	}
#endif
	return(n);
} // MonteCarloIS


//-----------------------------------------------------------------------------------------------------------------------------

// Construye un IS con el metodo de Monte Carlo
int MonteCarloIS_old(T_Malla *Malla, int *IS)
{
	int j;
	int n = 0;  // Numero de nodos del IS

	AsignaAleatorios(Malla);
	//#pragma omp parallel for 
	for (int i=0; i < Malla->Num_nodos; i++)
		//XXX for (it = NodosLibres.begin(); it != fin; it++)
	{
		//XXX   int i=*it;
		if (Malla->nodo[i].ok) continue; // El nodo i ya pertenece a un IS
		if (Malla->nodo[i].Adj) continue; // El nodo i es vecino de alguno del IS
		long int ai = Malla->nodo[i].a;
		// for (j=0 ; j < Malla->nodo[i].NCarasVistas; j++)
		//  {
		//  for (int k=0; k<3; k++)
		for (j=0; j < Malla->nodo[i].NVecinos; j++)
		{
			//	      int vecino = Malla->nodo[i].CarasVistas[k][j];
			int vecino = Malla->nodo[i].Vecinos[j];
			// Descarto los vecinos que ya pertenecen a un IS o a los
			// adyacentes de un IS
			//XXX if (Malla->nodo[vecino].ok) continue;
			if (Malla->nodo[vecino].Adj) continue;
			long int avecino = Malla->nodo[vecino].a;
			assert (avecino != ai);
			if (avecino > ai)
			{
				j = Malla->Num_nodos; // Para forzar la salida del bucle j
				break;
			}
		}
		// Si algun vecino tiene mayor numero aleatorio que el de i, i
		// ya no puede pertenecer al IS y pasamos al siguiente nodo
		if (j != Malla->nodo[i].NVecinos) continue;
		//#pragma omp critical
		{
			// i pertenece al IS
			Malla->nodo[i].ok = true;
			//	Malla->nodo[i].a = (long int) -1;
			IS[n] = i;
			n++ ;
		}
	}

	return(n);
} // MonteCarloIS_old


//-----------------------------------------------------------------------------------------------------------------------------

// Construir un MIS, segun Luby
// Devuelve el numero de nodos del MIS
// NodosLibres es el numero de nodos que aun no pertenecen a ningun IS
int ConstruyeMIS (T_Malla *Malla, int *is, int NodosLibres)
{
	int i;
	int numeltos = 0;
	int Proc = NodosLibres;

	while (Proc > 0)
	{
		// Obtiene un independent set
		int neltos = MonteCarloIS(Malla, is+numeltos);
		// OJO: compruebo que no me paso de memoria
		assert (neltos <= MAX_NUM_ELEM_IS);

		// Marca los eltos del IS para sacarlos de G' (solo contandolos)
		Proc = Proc - neltos;

		// Marca los vecinos del IS para sacarlos de G'(solo contandolos)
		int cont = 0;
		for (i=0; i < neltos; i++)
		{
			int nodo = *(is+numeltos+i);
			//	  for (j=0; j < Malla->nodo[nodo].NCarasVistas; j++)
			//  {
			//    for (int k=0; k<3; k++)
			for (int j=0; j < Malla->nodo[nodo].NVecinos; j++)
			{
				//	  int vecino = Malla->nodo[nodo].CarasVistas[k][j];
				int vecino = Malla->nodo[nodo].Vecinos[j];
				if (Malla->nodo[vecino].Adj == false)
				{
					cont++ ;
					Malla->nodo[vecino].Adj = true;
				}
			}
			//}
		}
		Proc = Proc - cont;
		numeltos = numeltos + neltos;
	}
	return(numeltos);
} // ConstruyeMIS

//-----------------------------------------------------------------------------------------------------------------------------

// Construir un MIS, segun Luby
// Devuelve el numero de nodos del MIS
// NodosLibres es el numero de nodos que aun no pertenecen a ningun IS
int ConstruyeMIS_old (T_Malla *Malla, int *is, int NodosLibres)
{
	int i;
	int numeltos = 0;
	int Proc = NodosLibres;

	while (Proc > 0)
	{
		// Obtiene un independent set
		int neltos = MonteCarloIS(Malla, is+numeltos);

		// OJO: compruebo que no me paso de memoria
		assert (neltos <= MAX_NUM_ELEM_IS);

		// Marca los eltos del IS para sacarlos de G' (solo contandolos)
		Proc = Proc - neltos;

		// Marca los vecinos del IS para sacarlos de G'(solo contandolos)
		int cont = 0;
		for (i=0; i < neltos; i++)
		{
			int nodo = *(is+numeltos+i);
			//	  for (j=0; j < Malla->nodo[nodo].NCarasVistas; j++)
			//  {
			//    for (int k=0; k<3; k++)
			for (int j=0; j < Malla->nodo[nodo].NVecinos; j++)
			{
				//	  int vecino = Malla->nodo[nodo].CarasVistas[k][j];
				int vecino = Malla->nodo[nodo].Vecinos[j];
				if (Malla->nodo[vecino].Adj == false)
				{
					cont++ ;
					Malla->nodo[vecino].Adj = true;
				}
			}
			//}
		}
		Proc = Proc - cont;
		numeltos = numeltos + neltos;
	}

	return(numeltos);
} // ConstruyeMIS_old

//-----------------------------------------------------------------------------------------------------------------------------

// Construye todos los MIS necesarios para que todos los nodos de la
// malla pertenezcan a uno de ellos (una especie de coloreado no
// optimo de la malla).
// Devuelve el numero de colores
int ConstruyeColoreado_old(T_Malla *Malla)
{
	int ncolores = 0;
	int nnodos = Malla->Num_nodos;
	vector<int> auxnodos(MAX_NODES);

	// Inicializa la lista de nodos libres (aquellos que aun no
	// pertenecen a ningun IS)
	for (int i=0; i<nnodos; i++) NodosLibres[i] = i;
	nNodosLibres = nnodos;
	fin = NodosLibres.end();

#ifdef DEBUG
	cout << "DEBUG: estamos en ConstruyeColoreado, nnodos: " << nnodos << endl;
#endif

	while (nnodos > 0)
	{
		int n = ConstruyeMIS(Malla, IS[ncolores], nnodos);
		IS_n[ncolores] = n;

#ifdef DEBUG
		cout << "DEBUG: dentro while, ncolores: " << ncolores << ", n: " << n << ", nnodos: " << nnodos << endl;
#endif

		// Actualiza el conjunto de nodos libres
		sort (IS[ncolores], IS[ncolores]+n);
		auxnodos = NodosLibres;
		sort (auxnodos.begin(), auxnodos.begin() + nNodosLibres);
		fin = set_difference(auxnodos.begin(), auxnodos.begin() + nNodosLibres,
				IS[ncolores], IS[ncolores]+n, NodosLibres.begin());

		// Actualiza el numero de nodos que quedan por colorear
		nnodos = nnodos - n;
		nNodosLibres = nnodos;
#ifdef DEBUG
		cout << "DEBUG: encontrado MIS " << ncolores << " con "
				<< n << " nodos. Quedan por colorear " << nnodos << endl;
#endif
		/*
	cout << "DEBUG: NodosLibres: " << endl;
	for (it = NodosLibres.begin(); it != fin; it++)
	cout << *it << " " ;
	cout << endl;
		 */

		// Recorre la malla quitando las Adj, para que pueda construirse
		// un nuevo MIS
		/*
	for (int i=0; i < Malla->Num_nodos; i++)
	{
	Malla->nodo[i].Adj = false;
	}
		 */
		for (int i=0; i < n; i++)
		{
			int nodo = IS[ncolores][i];
			//	  for (int j=0; j< Malla->nodo[nodo].NCarasVistas; j++)
			//  for (int k=0; k<3; k++)
			for (int j=0; j < Malla->nodo[nodo].NVecinos; j++) 
			{
				//	int vecino = Malla->nodo[nodo].CarasVistas[k][j];
				int vecino = Malla->nodo[nodo].Vecinos[j];
				Malla->nodo[vecino].Adj = false;
			}
		}

		ncolores++ ;
	}
	return(ncolores);
} //ConstruyeColoreado_old

//-----------------------------------------------------------------------------------------------------------------------------

// Construye todos los MIS necesarios para que todos los nodos de la
// malla pertenezcan a uno de ellos (una especie de coloreado no
// optimo de la malla).
// Devuelve el numero de colores
int ConstruyeColoreado(T_Malla *Malla)
{
	int ncolores = 0;
	int nnodos = Malla->Num_nodos;

	// Inicializa el numero de nodos libres
	nNodosLibres = nnodos;

	while (nnodos > 0)
	{
		int n = ConstruyeMIS(Malla, IS[ncolores], nnodos);
		IS_n[ncolores] = n;

		// Actualiza el numero de nodos que quedan por colorear
		nnodos = nnodos - n;
		nNodosLibres = nnodos;
#ifdef DEBUG
		cout << "DEBUG: encontrado MIS " << ncolores << " con "
				<< n << " nodos. Quedan por colorear " << nnodos << endl;
#endif

		// Recorre los nodos del MIS para marcarlos como definitivamente
		// coloreados (colored=true) y quitando las Adj, para que pueda construirse
		// un nuevo MIS 
		for (int i=0; i < n; i++)
		{
			int nodo = IS[ncolores][i];
			Malla->nodo[nodo].colored = true;
			for (int j=0; j < Malla->nodo[nodo].NVecinos; j++) 
			{
				int vecino = Malla->nodo[nodo].Vecinos[j];
				Malla->nodo[vecino].Adj = false;
			}
		}

		ncolores++ ;
	}
	return(ncolores);
} //ConstruyeColoreado


//-----------------------------------------------------------------------------------------------------------------------------

// Plassmann

// AsignaWait
//
// Recorre toda la malla y asigna el numero de esperas (n_wait) a cada nodo
//
void AsignaWait (T_Malla *Malla)
{
	for (int i=0; i<Malla->Num_nodos; i++)
	{ 
		int al = Malla->nodo[i].a;
		for (int j=0; j<Malla->nodo[i].NVecinos; j++)	
		{
			int v = Malla->nodo[i].Vecinos[j];
			int alv = Malla->nodo[v].a;	
			if (alv > al)
			{
				Malla->nodo[i].n_wait = Malla->nodo[i].n_wait + 1; 
			} 
		} 	
	}

}
//AsignaWait

//-----------------------------------------------------------------------------------------------------------------------------

//GeneradorSet
//
//Construye cada conjunto de nodos independientes entre si (n_wait < 1) y 
//devuelve el numero de elementos de ese conjunto
//
int GeneradorSet (T_Malla *Malla, int *IS)
{
	int n = 0;
	for (int a = 0; a < Malla->Num_nodos; a++)
	{
		if (Malla->nodo[a].ok)  continue;
		if (Malla->nodo[a].n_wait < 1) 
		{
			Malla->nodo[a].ok = true;
			IS[n] = a;
			n++;
		}    
	}
	return (n);
}
//GeneradorSet

//-----------------------------------------------------------------------------------------------------------------------------

//ControlaCola
//
//Controla el paso del nodo que debe ser procesado restando uno a la espera
//de cada vecino de los nodos ya pertenecientes a un conjunto y devuelve el
//numero de elementos del conjunto
int ControlaCola (T_Malla *Malla, int *is)
{
	int set = GeneradorSet (Malla, is);
	for (int i = 0; i < set; i++)
	{
		int nodo = *(is + i);
		for (int j=0; j < Malla->nodo[nodo].NVecinos; j++)
		{
			int vecino = Malla->nodo[nodo].Vecinos[j];
			Malla->nodo[vecino].n_wait = Malla->nodo[vecino].n_wait - 1;
		}
	}	
	return(set);
}
//ControlaCola

//-----------------------------------------------------------------------------------------------------------------------------

//ConstruyeGrafo (T_Malla *Malla)
//
//Construye el grafo con todos los colores necesarios para coger todos los nodos
//de la malla y devuelve el numero de colores
int ConstruyeGrafo (T_Malla *Malla)
{
	int ncolores = 0;
	int nnodos = Malla->Num_nodos;
	//  vector<int> auxnodos(MAX_NODES);

	// Inicializa la lista de nodos libres (aquellos que aun no
	// pertenecen a ningun IS)
	for (int i=0; i<nnodos; i++) NodosLibres[i] = i;
	nNodosLibres = nnodos;
	fin = NodosLibres.end();

	// Eduardo
	AsignaAleatorios(Malla);

	AsignaWait(Malla);

	while (nnodos > 0)
	{
		int n = ControlaCola(Malla, IS[ncolores]);
		IS_n[ncolores] = n;

		/*
      // Actualiza el conjunto de nodos libres
      sort (IS[ncolores], IS[ncolores]+n);
      auxnodos = NodosLibres;
      sort (auxnodos.begin(), auxnodos.begin() + nNodosLibres);
      fin = set_difference(auxnodos.begin(), auxnodos.begin() + nNodosLibres,
			   IS[ncolores], IS[ncolores]+n, NodosLibres.begin());
		 */

		// Actualiza el numero de nodos que quedan por colorear
		nnodos = nnodos - n;
		// nNodosLibres = nnodos;

		//cout << "DEBUG Tipo-B: encontrado MIS " << ncolores << " con "
		//   << n << " nodos. Quedan por colorear " << nnodos << endl;

		ncolores++ ;
	}
	return(ncolores);

}
//ConstruyeGrafo

//-----------------------------------------------------------------------------------------------------------------------------

// CompruebaColoreado
//
// Devuelve el numero de colores si el coloreado es correcto
// o -1 si no lo es
int CompruebaColoreado(T_Malla *Malla, int ncolores)
{
	//  int ncolores =  ConstruyeColoreado(Malla);

#ifndef GEBREMEDHIM
	// Inicializa los colores de los nodos
	for (int i=0; i<Malla->Num_nodos; i++) Malla->nodo[i].color = -1;

	// Asigna colores a los nodos
	for (int color=0; color<ncolores; color++)
	{
		for (int k=0; k<IS_n[color]; k++)
		{
			int nodo = IS[color][k];
			if (Malla->nodo[nodo].color > -1)
			{
				cout << "CompruebaColoreado: ERROR: El nodo " << nodo << " tiene mas de un color "
						<< color << " (" << Malla->nodo[nodo].color << ")" << endl;
				ncolores = -1;
			}
			Malla->nodo[nodo].color = color;
		}
	}
#endif

	// Comprueba que ninguno de los vecinos de un nodo tiene el mismo
	// color que el
	for (int i=0; i<Malla->Num_nodos; i++)
	{
		int color = Malla->nodo[i].color;
		if (color == -1)
		{
			cout << "CompruebaColoreado: ERROR: El nodo " << i << " no ha sido coloreado (color "
					<< color << ")" << endl;
		} 
		for (int j=0; j<Malla->nodo[i].NCarasVistas; j++)
		{
			for (int k=0; k<3; k++)
			{
				int vecino = Malla->nodo[i].CarasVistas[k][j];
				int colorVecino = Malla->nodo[vecino].color;
				if (color == colorVecino)
				{
					// ERROR: un nodo y su vecino tienen el mismo color
					cout << "CompruebaColoreado: ERROR: El nodo " << i << " y su vecino "
							<< vecino << " tienen el mismo color (" << color << ")" << endl;
					ncolores = -1;
				}
			}
		}
	}

	return(ncolores);
} //CompruebaColoreado


//-----------------------------------------------------------------------------------------------------------------------------


// Construir los Is
int ConstruyeIS_old (T_Malla *Malla)
{
	const int NUM=10000;  
	int Proc,x,y,i,j,k,cont,ii,kk,paso,d;
	bool salir;
	int aux[NUM];  /* Almacenamiento no repetido de los nodos adyacentes*/
	bool encontrado;

	Proc = Malla->Num_nodos;
	x = -1;
	while (Proc > 0)
	{
		int neltos = 0; // numero de elementos del independent set
		x = x+1;
		y = -1; 
		cont = -1;
		do
		{
			encontrado = false;
			paso = y;
			for (i=0; i < Malla->Num_nodos; i++)
			{
				if ((Malla->nodo[i].ok == false) && (Malla->nodo[i].Adj == false))
				{
					// Comprueba todos los nodos j que pertenecen a la
					// estrella del nodo i
					j = 0;
					salir = false;

					while ((j < Malla->nodo[i].NCarasVistas) && (salir == false))
					{
						k=0;
						while ((k <= 2) && (salir == false))
						{
							int vecino = Malla->nodo[i].CarasVistas[k][j];
							if (Malla->nodo[vecino].Adj == false)
							{   
								if (Malla->nodo[i].a < Malla->nodo[vecino].a)
								{
									salir = true;    
								}
							}
							k = k+1;     
						}
						j = j+1;     
					}

					if (salir == false)
					{
						encontrado = true;
						neltos++ ;
						y = y+1;
						IS[x][y] = i;
						Malla->nodo[i].ok = true; 
						Proc = Proc-1;
						for (ii=0; ii < Malla->nodo[i].NCarasVistas; ii++)
						{
							for (kk=0; kk < 3; kk++)
							{
								int vecino = Malla->nodo[i].CarasVistas[kk][ii];
								if (Malla->nodo[vecino].Adj == false)
								{  
									Malla->nodo[vecino].Adj = true;
									cont = cont+1;
									aux[cont] = vecino;
								} 
							}    
						}

					}       
				}       
			}
			//	} while((y-paso)>0);   // Se ha terminado de crear un IS
		} while(encontrado);   // Se ha terminado de crear un IS
		cout << "DEBUG: Creado el IS " << x << " con eltos " << neltos << endl;
		cout << "DEBUG: Proc = " << Proc << endl;
		for (d=0; d <= cont; d++)
		{
			Malla->nodo[aux[d]].Adj = false;    
		} 
	} 
	return(x);             
}

//-----------------------------------------------------------------------------------------------------------------------------

// ******************************* Coloreado Gebremedhim-Manne ******
// Inicializa todos colores a un color no permitido
void InitColors(T_Malla *m)
{
	for (int i=0; i < m->Num_nodos; i++) m->nodo[i].color = NOCOLOR;
}

//-----------------------------------------------------------------------------------------------------------------------------

// Return the color of neighbor neigh of node node
int GetNeighColor(T_Malla *m, int node, int neigh)
{
	int n = m->nodo[node].Vecinos[neigh]; 
	return(m->nodo[n].color);
}

//-----------------------------------------------------------------------------------------------------------------------------


// Return the minimum color used by the neighbors of node or NOCOLOR if none is colored
int GetMinNeighColor(T_Malla *m, int node)
{
	int mincol = INT_MAX;
	for (int i=0; i<m->nodo[node].NVecinos; i++)
	{
		int c = GetNeighColor(m, node, i);
		if ( (c > NOCOLOR) && (c < mincol) )	
			mincol = c;
	}
	if (mincol == INT_MAX) return(NOCOLOR);
	return(mincol);
}

//-----------------------------------------------------------------------------------------------------------------------------

// Return the minimum available color not used by the neighbors
int GetMinAvalaibleColor(T_Malla *m, int node)
{
	bool UsedColors[MAXCOLORS] = {false};

	for (int i=0; i<m->nodo[node].NVecinos; i++)
	{
		int c = GetNeighColor(m, node, i);
		if (c == NOCOLOR) continue;
		UsedColors[c] = true;
	}

	for (int i=0; i < MAXCOLORS; i++)
		if (UsedColors[i] == false) return(i);

	// if no neighbor has a valid color, first non used color is 0
	return(0);
} // GetMinAvalaibleColor

//-----------------------------------------------------------------------------------------------------------------------------

// Phase 1: pseudo-color
void Phase1(T_Malla *m)
{
	//printf("Coloreado-Phase1\n");

	InitColors(m);

#if defined ompBase || defined ompDynamic /* OMP-Base Begin */
	printf("Coloreado-Phase1\n");
	//#pragma omp parallel for
#endif /* OMP-Base End */
	for (int i=0; i < m->Num_nodos; i++)
	{
		int newcolor = GetMinAvalaibleColor(m, i);
		m->nodo[i].color = newcolor;
	}
}

//-----------------------------------------------------------------------------------------------------------------------------


// Phase 2: detect conflicts
// Return the number of conflicts 
// Output parameter "conflict_list": a pointer to a vector of conflicting nodes
int Phase2(T_Malla *m, int **conflict_list)
{
	printf("Coloreado-Phase2\n");

	int *conflict = new int[m->Num_nodos / 5];
	*conflict_list = conflict;
	int idx = 0;
	int nconflicts = 0;
#if defined ompBase || defined ompDynamic /* OMP-Base Begin */
	//#pragma omp parallel shared(nconflicts, conflict, idx)
	{
		//#pragma omp for 
#endif /* OMP-Base End */
		for (int i=0; i < m->Num_nodos; i++)
		{
			int mycolor = m->nodo[i].color;
			for (int j=0; j< m->nodo[i].NVecinos; j++)
			{
				int neighbor = m->nodo[i].Vecinos[j];
				int neighcolor = m->nodo[neighbor].color;
				if (mycolor == neighcolor)
				{
#if defined ompBase || defined ompDynamic /* OMP-Base Begin */
					//#pragma omp critical
#endif /* OMP-Base End */
					cout << "nodos " << i << " " << neighbor 
							<< " tienen mismo color " 
							<< mycolor << " " << neighcolor << endl;


					if (i < neighbor) 
					{
#if defined ompBase || defined ompDynamic /* OMP-Base Begin */
						//#pragma omp critical
						{
#endif /* OMP-Base End */
							conflict[idx] = i;
							idx++ ;
							nconflicts++ ;
#if defined ompBase || defined ompDynamic /* OMP-Base Begin */
						}
#endif /* OMP-Base End */
					}
				}
			}
		}
#if defined ompBase || defined ompDynamic /* OMP-Base Begin */
	} 
#endif /* OMP-Base End */

	return(nconflicts);
}

//-----------------------------------------------------------------------------------------------------------------------------


// Phase 3: Resolve conflicts (sequentially)
void Phase3(T_Malla *m, int *conflict_list, int nconflicts)
{
	printf("Coloreado-Phase3\n");

#if defined ompBase || defined ompDynamic /* OMP-Base Begin */
	//#pragma omp parallel for
#endif /* OMP-Base End */
	for (int i=0; i<nconflicts; i++)
	{
		int node = conflict_list[i];
		int newcolor = GetMinAvalaibleColor(m, node);
		m->nodo[node].color = newcolor;
	}
	// free the memory used by the list of conflicting nodes
	delete [] conflict_list;
}

//-----------------------------------------------------------------------------------------------------------------------------


// Postprocess:
// Update data structures IS and IS_n
// Return the number of colors used to colorize the mesh
int PostProc(T_Malla *m)
{
	int ncolor = 0;

	for (int i=0; i<m->Num_nodos; i++)
	{
		int color = m->nodo[i].color;
		ncolor = (color > ncolor) ? color : ncolor;
		int indice = IS_n[color];
		IS[color][indice] = i;
		IS_n[color]++ ;
	}

	return(ncolor+1);
}


//-----------------------------------------------------------------------------------------------------------------------------

// Gebremedhim-Manne coloring algorithm
// Return the number of colors
int GebremedhimManneColoring(T_Malla *m)
{
	// Domingo: se inicializa el vector de numero nodos/color
	for (int jj=0; jj<MAX_NUM_IS; jj++) IS_n[jj]=0;

	Phase1(m);
	int *conflict_list;
	int nconflicts = Phase2(m, &conflict_list);
	cout << "numero conflictos: " << nconflicts << endl;

	//  for (int i=0; i < m->Num_nodos; i++) cout << i << " " << m->nodo[i].color << endl;
#ifdef DEBUG
	for (int i=0; i<nconflicts; i++)
		cout << "conflictivo: " << conflict_list[i] << endl;
#endif

	Phase3(m, conflict_list, nconflicts);
	int ncolor = PostProc(m);
	return(ncolor);
}
// ******************************* Fin Coloreado Gebremedhim-Manne ******

//-----------------------------------------------------------------------------------------------------------------------------

// Report Coloring summary: color, number of nodes
void ReportColoring(int ncolors)
{
	long long unsigned dumy = 0, dumy2 = 0;
	cout << "color   # of nodes" << endl;
	for (int i=0; i<ncolors; i++) {
		cout <<  i << "     " << IS_n[i] << endl;
		dumy += IS_n[i];
	}
	dumy /= ncolors;
	for (int i=0; i<ncolors; i++) {
		dumy2 += ((dumy - IS_n[i])*(dumy - IS_n[i]));
	}
	dumy2 /= (ncolors-1);
	dumy2 = (long long)sqrt((float)dumy2);
	printf("numeroDeColores: %i promedioNodosPorColor: %llu desviacionNodosPorColor: %llu\n", ncolors, dumy, dumy2);
}

//-----------------------------------------------------------------------------------------------------------------------------

// Reports the color of each node
void ReportColors(T_Malla *m)
{
	cout << "node  color" << endl;
	for (int i=0; i<m->Num_nodos; i++)
		cout << i << " " << m->nodo[i].color << endl;
}

//-----------------------------------------------------------------------------------------------------------------------------

int contadorPapiActivado(int tipoContador, int ordenContador) // indica si el número de contador PAPI está seleccionado para profiling
{
#ifdef miPapi 
	int contActivado = 0;
	int nContadores = 0;
	if(tipoContador == 0 && sizeof(contadoresPapiNativosActivados[0]) > 0) { // tipo Nativo
		nContadores = sizeof (contadoresPapiNativosActivados) / sizeof (contadoresPapiNativosActivados[0]);
	} 
	else if(tipoContador == 1 && sizeof(contadoresPapiPresetActivados[0]) > 0) { // tipo Preset
		nContadores = sizeof (contadoresPapiPresetActivados) / sizeof (contadoresPapiPresetActivados[0]);
	}
	for(int i=0; i<nContadores; i++) {
		if ( (tipoContador == 0 && contadoresPapiNativosActivados[i] == ordenContador) ||
				(tipoContador == 1 && contadoresPapiPresetActivados[i] == ordenContador) )  {
			contActivado = 1;       
			break;
		}
	}
	return contActivado;
#endif	
}

//-----------------------------------------------------------------------------------------------------------------------------

// --- Main segment methods ---

// MALLA -----------------------

void InitMesh(){	
	// Inicializa el generador de numeros aleatorios
	time_t semilla = (long int) 223456;  //OJO solo para depurar. Quitar al final
	srand48((long int)semilla);

	// Read input mesh
	n = LeerMalla(fnodes, felems, &LaMalla);
	printf("infoMem: despues leer la malla; ");
	infoMem(3);
	if (n < 1)
	{
		cerr << "Error reading the mesh" << endl;
		exit(EXIT_FAILURE);
	}

	/* PAPI ---	*/			
	#ifdef miPapi 
	usdumy  = PAPI_get_real_usec(  ) - us; // en uSegundos
	
	printf("[Papi] tiempo en LeerMalla (sec): %f \n", 1.0e-6*usdumy);
	#endif 
	 /* - */

	cout << "Mesh data:" << endl;
	cout << "   nodes   : " << LaMalla.Num_nodos <<  endl;
	cout << "   elements: " << LaMalla.Num_tetra <<  endl;
	NumeroTotalNodos = LaMalla.Num_nodos;

	///////////// Resetea los siguientes campos de los nodos de la malla:
	//////////      .- ok y Adj 
	for (int i=0; i<LaMalla.Num_nodos; i++)
	{
		LaMalla.nodo[i].ok =  LaMalla.nodo[i].Adj = false;
		LaMalla.nodo[i].color = -1;
		LaMalla.nodo[i].n_wait = 0;
		LaMalla.nodo[i].a = -1L;
	}

	calculavecinos(&LaMalla);
	cout << "   valencia: " << CalculaValencia(&LaMalla) << endl;

	#if defined ompBase || defined ompDynamic /* OMP-Base Begin */
		#pragma omp parallel
		#pragma omp master
		{
			cout << "   threads : " << omp_get_num_threads() << endl;
		}
	#else
		/* PAPI --- */
		#ifdef miPAPI
		cout << "   threads Secuenciales : " << numeroHilosActuales  << endl;
		#endif
		/* - */
	#endif
	
	// Comprobacion de los vecinos
	if (!CompruebaVecinos(&LaMalla))
	{
		cout << "Error. Se han calculado mal los vecinos. Abortado" << endl;
		exit(1);
	}

	#if defined ompBase || defined ompDynamic /* OMP-Base Begin */
		// Coloring time
		start = omp_get_wtime();
	#endif

	m = &LaMalla;

	for (int kk=0; kk<4; kk++) assert(LaMalla.tetra[23815].nodo[kk] < LaMalla.Num_tetra);

	/* PAPI --- */
	#ifdef miPapi 
	usdumy  = PAPI_get_real_usec(  ) - us; // en uSegundos
	printf("[Papi] tiempo hasta ANTES de Colorear incluyendo LeerMalla (sec): %f \n", 1.0e-6*usdumy);
	#endif 
	/* - */
}	


// 2. COLOREADO -----------------------
			
void ColourMesh(){
#ifdef MONTECARLO
	// Esto es para el primer coloreado

	// Domingo: se inicializa el vector de numero nodos/color
	for (int jj=0; jj<MAX_NUM_IS; jj++) {
		IS_n[jj]=0;
		for (int rr=0; rr<MAX_NUM_ELEM_IS; rr++) {
			IS[jj][rr]=0;
		}
	}

	for (int jj=0; jj<LaMalla.Num_nodos; jj++) NodosLibres[jj]=0;
	for (int i=0; i < LaMalla.Num_nodos; i++)
	{
		LaMalla.nodo[i].ok = false; //Domingo
		LaMalla.nodo[i].Adj = false; //Domingo
		LaMalla.nodo[i].colored = false; //Domingo
		//LaMalla.nodo[i].n_wait = 0; //Domingo
	}

	cout << "Algorithm: Montecarlo" << endl << std::flush;

	ncolor = ConstruyeColoreado(&LaMalla);
	NumColors = ncolor;
#endif

#ifdef TIPOB
	// Esto es para el 2º coloreado
	// Domingo: se inicializa el vector de numero nodos/color
	for (int jj=0; jj<MAX_NUM_IS; jj++) {
		IS_n[jj]=0;
	}

	for (int jj=0; jj < LaMalla.Num_nodos; jj++)
	{
		NodosLibres[jj]=0;
		LaMalla.nodo[jj].ok = 0; //Domingo
		LaMalla.nodo[jj].Adj = 0; //Domingo
		LaMalla.nodo[jj].n_wait = 0; //Domingo
		LaMalla.nodo[jj].colored = false; //Domingo
	}
	// Domingo
	cout << "Algorithm: B" << endl;
	ncolor = ConstruyeGrafo(&LaMalla);
	NumColors = ncolor;
#endif

#ifdef GEBREMEDHIM
	cout << "Algorithm: Gebremedhim" << endl;
	ncolor = GebremedhimManneColoring(m);
	NumColors = ncolor;
#endif

#ifdef SINCOLOR
	for (int jj=0; jj<MAX_NUM_IS; jj++) {
		IS_n[jj]=0;
		for (int rr=0; rr<MAX_NUM_ELEM_IS; rr++) {
			IS[jj][rr]=0;
		}
	}
	IS_n[0]=LaMalla.Num_nodos;
	for (int a = 0; a < LaMalla.Num_nodos; a++) IS[0][a]=a;
	ncolor = 1;
	NumColors = ncolor;
	cout << "Algorithm: SIN-COLOREADO" << endl;
#endif

/* PAPI ---*/
#ifdef miPapi 
	usdumy2  = PAPI_get_real_usec(  ) - us - usdumy; // en uSegundos
	printf("[Papi] tiempo en Colorear (sec): %f \n", 1.0e-6*usdumy2);
#endif 
/* - */

#if defined ompBase || defined ompDynamic // OMP-Base Begin
	// Final del tiempo de ejecucion
	end = omp_get_wtime();
	time1 = end - start;
	
	cout << "Coloring OMP time (sec): " << time1 << endl;
	cout << "colors: " << ncolor << endl;
#endif 

//Comprobacion del coloreado
#ifndef SINCOLOR
	if (CompruebaColoreado(&LaMalla, ncolor) > -1)
		cout << "Coloreado correcto" << endl << endl;
	else
	{
		cout << "Error en el coloreado. Abortado" << endl;
		exit(1);
	}
#endif /*End comprobacion coloreado*/

ReportColoring(ncolor);

printf("infoMem: despues coloreado; ");
infoMem(3);

/*PAPI ---*/
#ifdef miPapi 
	usdumy  = PAPI_get_real_usec(  ) - us; // en uSegundos
	printf("[Papi] tiempo hasta ANTES de untangling incluyendo LeerMalla+Coloreo+Comprobacion (sec): %f \n", 1.0e-6*usdumy);
#endif 
/* - */
}

			

// --- StartPAPI

	
void StartPAPI(){
#ifdef miPapi 
	#ifdef ompBase
		printf("[Papi] SUS Version OMP-Base \n");
	#else
		#ifdef ompDynamic
			printf("[Papi] SUS Version OMP-Dynamic \n");
			printf("infoMem: inicio programa y antes miPapiInicializacion; ");
			infoMem(3);
		#else
			printf("[Papi] SUS Version Secuencial \n");
			printf("infoMem: inicio programa y antes miPapiInicializacion; ");
			infoMem(3);
		#endif
	#endif

	miPapiInicializacion();
	printf("infoMem: despues miPapiInicializacion; ");
	infoMem(3);
		
		
	#if defined ompBase || defined ompDynamic
		#pragma omp parallel
		{
			numeroHilosActuales = omp_get_num_threads(); // numero maximo posible de hilos paralelos
		}
		omp_set_num_threads(numeroHilosActuales);
	
		hiloActual = omp_get_thread_num();
		
		
		retval = PAPI_thread_init ((unsigned long (*)(void)) (omp_get_thread_num));
		if (retval != PAPI_OK) {
			PAPI_perror((char *)"PAPI_thread_init");
			printf("%s:%d::PAPI_thread_init failed. %d %s\n",
					__FILE__,__LINE__,retval,errstring);
			exit(1);
		}
		
		
		numeroProcesadoresActuales = omp_get_num_procs();
		printf("[Papi] Hilos Paralelos OMP Activados: %i hiloActual: %i numero procesadores: %i\n", numeroHilosActuales, hiloActual, numeroProcesadoresActuales);
	#else
		numeroHilosActuales = 1; // numero maximo posible de hilos en version secuencial
		numeroProcesadoresActuales = 1;
		hiloActual = 0;
		cpuActual = 0;
		printf("[Papi] Version Secuencial Hilos Activados: %i hiloActual: %i \n", numeroHilosActuales, hiloActual);
	#endif
#endif	
}

// --- PrintPAPIAnalysis ---

	void PrintPAPIAnalysis(){
	#ifdef miPapi
	
		printf("[Papi] Termina U&S, se para tiempo PAPI, contadorPapi: %i, tiempo U&S(s): %f, iteracionesU&S: %i, Nodos: %i, Colores: %i \n\n", kkP, 1e-6*(us-usdumy), iteraciones, NumeroTotalNodos, NumColors);
		
		printf("\n\nAnalisis I\n");
	
		for (int i=1; i<=iteraciones; i++){
			long long minTiempoNodo=10000000000, maxTiempoNodo=0, promedioTiempoNodo=0;
			long long unsigned desviacionTiempoNodo=0;
			int indiceMaxTiempoNodo[3], indiceMinTiempoNodo[3];
			
			for (int k=0; k<NumeroTotalNodos; k++){
				if (tiempoPorNodo[i][k] > maxTiempoNodo) {
					maxTiempoNodo = tiempoPorNodo[i][k];
					indiceMaxTiempoNodo[0]=i; 
					indiceMaxTiempoNodo[1]=k; 
				}
				
				if (tiempoPorNodo[i][k] < minTiempoNodo && tiempoPorNodo[i][k] > 0) {
					minTiempoNodo = tiempoPorNodo[i][k];
					indiceMinTiempoNodo[0]=i; 
					indiceMinTiempoNodo[1]=k; 
				}
				
				promedioTiempoNodo += tiempoPorNodo[i][k];
			}
			
			promedioTiempoNodo /= NumeroTotalNodos;
			for (int k=0; k<NumeroTotalNodos; k++){
				long long ldumy = abs(promedioTiempoNodo - tiempoPorNodo[i][k]);
				ldumy = ldumy * ldumy;
				desviacionTiempoNodo += (long long unsigned)ldumy;
			}
			
			desviacionTiempoNodo /= NumeroTotalNodos;
			desviacionTiempoNodo = (long long) sqrt((float)desviacionTiempoNodo);
			
			printf("Iteracion: %i \n",i);
			printf("\tpromedioTiempoNodo = %10llu desviacionTiempoNodo = %llu \n", promedioTiempoNodo, desviacionTiempoNodo);
			printf("\tTiempoPorNodo MAX  = %10llu iteracionU&S: %i nodo: %i\n", maxTiempoNodo, indiceMaxTiempoNodo[0], indiceMaxTiempoNodo[1]);
			printf("\tTiempoPorNodo MIN  = %10llu iteracionU&S: %i nodo: %i\n", minTiempoNodo, indiceMinTiempoNodo[0], indiceMinTiempoNodo[1]);
		}

		printf("\n\nAnalisis II\n");
	
		for (int i=1; i<=iteraciones; i++){
			long long minTiempoColor=10000000000, maxTiempoColor=0, promedioTiempoColor=0, promedioTiempoNodo=0, promedioOverheadColor=0, promedioOverheadNodo=0;
			int indiceMaxTiempoColor[3], indiceMinTiempoColor[3];
		
			printf("Iteracion: %i \n",i);
			
			for (int k=0; k<NumColors; k++){
				printf("Color: %2i nodos: %5i tiempoPorColor: %9llu overheadPorColor(%4.1f%%): %8llu tiempoPorNodoEquivalente = %7llu overheadPorNodoEquivalente = %5llu \n",k, IS_n[k], tiempoPorColor[i][k], 100.0*((float)overheadPorColor[i][k]/(float)tiempoPorColor[i][k]), overheadPorColor[i][k], tiempoPorColor[i][k]/IS_n[k], overheadPorColor[i][k]/IS_n[k]);
				
				if (tiempoPorColor[i][k] > maxTiempoColor) {
					maxTiempoColor= tiempoPorColor[i][k];
					indiceMaxTiempoColor[0]=i; 
					indiceMaxTiempoColor[1]=k; 
				}
				
				if (tiempoPorColor[i][k] < minTiempoColor && tiempoPorColor[i][k] > 0) {
					minTiempoColor = tiempoPorColor[i][k];
					indiceMinTiempoColor[0]=i; 
					indiceMinTiempoColor[1]=k; 
				}
				
				promedioTiempoColor   += tiempoPorColor[i][k];
				promedioOverheadColor += overheadPorColor[i][k];
			}
			
			promedioTiempoNodo    =  promedioTiempoColor/NumeroTotalNodos;
			promedioOverheadNodo  =  promedioOverheadColor/NumeroTotalNodos;
			promedioTiempoColor   /= NumColors;
			promedioOverheadColor /= NumColors;
			float dPorc1 = 100.0*((float)promedioOverheadColor/(float)promedioTiempoColor);
			float dPorc2 = 100.0*((float)promedioOverheadNodo/(float)promedioTiempoNodo);
			
			printf("\tpromedioTiempoColor   = %10llu         promedioTiempoNodoEquivalente   = %llu\n", promedioTiempoColor, promedioTiempoNodo);
			printf("\tpromedioOverheadColor = %10llu (%4.1f%%) promedioOverheadNodoEquivalente = %llu (%4.1f%%)\n", promedioOverheadColor, dPorc1, promedioOverheadNodo, dPorc2);
			printf("\tTiempoPorColor MAX    = %10llu iteracionU&S: %i color: %i\n", maxTiempoColor, indiceMaxTiempoColor[0], indiceMaxTiempoColor[1]);
			printf("\tTiempoPorColor MIN    = %10llu iteracionU&S: %i color: %i\n", minTiempoColor, indiceMinTiempoColor[0], indiceMinTiempoColor[1]);
		}
		
		printf("\n\n");
		#endif	
	}


// --- PrintPAPIStats ---

void PrintPAPIStats(){
#ifdef miPapi

				char *sdumy;
			
				if ( contadoresAnalizados > 0) 
				{
					printf("\nResultados Totales uniendo todos los hilos --> ");
				
					for(int i = 0; i < 5; i++){
						lldumyTotalCiclos = 0;
						lldumyTotal_us = 0;
					
						switch (i) {
							case 0: printf("\nciclos:     \n"); 	sdumy=(char*)"ciclos";   break;
							case 1: printf("us:         \n"); 	sdumy=(char*)"us    ";   break;
							case 2: printf("medidaPapi: \n"); 	sdumy=(char*)"medida";   break;
							case 3: printf("clkSoloIteracion: \n"); sdumy=(char*)"clkIte";   break;
							case 4: printf("usSoloIteracion: \n"); 	sdumy=(char*)"usIter";   break;
						}
						
						for (int kk = 0;kk < numeroContadores; kk++) {
							int contActivado = contadorPapiActivado(numeroTiposContador, kk); // mira si el contador está seleccionado para profiling
							
							if (contActivado)       // contadores PAPI preset
							{
								if (numeroTiposContador == 0) PAPI_event_code_to_name(localStructNativos.eventID[kk],eventName);
								else PAPI_event_code_to_name(localStructPreset.eventID[kk],eventName);

								printf("\tTotal %6s Evento Tipo-%6s %3i: %50s %8llu \n", sdumy, numeroTiposContador==0?"NATIVO":"PRESET", kk, eventName, PapiResults[kk][i]);
								
								if     (i == 0) lldumyTotalCiclos += PapiResults[kk][0];
								else if(i == 1) lldumyTotal_us    += PapiResults[kk][1];
								else if(i == 3) lldumyTotalCiclos += PapiResults[kk][3];
								else if(i == 4) lldumyTotal_us    += PapiResults[kk][4];
							}
						}
						
						if(i == 0) printf("\n\t  Promedio Ciclos Totales Tipo-%6s= %llu\n",numeroTiposContador==0?"NATIVO":"PRESET", lldumyTotalCiclos/contadoresAnalizados);
						
						if(i == 0) printf("\n\t  Promedio Ciclos Totales Por Iteracion U&S Tipo-%6s= %llu\n",numeroTiposContador==0?"NATIVO":"PRESET", (lldumyTotalCiclos/contadoresAnalizados)/iteraciones);
						
						if(i == 0) printf("\n\t  Promedio Ciclos Totales Por Color Tipo-%6s= %llu\n",numeroTiposContador==0?"NATIVO":"PRESET", ((lldumyTotalCiclos/contadoresAnalizados)/iteraciones)/NumColors);
						
						if(i == 0) printf("\n\t  Promedio Ciclos Totales Por Nodo Tipo-%6s= %llu\n",numeroTiposContador==0?"NATIVO":"PRESET", ((lldumyTotalCiclos/contadoresAnalizados)/iteraciones)/NumeroTotalNodos);
						
						if(i == 1) printf("\n\t  Promedio uSegundos Totales= %llu\n", lldumyTotal_us/contadoresAnalizados);
						
						if(i == 3) printf("\n\t  Promedio ciclos Totales solo Iteraciones= %llu\n", lldumyTotalCiclos/contadoresAnalizados);
						
						if(i == 3) printf("\n\t  Promedio ciclos Totales solo Iteraciones Por Iteracion= %llu\n", (lldumyTotalCiclos/contadoresAnalizados)/iteraciones);
						
						if(i == 3) printf("\n\t  Promedio ciclos Totales solo Iteraciones Por Color= %llu\n", ((lldumyTotalCiclos/contadoresAnalizados)/iteraciones)/NumColors);
						
						if(i == 3) printf("\n\t  Promedio ciclos Totales solo Iteraciones Por Nodo= %llu\n", ((lldumyTotalCiclos/contadoresAnalizados)/iteraciones)/NumeroTotalNodos);
						
						if(i == 4) printf("\n\t  Promedio uSegundos Totales solo Iteraciones= %llu\n", lldumyTotal_us/contadoresAnalizados);
						
						printf("\n");
					}
				
					printf("\n\n");

					printf("VALORES de EVENTO por HILO - Dentro bucle: \t\t\t\t\t\t\t");			
					
					for(int i=0; i< numeroProcesadoresActuales; i++) printf("\tCPU-%i ",i);
						
					printf("\n");
					
					lldumyTotalCiclos = 0;
					lldumyTotal_us = 0;
					// --- TODO
					for (int kk=0; kk<numeroContadores; kk++) 
					{
						int contActivado = contadorPapiActivado(numeroTiposContador, kk); // mira si el contador está seleccionado para profiling
							
						if (contActivado)       // contadores PAPI preset
						{
							if (numeroTiposContador == 0) PAPI_event_code_to_name(localStructNativos.eventID[kk],eventName);
							else PAPI_event_code_to_name(localStructPreset.eventID[kk],eventName);
							
							printf("DentroBucle - Valor Evento Tipo-%6s %3i: %50s ", numeroTiposContador==0?"NATIVO":"PRESET", kk, eventName);
							
							lldumy = 0;
							maxDumy = 0;
							minDumy = LLONG_MAX; // (unsigned long long) 1e+19;
							int Nmedidas = 0;
							
							for(int i=11; i> (11-numeroProcesadoresActuales); i--){
								
								if(PapiThreadResults[i][kk][2] > 0) {
									Nmedidas++;
									
									printf("\t%llu ",PapiThreadResults[i][kk][2]);
									
									lldumy += PapiThreadResults[i][kk][2];
									
									if (PapiThreadResults[i][kk][2] > maxDumy) maxDumy = PapiThreadResults[i][kk][2];
									if (PapiThreadResults[i][kk][2] < minDumy) minDumy = PapiThreadResults[i][kk][2];
								}
							}

							if (Nmedidas > 0) resultados[kk] = lldumy/Nmedidas;
							else resultados[kk]=0; 	
							
							printf("\t  promedio= %llu", resultados[kk]);
							printf("\t  maximo= %llu", maxDumy);
							printf("\t  minimo= %llu", minDumy);
							
							float fdumy = 100.0*(float)(maxDumy-minDumy)/(float)resultados[kk];
							
							printf("\t  variacion= %f%%", fdumy);
							printf("\t  suma= %llu \n", lldumy);
						}
					}
					// ---
					
					printf("\n\n");
					printf("VALORES de EVENTO por HILO - Fuera bucle: \t\t\t\t\t\t\t");

					for(int i=0; i< numeroProcesadoresActuales; i++) printf("\tCPU-%i ",i);
					
					printf("\n");
						
					lldumyTotalCiclos = 0;
					lldumyTotal_us = 0;
				
					for (int kk=0; kk<numeroContadores; kk++) 
					{
						int contActivado = contadorPapiActivado(numeroTiposContador, kk); // mira si el contador está seleccionado para profiling
						
						if (contActivado)       // contadores PAPI preset
						{
							if (numeroTiposContador == 0) PAPI_event_code_to_name(localStructNativos.eventID[kk],eventName);
							else PAPI_event_code_to_name(localStructPreset.eventID[kk],eventName);
							
							printf("FueraBucle - Valor Evento Tipo-%6s %3i: %50s ", numeroTiposContador==0?"NATIVO":"PRESET", kk, eventName);
							
							lldumy = 0;
							maxDumy = 0;
							minDumy = LLONG_MAX; // (unsigned long long) 1e+19;

							int Nmedidas = 0;
							
							int num=0;	
							for(int i=11; i> (11-numeroProcesadoresActuales); i--){			
								if(PapiThreadResults[i][kk][5] > 0) {
									Nmedidas++;
									
									printf("\n %i - %i : %llu ",kk,num, PapiThreadResults[i][kk][5]);
									
									switch(kk){
										case 4:
											L1_TCM[num] = PapiThreadResults[i][kk][5];
											printf("\n %llu ", L1_TCM[num]);
										break;
										case 5:
											L2_TCM[num] = PapiThreadResults[i][kk][5];
										break;
										case 6:
											L3_TCM[num] = PapiThreadResults[i][kk][5];
										break;
										case 23:
											FP_INS[num] = PapiThreadResults[i][kk][5];
										break;
										case 24:
											LD_INS[num] = PapiThreadResults[i][kk][5];
										break;
										case 25:
											SR_INS[num] = PapiThreadResults[i][kk][5];
										break;
										case 46:
											L2_TCA[num] = PapiThreadResults[i][kk][5];
										break;
										case 47:
											L3_TCA[num] = PapiThreadResults[i][kk][5];
										break;
										
										default:
										break;
										
									}
									num++; 
									
									lldumy += PapiThreadResults[i][kk][5];
									
									if (PapiThreadResults[i][kk][5] > maxDumy) maxDumy = PapiThreadResults[i][kk][5];
									if (PapiThreadResults[i][kk][5] < minDumy) minDumy = PapiThreadResults[i][kk][5];
								}
							}
							
							if (Nmedidas > 0) resultados[kk] = lldumy/Nmedidas;
							else resultados[kk]=0; 	
							
							printf("\t  promedio= %llu", resultados[kk]);
							printf("\t  maximo= %llu", maxDumy);
							printf("\t  minimo= %llu", minDumy);
							
							float fdumy = 100.0*(float)(maxDumy-minDumy)/(float)resultados[kk];
							
							printf("\t  variacion= %f%%", fdumy);
							printf("\t  suma= %llu \n", lldumy);
						}
					}
				
					printf("\n");
					
					// --- TODO
					printf("CICLOS de EVENTO por HILO - dentro bucle: \t\t\t\t\t\t\t");
						
					for(int i=0; i< numeroProcesadoresActuales; i++) printf("\tCPU-%i ",i);
					
					printf("\n");
				
					for (int kk=0; kk<numeroContadores; kk++) 
					{
						int contActivado = contadorPapiActivado(numeroTiposContador, kk); // mira si el contador está seleccionado para profiling
						if (contActivado)       // contadores PAPI preset
						{
							if (numeroTiposContador == 0) PAPI_event_code_to_name(localStructNativos.eventID[kk],eventName);
							else PAPI_event_code_to_name(localStructPreset.eventID[kk],eventName);
							
							printf("DentroBucle - Ciclos Evento Tipo-%6s %3i: %50s ", numeroTiposContador==0?"NATIVO":"PRESET", kk, eventName);
							
							lldumy = 0;
							maxDumy = 0;
							minDumy = LLONG_MAX; // (unsigned long long) 1e+19;

							int Nmedidas = 0;
													
							for(int i=11; i> (11-numeroProcesadoresActuales); i--){
								if(PapiThreadResults[i][kk][0] > 0) {
									Nmedidas++;
									
									printf("\t%llu ",PapiThreadResults[i][kk][0]);
									
									lldumy += PapiThreadResults[i][kk][0];
									lldumyTotalCiclos += PapiThreadResults[i][kk][0];
									
									if (PapiThreadResults[i][kk][0] > maxDumy) maxDumy = PapiThreadResults[i][kk][0];
									if (PapiThreadResults[i][kk][0] < minDumy) minDumy = PapiThreadResults[i][kk][0];
								}
							}
							
							if(Nmedidas>0) printf("\t  promedio= %llu", lldumy/Nmedidas);
							else printf("\t  promedio= %f", 0.0);
							
							printf("\t  maximo= %llu", maxDumy);
							printf("\t  minimo= %llu", minDumy);
							
							float fdumy = 100.0*(float)(maxDumy-minDumy)/(float)(lldumy/numeroProcesadoresActuales);
							
							printf("\t  variacion= %f%% \n", fdumy);
						}
					}
					// ---
					
					// --- TODO
					printf("\n");
					printf("uSEGUNDOS de EVENTO por HILO - dentro bucle: \t\t\t\t\t\t\t");
					
					for(int i=0; i< numeroProcesadoresActuales; i++) printf("\tCPU-%i ",i);
					
					printf("\n");
				
					for (int kk=0; kk<numeroContadores; kk++) {
						int contActivado = contadorPapiActivado(numeroTiposContador, kk); // mira si el contador está seleccionado para profiling
						
						if (contActivado)       // contadores PAPI preset
						{
							if (numeroTiposContador == 0) PAPI_event_code_to_name(localStructNativos.eventID[kk],eventName);
							else PAPI_event_code_to_name(localStructPreset.eventID[kk],eventName);
							
							printf("DentroBucle - uSegundos Evento Tipo-%6s %3i: %50s ", numeroTiposContador==0?"NATIVO":"PRESET", kk, eventName);
							
							lldumy = 0;
							maxDumy = 0;
							minDumy = LLONG_MAX; // (unsigned long long) 1e+19;

							int Nmedidas = 0;
							
							for(int i=11; i> (11-numeroProcesadoresActuales); i--)
							{
								if(PapiThreadResults[i][kk][1] > 0) {
									Nmedidas++;
									
									printf("\t%llu ",PapiThreadResults[i][kk][1]);
									
									lldumy += PapiThreadResults[i][kk][1];
									lldumyTotal_us += PapiThreadResults[i][kk][1];
									
									if (PapiThreadResults[i][kk][1] > maxDumy) maxDumy = PapiThreadResults[i][kk][1];
									if (PapiThreadResults[i][kk][1] < minDumy) minDumy = PapiThreadResults[i][kk][1];
								}
							}

							if(Nmedidas>0) printf("\t  promedio= %llu", lldumy/Nmedidas);
							else printf("\t  promedio= %f", 0.0);

							float fdumy = 100.0*(float)(maxDumy-minDumy)/(float)(lldumy/numeroProcesadoresActuales);
							
							printf("\t  maximo= %llu", maxDumy);
							printf("\t  minimo= %llu", minDumy);
							printf("\t  variacion= %f%% \n", fdumy);
						}
					}
					// ---
					printf("\n");

				} /* if contadoresAnalizados > 0 */
				#endif	
			}

// --- PrintPAPIResults ---

	void PrintPAPIResults(){	
	#ifdef miPapi
		printf("ContadoresAnalizados= %i\n\n", contadoresAnalizados);
		printf("NumeroColores= %i\n\n", NumColors);
		printf("Iteraciones desenredado+suavizado= %i\n\n", iteraciones);
		printf("Calidad Minima= %e\n\n", CalidadMinima);
		printf("Calidad Media= %e\n\n", CalidadMedia);
	
	//////////////	printf("LST: %f\n\n",(float)resultados[29]);
		printf("L1 ACCESSES = %f%%\n\n", (float)resultados[24]+resultados[25]);
		printf("L1 MISS RATE = %f%%\n\n", 100.0*resultados[4]/(resultados[24]+resultados[25]));
	
		printf("L2 ACCESSES = %f\n\n", (float)resultados[46]);
		printf("L2 MISS RATE = %f%%\n\n", 100.0*resultados[5]/resultados[46]);
	
		printf("L3 ACCESSES = %f\n\n", (float)resultados[47]);
		printf("L3 MISS RATE = %f%%\n\n", 100.0*resultados[6]/resultados[47]);
/*	
		printf("Memory Bandwith (L1 cache) = %f\n\n", 2400.0*((resultados[4])*64)/resultados[28]);
		printf("Memory Bandwith (L2 cache) = %f\n\n", 2400.0*((resultados[5])*64)/resultados[28]);
		printf("Memory Bandwith (L3 cache) = %f\n\n", 2400.0*((resultados[6])*64)/resultados[28]);
	
		printf("IPC= %f\n\n", (float) resultados[22]/resultados[28]);
	
		printf("STALLs= %f%%\n\n", 100.0*(resultados[27]/resultados[28]));
	
		printf("MFLOPS/procesador= %f\n\n",(float)((2400.0*resultados[23])/resultados[28]));
		printf("MFLOPS total= %f\n\n",(float)numeroHilosActuales*((2400.0*resultados[23])/resultados[28]));
*/		
	
printf("\n\n Tiempos:");
		long long tiempoAvg=0;
		for(int i=0; i<contadoresAnalizados; i++){
	//		printf("\n Iteracion %i : %llu", i, tiemposTotales[i]);
			tiempoAvg += tiemposTotales[i];
		}
tiempoAvg= tiempoAvg/contadoresAnalizados;		
printf("\n tiempoAvg : %llu uS ", tiempoAvg);
	
printf("\n\n FP_INS:");
		long long FP_INS_total=0;
		for(int i=0; i<numeroHilosActuales; i++){
	//		printf("\n Hilo %i : %llu", i, FP_INS[i]);
			FP_INS_total += FP_INS[i];
		}
		printf("\n FP_INS_total : %llu", FP_INS_total);
		printf("\n MFLOPs: %f", (float)(FP_INS_total)/tiempoAvg);

		printf("\n\n L1 Miss Rate:");
		float L1_MISS_MIN=LLONG_MAX; ;
		float L1_MISS_MAX=0;
		float L1_MISS_total=0;
		float L1_MISS_AVG=0;
		float B_total=0;
		for(int i=0; i<numeroHilosActuales; i++){
		//	printf("\n %llu", L1_TCM[i]);
		//	printf("\n %llu", LD_INS[i]);
		//	printf("\n %llu", SR_INS[i]);
			float A= 100*L1_TCM[i];
			float B= LD_INS[i]+SR_INS[i];
			B_total+= B;
			float L1_MISS_tmp= (float) A/B;
		//	printf("\n Hilo %i : %f", i, L1_MISS_tmp);

			L1_MISS_total+= (L1_MISS_tmp*B);
			if(L1_MISS_tmp > L1_MISS_MAX) L1_MISS_MAX= L1_MISS_tmp;
			if(L1_MISS_tmp < L1_MISS_MIN) L1_MISS_MIN= L1_MISS_tmp;

		}
		L1_MISS_AVG= (L1_MISS_total/B_total);
		
		printf("\n L1_MISS_AVG : %f %%",(float) L1_MISS_AVG);
		printf("\n L1_MISS_MIN : %f %%",(float) L1_MISS_MIN);
		printf("\n L1_MISS_MAX : %f %%",(float) L1_MISS_MAX);
		

		printf("\n\n L2 Miss Rate:");
                float L2_MISS_MIN=LLONG_MAX; ;
                float L2_MISS_MAX=0;
                float L2_MISS_total=0;
                float L2_MISS_AVG=0;
                B_total=0;
                for(int i=0; i<numeroHilosActuales; i++){
                //        printf("\n %llu", L2_TCM[i]);
                //        printf("\n %llu", L2_TCA[i]);
                        float A= 100*L2_TCM[i];
                        float B= L2_TCA[i];
                        B_total+= B;
                        float L2_MISS_tmp= (float) A/B;
                     //   printf("\n Hilo %i : %f", i, L2_MISS_tmp);

                        L2_MISS_total+= (L2_MISS_tmp*B);
                        if(L2_MISS_tmp > L2_MISS_MAX) L2_MISS_MAX= L2_MISS_tmp;
                        if(L2_MISS_tmp < L2_MISS_MIN) L2_MISS_MIN= L2_MISS_tmp;

                }
                L2_MISS_AVG= (L2_MISS_total/B_total);

                printf("\n L2_MISS_AVG : %f %%",(float) L2_MISS_AVG);
                printf("\n L2_MISS_MIN : %f %%",(float) L2_MISS_MIN);
                printf("\n L2_MISS_MAX : %f %%",(float) L2_MISS_MAX);


		printf("\n\n L3 Miss Rate:");
                float L3_MISS_MIN=LLONG_MAX; ;
                float L3_MISS_MAX=0;
                float L3_MISS_total=0;
                float L3_MISS_AVG=0;
                B_total=0;
                for(int i=0; i<numeroHilosActuales; i++){
                //        printf("\n %llu", L3_TCM[i]);
                 //       printf("\n %llu", L3_TCA[i]);
                        float A= 100*L3_TCM[i];
                        float B= L3_TCA[i];
                        B_total+= B;
                        float L3_MISS_tmp= (float) A/B;
                   //     printf("\n Hilo %i : %f", i, L3_MISS_tmp);

                        L3_MISS_total+= (L3_MISS_tmp*B);
                        if(L3_MISS_tmp > L3_MISS_MAX) L3_MISS_MAX= L3_MISS_tmp;
                        if(L3_MISS_tmp < L3_MISS_MIN) L3_MISS_MIN= L3_MISS_tmp;

                }
                L3_MISS_AVG= (L3_MISS_total/B_total);

                printf("\n L3_MISS_AVG : %f %%",(float) L3_MISS_AVG);
                printf("\n L3_MISS_MIN : %f %%",(float) L3_MISS_MIN);
                printf("\n L3_MISS_MAX : %f %%",(float) L3_MISS_MAX);



	/*	
		printf("\n\n L2 Miss Rate:");
		long long L2_MISS_MIN=LLONG_MAX; ;
		long long L2_MISS_MAX=0;
		long long L2_MISS_total=0;
		long long L2_MISS_AVG=0;
		for(int i=0; i<numeroHilosActuales; i++){
			long long L2_MISS_tmp= 100*(L2_TCM[i]/L2_TCA[i]);
			printf("\n Hilo %i : %llu", i, L2_MISS_tmp);
			
			L2_MISS_total+= L2_MISS_tmp;
			if(L2_MISS_tmp > L2_MISS_MAX) L2_MISS_MAX= L2_MISS_tmp;
			if(L2_MISS_tmp < L2_MISS_MIN) L2_MISS_MIN= L2_MISS_tmp;
		}
		L2_MISS_AVG= (L2_MISS_total/numeroHilosActuales);
		
		printf("\n L2_MISS_AVG : %llu %%",L2_MISS_AVG);
		printf("\n L2_MISS_MIN : %llu %%",L2_MISS_MIN);
		printf("\n L2_MISS_MAX : %llu %%",L2_MISS_MAX);
		
		
		printf("\n\n L3 Miss Rate:");
		long long L3_MISS_MIN=LLONG_MAX; ;
		long long L3_MISS_MAX=0;
		long long L3_MISS_total=0;
		long long L3_MISS_AVG=0;
		for(int i=0; i<numeroHilosActuales; i++){
			long long L3_MISS_tmp= 100*(L3_TCM[i]/L3_TCA[i]);
			printf("\n Hilo %i : %llu", i, L3_MISS_tmp);
			
			L3_MISS_total+= L3_MISS_tmp;
			if(L3_MISS_tmp > L3_MISS_MAX) L3_MISS_MAX= L3_MISS_tmp;
			if(L3_MISS_tmp < L3_MISS_MIN) L3_MISS_MIN= L3_MISS_tmp;
		}
		L3_MISS_AVG= (L3_MISS_total/numeroHilosActuales);
		
		printf("\n L3_MISS_AVG : %llu %%",L3_MISS_AVG);
		printf("\n L3_MISS_MIN : %llu %%",L3_MISS_MIN);
		printf("\n L3_MISS_MAX : %llu %%",L3_MISS_MAX);
*/
#endif	
	}
	
// --- InitStats ---

void InitStats(){	
				#if defined ompBase || defined ompDynamic /* OMP-Base Begin */
					// Optimization timing
					start = omp_get_wtime();
				#endif

				// Mesh normalization
				if (Normalized) NormalizarLaMalla(&LaMalla);

				for (int kk=0; kk<4; kk++) assert(LaMalla.tetra[23815].nodo[kk] < LaMalla.Num_tetra);

				/* Memoria para guardar las calidades de los tetraedros */
				lista = (double *)malloc(LaMalla.Num_tetra * sizeof(double));

				if (lista == NULL)
				{
					fprintf(stderr, "Error: Can't allocate memory for quality list\n");
					exit(EXIT_FAILURE);
				}

				if (fstat != NULL)
				{ 
					if ((f_estadisticas = fopen(fstat, "w")) == NULL)
					{
						cout << "Can't create statistics file " << fstat << endl;
					}
					else
					{
						// Statistics file header
						fprintf(f_estadisticas, "     Min.         Median       Max.      # Invalid \n");
						fprintf(f_estadisticas, "#it  Quality      Quality      Quality   Elements  \n");
						fprintf(f_estadisticas, "-----------------------------------------------------\n");
						fprintf(f_estadisticas, "%03d ", 0);
					}
				}

				// Print statistics file header
				fprintf(stdout, "\n             Mesh quality statistics\n");
				fprintf(stdout, "     Min.         Median       Max.      # Invalid \n");
				fprintf(stdout, "#it  Quality      Quality      Quality   Elements \t    tiempo(us)    Ciclos    FPops (MFLOPs)  Incremento Q min  ciclosCalculoCalidad (%%)\n");
				fprintf(stdout, "-----------------------------------------------------------------------------------------------------------------------------------------\n");

				////
				for (int kk=0; kk<4; kk++) assert(LaMalla.tetra[23815].nodo[kk] < LaMalla.Num_tetra);

				// calcula las calidades de los tetraedros 
				n = CalculaCalidades(&LaMalla, lista, 0.0);

				////
				for (int kk=0; kk<4; kk++) assert(LaMalla.tetra[23815].nodo[kk] < LaMalla.Num_tetra);

				// Save mesh quality 
				if (fquality != NULL) GrabaFicheroCalidades(lista, &LaMalla, LaMalla.Num_tetra, fquality);

				// Save statistics
				if (f_estadisticas != NULL) GrabaEstadisticas(f_estadisticas, &LaMalla, lista, LaMalla.Num_tetra, 0); // modificado por Domingo

				// Print statistics
				GrabaEstadisticas(stdout, &LaMalla, lista, LaMalla.Num_tetra, 0); // modificado por Domingo
				printf("\n");

				for (int kk=0; kk<4; kk++) assert(LaMalla.tetra[23815].nodo[kk] < LaMalla.Num_tetra);

				CalculaMagnitudTipica(m); // Obtiene la magnitud tipica de la malla (necesario para optimizar)
			
				/* PAPI ---*/
				#ifdef miPapi
					for(int i=0; i< numeroHilosActuales; i++)
					{ 
						for(int j=0; j< numeroProcesadoresActuales; j++) count_hilo[i][j] = 0; //se cuenta el numero de numeros primos procesados por hilos
					}
					timeIteracionTotal = 0;
					usIteracionTotal = 0;
				#endif
				/* - */

				iter_suav = -1;            // Contador de iteraciones de suavizado
				printf("infoMem: empieza U&S; ");
				infoMem(3);
				
				
				operacionesFP, operacionesFPantes = 0;
				
			#ifdef miPapi
				printf("[Papi] Reset y Empieza de nuevo a contar el tiempo Papi real -cyc, us-\n");
				cyc = PAPI_get_real_cyc(  ); // se toma la marca de tiempo global en ciclos
				us  = PAPI_get_real_usec( ); // en uSegundos
			#endif
			}
			
// --- 

void RecordTimerStats(){
	
}			

	
// --- 

void ReportOutputAndReset(){
	#ifdef miPapi /* PAPI Begin */
		#if defined ompBase || defined ompDynamic /* OMP-Base Begin */
			#pragma omp parallel for private (iP)
		#endif

		for(iP=0; iP < numeroHilosActuales; iP++) { /* se leen los contadores por hilo y por evento Papi */
			retvalHilos[iP] = PAPI_stop( eventsetHilos[iP] , valuesIntermedio[iP]);
			if(retvalHilos[iP]!=PAPI_OK) printf("Evento %i (%s), Error inicializacion PAPI_stop, hilo: %i\n",kkP,eventName,iP);
		}
		
		cyc = PAPI_get_real_cyc(  )- cyc; // tiempo global en ciclos
		us  = PAPI_get_real_usec( )- us;  // tiempo global en useg
		printf("\n -tiempo total: %llu s \n", us);
		PapiResults[kkP][0] = cyc;
		PapiResults[kkP][1] = us;
		PapiResults[kkP][3] = timeIteracionTotal;
		PapiResults[kkP][4] = usIteracionTotal;

		//--T
tiemposTotales[contadoresAnalizados-1]= us;



		
		for(iP=0; iP<numeroHilosActuales; iP++) PapiResults[kkP][2]+= valuesIntermedio[iP][0];
		
		#if defined ompBase || defined ompDynamic /* OMP-Base Begin */
			printf("\n\nhiloMaestroActual = %i, cpuActual = %i\n", omp_get_thread_num(), sched_getcpu());
		#endif
			
			printf("\nNodos procesados por CPU0 ... CPU%i: \n", numeroProcesadoresActuales-1);
	
			int nodosPorCPU[numeroProcesadoresActuales];
	
			for(int j = 0; j < numeroProcesadoresActuales; j++) nodosPorCPU[j]=0;
	
			for(int i = 0; i < numeroHilosActuales; i++) {
				printf("nodosHilo-%i: ",i); 
				
				int total = 0;
				
				for(int j = 0; j < numeroProcesadoresActuales; j++) {
					printf("\t %i",count_hilo[i][j]); 
					total += count_hilo[i][j]; 
					nodosPorCPU[j] += count_hilo[i][j]; 
				}
				
				printf("\t total nodos: %i\n", total);
			}
			
			printf("nodosCPU: ");
	
			for(int j = 0; j < numeroProcesadoresActuales; j++) printf("\t%i", nodosPorCPU[j]);
	
			printf("\n\n\n");
	#endif /* PAPI End */

	// Mesh denormalization
	if (Normalized) DesnormalizarLaMalla(m);

	#if defined ompBase || defined ompDynamic /* OMP-Base Begin */		
		// End of optimization timing
		end = omp_get_wtime();
		time1 = end - start;

		cout << "Optimization OMP time (sec): " << time1 << endl;
		
	#endif

	if (invalidElem) cout << "Error: mesh is still tangled" << endl;
	else cout << "Success: untangled mesh " << endl;

	// Save untangled mesh to a file
	if (fsmoot != NULL) GrabaFicheroSalida(fsmoot, m);

	if (f_estadisticas != NULL) fclose(f_estadisticas);

	free(lista);
	// Libera la memoria reservada en calculavecinos()
	
	for (int i=0; i < m->Num_nodos; i++) delete [] m->nodo[i].Vecinos;
	
	// Libera la memoria usada por la malla
	LiberarMalla(m);
}
				
// ---

void TimeMalloc(){
	tiempoPorNodo    = (long long **) malloc((MaxNumIter+1)*sizeof(long long *));
	tiempoPorColor   = (long long **) malloc((MaxNumIter+1)*sizeof(long long *));
	overheadPorColor = (long long **) malloc((MaxNumIter+1)*sizeof(long long *));
	
	for(int i=0;i<=MaxNumIter+1;i++) {
		tiempoPorNodo[i]   =(long long *)malloc(MAX_NODES * sizeof(long long));
		tiempoPorColor[i]  =(long long *)malloc(MAXCOLORS * sizeof(long long));
		overheadPorColor[i]=(long long *)malloc(MAXCOLORS * sizeof(long long));
	}
}

// ---

int ProcessNode(int w, int z){
	nodo = IS[z][w];

	if (m->nodo[nodo].nr != 0) return 0;

	int IndiceFactorUmbral = 0;
	
	while ( (IndiceFactorUmbral < DIMENSION_VECTOR_UMBRAL) && (OptimizeNode(m, nodo, IndiceFactorUmbral) > 0) ) {
		IndiceFactorUmbral++ ;

		#if defined ompBase || defined ompDynamic 
		{
			fflush(NULL);
			printf("thread id %d: Reintentado con nodo %d (factor %d)\n", omp_get_thread_num(), nodo, IndiceFactorUmbral);
			fflush(NULL);
		}
		#else
			fflush(NULL);
			printf("Reintentado con nodo %d (factor %d)\n", nodo, IndiceFactorUmbral);
			fflush(NULL);
		#endif
	}

	#ifdef miPapi 
		retvalHilos[hiloActual] = PAPI_read( eventsetHilos[hiloActual] ,  valuesIntermedio[hiloActual]);
		
		if (retvalHilos[hiloActual] != PAPI_OK) printf("[Papi] Fin iteracion - Evento %i (%s), Error inicializacion PAPI_read, hilo: %i\n", kkP, eventName, hiloActual);
	
		Evento2 = valuesIntermedio[hiloActual][0]  - Evento1;
		Time2   = PAPI_get_real_cyc(  )  - Time1;
		usTime2 = PAPI_get_real_usec(  ) - usTime1;
			
		PapiThreadResults[cpuActual][kkP][0] += Time2;
		PapiThreadResults[cpuActual][kkP][1] += usTime2;
		PapiThreadResults[cpuActual][kkP][2] += Evento2;
		tiempoPorNodo[iteraciones][nodo] = Time2;
						arrayTime2[cpuActual] += Time2;
	#endif	
	
	return 1;
}	


// --- process

int ProcessMesh(int iteraciones)
{
	flag_params = 1;

#ifdef miPapi
	Time3   = PAPI_get_real_cyc(  );
	usTime3 = PAPI_get_real_usec(  );
#endif

	// Iterate through all the colours generated						
	for (z = 0; z < ncolor; z++) 
	{ 
	
#ifdef miPapi 
	#if defined ompBase || defined ompDynamic 
		#pragma omp parallel for shared(PapiThreadResultsTemp) private(hiloActual,cpuActual,w)
			
		for (w = 0; w < numeroHilosActuales; w++) {
			hiloActual = omp_get_thread_num();
			cpuActual = sched_getcpu();
	#endif
		
			valuesIntermedio[hiloActual][0] = 0;
			retvalHilos[hiloActual] = PAPI_read( eventsetHilos[hiloActual] , valuesIntermedio[hiloActual]);
									
			if (retvalHilos[hiloActual] != PAPI_OK) printf("[Papi] Evento %i (%s), Error inicializacion PAPI_read, hilo: %i\n", w, eventName, hiloActual);
				
			PapiThreadResultsTemp[cpuActual][0] = PAPI_get_real_cyc(  ); // Time1
			PapiThreadResultsTemp[cpuActual][1] = PAPI_get_real_usec(  ); // usTime1
			PapiThreadResultsTemp[cpuActual][2] = valuesIntermedio[hiloActual][0]; // Event1 
			
	#if defined ompBase || defined ompDynamic 
		}
	#endif
			
		for(int k=0; k<numeroHilosActuales; k++) arrayTime2[k] = 0; // registra el tiempo total por hilo/cpu dentro de cada iteracion U&S
		
	#ifdef miPapi							
		Time4= PAPI_get_real_cyc(  ); // empieza a contar el tiempo de un bucle de nodos del mismo color, incluyendo el overhead OpenMP
	#endif
		
	#ifdef ompBase 
		#pragma omp parallel for shared(count_hilo,PapiThreadResults,tiempoPorNodo,arrayTime2) private(cpuActual,hiloActual,Time1,Time2,Evento1,Evento2,usTime1,usTime2,w)
	#endif
			
	#ifdef ompDynamic
		#pragma omp parallel for shared(count_hilo,PapiThreadResults,tiempoPorNodo,arrayTime2) private(cpuActual,hiloActual,Time1,Time2,Evento1,Evento2,usTime1,usTime2,w) schedule(dynamic, 1)
	#endif
			
	// bucle de nodos con mismo color, version PARALELA SI PAPI
		for (w = 0; w < IS_n[z]; w++) 
		{ 
		
		#if defined ompBase || defined ompDynamic 
			hiloActual = omp_get_thread_num();
			cpuActual = sched_getcpu();
		#else
			hiloActual = 0;
			cpuActual = 0;
		#endif
									
			count_hilo[hiloActual][cpuActual]++; //se cuenta el numero de numeros primos procesados por hilos
			valuesIntermedio[hiloActual][0] = 0;
			retvalHilos[hiloActual] = PAPI_read( eventsetHilos[hiloActual] , valuesIntermedio[hiloActual]);
					
			if (retvalHilos[hiloActual] != PAPI_OK) printf("[Papi] Evento %i (%s), Error inicializacion PAPI_read, hilo: %i\n", kkP, eventName, hiloActual);
				
			Evento1 = valuesIntermedio[hiloActual][0];
			Time1   = PAPI_get_real_cyc(  );
			usTime1 = PAPI_get_real_usec(  );
		
		#ifdef pDEBUG
			printf("(Inicio)-->Hilo:%i, Color:%i, Nodo: %i, Contador: %i(%s), clk_ini_iter= %llu, valorCont_ini_iter= %lli\n",\
			hiloActual, z, w, kkP, eventName, Time1, Evento1);
		#endif

	#else 
		#if defined ompBase || defined ompDynamic 
			#pragma omp parallel for 
		#endif
		
		// bucle de nodos con mismo color, version PARALELA NO PAPI	
		for (w = 0; w < IS_n[z]; w++) { 			
	#endif 
				
			int i=ProcessNode(w, z);
														
		} 
		
		#ifdef miPapi
			Time4 = PAPI_get_real_cyc(  ) - Time4;
		#endif
		
	#if defined ompBase || defined ompDynamic 
		#ifdef miPapi 
		#pragma omp parallel for shared(PapiThreadResults) private(hiloActual,cpuActual,Time2,usTime2,w)
		#endif
			
		for (w = 0; w < numeroHilosActuales; w++) {
			hiloActual = omp_get_thread_num();
			cpuActual = sched_getcpu();
	#endif
				
		#ifdef miPapi
			valuesIntermedio[hiloActual][0] = 0;
			retvalHilos[hiloActual] = PAPI_read( eventsetHilos[hiloActual] , valuesIntermedio[hiloActual]);
		
			if (retvalHilos[hiloActual] != PAPI_OK) printf("[Papi] Fin iteracion - Evento %i (%s), Error inicializacion PAPI_read, hilo: %i\n", kkP, eventName, hiloActual);
			
			Time2   = PAPI_get_real_cyc(  )  - PapiThreadResultsTemp[cpuActual][0]; // - Time1;
			usTime2 = PAPI_get_real_usec(  ) - PapiThreadResultsTemp[cpuActual][1]; // - usTime1;
			Evento2 = valuesIntermedio[hiloActual][0]  - PapiThreadResultsTemp[cpuActual][2]; //- Evento1;

			PapiThreadResults[cpuActual][kkP][3] += Time2;
			PapiThreadResults[cpuActual][kkP][4] += usTime2;
			PapiThreadResults[cpuActual][kkP][5] += Evento2;

			#ifdef pDEBUG
				printf("(Final)-->Hilo:%i, Color:%i, Nodo: %i, Contador: %i(%s), clk_ini_iter= %llu, incremento_clk_fin_iter= %lli, \n valorCont_ini_iter= %lli, valorCont_fin_iter= %llu, incrCont= %lli, contadorAcumulado= %llu\n", \
				hiloActual, z, w, kkP, eventName, Time1, Time2, Evento1, valuesIntermedio[hiloActual][0], Evento2, PapiThreadResults[hiloActual][kkP][2]);
			#endif
			
		#endif
							
		#if defined ompBase || defined ompDynamic 
			}
		#endif
			
		#ifdef miPapi			
			tiempoPorColor[iteraciones][z]= Time4;

			long long qMax = 0; // mide el maximo tiempo interior de un hilo acumulando las sucesivas veces que optimiza un nodo
		
			for(int k=0; k<numeroHilosActuales; k++) if(qMax < arrayTime2[k]) qMax = arrayTime2[k];	
						
			overheadPorColor[iteraciones][z]= Time4 - qMax; // compara el tiempo interior maximo de un hilo (qMax) con el tiempo justo fuera del bucle del color actual (Time4)
		#endif

		} 

	#ifdef miPapi
		Time5		    = PAPI_get_real_cyc(  );
		timeIteracion	    = Time5 - Time3;
		timeIteracionTotal += timeIteracion;
		usIteracion         = PAPI_get_real_usec( ) - usTime3;
		usIteracionTotal   += usIteracion;
	#endif
							
	// Print statistics
		invalidElem = iGrabaEstadisticas(stdout, &LaMalla, lista, LaMalla.Num_tetra, iteraciones, &CalidadMedia, &CalidadMinima, 0.0); // aqui se va la mitad del overhead despues de terminar una iteraciones U&S

		if (invalidElem == 0) iter_suav++ ;

	#ifdef miPapi
		printf("\t %9llu \t %llu", usIteracion, timeIteracion);

		operacionesFP = 0;
			
		for(int k=0; k< numeroHilosActuales; k++){
			operacionesFP += PapiThreadResults[k][kkP][5];
		}
			
		printf("\t %llu  (%llu)", operacionesFP-operacionesFPantes, (operacionesFP-operacionesFPantes)/usIteracion);
			
		operacionesFPantes = operacionesFP;								

	#endif
		
		double idumy3 = fabs(CalidadMinima-CalidadMinimaAnterior);
			
		printf("\t %e", idumy3);
					
	// Quit the for loop if all smooth iterations have been done
		if (iter_suav == NumIterSuavizado) return 0;

		CalidadMinimaAnterior = CalidadMinima;
			
		if (invalidElem == 0 && idumy3 < 0.05*CalidadMinima) return 0;

	#ifdef miPapi
		Time5 = PAPI_get_real_cyc(  ) - Time5;
			
		printf("\t %9llu \t %5.2f%%\n", Time5, 100.0*(float)Time5/(float)timeIteracion);
	
	#endif	
	
	return 1;
}	

// ----

void ProcessPAPICounter(int kkP){
#ifdef miPapi	
	contadoresAnalizados++;
	if (numeroTiposContador == 0) eventDummy[0]= localStructNativos.eventID[kkP];
	else eventDummy[0]= localStructPreset.eventID[kkP];
	
	values[0] = 0;
	
	if (numeroTiposContador == 0) PAPI_event_code_to_name(localStructNativos.eventID[kkP],eventName);
	else PAPI_event_code_to_name(localStructPreset.eventID[kkP],eventName);
	
	printf("[Papi] Empieza iteracion untagled Nº %i, contadorPapi Nº %i (%s)\n",contadoresAnalizados, kkP, eventName);
	
	for(iP = 0; iP < numeroProcesadoresActuales; iP++) { //se resetean los contadores por hilo y por evento Papi 
		PapiThreadResults[iP][kkP][0] = 0;
		PapiThreadResults[iP][kkP][1] = 0;
		PapiThreadResults[iP][kkP][2] = 0;
		PapiThreadResults[iP][kkP][3] = 0;
		PapiThreadResults[iP][kkP][4] = 0;
		PapiThreadResults[iP][kkP][5] = 0;
	}
	
#if defined ompBase || defined ompDynamic 
	#pragma omp parallel for private(iP) 
#endif 
	
	for (iP = 0; iP < numeroHilosActuales; iP++) {
		eventsetHilos[iP]=PAPI_NULL;
		retvalHilos[iP] = PAPI_create_eventset( &eventsetHilos[iP] );
		if (retvalHilos[iP] != PAPI_OK) { printf("[Papi] Evento %i (%s), Error PAPI_create_eventset, hilo: %i\n", kkP, eventName, iP);}
	
	#ifdef pDEBUG
		else { printf("[Papi] OK! Crea Evento %i (%s), hilo: %i\n", kkP, eventName, iP);}
	#endif 
		
		retvalHilos[iP] = PAPI_add_events( eventsetHilos[iP], eventDummy, 1);
		retvalHilos[iP] = PAPI_start( eventsetHilos[iP] );
		if (retvalHilos[iP] != PAPI_OK) { printf("[Papi] Evento %i (%s), Error inicializacion PAPI_start, hilo: %i\n", kkP, eventName, iP);}
		
	#ifdef pDEBUG
		else { printf("[Papi] OK! Start Evento %i (%s), hilo: %i\n", kkP, eventName, iP);}
	#endif
	}
	
	printf("[Papi] Empieza a contar el tiempo Papi real -cyc, us-\n");
	cyc = PAPI_get_real_cyc(  ); 
	us  = PAPI_get_real_usec(  ); 
	
	printf("infoMem: despues crear y anyadir eventos Papi; ");
	infoMem(3);

#endif	
}
				

// ---

void ParseOptions(int argc, char **argv){
	while((optchar = getopt(argc, argv, "n:e:s:o:q:i:m:d")) != -1)
	{
		int k;
		switch(optchar)
		{
		case 'n':
			fnodes = (char*)strdup(optarg);
			break;
		case 'e':
			felems = (char*)strdup(optarg);
			break;
		case 's':
			// Statistics file
			fstat = (char*)strdup(optarg);
			break;
		case 'o':
			// Smoothed nodes file
			fsmoot = (char*)strdup(optarg);
			break;
		case 'd':
			// No normalize input mesh
			Normalized = false;
			break;
		case 'q':
			// Save quality file(s)
			SaveQuality = true;
			fquality = (char *)strdup(optarg);
			break;
		case 'i':
			// Max. number of iterations
			k = sscanf(optarg, "%d", &MaxNumIter);
			if ((k == EOF) || (k < 1))
			{
				cout << "Error reading number of iterations" << endl;
				uso();
				exit(EXIT_FAILURE);
			}
			break;
		case 'm':
			// Number of smoothing iterations
			k = sscanf(optarg, "%d", &NumIterSuavizado);
			if ((k == EOF) || (k < 1))
			{
				cout << "Error reading number of smoothing iterations" << endl;
				uso();
				exit(EXIT_FAILURE);
			}
			break;
		default:
			cout << "Unknown command option" << endl;
			uso();
			exit(EXIT_FAILURE);
			break;
		}
	}

	if ((fnodes==NULL) ||(felems==NULL))
	{
		cout << "Missing file(s) name(s)"<< endl;
		uso();
		exit(EXIT_FAILURE);
	}
}



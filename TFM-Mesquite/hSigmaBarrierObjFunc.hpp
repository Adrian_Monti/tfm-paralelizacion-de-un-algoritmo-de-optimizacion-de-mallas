/* ***************************************************************** 
    MESQUITE -- The Mesh Quality Improvement Toolkit

    Copyright 2004 Sandia Corporation and Argonne National
    Laboratory.  Under the terms of Contract DE-AC04-94AL85000 
    with Sandia Corporation, the U.S. Government retains certain 
    rights in this software.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License 
    (lgpl.txt) along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 
    diachin2@llnl.gov, djmelan@sandia.gov, mbrewer@sandia.gov, 
    pknupp@sandia.gov, tleurent@mcs.anl.gov, tmunson@mcs.anl.gov      
   
  ***************************************************************** */
// -*- Mode : c++; tab-width: 3; c-tab-always-indent: t; indent-tabs-mode: nil; c-basic-offset: 3 -*-

/*! \file hSigmaBarrierObjFunc.hpp
  \brief Header file for the Mesquite::hSigmaBarrierObjFunc class
 \author Michael Brewer
 \author Thomas Leurent
  \date   2002-05-23
 */


#ifndef hSigmaBarrierObjFunc_hpp
#define hSigmaBarrierObjFunc_hpp

#include "Mesquite.hpp"
#include "ObjectiveFunctionTemplate.hpp"
#include "PatchSet.hpp"

namespace MESQUITE_NS
{
  class Matrix3D;

     /*! \class hSigmaBarrierObjFunc
       \brief Calculates the L_p objective function raised to the pth
       power.  That is, sums the p_th powers of (the absolute value of)
       the quality metric values.

     */
  class hSigmaBarrierObjFunc :public ObjectiveFunctionTemplate
  {
  public:
	MESQUITE_EXPORT
    hSigmaBarrierObjFunc( QualityMetric* qm, double , double );
     
	MESQUITE_EXPORT
    virtual ~hSigmaBarrierObjFunc();
    
	MESQUITE_EXPORT
    virtual void clear();
    
    
	MESQUITE_EXPORT
    virtual bool evaluate( EvalType type, 
                           PatchData& pd,
                           double& value_out,
                           bool free,
                           MsqError& err ); 
     
	MESQUITE_EXPORT
    virtual bool evaluate_with_gradient( EvalType type, 
                                         PatchData& pd,
                                         double& value_out,
                                         std::vector<Vector3D>& grad_out,
                                         MsqError& err ); 

	MESQUITE_EXPORT
    virtual bool evaluate_with_Hessian_diagonal( EvalType type, 
                                        PatchData& pd,
                                        double& value_out,
                                        std::vector<Vector3D>& grad_out,
                                        std::vector<SymMatrix3D>& hess_diag_out,
                                        MsqError& err ); 

	MESQUITE_EXPORT
    virtual bool evaluate_with_Hessian( EvalType type, 
                                        PatchData& pd,
                                        double& value_out,
                                        std::vector<Vector3D>& grad_out,
                                        MsqHessian& Hessian_out,
                                        MsqError& err ); 

	MESQUITE_EXPORT
    virtual ObjectiveFunction* clone() const;

       /*!Use set_dividing_by_n to control whether this objective
         function divides it's final value by the number of
         metric values used to compute the objective function
         value.  That is, if the associated metric is element
         based, the obejctive function value is divided by
         the number of elements.  If it is vertex based, the
         objective function is divided by the number of vertices.
         If this function is passed 'true', the function value
         will be scale.  If it is passed false, the function
         value will not be scaled.*/

	MESQUITE_EXPORT
    void set_dividing_by_n(bool d_bool){dividingByN=d_bool;}
     
	MESQUITE_EXPORT
    virtual void set_calidad_minima(double Qmin) 
	{mCalidadMinima = Qmin;}

	MESQUITE_EXPORT
    virtual void set_mu(double mu) 
	{mMU = mu;}

	MESQUITE_EXPORT
    double get_calidad_minima_patch(PatchData& pd);

	MESQUITE_EXPORT
    double get_calidad_minima2_patch(PatchData& pd);

	MESQUITE_EXPORT
    double get_mu() {return mMU;}

	MESQUITE_EXPORT
    double get_calidad_minima_ObjFunc() {return mCalidadMinima;}

	MESQUITE_EXPORT
    int get_negate_flag( ) const { return -1; }

	MESQUITE_EXPORT
    std::string get_name( ) const { return "logBarrierObjectiveFunction"; }

	MESQUITE_EXPORT
    void get_parametros_patch(PatchData& pd, double& Qmed, double& Vmed, double& Lmed, double& Vstd);

	MESQUITE_EXPORT
    double calculaDeltaParaHSigmaBarrier(PatchData& pd);

	MESQUITE_EXPORT
    double getDeltaParaHSigmaBarrier() { return deltaParaHSigmaBarrier; }

	MESQUITE_EXPORT
    void setDeltaParaHSigmaBarrier(double delta) { deltaParaHSigmaBarrier = delta; }

   private:

     double mCalidadMinima;
     double mMU;

     double get_value( double power_sum, size_t count, EvalType type, 
		       size_t& global_count, MsqError& err );
     
       //! The metric value entries are raised to the pVal power
     short pVal;
       //! dividingByN is true if we are dividing the objective function
       //! by the number of metric values.
     bool dividingByN;
     
     
     size_t mCount;    /**< The number of accumulated entires */
     double mPowSum;   /**< The accumulated sum of values */
     size_t saveCount; /**< Saved count from previous patch */
     double savePowSum;/**< Saved sum from previous patch */
     
     /** Temporary storage for qm sample handles */
     mutable std::vector<size_t> qmHandles;
     /** Temporary storage for qm vertex indices */
     mutable std::vector<size_t> mIndices;
     /** Temporary storage for qm gradient */
     mutable std::vector<Vector3D> mGradient;
     /** Temporary storage for qm Hessian diagonal blocks */
     mutable std::vector<SymMatrix3D> mDiag;
      /** Temporary storage for qm Hessian */
     mutable std::vector<Matrix3D> mHessian;

//   public:
     Mesh::VertexHandle handleVerticesCache[100];
     double deltaParaHSigmaBarrier;

   };
   
}//namespace

#endif // hSigmaBarrierObjFunc_hpp


/* ***************************************************************** 
    MESQUITE -- The Mesh Quality Improvement Toolkit

    Copyright 2007 Sandia National Laboratories.  Developed at the
    University of Wisconsin--Madison under SNL contract number
    624796.  The U.S. Government and the University of Wisconsin
    retain certain rights to this software.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License 
    (lgpl.txt) along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    (2008) kraftche@cae.wisc.edu    
    (2013) dbenitez@siani.es

  ***************************************************************** */


/** \file ThMixedQ.hpp
 *  \brief 
 *  \author Domingo Benitez
 *  Calcula la metrica ||S||^2 / 3 (h(sigma))^2/3 de un elemento 2D (triangulo) o 3D (tetraedro)
 */

#ifndef MSQ_THMIXEDQ_HPP
#define MSQ_THMIXEDQ_HPP

#include "Mesquite.hpp"
#include "TMetric.hpp"
#include "TMPQualityMetric.hpp"
#include "MsqVertex.hpp"
#include <limits>
#include <iostream>
#include <vector>
#include "PatchSet.hpp"
#include "PatchData.hpp"
#include "ThMixedQ.hpp"

namespace MESQUITE_NS {

class ThMixedQ : public TMPQualityMetric {
public:

  MESQUITE_EXPORT 
  ThMixedQ( TargetCalculator *tc)
  	: TMPQualityMetric(tc,0),
      	  NtetraVisitados(0)
	{
   	 /* desenreda TangledCube*/ //mALFA=1.0; mBETA=2.0; mLAMBDA=10.0;
   	 /* desenreda TangledCube*/ //mALFA=1.0; mBETA=5.0e1; mLAMBDA=1.0e+2;
   	 // conejo, entorno a 1300 enredos - mALFA=1.0e7; mBETA=1.0e7; mLAMBDA=1.0e+2;
   	 /* Screwdriver: 52 iter, 232 enredos */ //mALFA=1.0e3; mBETA=1.0e3; mLAMBDA=0.7e+1;
   	 /* Screwdriver: 24 iter, 130 enredos */ //mALFA=1.0e3; mBETA=1.0e3; mLAMBDA=0.6e+1;
   	 /* Screwdriver: 23 iter,  83 enredos */ //mALFA=1.0e3; mBETA=1.0e3; mLAMBDA=0.5e+1;
   	 /* Screwdriver: 28 iter,  39 enredos */ //mALFA=1.0e3; mBETA=1.0e3; mLAMBDA=0.4e+1;
   	 /* Screwdriver: 21 iter,  10 enredos */ //mALFA=1.0e3; mBETA=1.0e3; mLAMBDA=0.31e+1;
   	 /* Screwdriver: 19 iter,   6 enredos */ //mALFA=1.0e3; mBETA=1.0e3; mLAMBDA=0.3e+1;
   	 /* Screwdriver: 21 iter,  12 enredos */ //mALFA=1.0e3; mBETA=1.0e3; mLAMBDA=0.29e+1;
   	 /* Screwdriver: 15 iter,  17 enredos */ //mALFA=1.0e3; mBETA=1.0e3; mLAMBDA=0.28e+1;
   	 /* Screwdriver: 13 iter,  17 enredos */ //mALFA=1.0e3; mBETA=1.0e3; mLAMBDA=0.25e+1;
   	 /* Screwdriver: 10 iter,  26 enredos */ //mALFA=1.0e3; mBETA=1.0e3; mLAMBDA=0.2e+1;
   	 //   mALFA=1.0e0; mBETA=1.0e0; mLAMBDA=1.0e+1;
   	 /* prueba Screwdriver */ //mALFA=1.0e3; mBETA=1.0e3; mLAMBDA=1.0e+2;
   	 /* probado para 2D paro no desenredo */ //mALFA=1.0e0; mBETA=1.0e0; mLAMBDA=1.0e1;
   	 /* desenreda TangledCube, el mejor*/ mALFA=1.0e3; mBETA=1.0e3; mLAMBDA=1.0e+0;
        }

  MESQUITE_EXPORT virtual
  ~ThMixedQ();

  MESQUITE_EXPORT virtual
  std::string get_name() const;

  //MESQUITE_EXPORT void set_calidadMinima(double delta_in)
  //	{mCalidadMinima = delta_in;}

  //MESQUITE_EXPORT void set_mu(double epsilon_in)
  //	{mMU = epsilon_in;}

  MESQUITE_EXPORT void set_pd(PatchData* pd)
	{local_pd = pd;}

  MESQUITE_EXPORT void set_element_index(size_t ID)
	{elem_index = ID;}

	MESQUITE_EXPORT
    int get_negate_flag( ) const { return -1; }

  //MESQUITE_EXPORT double get_calidadMinima()
  //	{return mCalidadMinima;}


  //MESQUITE_EXPORT double get_lambda()
  //	{return mLAMBDA;}

  MESQUITE_EXPORT 
  size_t get_NtetraVisitados() { return NtetraVisitados; }
 

  MESQUITE_EXPORT virtual
  bool evaluate_with_gradient( PatchData& pd,
                               size_t handle,
                               double& value,
                               std::vector<size_t>& indices,
                               std::vector<Vector3D>& grad,
                               MsqError& err );

  MESQUITE_EXPORT virtual
  void get_evaluations( PatchData& pd,
	std::vector<size_t>& handles,
	bool free_only,
	MsqError& err );

//protected:

  MESQUITE_EXPORT virtual
  bool evaluate_internal( PatchData& pd,
                  size_t handle,
                  double& value,
		size_t* indices,
		size_t& num_indices,
                  MsqError& err );

public:
  double mALFA, mBETA, mLAMBDA;
  double ultimoL, ultimoVol;
  mutable std::vector<size_t> qmHandles;
  PatchData* local_pd;
  size_t elem_index;
  MsqVector<3> mDerivs3D[MAX_ELEM_NODES];
  size_t NtetraVisitados;
};


} // namespace Mesquite

#endif

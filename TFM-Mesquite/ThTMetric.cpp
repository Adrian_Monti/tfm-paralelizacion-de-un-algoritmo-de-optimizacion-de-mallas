/*
    MESQUITE -- The Mesh Quality Improvement Toolkit

    Copyright 2006 Sandia National Laboratories.  Developed at the
    University of Wisconsin--Madison under SNL contract number
    624796.  The U.S. Government and the University of Wisconsin
    retain certain rights to this software.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License 
    (lgpl.txt) along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 
    (2006) kraftche@cae.wisc.edu
   
  ***************************************************************** */


/** \file ThTMetric.cpp (from TQualityMetric.cpp)
 *  \brief 
 *  \author Domingo Benitez
 *  \author Jason Kraftcheck 
 */
//#define debugThTMetric

#undef PRINT_INFO
#undef miPRINT
//#define PRINT_INFO

#include "Mesquite.hpp"
#include "ThTMetric.hpp"
//#include "hSigmaMetric.hpp"
#include "MsqMatrix.hpp"
#include "ElementQM.hpp"
#include "MsqError.hpp"
#include "Vector3D.hpp"
#include "PatchData.hpp"
#include "MappingFunction.hpp"
#include "WeightCalculator.hpp"
#include "TargetCalculator.hpp"
#include "TMetric.hpp"
#include "TMetricBarrier.hpp"
#include "TargetMetricUtil.hpp"
#include "TMPDerivs.hpp"
#include "PatchData.hpp"
#include "omp.h"
#include <iostream>
#include <functional>
#include <algorithm>
#include <stdio.h>

using std::cout;
using std::endl;
using std::vector;

#define NUMERICAL_2D_HESSIAN

size_t Ntetra2;

namespace MESQUITE_NS {

std::string ThTMetric::get_name() const
{
  return targetMetric->get_name();
}

// calcula la delta de la metrica TMetric para cada PatchData (estrella)
double ThTMetric::calculaDelta(PatchData& pd)
{
  //printf("calculaDelta ThTMetric\n");

  cout.precision(3);
  size_t e2;
  size_t numNodosPatch = pd.num_nodes();
  size_t numElemPatch = pd.num_elements();
  Ntetra2=0; // tetraedros visitados durante calculaDelta

  //double temp = 1.5e-3; //bien para: TangledCube (9 iter, np=1,2), conejo, ej9 (57 iter., np=1), Screwdriver
  //double temp = 0.25; // ej9 (5 iter, np=2, optimizacion global)
  //double temp = 0.01; // ej9 

  //double temp = 0.20542; // TangledCube
  //double temp = 1.0e-2; //2.5e-2; //5.0e-2; //3.0e-2; // Conejo (no funciona np=2)
  //double temp = 1.5e-9; // hueso 
  //double temp = 1.5e-4; 1.5e-3 // probado con armadillo
  //double temp = 1.5e-10; // bien para: hueso
  //---> targetMetric->set_delta(temp); // almacena en la cache un nuevo delta
  //std::cout << "\tdeltaFija= " << temp << endl;
  //---> return temp; // para devolver un delta constante, independiente de sigma

  // para devolver un delta dependiente de sigma
  MsqError err;
  int i, j, coincide=0, nodo=0;
  //std::vector< size_t > vertex_list;
  MsqMeshEntity elem2 ;
  //MsqVertex dumy4, * dumy3 ;
  //Mesh::VertexHandle * vtxHandle, dumy5;
  Mesh::VertexHandle * vtxHandle;

/*
  // BEGIN: cache-rudimentaria de la metrica
  size_t dumy5;
  dumy5 = targetMetric->handleVerticesCache;
  targetMetric->handleVerticesCache = numElemPatch;
  //targetMetric->handleVerticesCache = numNodosPatch;
  if(dumy5 == numElemPatch) {
  //if(dumy5 == numNodosPatch) {
    //cout << " cacheHIT!!, deltaCache= " << targetMetric->get_delta() << std::endl;
    return targetMetric->get_delta(); 
  } 
  //else {
    //cout << " cacheNOHIT!!" << std::endl;
  //} 
  // END: cache-rudimentaria de la metrica
*/
 
  // BEGIN: cache de la metrica
  // devuelve ultimo valor si se ha calculado previamente para el mismo patch
  //
  // 1: lee coordenadas de los nodos del patch
  Mesh::VertexHandle dumy5;
  vtxHandle  = (Mesh::VertexHandle *) pd.get_vertex_handles_array();

#ifdef debugThTMetric
  std::cout << "ThTMetric::calculaDelta, entramos con "; 
  std::cout << "pd.nodos= " << numNodosPatch ;
  std::cout << ", pd.elementos= " << pd.num_elements() << std::endl;
#endif

  // 2: cuenta el numero de coincidencias de los vertices del patch
  int limiteCuentaCoincidencias = (numNodosPatch > 100 ? 100 : numNodosPatch);

  for (i=0; i<limiteCuentaCoincidencias; i++) { 
  //for (i=0; i<numNodosPatch; i++) { 
    // lee la cache de nodos del patch presente

#ifdef debugThTMetric
    if(0) cout << "cache, i: " << i << ", " << targetMetric->handleVerticesCache[i] 
    << ", " << coincide << ", " << nodo << std::endl ;
#endif

    dumy5 = targetMetric->handleVerticesCache[i];
    if (vtxHandle[i] == dumy5) coincide++; // si coincide los handles
    targetMetric->handleVerticesCache[i] = vtxHandle[i];
    nodo++;
  }
  //cout << "\tcoincidencias: " << coincide << ", nodos: " << nodo;

  // 3: si coinciden los handle, devuelve delta que tiene en cache
  if(coincide == nodo) {

#ifdef debugThTMetric
    cout << " cacheHIT!!, deltaCache= " << targetMetric->get_delta() << std::endl;
    //<< " handleVerticesCache.size= " << targetMetric->handleVerticesCache.size()  << std::endl;
#endif

    return targetMetric->get_delta(); 
  } 

#ifdef debugThTMetric
  else {
    cout << " cacheNOHIT!!" 
    << "\tThTMetric-pd.nodos= " << pd.num_nodes()
    << ", pd.elem= " << pd.num_elements() << std::endl; 
  }
#endif
  // END: cache de la metrica

  // No hay HIT y se calcula la Delta para el patch pd
  //
/*
  double determinante;
  double sigma_min=MSQ_MAX, sigma_max=-1*MSQ_MAX, sigma_media=0;
  double EpsilonEfectivo, EpsSafetyFactor=1e11, delta_min, delta;
  MsqMatrix<3,3> Adelta, Wdelta, dmdTdelta;
  EntityTopology typeDelta;
  const MappingFunction3D* mfDelta ;
  NodeSet bitsDelta;
  Sample sDelta;
  MsqMatrix<3,3> WinvDelta, Tdelta;
  size_t num_idx = 0, vI;
*/


  // No hay HIT y se calcula la Delta para el patch pd
  //
  double determinante;
  // se usan constantes de Mesquite.hpp
  double  sigma_min=MSQ_MAX, sigma_max=-1*MSQ_MAX, sigma_media=0;
  double  EpsilonEfectivo, EpsSafetyFactor=1e11, delta_min, delta;
  EntityTopology typeDelta;
  NodeSet bitsDelta;
  Sample  sDelta;
  size_t  num_idx = 0, vI;


  //recorre elementos del patch para calcular sigma media
  for (j = 0; j < numElemPatch; j++) { 
        e2 = ElemSampleQM::elem(j); // indice del elemento

        elem2 = pd.element_by_index( e2 ); // estructura asociada al elemento
        typeDelta = elem2.get_element_type(); // tipo de elemento

        bitsDelta = pd.non_slave_node_set( e2 );// indica que nodos no son esclavos
        sDelta = ElemSampleQM::sample(j);

	unsigned edim = TopologyInfo::dimension( typeDelta );
	if (edim ==3) { // para elementos 3D (tetraedros, etc.)
  	  const MappingFunction3D* mfDelta ;
  	  MsqMatrix<3,3> Adelta, Wdelta, dmdTdelta;
  	  MsqMatrix<3,3> WinvDelta, Tdelta;

          mfDelta = pd.get_mapping_function_3D( typeDelta );
          mfDelta->jacobian(pd, e2, bitsDelta, sDelta, mIndices, mDerivs3D, num_idx, Adelta, err); // devuelve el jacobiano: Adelta
          targetCalc->get_3D_target( pd, e2, sDelta, Wdelta, err ); MSQ_ERRZERO(err);
          WinvDelta = inverse(Wdelta);
          Tdelta = Adelta * WinvDelta; // weighted Jacobian matrix
	  // se obtiene la sigma=determinante
          determinante = det(Tdelta);
	}
	else if (edim ==2) { // para elementos de tipo 2D (triandulos, etc.)
  	  const MappingFunction2D* mfDelta ;
  	  MsqMatrix<2,2> Adelta, Wdelta; 
  	  MsqMatrix<2,2> WinvDelta, Tdelta;
    	  MsqMatrix<3,2> S_a_transpose_Theta;

    	  bool rval = evaluate_surface_common( pd, sDelta, e2, bitsDelta, mIndices, num_idx,
                                 mDerivs2D, Wdelta, Adelta, S_a_transpose_Theta, err ); 
          WinvDelta = inverse(Wdelta);
          Tdelta = Adelta * WinvDelta; // weighted Jacobian matrix
          determinante = det(Tdelta);
	}
	//printf(" %4.1e), ", determinante);
        if(determinante < sigma_min) sigma_min = determinante;
        if(determinante > sigma_max) sigma_max = determinante;
        //sigma_media += fabs(determinante);
        sigma_media += determinante;
  }
  sigma_media /= numElemPatch;
  Ntetra2 += numElemPatch;
  NtetraVisitados += Ntetra2;









/*

  //recorre elementos del patch para calcular sigma media
  for (j = 0; j < numElemPatch; j++) { 
        e2 = ElemSampleQM::elem(j); // indice del elemento
        //pd.get_element_vertex_indices (e2, vertex_list, err); // indice de los vertices: vertex_list
        elem2 = pd.element_by_index( e2 ); // estructura asociada al elemento
        typeDelta = elem2.get_element_type(); // tipo de elemento
        mfDelta = pd.get_mapping_function_3D( typeDelta );
        bitsDelta = pd.non_slave_node_set( e2 );// indica que nodos no son esclavos
        sDelta = ElemSampleQM::sample(j);
        mfDelta->jacobian(pd, e2, bitsDelta, sDelta, mIndices, mDerivs3D, num_idx, Adelta, err); // devuelve el jacobiano: Adelta
        targetCalc->get_3D_target( pd, e2, sDelta, Wdelta, err ); MSQ_ERRZERO(err);
        WinvDelta = inverse(Wdelta);
        Tdelta = Adelta * WinvDelta; // weighted Jacobian matrix
	// se obtiene la sigma=determinante
        determinante = det(Tdelta);
	//printf(" %4.1e), ", determinante);
        if(determinante < sigma_min) sigma_min = determinante;
        if(determinante > sigma_max) sigma_max = determinante;
        //sigma_media += fabs(determinante);
        sigma_media += determinante;
  }
  sigma_media /= numElemPatch;
  Ntetra2 += numElemPatch;
  NtetraVisitados += Ntetra2;
*/

  // DBL_EPSILON esta definido en float.h: upper bound on the relative error due to rounding in floating point arithmetic
  EpsilonEfectivo = DBL_EPSILON * EpsSafetyFactor;
  // se obtiene delta_minimo
  delta_min = EpsilonEfectivo * (EpsilonEfectivo - sigma_min);
  if(delta_min > 0) delta_min = sqrt(delta_min);
  else delta_min = 0.0; //DBL_EPSILON; //0.0;

  //delta = (delta_min > 5e-3*sigma_media)? delta_min : 5e-3*sigma_media; // TangledCube 18 iter
  //delta = (delta_min > 5e-5*sigma_media)? delta_min : 5e-5*sigma_media; // TangledCube 17 iter
  delta = (delta_min > 1e-4*sigma_media)? delta_min : 1e-4*sigma_media; // TangledCube 24 iter
  //delta = (delta_min > 1e-5*sigma_media)? delta_min : 1e-5*sigma_media; // TangledCube 38 iter
  //delta = (delta_min > 5e-5*sigma_media)? delta_min*8e-2 : 5e-5*sigma_media; //  mi prueba

  delta *= 1.00e+1; // pruebas para hueso 
  //delta *= 2.00e-1; // pruebas para hueso 
  /* BUENO */  //delta *= 1.0e+1;  

#ifdef debugThTMetric
  //printf("ThTMetric-patchNumElements: %4i, sigma_media= %8.1e, ", (int)numElemPatch(),sigma_media) ;
  printf("ThTMetric-patchNumElements: sigma_media= %8.1e, ", sigma_media) ;
  printf("sigma_min= %8.1e, ", sigma_min) ;
  printf("sigma_max= %8.1e, ", sigma_max) ;
  printf("deltaMin= %8.1e, ", delta_min) ;
  printf("deltaVariable= %8.1e\n", delta) ;
#endif

  //*NtetraAccedido = Ntetra;  
  targetMetric->set_delta(delta); // almacena en la cache un nuevo delta
  return delta;

} // ThTMetric::calculaDelta(PatchData& pd)


bool ThTMetric::evaluate_internal( PatchData& pd,
                                        size_t handle,
                                        double& value,
                                        size_t* indices,
                                        size_t& num_indices,
                                        MsqError& err )
{
#ifdef miPRINT
  std::cout << "ThTMetric::evaluate_internal= " << std::endl ;
#endif

  bool rval;
  const Sample s = ElemSampleQM::sample( handle );
  const size_t e = ElemSampleQM::  elem( handle );
  MsqMeshEntity& elem = pd.element_by_index( e );
  EntityTopology type = elem.get_element_type();
  unsigned edim = TopologyInfo::dimension( type );
  const NodeSet bits = pd.non_slave_node_set( e );

  //std::cout << "deltaPrevio= " << targetMetric->get_delta() << " hilo: " << omp_get_thread_num() << std::endl;

  double delta = calculaDelta(pd); // calculo necesario de la delta antes de calcular la metrica del patch

#ifdef miPRINT
  printf("ThTMetric::evaluate_internal - tetraID: %lu, Ntetra: %lu, NtetraVisitados: %lu\n", (size_t)e, Ntetra2, NtetraVisitados);
  std::cout << "ThTMetric::evaluate_internal-> delta= " << delta << std::endl ;
#endif

  if (edim == 3) { // 3x3 or 3x2 targets ?
    const MappingFunction3D* mf = pd.get_mapping_function_3D( type );
    if (!mf) {
      MSQ_SETERR(err)( "No mapping function for element type", MsqError::UNSUPPORTED_ELEMENT );
      return false;
    }

    MsqMatrix<3,3> A, W;
    mf->jacobian( pd, e, bits, s, indices, mDerivs3D, num_indices, A, err );
    MSQ_ERRZERO(err);
    targetCalc->get_3D_target( pd, e, s, W, err ); MSQ_ERRZERO(err);
    const MsqMatrix<3,3> Winv = inverse(W);
    const MsqMatrix<3,3> T = A*Winv;
    rval = targetMetric->evaluate( T, value, err ); MSQ_ERRZERO(err);

#ifdef PRINT_INFO
    print_info<3>( e, s, A, W, A * inverse(W) );
#endif

    // Actualiza la metrica para performance evaluation
    NtetraVisitados++;
  }
  else if (edim == 2) {
    MsqMatrix<2,2> W, A;
    MsqMatrix<3,2> S_a_transpose_Theta;
    rval = evaluate_surface_common( pd, s, e, bits, indices, num_indices,
                                 mDerivs2D, W, A, S_a_transpose_Theta, err ); 
    if (MSQ_CHKERR(err) || !rval)
      return false;
    const MsqMatrix<2,2> Winv = inverse(W);
    const MsqMatrix<2,2> T = A * Winv;
    rval = targetMetric->evaluate( T, value, err );
    MSQ_ERRZERO(err);

#ifdef PRINT_INFO
    print_info<2>( e, s, J, Wp, A * inverse(W) );
#endif
    NtetraVisitados++;
  }
  else {
    assert(false);
    return false;
  }
  
  //std::cout << "ThTMetric::evaluate_internal-> val= " << rval << std::endl ;
  return rval;
//}

}

bool ThTMetric::evaluate_with_gradient( PatchData& pd,
                                             size_t handle,
                                             double& value,
                                             std::vector<size_t>& indices,
                                             std::vector<Vector3D>& grad,
                                             MsqError& err )
{
  const Sample s = ElemSampleQM::sample( handle );
  const size_t e = ElemSampleQM::  elem( handle );
  //#pragma omp critical
  MsqMeshEntity& elem = pd.element_by_index( e );
  EntityTopology type = elem.get_element_type();
  unsigned edim = TopologyInfo::dimension( type );
  size_t num_idx = 0;
  const NodeSet bits = pd.non_slave_node_set( e );
 
  double delta = calculaDelta(pd);
#ifdef miPRINT
  printf("ThTMetric::evaluate_internal_gradiente - tetraID: %lu, Ntetra: %lu, NtetraVisitados: %lu\n", (size_t)e, Ntetra2, NtetraVisitados);
  //NtetraVisitados+=Ntetra;
  std::cout << "ThTMetric::evaluate_with_gradient-> delta= " << delta << std::endl ;
#endif
 
  bool rval;
  if (edim == 3) { // 3x3 or 3x2 targets ?
    const MappingFunction3D* mf = pd.get_mapping_function_3D( type );
    if (!mf) {
      MSQ_SETERR(err)( "No mapping function for element type", MsqError::UNSUPPORTED_ELEMENT );
      return false;
    }

    MsqMatrix<3,3> A, W, dmdT;
    mf->jacobian( pd, e, bits, s, mIndices, mDerivs3D, num_idx, A, err );
    MSQ_ERRZERO(err);
    targetCalc->get_3D_target( pd, e, s, W, err ); MSQ_ERRZERO(err);
    const MsqMatrix<3,3> Winv = inverse(W);
    const MsqMatrix<3,3> T = A*Winv;
    rval = targetMetric->evaluate_with_grad( T, value, dmdT, err );
    MSQ_ERRZERO(err);
    gradient<3>( num_idx, mDerivs3D, dmdT * transpose(Winv), grad );
#ifdef PRINT_INFO
    print_info<3>( e, s, A, W, A * inverse(W) );
#endif
    NtetraVisitados++;
  }
  else if (edim == 2) {
    MsqMatrix<2,2> W, A, dmdT;
    MsqMatrix<3,2> S_a_transpose_Theta;
    rval = evaluate_surface_common( pd, s, e, bits, mIndices, num_idx,
                             mDerivs2D, W, A, S_a_transpose_Theta, err ); 
    if (MSQ_CHKERR(err) || !rval)
      return false;
    const MsqMatrix<2,2> Winv = inverse(W);
    const MsqMatrix<2,2> T = A*Winv;
    rval = targetMetric->evaluate_with_grad( T, value, dmdT, err );
    MSQ_ERRZERO(err);
    gradient<2>( num_idx, mDerivs2D, S_a_transpose_Theta*dmdT*transpose(Winv), grad );
#ifdef PRINT_INFO
    print_info<2>( e, s, J, Wp, A * inverse(W) );
#endif
    NtetraVisitados++;
  }
  else {
    assert(false);
    return false;
  }
  
    // pass back index list
  indices.resize( num_idx );
  std::copy( mIndices, mIndices+num_idx, indices.begin() );
  
    // apply target weight to value
  weight( pd, s, e, num_idx, value, grad.empty() ? 0 : arrptr(grad), 0, 0, err ); MSQ_ERRZERO(err);
  return rval;
}


bool ThTMetric::evaluate_with_Hessian( PatchData& pd,
                                            size_t handle,
                                            double& value,
                                            std::vector<size_t>& indices,
                                            std::vector<Vector3D>& grad,
                                            std::vector<Matrix3D>& Hessian,
                                            MsqError& err )
{
  const Sample s = ElemSampleQM::sample( handle );
  const size_t e = ElemSampleQM::  elem( handle );
  //#pragma omp critical
  MsqMeshEntity& elem = pd.element_by_index( e );
  EntityTopology type = elem.get_element_type();
  unsigned edim = TopologyInfo::dimension( type );
  size_t num_idx = 0;
  const NodeSet bits = pd.non_slave_node_set( e );
 
#ifdef miPRINT
  double delta = calculaDelta(pd);
  NtetraVisitados+=Ntetra2;
  std::cout << "ThTMetric::evaluate_with_hessian-> delta= " << delta << std::endl ;
#endif

  bool rval;
  if (edim == 3) { // 3x3 or 3x2 targets ?
    const MappingFunction3D* mf = pd.get_mapping_function_3D( type );
    if (!mf) {
      MSQ_SETERR(err)( "No mapping function for element type", MsqError::UNSUPPORTED_ELEMENT );
      return false;
    }

    MsqMatrix<3,3> A, W, dmdT, d2mdT2[6];
    mf->jacobian( pd, e, bits, s, mIndices, mDerivs3D, num_idx, A, err );
    MSQ_ERRZERO(err);
    targetCalc->get_3D_target( pd, e, s, W, err ); MSQ_ERRZERO(err);
    const MsqMatrix<3,3> Winv = inverse(W);
    const MsqMatrix<3,3> T = A*Winv;
    rval = targetMetric->evaluate_with_hess( T, value, dmdT, d2mdT2, err );
    MSQ_ERRZERO(err);
    gradient<3>( num_idx, mDerivs3D, dmdT*transpose(Winv), grad );
    second_deriv_wrt_product_factor( d2mdT2, Winv );
    Hessian.resize( num_idx*(num_idx+1)/2 );
    if (num_idx)
      hessian<3>( num_idx, mDerivs3D, d2mdT2, arrptr(Hessian) );
    
#ifdef PRINT_INFO
    print_info<3>( e, s, A, W, A * inverse(W) );
#endif
    NtetraVisitados++;
  }
  else if (edim == 2) {
#ifdef NUMERICAL_2D_HESSIAN
    // return finite difference approximation for now

    return QualityMetric::evaluate_with_Hessian( pd, handle,
                                           value, indices, grad, Hessian,
                                           err );
#else
    MsqMatrix<2,2> W, A, dmdT, d2mdT2[3];
    MsqMatrix<3,2> M;
    rval = evaluate_surface_common( pd, s, e, bits, mIndices, num_idx,
                             mDerivs2D, W, A, M, err ); 
    if (MSQ_CHKERR(err) || !rval)
      return false;
    const MsqMatrix<2,2> Winv = inverse(W);
    const MsqMatrix<2,2> T = A*Winv;
    rval = targetMetric->evaluate_with_hess( T, value, dmdT, d2mdT2, err );
    MSQ_ERRZERO(err);
    gradient<2>( num_idx, mDerivs2D, M * dmdT * transpose(Winv), grad );
      // calculate 2D hessian
    second_deriv_wrt_product_factor( d2mdT2, Winv );
    const size_t n = num_idx*(num_idx+1)/2;
    hess2d.resize(n);
    if (n)
      hessian<2>( num_idx, mDerivs2D, d2mdT2, arrptr(hess2d) );
      // calculate surface hessian as transform of 2D hessian
    Hessian.resize(n);
    for (size_t i = 0; i < n; ++i)
      Hessian[i] = Matrix3D( (M * hess2d[i] * transpose(M)).data() );
#ifdef PRINT_INFO
    print_info<2>( e, s, J, Wp, A * inverse(W) );
#endif
#endif
  }
  else {
    assert(0);
    return false;
  }
  
    // pass back index list
  indices.resize( num_idx );
  std::copy( mIndices, mIndices+num_idx, indices.begin() );
  
    // apply target weight to value
  if (!num_idx) 
    weight( pd, s, e, num_idx, value, 0, 0, 0, err );
  else
    weight( pd, s, e, num_idx, value, arrptr(grad), 0, arrptr(Hessian), err ); 
  MSQ_ERRZERO(err);
  return rval;
}


bool ThTMetric::evaluate_with_Hessian_diagonal( 
                                           PatchData& pd,
                                           size_t handle,
                                           double& value,
                                           std::vector<size_t>& indices,
                                           std::vector<Vector3D>& grad,
                                           std::vector<SymMatrix3D>& diagonal,
                                           MsqError& err )
{
  const Sample s = ElemSampleQM::sample( handle );
  const size_t e = ElemSampleQM::  elem( handle );
  //#pragma omp critical
  MsqMeshEntity& elem = pd.element_by_index( e );
  EntityTopology type = elem.get_element_type();
  unsigned edim = TopologyInfo::dimension( type );
  size_t num_idx = 0;
  const NodeSet bits = pd.non_slave_node_set( e );
 
  //double delta = calculaDelta(pd,&Ntetra);
#ifdef miPRINT
  double delta = calculaDelta(pd);
  NtetraVisitados+=Ntetra2;
  std::cout << "ThTMetric::evaluate_with_hessian_diagonal-> delta= " << delta << std::endl ;
#endif

  bool rval;
  if (edim == 3) { // 3x3 or 3x2 targets ?
    const MappingFunction3D* mf = pd.get_mapping_function_3D( type );
    if (!mf) {
      MSQ_SETERR(err)( "No mapping function for element type", MsqError::UNSUPPORTED_ELEMENT );
      return false;
    }

    MsqMatrix<3,3> A, W, dmdT, d2mdT2[6];
    mf->jacobian( pd, e, bits, s, mIndices, mDerivs3D, num_idx, A, err );
    MSQ_ERRZERO(err);
    targetCalc->get_3D_target( pd, e, s, W, err ); MSQ_ERRZERO(err);
    const MsqMatrix<3,3> Winv = inverse(W);
    const MsqMatrix<3,3> T = A*Winv;
    rval = targetMetric->evaluate_with_hess( T, value, dmdT, d2mdT2, err );
    MSQ_ERRZERO(err);
    gradient<3>( num_idx, mDerivs3D, dmdT * transpose(Winv), grad );
    second_deriv_wrt_product_factor( d2mdT2, Winv );
    
    diagonal.resize( num_idx );
    hessian_diagonal<3>(num_idx, mDerivs3D, d2mdT2, arrptr(diagonal) );
#ifdef PRINT_INFO
    print_info<3>( e, s, A, W, A * inverse(W) );
#endif
    NtetraVisitados++;
  }
  else if (edim == 2) {
#ifdef NUMERICAL_2D_HESSIAN
    // use finite diference approximation for now
    return QualityMetric::evaluate_with_Hessian_diagonal( pd, handle,
                                           value, indices, grad, diagonal,
                                           err );
#else
    MsqMatrix<2,2> W, A, dmdT, d2mdT2[3];
    MsqMatrix<3,2> M;
    rval = evaluate_surface_common( pd, s, e, bits, mIndices, num_idx,
                             mDerivs2D, W, A, M, err ); 
    if (MSQ_CHKERR(err) || !rval)
      return false;
    const MsqMatrix<2,2> Winv = inverse(W);
    const MsqMatrix<2,2> T = A*Winv;
    rval = targetMetric->evaluate_with_hess( T, value, dmdT, d2mdT2, err );
    MSQ_ERRZERO(err);
    gradient<2>( num_idx, mDerivs2D, M * dmdT * transpose(Winv), grad );
    second_deriv_wrt_product_factor( d2mdT2, Winv );

    diagonal.resize( num_idx );
    for (size_t i = 0; i < num_idx; ++i) {
      MsqMatrix<2,2> block2d;
      block2d(0,0) = transpose(mDerivs2D[i]) * d2mdT2[0] * mDerivs2D[i];
      block2d(0,1) = transpose(mDerivs2D[i]) * d2mdT2[1] * mDerivs2D[i];
      block2d(1,0) = block2d(0,1);
      block2d(1,1) = transpose(mDerivs2D[i]) * d2mdT2[2] * mDerivs2D[i];
      MsqMatrix<3,2> p = M * block2d;
      
      SymMatrix3D& H = diagonal[i];
      H[0] = p.row(0) * transpose(M.row(0));
      H[1] = p.row(0) * transpose(M.row(1));
      H[2] = p.row(0) * transpose(M.row(2));
      H[3] = p.row(1) * transpose(M.row(1));
      H[4] = p.row(1) * transpose(M.row(2));
      H[5] = p.row(2) * transpose(M.row(2));
    }
#ifdef PRINT_INFO
    print_info<2>( e, s, J, Wp, A * inverse(W) );
#endif
#endif
  }
  else {
    assert(0);
    return false;
  }
  
    // pass back index list
  indices.resize( num_idx );
  std::copy( mIndices, mIndices+num_idx, indices.begin() );
  
    // apply target weight to value
  if (!num_idx) 
    weight( pd, s, e, num_idx, value, 0, 0, 0, err );
  else
    weight( pd, s, e, num_idx, value, arrptr(grad), arrptr(diagonal), 0, err ); 
  MSQ_ERRZERO(err);
  return rval;
}



} // namespace Mesquite

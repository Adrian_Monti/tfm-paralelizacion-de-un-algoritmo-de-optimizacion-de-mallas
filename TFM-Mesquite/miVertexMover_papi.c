#include "miVertexMover_papi.h"

const   PAPI_hw_info_t *hwinfo = NULL;
const   PAPI_mh_info_t *mh;

int TESTS_QUIET = 0;
static int TEST_FAIL = 0;
static int TEST_WARN = 0;

extern struct miPapiEventosNativos      localStructNativos;
extern struct miPapiEventosPreset       localStructPreset;

void miPapiInicializacion(void)
{
        int i, j;
        int retval;
        char * cache_types[20]={(char *)"0",(char *)"instrucciones",(char *)"datos",(char *)"unificada"};

        retval = PAPI_library_init( PAPI_VER_CURRENT );
        if ( retval != PAPI_VER_CURRENT ) printf("Error en miPapiInicializacion.c > PAPI_library_init\n");
        /* muestra por pantalla informacion de procesador */
        retval = papi_print_header((char *)"Informacion del hardware de ejecucion.\n", 0, &hwinfo );

        mh = &hwinfo->mem_hierarchy;
        printf("Informacion de la jerarquia de memoria cache\n");
        printf("Vendor string: %s\n", hwinfo->vendor_string);
        printf("Vendor string: %s\n", hwinfo->model_string);

        for ( i = 0; i < mh->levels; i++ ) {
         for ( j = 0; j < PAPI_MH_MAX_LEVELS; j++ ) {
             const PAPI_mh_cache_info_t *c = &mh->level[i].cache[j];
             /*const PAPI_mh_tlb_info_t *t = &mh->level[i].tlb[j];*/
             /*printf( "Level %d, TLB %d: %d, %d, %d\n", i, j, t->type,t->num_entries, t->associativity );*/
             if(c->type !=0) printf( "Level %2d, Cache %2d: %d(%13s), size: %8d, line_size:%4d, num_lines:%6d, associ:%2d\n", i+1, j, c->type, cache_types[c->type], c->size, c->line_size, c->num_lines, c->
associativity );
        }
       }
        printf("----------------------------------------------------------------------------\n");

        localStructNativos = listaPAPInativeEvents();
        localStructPreset  = listaPAPIpresetEvents();
}

int
papi_print_header( char *prompt, int event_flag,
                                   const PAPI_hw_info_t ** hwinfo )
{
        if ( ( *hwinfo = PAPI_get_hardware_info(  ) ) == NULL )
                return ( PAPI_ESBSTR );

        printf( "%s", prompt );
        printf
                ( "--------------------------------------------------------------------------------\n" );
        printf( "PAPI Version             : %d.%d.%d.%d\n",
                        PAPI_VERSION_MAJOR( PAPI_VERSION ),
                        PAPI_VERSION_MINOR( PAPI_VERSION ),
                        PAPI_VERSION_REVISION( PAPI_VERSION ),
                        PAPI_VERSION_INCREMENT( PAPI_VERSION ) );
        printf( "Vendor string and code   : %s (%d)\n", ( *hwinfo )->vendor_string,
                        ( *hwinfo )->vendor );
        printf( "Model string and code    : %s (%d)\n", ( *hwinfo )->model_string,
                        ( *hwinfo )->model );
        printf( "CPU Revision             : %f\n", ( *hwinfo )->revision );
        if ( ( *hwinfo )->cpuid_family > 0 )
                printf
                        ( "CPUID Info               : Family: %d  Model: %d  Stepping: %d\n",
                          ( *hwinfo )->cpuid_family, ( *hwinfo )->cpuid_model,
                          ( *hwinfo )->cpuid_stepping );
        printf( "CPU Megahertz            : %f\n", ( *hwinfo )->mhz );
        printf( "CPU Clock Megahertz      : %d\n", ( *hwinfo )->clock_mhz );
        if ( ( *hwinfo )->threads > 0 )
                printf( "Hdw Threads per core     : %d\n", ( *hwinfo )->threads );
        if ( ( *hwinfo )->cores > 0 )
                printf( "Cores per Socket         : %d\n", ( *hwinfo )->cores );
        if ( ( *hwinfo )->sockets > 0 )
                printf( "Sockets                  : %d\n", ( *hwinfo )->sockets );
        if ( ( *hwinfo )->nnodes > 0 )
                printf( "NUMA Nodes               : %d\n", ( *hwinfo )->nnodes );
        printf( "CPU's per Node           : %d\n", ( *hwinfo )->ncpu );
        printf( "Total CPU's              : %d\n", ( *hwinfo )->totalcpus );
        printf( "Number Hardware Counters : %d\n",
                        PAPI_get_opt( PAPI_MAX_HWCTRS, NULL ) );
        printf( "Max Multiplex Counters   : %d\n",
                        PAPI_get_opt( PAPI_MAX_MPX_CTRS, NULL ) );
        printf
                ( "--------------------------------------------------------------------------------\n" );
        if ( event_flag )
                printf
                        ( "The following correspond to fields in the PAPI_event_info_t structure.\n" );
        printf( "\n" );
        return ( PAPI_OK );
}

struct miPapiEventosNativos listaPAPInativeEvents(){
        int numcmp, cid=0, i, event_code, k, nNativeEvents;
        int retval;
        PAPI_event_info_t info, info1;
        const PAPI_component_info_t *s = NULL;
        struct miPapiEventosNativos localStructNativos;

        s = PAPI_get_component_info( 0 );
        //i = 0 | PAPI_NATIVE_MASK | PAPI_COMPONENT_MASK( cid );
        i = 0 | PAPI_NATIVE_MASK ;
        //i = 0;
        PAPI_enum_event( &i, PAPI_ENUM_FIRST );
        /*printf ("i: 0x%x\n",i);*/
        nNativeEvents = 0;
        localStructNativos.nNativeEvents=0;
        while (PAPI_enum_event( &i, PAPI_ENUM_EVENTS ) == PAPI_OK ){
                retval = PAPI_get_event_info( i, &info );
                if ( s->cntr_umasks ) {
                        k = i;
                        if ( PAPI_enum_event( &k, PAPI_NTV_ENUM_UMASKS ) == PAPI_OK ) {
                                do {
                                        retval = PAPI_get_event_info( k, &info1 );
                                        event_code = ( int ) info1.event_code;
#ifdef DEBUG
                                        printf ("Contador NATIVO tipo 1 del procesador, orden: %3i, ID: 0x%x, nombre: %50s, count: %i\n",nNativeEvents, event_code, info1.symbol, info1.count);
#endif
                                        nNativeEvents++;
                                        strcpy(localStructNativos.papiNativeEventos[localStructNativos.nNativeEvents], info1.symbol);
                                        localStructNativos.eventID[localStructNativos.nNativeEvents]= (int)info1.event_code;
                                        localStructNativos.eventCount[localStructNativos.nNativeEvents]= info1.count;
                                        localStructNativos.nNativeEvents++;
                                } while ( PAPI_enum_event( &k, PAPI_NTV_ENUM_UMASKS ) == PAPI_OK );
                        } else {
                                event_code = ( int ) info.event_code;
#ifdef DEBUG
                                printf ("Contador NATIVO tipo 2 del procesador, orden: %3i, ID: 0x%x, nombre: %50s, count: %i\n",nNativeEvents, event_code, info.symbol, info.count);
#endif
                                nNativeEvents++;
                                strcpy(localStructNativos.papiNativeEventos[localStructNativos.nNativeEvents], info.symbol);
                                localStructNativos.eventID[localStructNativos.nNativeEvents]= (int)info.event_code;
                                localStructNativos.eventCount[localStructNativos.nNativeEvents]= info.count;
                                localStructNativos.nNativeEvents++;
                        }
                } else {
                        event_code = ( int ) info.event_code;
                        //if ( s->cntr_groups ) event_code &= ~PAPI_NTV_GROUP_AND_MASK;
#ifdef DEBUG
                        printf ("Contador NATIVO tipo 3 del procesador, orden: %3i, ID: 0x%x, nombre: %50s, count: %i\n",nNativeEvents, event_code, info.symbol, info.count);
#endif
                        nNativeEvents++;
                        strcpy(localStructNativos.papiNativeEventos[localStructNativos.nNativeEvents], info.symbol);
                        localStructNativos.eventID[localStructNativos.nNativeEvents]= (int)info.event_code;
                        localStructNativos.eventCount[localStructNativos.nNativeEvents]= info.count;
                        localStructNativos.nNativeEvents++;
                }
        } /* end while */
#ifdef DEBUG
        for ( i = 0; i < localStructNativos.nNativeEvents; i++ ) {
                printf ("Contador PAPInativos, orden: %3i, ID: 0x%x, nombre: %50s, count: %i\n",i, localStructNativos.eventID[i], localStructNativos.papiNativeEventos[i], localStructNativos.eventCount[i]);
        }
#endif
        return localStructNativos;
}

/* contadores PRESET de PAPI */
struct  miPapiEventosPreset  listaPAPIpresetEvents(void)
{
        int i;
        PAPI_event_info_t info;
        struct miPapiEventosPreset localStruct;

        localStruct.nNativeEvents=0;
        for ( i = 0; i < PAPI_MAX_PRESET_EVENTS; i++ ) {
                if ( PAPI_get_event_info( PAPI_PRESET_MASK | i, &info ) == PAPI_OK ) {
                   if(info.count > 0){
                        strcpy(localStruct.papiNativeEventos[localStruct.nNativeEvents], info.symbol);
                        localStruct.eventID[localStruct.nNativeEvents]= (int)info.event_code;
                        localStruct.eventCount[localStruct.nNativeEvents]= info.count;
#ifdef DEBUG
                        printf ("Contador PAPIpreset tipo 4 del procesador, orden: %3i, ID: 0x%x, nombre: %14s, count: %i\n",localStruct.nNativeEvents, (int)info.event_code, info.symbol, info.count);
#endif
                        localStruct.nNativeEvents++;
                   } else {
#ifdef DEBUG
                        printf ("Contador PAPIpreset NO POSIBLE, ID: 0x%x, nombre: %14s, count: %i\n",(int)info.event_code, info.symbol, info.count);
#endif
                   }
                }
        }
        for ( i = 0; i < localStruct.nNativeEvents; i++ ) {
#ifdef DEBUG
                printf ("Contador PAPIpreset, orden: %3i, ID: 0x%x, nombre: %14s, count: %i\n",i, localStruct.eventID[i], localStruct.papiNativeEventos[i], localStruct.eventCount[i]);
#endif
        }
        return localStruct;
}



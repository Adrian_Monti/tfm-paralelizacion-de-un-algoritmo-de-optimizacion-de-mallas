/* ***************************************************************** 
    MESQUITE -- The Mesh Quality Improvement Toolkit

    Copyright 2007 Sandia National Laboratories.  Developed at the
    University of Wisconsin--Madison under SNL contract number
    624796.  The U.S. Government and the University of Wisconsin
    retain certain rights to this software.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License 
    (lgpl.txt) along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    (2008) kraftche@cae.wisc.edu    
    (2013) dbenitez@siani.es

  ***************************************************************** */


/** \file hSigmaMetric3.hpp
 *  \brief 
 *  \author Domingo Benitez
 *  Calcula la metrica ||S||^2 / 3 (h(sigma))^2/3 de un elemento 2D (triangulo) o 3D (tetraedro)
 */

#ifndef MSQ_H_SIGMA3_HPP
#define MSQ_H_SIGMA3_HPP

#include "Mesquite.hpp"
#include "TMetric.hpp"
#include "MsqVertex.hpp"
#include <limits>
#include <iostream>
#include <vector>
#include "PatchSet.hpp"

namespace MESQUITE_NS {

/** \f$ \frac{|T|^2}{3 h(det(T))^2/3}\f$ */

class hSigmaMetric3 : public TMetric {
public:

  Mesh::VertexHandle handleVerticesCache[100]; // Funciona, vector para saber si se activa la cache que permite proporcionar la delta anterior

  MESQUITE_EXPORT 
  hSigmaMetric3(double delta, double epsilon);
  hSigmaMetric3(size_t nodosMalla);

  MESQUITE_EXPORT 
  hSigmaMetric3(void);

  MESQUITE_EXPORT virtual
  ~hSigmaMetric3();

  MESQUITE_EXPORT virtual
  std::string get_name() const;

  MESQUITE_EXPORT void set_delta(double delta_in)
	{mDelta = delta_in;}

  MESQUITE_EXPORT void set_epsilon(double epsilon_in)
	{mEpsilon = epsilon_in;}

  MESQUITE_EXPORT double get_delta()
	{return mDelta;}

  MESQUITE_EXPORT double get_epsilon()
	{return mEpsilon;}

	MESQUITE_EXPORT
    int get_negate_flag( ) const { return -1; }

  MESQUITE_EXPORT virtual
  bool evaluate( const MsqMatrix<2,2>& T, 
                 double& result, 
                 MsqError& err );

  /** \f$ \frac{1}{det(T)} [ T - \frac{|T|^2}{2 det(T)}adj(T) ] \f$ */
  MESQUITE_EXPORT virtual
  bool evaluate_with_grad( const MsqMatrix<2,2>& T,
                           double& result,
                           MsqMatrix<2,2>& deriv_wrt_T,
                           MsqError& err );

  MESQUITE_EXPORT virtual
  bool evaluate_with_hess( const MsqMatrix<2,2>& T,
                           double& result,
                           MsqMatrix<2,2>& deriv_wrt_T,
                           MsqMatrix<2,2> second_wrt_T[3],
                           MsqError& err );

  MESQUITE_EXPORT virtual
  bool evaluate( const MsqMatrix<3,3>& T, 
                 double& result,
                 MsqError& err );

  MESQUITE_EXPORT virtual
  bool evaluate_with_grad( const MsqMatrix<3,3>& T,
                           double& result,
                           MsqMatrix<3,3>& deriv_wrt_T,
                           MsqError& err );
  
  MESQUITE_EXPORT virtual
  bool evaluate_with_hess( const MsqMatrix<3,3>& T,
                           double& result,
                           MsqMatrix<3,3>& deriv_wrt_T,
                           MsqMatrix<3,3> second_wrt_T[6],
                           MsqError& err );

  MESQUITE_EXPORT virtual
  void setNumeroVertices(size_t nodosMalla);

private:
  double mDelta;
  double mEpsilon;
  size_t Nvertices;
};


} // namespace Mesquite

#endif

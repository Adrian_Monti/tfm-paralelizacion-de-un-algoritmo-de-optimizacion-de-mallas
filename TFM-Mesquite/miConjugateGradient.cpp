/* ***************************************************************** 
    MESQUITE -- The Mesh Quality Improvement Toolkit

    Copyright 2004 Sandia Corporation and Argonne National
    Laboratory.  Under the terms of Contract DE-AC04-94AL85000 
    with Sandia Corporation, the U.S. Government retains certain 
    rights in this software.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License 
    (lgpl.txt) along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 
    diachin2@llnl.gov, djmelan@sandia.gov, mbrewer@sandia.gov, 
    pknupp@sandia.gov, tleurent@mcs.anl.gov, tmunson@mcs.anl.gov      
   
  ***************************************************************** */
/*!
  \file   miConjugateGradient.cpp
  \brief  

  The Conjugate Gradient class is a concrete vertex mover
  which performs conjugate gradient minimizaiton.

  \author Michael Brewer
  \date   2002-06-19
*/
//#define debugPrintf
//#define intervaloNumSeguridad DBL_EPSILON * 1e4 

#include "miConjugateGradient.hpp"
#include <math.h>
#include "MsqDebug.hpp"
#include "MsqTimer.hpp"
//#include "MsqFreeVertexIndexIterator.hpp"
#include "hSigmaMetric.hpp"
#include "hSigmaBarrierObjFunc.hpp"
#include "ThSigmaMetric.hpp"
#include "IdealShapeTarget.hpp"
#include "ElementPMeanP.hpp"
#include "LPtoPTemplate.hpp"
#include "TQualityMetric.hpp"
//#include "miIdealWeightMeanRatio.hpp"
#include "AveragingQM.hpp"
#include "ObjectiveFunction.hpp"
#include "ThMixedQ.hpp"
#include "ThTMetric.hpp"
#include "logBarrierObjFunc.hpp"
#include "logBarrierObjFunc2.hpp"

namespace MESQUITE_NS {

extern int get_parallel_rank();
extern int get_parallel_size();

std::string miConjugateGradient::get_name() const 
  { return "miConjugateGradient"; }
  
PatchSet* miConjugateGradient::get_patch_set()
  { return PatchSetUser::get_patch_set(); }

miConjugateGradient::miConjugateGradient(ObjectiveFunction* objective) :
  VertexMover(objective),
  PatchSetUser(true),
  pMemento(NULL),
  Npatches(0),
  conjGradDebug(0)
{ }  

miConjugateGradient::miConjugateGradient(ObjectiveFunction* objective,
                                     MsqError &err) :
  VertexMover(objective),
  PatchSetUser(true),
  pMemento(NULL),
  Npatches(0),
  conjGradDebug(0)
{
    //Michael:: default to global?
  set_debugging_level(0);
    //set the default inner termination criterion
  TerminationCriterion* default_crit=get_inner_termination_criterion();
  if(default_crit==NULL){
    MSQ_SETERR(err)("QualityImprover did not create a default inner "
                    "termination criterion.", MsqError::INVALID_STATE);
    return;
  }
  else{
    default_crit->add_iteration_limit( 5 );
    MSQ_ERRRTN(err);
  }
  
}  


miConjugateGradient::~miConjugateGradient()
{
  // Checks that cleanup() has been called. 
  assert(pMemento==NULL);
}
  
void miConjugateGradient::initialize(PatchData &pd, MsqError &err)
{
  if (get_parallel_size())
    MSQ_DBGOUT(2) << "\nP[" << get_parallel_rank() << "] " << "o   Performing Conjugate Gradient optimization.\n";
  else
    MSQ_DBGOUT(2) << "\no   Performing Conjugate Gradient optimization.\n";
  pMemento=pd.create_vertices_memento(err);
}

size_t miConjugateGradient::count_inverted( PatchData& pd, MsqError& err )
{
   size_t num_elem = pd.num_elements();
   size_t count=0;
   int inverted, samples;
   for (size_t i = 0; i < num_elem; i++) {
	pd.element_by_index(i).check_element_orientation(pd, inverted, samples, err);
	if (inverted)
	++count;
   }
   return count;
}

size_t miConjugateGradient::count_Npatches()
{
	return Npatches;
}
void miConjugateGradient::set_NpatchesInterior(size_t Npatches) // funcion que escribe en miConjugateGradient el numero de patches del interior de la particion
{
	NpatchesInterior = Npatches;
}
void miConjugateGradient::set_NpatchesFrontera(size_t Npatches) // funcion que escribe en miConjugateGradient el numero de patches del frontera de la particion
{
	NpatchesFrontera = Npatches;
}
void miConjugateGradient::initialize_mesh_iteration(PatchData &/*pd*/,
                                                  MsqError &/*err*/)
{
  
}

/*!Performs Conjugate gradient minimization on the PatchData, pd.*/
void miConjugateGradient::optimize_vertex_positions(PatchData &pd, 
                                                MsqError &err){
#ifdef debugPrintf
  // fuerzo que calcule la funcion objetivo para ver que valores tiene
  OFEvaluator& objFunc2 = get_objective_function_evaluator();
  double fnew;
  objFunc2.evaluate(pd,fnew, err);
  printf("\nP[%i] miConjugateGradient::optimize_vertex_positions -ENTRADA, OF= %8.3e\n",get_parallel_rank(),fnew); 
#endif

  std::vector< Vector3D > coords_out ;
  Timer c_timer;
  size_t num_vert=pd.num_free_vertices();
  TerminationCriterion* term_crit=get_inner_termination_criterion();
  
// para calcular la calidad del patch
  hSigmaMetric tm(1.2e-3, 1.0e-10);
  IdealShapeTarget target;
  ThSigmaMetric m1( &target, &tm );
  //TQualityMetric m1( &target, &tm );
  //IdealWeightMeanRatio m1();
  ElementPMeanP untangle_metric(-1, &m1);
  //AveragingQM untangle_metric(m1);
  LPtoPTemplate untangle_func( 1, &untangle_metric );
  double Q;
/////////

  // pd.reorder();

  MSQ_FUNCTION_TIMER( "miConjugateGradient::optimize_vertex_positions" );


#ifdef debugPrintf
  printf("\nP[%i] miConjugateGradient::optimize_vertex_positions - informacion del patchData - Inicio interaciones internas\n", get_parallel_rank() );
  printf("P[%i]\tnumero vertices: %i, vertices libres: %i\n", get_parallel_rank(), (int)pd.num_nodes(), (int)num_vert);
  printf("P[%i]\tnumero elementos: %i, elementos enredados: %i\n", get_parallel_rank(), (int)pd.num_elements(), (int)count_inverted(pd, err));
  std::cout << "P[" << get_parallel_rank() << "] Npatches= " <<  Npatches  << std::endl;
#endif

  if(num_vert<1){
     MSQ_DBGOUT(1) << "\nEmpty free vertex list in miConjugateGradient\n";
     return;
  }
/*  
    //zero out arrays
  int zero_loop=0;
  while(zero_loop<arraySize){
    fGrad[zero_loop].set(0,0,0);
    pGrad[zero_loop].set(0,0,0);
    fNewGrad[zero_loop].set(0,0,0);
    ++zero_loop;
  }
*/
  
  // get OF evaluator
  OFEvaluator& objFunc = get_objective_function_evaluator();

  // se obtiene la metrica ThSigmaMetric para leer las medidas de prestaciones
/*
  ObjectiveFunctionTemplate* OF = (ObjectiveFunctionTemplate *) objFunc.get_objective_function();
  QualityMetric* quaMetr = OF->get_quality_metric();
  ElemSampleQM* ESQM = (ElemSampleQM *) quaMetr;
  TMPQualityMetric* ESQM2 = (TMPQualityMetric *) ESQM;
  ThSigmaMetric * QM = (ThSigmaMetric *) ESQM2;
  std::cout << "\n miCG - NtetraVisitados: " << QM->get_NtetraVisitados() << std::endl;
*/

  size_t ind;
    //Michael cull list:  possibly set soft_fixed flags here
  
   //MsqFreeVertexIndexIterator free_iter(pd, err);  MSQ_ERRRTN(err);
  
      
   double f=0;
   //Michael, this isn't equivalent to CUBIT because we only want to check
   //the objective function value of the 'bad' elements
   //if invalid initial patch set an error.

//
// TMP
for(int kk = 0; kk < 1; kk++){ 
// bucle Jose Maria, cambiando kk se fuerzan un numero de cambios de la barrera antes de pasar a otra estrella

    if( dynamic_cast<hSigmaBarrierObjFunc*>(objFunc.get_objective_function()) != NULL){ 
    // solo se activa si la funcion objetivo es tipo hSigmaBarrier
    //	hSigmaBarrierObjFunc* OFr = (hSigmaBarrierObjFunc*) objFunc.get_objective_function();
	/* Qmin tal que Dmax = Dmax_i * 1,01 */ //double Ddumy = OFr->get_calidad_minima_patch(pd);
	  					  //Ddumy *= 1.01; // para 1_h
	  					  //Ddumy *= 0.99; // para 1_hPrima
    //	OFr->set_calidad_minima(Ddumy); // inicializamos valor de Qmin de la funcion log-barrier
#ifdef debugPrintf
/*
  	const char* Cdumy = OFr->get_quality_metric()->get_name().c_str();
  	printf("miCG - hSigmaBarrierObjFunc - Metrica= %s, iterJM= %i\n", Cdumy, kk);
	//double Dmu = OFr->get_mu();
	//printf("miCG - hSigmaBarrier - Calidad max  patch en bounding box= %9.4e, mu= %9.2e\n", Ddumy, Dmu);
	printf("miCG - Nueva barrera Calidad max hSigma-barrier= %9.4e\n", OFr->get_calidad_minima_ObjFunc());
*/
#endif
    }
//
  logBarrierObjFunc2* OFdumy = dynamic_cast<logBarrierObjFunc2*>(objFunc.get_objective_function()); // si OFdumy!=NULL, la funcion objetivo es log-barrier
  double DumyMuInicial, Qmax, QmaxObjFunc;
  long int orden_max;
  if(OFdumy != NULL ){ // solo se activa si la funcion objetivo es de tipo log-barrier
  	logBarrierObjFunc2* OFr = (logBarrierObjFunc2*) objFunc.get_objective_function();
	//OFr->set_mu(0.1);
	DumyMuInicial = OFr->get_mu();
	Qmax = OFr -> get_calidad_minima_patch(pd, orden_max);
#ifdef debugPrintf
  	printf("miCG - OF es: %s, MuInicial= %f, Qmax_patch= %8.3e, orden_max= %li\n", OFdumy != NULL ? "logBarrier" : "null", DumyMuInicial, Qmax, orden_max);
#endif
  }
// TMP


#ifdef debugPrintf
  printf("\nP[%i] miConjugateGradient::optimize_vertex_positions - objFunc.update\n",get_parallel_rank()); 
#endif
   bool temp_bool = objFunc.update(pd, f, fGrad, err);
   assert(fGrad.size() == num_vert);
   if(MSQ_CHKERR(err))
      return;
  if( ! temp_bool){
    MSQ_SETERR(err)("Conjugate Gradient not able to get valid gradient "
                    "and function values on intial patch.", 
                    MsqError::INVALID_MESH);
    return;
  }
  double grad_norm=MSQ_MAX_CAP;
  
  if(conjGradDebug>0){
    MSQ_PRINT(2)("\nCG's DEGUB LEVEL = %i \n",conjGradDebug);
    grad_norm=Linf(arrptr(fGrad),fGrad.size());
    MSQ_PRINT(2)("\nCG's FIRST VALUE = %f,grad_norm = %f",f,grad_norm);
    MSQ_PRINT(2)("\n   TIME %f",c_timer.since_birth());
    grad_norm=MSQ_MAX_CAP;
  }

    //Initializing pGrad (search direction).  
  pGrad.resize(fGrad.size());
  for (ind = 0; ind < num_vert; ++ind)
    pGrad[ind]=(-fGrad[ind]);

  int j=0; // total nb of step size changes ... not used much
  int i=0; // iteration counter
  unsigned m=0; // 
  double alp=MSQ_MAX_CAP; // alp: scale factor of search direction

  //we know inner_criterion is false because it was checked in
  //loop_over_mesh before being sent here.
  //TerminationCriterion* term_crit=get_inner_termination_criterion();



  while(!term_crit->terminate()){
    ++i;
//
// TMP
/*
    if(OFdumy != NULL ){ 
    // solo se activa si la funcion objetivo es de tipo log-barrier
  	logBarrierObjFunc2* OFr = (logBarrierObjFunc2*) objFunc.get_objective_function();
	double Ddumy = OFr->get_mu();
	//OFr->set_mu(Ddumy*0.90);
	OFr->set_mu(Ddumy*1.00);
	Qmax = OFr->get_calidad_minima_patch(pd, orden_max);
	QmaxObjFunc = OFr->get_calidad_maxima_ObjFunc();
	OFr->set_calidad_maxima(Qmax);
#ifdef debugPrintf
  	printf("miCG - OF log-barrier - nuevo MU: %f,  Qmax_patch= %8.3e, orden_max= %li, nuevo Qmax_ObjFunc= %8.3e\n", OFr->get_mu(), Qmax, orden_max,  OFr->get_calidad_maxima_ObjFunc());
#endif
    }
    if( dynamic_cast<hSigmaBarrierObjFunc*>(objFunc.get_objective_function()) != NULL){ 
    // solo se activa si la funcion objetivo es tipo hSigmaBarrier
  	hSigmaBarrierObjFunc* OFr = (hSigmaBarrierObjFunc*) objFunc.get_objective_function();
	// Qmin tal que Dmax = Dmax_i * 1,01  //double Ddumy = OFr->get_calidad_minima_patch(pd);
	  					  //Ddumy *= 1.01; // para 1_h
	  					  //Ddumy *= 0.99; // para 1_hPrima
	//OFr->set_calidad_minima(Ddumy); // inicializamos valor de Qmin de la funcion log-barrier
#ifdef debugPrintf
  	const char* Cdumy = OFr->get_quality_metric()->get_name().c_str();
  	printf("miCG - hSigmaBarrierObjFunc - Metrica= %s\n", Cdumy);
	double Dmu = OFr->get_mu();
	//printf("miCG - hSigmaBarrier - Calidad max  patch en bounding box= %9.4e, mu= %9.2e\n", Ddumy, Dmu);
	printf("miCG - Nueva barrera Calidad max hSigma-barrier= %9.4e\n", OFr->get_calidad_minima_ObjFunc());
#endif
    }
*/
// TMP
//

#ifdef debugPrintf
    ObjectiveFunctionTemplate* OF = (ObjectiveFunctionTemplate *) objFunc.get_objective_function();
    QualityMetric* quaMetr = OF->get_quality_metric();
    ElemSampleQM* ESQM = (ElemSampleQM *) quaMetr;
    TMPQualityMetric* ESQM2 = (TMPQualityMetric *) ESQM;
    /* BUENO */ ThSigmaMetric * QM = (ThSigmaMetric *) ESQM2;
    /* TMP */ //ThMixedQ * QM = (ThMixedQ *) ESQM2;
    /* TMP */ //ThTMetric * QM = (ThTMetric *) ESQM2;

    pd.get_free_vertex_coordinates(coords_out);
    //untangle_func.evaluate(ObjectiveFunction::CALCULATE,pd,Q,1,err); // se obtiene la calidad Q del patch siguiento la metrica con hSigma invertida (-1)
    OF->evaluate(ObjectiveFunction::CALCULATE,pd,Q,1,err); // se obtiene la calidad Q del patch siguiento la metrica con hSigma invertida (-1)
    //printf("P[%i] miCG::o_v_p - \n", get_parallel_rank() );

    printf("\nP[%i]\t miCG-iterINTERNA: %2i, OF: %7.1e, QhSigma: %6.3f, NpatchesAccedidos: %lu, NtetraVisitados: %lu\n", get_parallel_rank(), i, f, Q, Npatches, QM->get_NtetraVisitados());
    //printf("\nP[%i]\t miCG-iterINTERNA: %2i, OF: %7.1e, Q: %6.3f, fGrad: ", get_parallel_rank(), i, f, Q);
    //std::cout << fGrad[0] << ",\tcoordFreeVtx: " << coords_out[0] << std::endl;
    std::cout << "P[" << get_parallel_rank() << "]\t" <<  fGrad[0] << ",\n\tcoordFreeVtx - gradiente - normaGrad (" << num_vert << " freeVTXs): " << std::endl;
    for(m=0; m<num_vert; m++){
      std::cout << "P[" << get_parallel_rank() << "]\t[freeVTX: " << m << "]: " << coords_out[m] << "\t" << fGrad[m] << "\t" << fGrad[m].length() << std::endl;
    }
#endif


#ifdef debugPrintf
  printf("\nP[%i] miConjugateGradient::optimize_vertex_positions - llama get_step_1\n",get_parallel_rank()); 
#endif
    int k=0;
    alp=get_step(pd,f,k,err);
    j+=k;
    if(conjGradDebug>2){ MSQ_PRINT(2)("\n  Alp initial, alp = %20.18f",alp); }

     // if alp == 0, revert to steepest descent search direction
    if(alp==0){
      for (m = 0; m < num_vert; ++m) {
        pGrad[m]=(-fGrad[m]);
      }
#ifdef debugPrintf
  printf("\nP[%i] miConjugateGradient::optimize_vertex_positions - llama get_step_2, alp= %8.2e\n",get_parallel_rank(),alp); 
#endif
      alp=get_step(pd,f,k,err);
      j+=k;
      if(conjGradDebug>1){
        MSQ_PRINT(2)("\n CG's search direction reset.");
        if(conjGradDebug>2)
          MSQ_PRINT(2)("\n  Alp was zero, alp = %20.18f",alp);
      }
      
    }
    if(alp!=0){
#ifdef debugPrintf
  printf("\nP[%i] miConjugateGradient::optimize_vertex_positions - move_freeVTX\n",get_parallel_rank()); 
#endif
      pd.move_free_vertices_constrained( arrptr(pGrad), num_vert, alp, err );
      MSQ_ERRRTN(err);

      //Npatches++; // cuenta el numero de veces que accede a un patch completo
      
      if (! objFunc.update(pd, f, fNewGrad, err)){
        MSQ_SETERR(err)("Error inside Conjugate Gradient, vertices moved "
                        "making function value invalid.", 
                        MsqError::INVALID_MESH);
        return;
      }
      assert(fNewGrad.size() == (unsigned)num_vert);
      
      if(conjGradDebug>0){
        grad_norm=Linf(arrptr(fNewGrad),num_vert);
        MSQ_PRINT(2)("\nCG's VALUE = %f,  iter. = %i,  grad_norm = %f,  alp = %f",f,i,grad_norm,alp);
        MSQ_PRINT(2)("\n   TIME %f",c_timer.since_birth());
      }
      double s11=0;
      double s12=0;
      double s22=0;
      //free_iter.reset();
      //while (free_iter.next()) {
      //  m=free_iter.value();
      for (m = 0; m < num_vert; ++m) {
        s11+=fGrad[m]%fGrad[m];
        s12+=fGrad[m]%fNewGrad[m];
        s22+=fNewGrad[m]%fNewGrad[m];
      }
      
        // Steepest Descent (takes 2-3 times as long as P-R)
        //double bet=0;
      
        // Fletcher-Reeves (takes twice as long as P-R)
        //double bet = s22/s11;

        // Polack-Ribiere  
      double bet;  
      if (!divide( s22-s12, s11, bet ))
        return; // gradient is zero    
      //free_iter.reset();
      //while (free_iter.next()) {
      //  m=free_iter.value();
      for (m = 0; m < num_vert; ++m) {
        pGrad[m]=(-fNewGrad[m]+(bet*pGrad[m]));
        fGrad[m]=fNewGrad[m];
      }
      if(conjGradDebug>2){
        MSQ_PRINT(2)(" \nSEARCH DIRECTION INFINITY NORM = %e",
                   Linf(arrptr(fNewGrad),num_vert));
      }
      
    }//end if on alp == 0

#ifdef debugPrintf
    printf("\nP[%i] miConjugateGradient::optimize_vertex_positions - actualiza patch\n",get_parallel_rank()); 
    double fnew;
    objFunc.evaluate(pd,fnew, err);
#endif

    term_crit->accumulate_patch( pd, err ); MSQ_ERRRTN(err);
    term_crit->accumulate_inner( pd, f, arrptr(fGrad), err );  MSQ_ERRRTN(err);

#ifdef debugPrintf
    printf("\nP[%i]\tFin IteracionInterna: %i -  NpatchesAccedidos: %lu, NtetraVisitados: %lu\n", get_parallel_rank(), i, Npatches, QM->get_NtetraVisitados());
#endif

  } //end while(!term_crit->terminate()){


  if(conjGradDebug>0){
    MSQ_PRINT(2)("\nConjugate Gradient complete i=%i ",i);
    MSQ_PRINT(2)("\n-  FINAL value = %f, alp=%4.2e grad_norm=%4.2e",f,alp,grad_norm);
    MSQ_PRINT(2)("\n   FINAL TIME %f",c_timer.since_birth());
  }

} // for kk

/*
// TMP
  if(OFdumy != NULL ){ // solo se activa si la funcion objetivo es de tipo log-barrier
  	logBarrierObjFunc* OFr = (logBarrierObjFunc*) objFunc.get_objective_function();
	OFr->set_mu(DumyMuInicial);
  }
// TMP
*/

#ifdef debugPrintf
  printf("\nP[%i] miConjugateGradient::optimize_vertex_positions - informacion del patchData - Fin interaciones internas\n", get_parallel_rank() );
  printf("P[%i]\tnumero vertices: %i, vertices libres: %i\n", get_parallel_rank(), (int)pd.num_nodes(), (int)num_vert);
  printf("P[%i]\tnumero elementos: %i, elementos enredados: %i\n", get_parallel_rank(), (int)pd.num_elements(), (int)count_inverted(pd, err));
    std::cout << "P[" << get_parallel_rank() << "] \tNpatchesAccedidos= " <<  Npatches  << std::endl;
#endif

}


void miConjugateGradient::terminate_mesh_iteration(PatchData &/*pd*/,
                                                 MsqError &/*err*/)
{
    //  cout << "- Executing miConjugateGradient::iteration_complete()\n";
}


void miConjugateGradient::cleanup()
{
    //  cout << "- Executing miConjugateGradient::iteration_end()\n";
  fGrad.clear();
  pGrad.clear();
  fNewGrad.clear();
    //pMemento->~PatchDataVerticesMemento();
  delete pMemento;
  pMemento = NULL;
}

//!Computes a distance to move vertices given an initial position and search direction (stored in data member pGrad).
/*!Returns alp, the double which scales the search direction vector
  which when added to the old nodal positions yields the new nodal
  positions.*/
/*!\todo Michael NOTE:  miConjugateGradient::get_step's int &j is only
  to remain consisitent with CUBIT for an initial test.  It can be
  removed.*/

double miConjugateGradient::get_step(PatchData &pd,double f0,int &j,
                                   MsqError &err)
{
  // get OF evaluator
  OFEvaluator& objFunc = get_objective_function_evaluator();

  size_t num_vertices=pd.num_free_vertices();

    //initial guess for alp
  double alp=1.0;
  int 	 jmax=100;
  double rho=0.5;

    //feasible=false implies the mesh is not in the feasible region
  bool 	 feasible=false;
  int 	 found=0;

    //f and fnew hold the objective function value
  double f=0;
  double fnew=0;

    //Counter to avoid infinitly scaling alp
  j=0;

  //save memento
  pd.recreate_vertices_memento(pMemento, err);

    //if we must check feasiblility
    //while step takes mesh into infeasible region and ...
    // PASO 1
  while (j<jmax && !feasible && alp>MSQ_MIN) {
    ++j;
    Npatches++; // cuenta el numero de veces que optimiza un patch completo
    pd.set_free_vertices_constrained(pMemento,arrptr(pGrad),num_vertices,alp,err);
    feasible=objFunc.evaluate(pd,f,err);
    if(err.error_code() == err.BARRIER_VIOLATED)
      err.clear();  // barrier violation does not represent an actual error here
    MSQ_ERRZERO(err);

#ifdef debugPrintf
  printf("\tP[%i] miCG::get_step - Fin Paso1_feasible, iter: %i, OF: %4.4e, found: %i, feasible: %i, alp: %4.1e\n\n",get_parallel_rank(),j,f,found,feasible,alp); 
#endif

      //if not feasible, try a smaller alp (take smaller step)
    if(!feasible){
      alp*=rho;
    }

  }//end while ...
  
    //if above while ended due to j>=jmax, no valid step was found.
  if(j>=jmax){
    MSQ_PRINT(2)("\nFeasible Point Not Found");
    return 0.0;
  }

    //Message::print_info("\nOriginal f %f, first new f = %f, alp = %f",f0,f,alp);
    //if new f is larger than original, our step was too large
    // PASO 2
  if(f>=f0){
    j=0;
    while (j<jmax && found == 0){
      ++j;
      Npatches++; // cuenta el numero de veces que optimiza un patch completo
      alp *= rho;
      pd.set_free_vertices_constrained(pMemento,arrptr(pGrad),num_vertices,alp,err);
        //Get new obj value
        //if patch is now invalid, then the feasible region is  convex or
        //we have an error.  For now, we assume an error.
      if(! objFunc.evaluate(pd,f,err) ){
/* ORIGINAL
        MSQ_SETERR(err)("Non-convex feasiblility region found.",MsqError::INVALID_MESH);
*/
      }
      pd.set_to_vertices_memento(pMemento,err);MSQ_ERRZERO(err);
        //if our step has now improved the objective function value
      //if(f + intervaloNumSeguridad < f0){ // intervaloNumSeguridad se intorudce por errores numericos que hace que se cumpla la condicion pra f muy proximas a f0
      //if(f + DBL_EPSILON * 1e4 < f0){
      // originalMesquite - 

      if(f<f0){
        found=1;
      }

#ifdef debugPrintf
  //printf("\tP[%i] miCG:get_step-Npaso2_f>f0: %i, objFun0: %4.4e, objFunIter: %4.4e, found: %i, feasib: %i, alp: %4.1e\n",get_parallel_rank(),j,f0,f,found,feasible,alp); 
  printf("\tP[%i] miCG:get_step - Fin Paso2_f>f0: %i, OFiter-OF0: %4.4e, found: %i, feasible: %i, alp: %4.1e\n\n",get_parallel_rank(),j,f-f0,found,feasible,alp); 
#endif

    }//   end while j less than jmax

      //Message::print_info("\nj = %d found = %d f = %20.18f f0 = %20.18f\n",j,found,f,f0);
      //if above ended because of j>=jmax, take no step
    if(found==0){
#ifdef debugPrintf
      printf("No se puede reducir la OF, found=0, Returning alp=zero, alp = %10.8f\n",alp);
#endif
      alp=0.0; 
      return alp;
    }

    j=0;
      //while shrinking the step improves the objFunc value further,
      //scale alp down.  Return alp, when scaling once more would
      //no longer improve the objFunc value.  
    while(j<jmax){
      ++j;
      Npatches++; // cuenta el numero de veces que optimiza un patch completo
      alp*=rho;
      //step alp in search direction from original positions
      pd.set_free_vertices_constrained(pMemento,arrptr(pGrad),num_vertices,alp,err);MSQ_ERRZERO(err);

        //get new objective function value
      if (! objFunc.evaluate(pd,fnew,err)){
// modificacion para la funcion log-barrier
	pd.set_to_vertices_memento(pMemento,err);MSQ_ERRZERO(err);
	alp/=rho;
	return alp;
        //MSQ_SETERR(err)("Non-convex feasiblility region found while "
        //                "computing new f.",MsqError::INVALID_MESH);
      }
#ifdef debugPrintf
  printf("\tP[%i] miCG::get_step - Paso3_shrinking, iter: %i, OF: %4.4e, found: %i, feasible: %i, alp: %4.1e\n\n",get_parallel_rank(),j,f,found,feasible,alp); 
#endif
      if(fnew<f){
        f=fnew;
      }
      else{
	//Reset the vertices to original position
	pd.set_to_vertices_memento(pMemento,err);MSQ_ERRZERO(err);
	alp/=rho;
	return alp;
      }
    }
    //Reset the vertices to original position and return alp
    pd.set_to_vertices_memento(pMemento,err);MSQ_ERRZERO(err);
    return alp;
  }
    //else our new f was already smaller than our original
  else{
    j=0;
      //check to see how large of step we can take
    while (j<jmax && found == 0) {
      	++j;
      	Npatches++; // cuenta el numero de veces que optimiza un patch completo
        //scale alp up (rho must be less than 1)
      	alp /= rho;
      	//step alp in search direction from original positions
      	pd.set_free_vertices_constrained(pMemento,arrptr(pGrad),num_vertices,alp,err);MSQ_ERRZERO(err);

      	feasible = objFunc.evaluate(pd,fnew, err);
      	if(err.error_code() == err.BARRIER_VIOLATED)
          err.clear();  // evaluate() error does not represent an actual problem here
      	MSQ_ERRZERO(err);
#ifdef debugPrintf
  	printf("\tP[%i] miCG::get_step - Fin Paso4_alpMayor, iter: %i, OFnew: %4.4e, feasible: %i, alp: %4.1e",get_parallel_rank(),j,fnew,feasible,alp); 
#endif
      	if ( ! feasible ){
          alp *= rho;
          //Reset the vertices to original position and return alp
          pd.set_to_vertices_memento(pMemento,err);MSQ_ERRZERO(err);
          return alp;
      	}
      	if (fnew<f) { 
          f = fnew; 
       	}
       	else {
          found=1;
          alp *= rho;
#ifdef debugPrintf
  	  printf(", alpSalida: %4.1e",alp); 
#endif
       	}
#ifdef debugPrintf
  	printf(", found: %i\n",found); 
#endif
    } // end while

    //Reset the vertices to original position and return alp
    pd.set_to_vertices_memento(pMemento,err);MSQ_ERRZERO(err);
    return alp;
  }
}

/*!Quadratic one-dimensional line search.*/
/*
double miConjugateGradient::get_step(PatchData &pd,double f0,int &j,
                                   MsqError &err){
  const double CGOLD = 0.3819660;
  const double ZEPS = 1.0e-10;
  int n=pd.num_free_vertices();
  MsqVertex* vertices=pd.get_vertex_array(err);
  double a,b,d,etemp,fb,fu,fv,fw,fx,p,q,r,tol,tol1,tol2,u,v,w,x,xm;
  double e=0.0;
  d=0.0;
  tol=.001;
  int iter, maxiter;
  maxiter=100;
  a=0;
  b=.125;
  int m=0;
  fb=f0-1.0;
  iter=0;
  //find b such that a b 'should' bracket the min
  while (fb<=f0 && iter<maxiter){
    ++iter;
    b*=2.0;
    for(m=0;m<n;++m){
      mCoord[m]=mCoord[m] + (b*pGrad[m]);
      vertices[m]=(mCoord[m]);
    }
    fb=objFunc->evaluate(pd,err);
  }
  iter=0;
  x=w=v=(b/2.0);
  for(m=0;m<n;++m){
    mCoord[m]=mCoord[m] + (x*pGrad[m]);
    vertices[m]=(mCoord[m]);
  }
  fw=fv=fx=objFunc->evaluate(pd,err);
  for(iter=0;iter<maxiter;++iter){
      //Message::print_info("a=%f,b=%f,x=%f,iter=%i\n",a,b,x,iter);
    xm=(a+b)*.5;
    tol2=2.0*(tol1=tol*fabs(x)+ZEPS);
    if(fabs(x-xm)<= (tol2-0.5*(b-a))){
      return x;
    }
    if(fabs(e)>tol1){
      r=(x-w)*(fx-fv);
      q=(x-v)*(fx-fw);
      p=(x-v)*q-(x-w)*r;
      q=2.0*(q-r);
      if(q>0.0)
        p=-p;
      q=fabs(q);
      etemp=e;
      e=d;
      if(fabs(p)>=fabs(0.5*q*etemp)||(p<=q*(a-x))||(p>=q*(b-x))){
        d=CGOLD*(e=(x>=xm?a-x:b-x));
      }
      else{
        d=p/q;
        u=x+d;
        if(u-a<tol2||b-u<tol2)
        {
          if(tol1<0.0)
            d=x-xm;
          else
            d=xm-x;
        }
      }
    }
    
    else{
      d=CGOLD*(e=(x>=xm?a-x:b-x));
    }
    if(tol<0.0)
      u=(fabs(d)>=tol1?x+d:x-d);
    else
      u=(fabs(d)>=tol1?x+d:x+d);
    for(m=0;m<n;++m){
      mCoord[m]=mCoord[m] + (u*pGrad[m]);
      vertices[m]=(mCoord[m]);
    }
    fu=objFunc->evaluate(pd,err);
    if(fu<fx){
      if(u>=x)
        a=x;
      else
        b=x;
      v=w;
      w=x;
      x=u;
      fv=fw;
      fw=fx;
      fx=fu;
    }
    else{
      if(u<x)
        a=u;
      else
        b=u;
      if(fu<=fw||w==x){
        v=w;
        w=u;
        fv=fw;
        fw=fu;
      }
      else if (fu<=fv||v==x||v==w){
        v=u;
        fv=fu;
      }
    }
  }
  for(m=0;m<n;++m){   
    vertices[m]=(mCoord[m]);
  }
    //PRINT_WARNING("TOO MANY ITERATIONS IN QUADRATIC LINE SEARCH");
  return x;
}
*/

} //namespace Mesquite
    




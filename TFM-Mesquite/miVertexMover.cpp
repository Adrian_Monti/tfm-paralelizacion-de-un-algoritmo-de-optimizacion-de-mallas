/* ***************************************************************** 
    MESQUITE -- The Mesh Quality Improvement Toolkit

    Copyright 2004 Sandia Corporation and Argonne National
    Laboratory.  Under the terms of Contract DE-AC04-94AL85000 
    with Sandia Corporation, the U.S. Government retains certain 
    rights in this software.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License 
    (lgpl.txt) along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 
    diachin2@llnl.gov, djmelan@sandia.gov, mbrewer@sandia.gov, 
    pknupp@sandia.gov, tleurent@mcs.anl.gov, tmunson@mcs.anl.gov      
   
  ***************************************************************** */
/*!
  \file   VertexMover.cpp
  \brief  

  The VertexMover Class is the base class for all the smoothing algorythms 

  \author Thomas Leurent
  \date   2002-01-17
*/

#define PAPI
#include "miVertexMover_papi.h"
#ifdef PAPI
  struct          miPapiEventosNativos localStructNativos;
  struct          miPapiEventosPreset localStructPreset;
#endif
#define debugBoundingBox // para activar la transformacion del patch al bounding box y al reves
//#define debugMensajes // para dejar ver los mensajes por pantalla

#include "miVertexMover.hpp"
#include "NonGradient.hpp"
#include "QualityMetric.hpp"
#include "TQualityMetric.hpp"
#include "TMetricBarrier.hpp"
#include "AWMetricBarrier.hpp"
#include "ElementMaxQM.hpp"
#include "ElementAvgQM.hpp"
#include "ElementPMeanP.hpp"
#include "PlanarDomain.hpp"
#include "ObjectiveFunctionTemplate.hpp"
#include "MaxTemplate.hpp"
#include "MsqTimer.hpp"
#include "MsqDebug.hpp"
#include "PatchSet.hpp"
#include "PatchData.hpp"
#include "ParallelHelperInterface.hpp"
#include <algorithm>

#include "hSigmaBarrierObjFunc.hpp"
#include "ThSigmaMetric.hpp"
#include "IdealShapeTarget.hpp"
#include "miIdealWeightMeanRatio.hpp"
#include "QualityAssessor.hpp"
#include "InstructionQueue.hpp"
#include "miUntangleBetaQualityMetric.hpp"

#include "logBarrierObjFunc.hpp"
#include "logBarrierObjFunc2.hpp"
#include "ThMixedQ.hpp"

using std::cout;
using std::endl;

int iteraciones = 0;

namespace MESQUITE_NS {

extern int get_parallel_rank();
extern int get_parallel_size();
extern void parallel_barrier();

#include <sched.h>
#include <sys/resource.h>
#include <sys/sysinfo.h>
int label(long long i) {/* generate text labels */
  if      (i<1e3) {printf(" %llu B",i);}
  else if (i<1e6) {printf(" %llu KB",i/1024);}
  else if (i<1e9) {printf(" %.2f MB",(double)i/1048576);}
  else            {printf(" %.2f GB",(double)i/1073741824);}
  printf("\n");
  return 0;
}
int parseLine(char* line){
        int i = strlen(line);
        while (*line < '0' || *line > '9') line++;
        line[i-3] = '\0';
        i = atoi(line);
        return i;
}
int getValue(){ //Note: this value is in KB!
        FILE* file = fopen("/proc/self/status", "r");
        int result = -1;
        char line[128];

        while (fgets(line, 128, file) != NULL){
            if (strncmp(line, "VmRSS:", 6) == 0){
                result = parseLine(line);
                break;
            }
        }
        fclose(file);
        return result;
}
void imprimeUsoMemoria()
{
  struct rusage r_usage;
  int rank = get_parallel_rank();
  //struct sysinfo memInfo;
  //long long info;
  //int num_cores = sysconf(_SC_NPROCESSORS_ONLN);

  //sysinfo (&memInfo);
  //info = 1024 * (long long)getValue(); // primera forma de obtener la memoria en uso

  getrusage(RUSAGE_SELF,&r_usage); // segunda forma de obtener la memoria en uso
  long unsigned maxrss=r_usage.ru_maxrss/1024;
  //long unsigned maxrss=r_usage.ru_maxrss/1024, idrss=r_usage.ru_idrss, isrss=r_usage.ru_isrss;
  getrusage(RUSAGE_CHILDREN,&r_usage);
  printf("P[%i] MemoriaUsada, maxrss(self+child,MB)= %5ld + %ld \n",rank,maxrss,r_usage.ru_maxrss/1024);
  //printf("P[%i] MemoriaUsada2, ", rank);
  //label(info); // printf del valor de info

  // se asigna una CPU al proceso/thread actual
  /*
  int cpuActual = sched_getcpu();
  cpu_set_t cpuset;
  CPU_ZERO(&cpuset);
  CPU_SET(cpuActual, &cpuset);
  pthread_t current_thread = pthread_self();
  pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
  */

  //printf("P[%i] CPUactual= %2i / %2i\n",rank,sched_getcpu(), num_cores);
  //printf("P[%i] maxrss(self+child,MB)= %5ld + %5ld, idrss(self+child,kB)= %3ld + %3ld, isrss(self+child,kB)= %3ld + %3ld\n",get_parallel_rank(),maxrss,r_usage.ru_maxrss/1024,idrss,r_usage.ru_idrss,isrss,r_usage.ru_isrss);
}

//void verDatos(MsqVertex* vertexArray, int N, double &incremento, double (&maximos)[3], double (&minimos)[3]);
void verDatos(MsqVertex* vertexArray, int N, double &incremento, double (&maximos)[3], double (&minimos)[3], Mesh::VertexHandle* vertex_handle, size_t * tag_data_ID);

size_t count_inverted( PatchData& pd, MsqError& err );

VertexMover::VertexMover( ObjectiveFunction* OF ) 
  : QualityImprover(),
    objFuncEval( OF ) ,
    NpatchesInterior(0),
    NpatchesFrontera(0),
    jacobiOpt(false)
  {}


VertexMover::~VertexMover() {}

/*
  
    +-----------+
    |Reset Outer|
    |Criterion  |
    +-----------+
          |
          V
          +
        /   \
       /Outer\  YES
+--> <Criterion>-----> DONE
|      \Done?/
|       \   /
|         + 
|         |NO
|         V
|   +-----------+
1   |Reset Mesh |
|   | Iteration |
|   +-----------+
|         |
|         V
|         +
|       /  \
|   NO /Next\
+-----<Patch > <-----+
       \    /        |
        \  /         |
          +          |
       YES|          |
          V          |
    +-----------+    |
    |Reset Inner|    |
    |Criterion  |    2
    +-----------+    |
          |          |
          V          |  
          +          |               
        /   \        |
       /Inner\  YES  |
+--> <Criterion>-----+    --------------+
|      \Done?/                          |
|       \   /                           |
|         +                             |
|         |NO                           |
|         V                          Inside
3   +-----------+                    Smoother
|   |   Smooth  |                       |
|   |   Patch   |                       |
|   +-----------+                       |
|         |                             |
----------+               --------------+
                      
*/        

/*! \brief Improves the quality of the MeshSet, calling some
    methods specified in a class derived from VertexMover

    \param const MeshSet &: this MeshSet is looped over. Only the
    mutable data members are changed (such as currentVertexInd).
  */
double VertexMover::loop_over_mesh( MeshDomainAssoc* mesh_and_domain,
                                    const Settings* settings,
                                    MsqError& err )
{
  Mesh* mesh = mesh_and_domain->get_mesh();
  MeshDomain* domain = mesh_and_domain->get_domain();

  TagHandle coord_tag = 0; // store uncommitted coords for jacobi optimization 
  TagHandle* coord_tag_ptr = 0;

  TerminationCriterion* outer_crit = 0;
  TerminationCriterion* inner_crit = 0;

    // Clear culling flag, set hard fixed flag, etc on all vertices
  initialize_vertex_byte( mesh_and_domain, settings, err ); MSQ_ERRZERO(err);

    // Get the patch data to use for the first iteration
  OFEvaluator& obj_func = get_objective_function_evaluator();
 
    // Check for impromer use of MaxTemplate
  ObjectiveFunctionTemplate* maxt_ptr = dynamic_cast<MaxTemplate*>(obj_func.get_objective_function());
  if (maxt_ptr)
  {
  QualityImprover* ngqi_ptr = dynamic_cast<NonGradient*>(this);
  if (!ngqi_ptr)
      std::cout << "Warning: MaxTemplate results in non-differentiable objective function." << std::endl <<
                   "   Therefore, it is best to use the NonGradient solver. Other Mesquite" << std::endl <<
                   "   solvers require derivative information." << std::endl;
  }

  PatchData patch;
  patch.set_mesh( mesh );
  patch.set_domain( domain );
  if (settings)
    patch.attach_settings( settings );
  bool one_patch = false, inner_crit_terminated, all_culled;
  std::vector<Mesh::VertexHandle> patch_vertices;
  std::vector<Mesh::ElementHandle> patch_elements;
  bool valid;
  
  PatchSet* patch_set = get_patch_set();
  if (!patch_set) {
    MSQ_SETERR(err)("No PatchSet for QualityImprover!", MsqError::INVALID_STATE);
    return 0.0;
  }
  patch_set->set_mesh( mesh );
  
  std::vector<PatchSet::PatchHandle> patch_list;
  patch_set->get_patch_handles( patch_list, err ); MSQ_ERRZERO(err);

    // check for inverted elements when using a barrietr target metric
  TQualityMetric* tqm_ptr = NULL;
  TMetricBarrier* tm_ptr = NULL;
  AWMetricBarrier* awm_ptr = NULL;
  ElemSampleQM* sample_qm_ptr = NULL;
  QualityMetric* qm_ptr = NULL;
  TQualityMetric* pmeanp_ptr = NULL;
  ElementMaxQM* elem_max_ptr = NULL;
  ElementAvgQM* elem_avg_ptr = NULL;
  ElementPMeanP* elem_pmeanp_ptr = NULL;

  ObjectiveFunctionTemplate* of_ptr =  dynamic_cast<ObjectiveFunctionTemplate*>(obj_func.get_objective_function() );
  if (of_ptr)
    qm_ptr = of_ptr->get_quality_metric();
  if (qm_ptr)
  {
    pmeanp_ptr = dynamic_cast<TQualityMetric*>(qm_ptr);  // PMeanP case
    elem_max_ptr = dynamic_cast<ElementMaxQM*>(qm_ptr);
    elem_avg_ptr = dynamic_cast<ElementAvgQM*>(qm_ptr);
    elem_pmeanp_ptr = dynamic_cast<ElementPMeanP*>(qm_ptr);
  }
  if (elem_max_ptr)
    sample_qm_ptr = elem_max_ptr->get_quality_metric();
  else if (elem_pmeanp_ptr)
    sample_qm_ptr = elem_pmeanp_ptr->get_quality_metric();
  else if (elem_avg_ptr)
    sample_qm_ptr = elem_avg_ptr->get_quality_metric();
  else if (pmeanp_ptr)
  {
    tm_ptr =  dynamic_cast<TMetricBarrier*>(pmeanp_ptr->get_target_metric());
    awm_ptr =  dynamic_cast<AWMetricBarrier*>(pmeanp_ptr->get_target_metric());
  }
   
  if (sample_qm_ptr || pmeanp_ptr)
  { 
    if (!pmeanp_ptr)
    {
      tqm_ptr = dynamic_cast<TQualityMetric*>(sample_qm_ptr);
      if (tqm_ptr)
      {
        tm_ptr =  dynamic_cast<TMetricBarrier*>(tqm_ptr->get_target_metric());
        awm_ptr =  dynamic_cast<AWMetricBarrier*>(tqm_ptr->get_target_metric());
      }
    }
    else
    {
      tqm_ptr = dynamic_cast<TQualityMetric*>(pmeanp_ptr);
    }
    
    if (tqm_ptr && (tm_ptr || awm_ptr))
    {
        // check for inverted elements
      this->initialize(patch, err); 
      std::vector<size_t> handles;
 
        // set up patch data      
      std::vector<PatchSet::PatchHandle>::iterator patch_iter = patch_list.begin();
      while( patch_iter != patch_list.end() )
      {
        do 
          {
            patch_set->get_patch( *patch_iter, patch_elements, patch_vertices, err );
            if (MSQ_CHKERR(err)) goto ERROR;
            ++patch_iter;
          } while (patch_elements.empty() && patch_iter != patch_list.end()) ;

        patch.set_mesh_entities( patch_elements, patch_vertices, err );
        if (MSQ_CHKERR(err)) goto ERROR;
    
        qm_ptr->get_evaluations( patch, handles, true, err ); // MSQ_ERRFALSE(err);
  
          // do actual check for inverted elements
        std::vector<size_t>::const_iterator i;
        double tvalue;
        for (i = handles.begin(); i != handles.end(); ++i)
        {
          bool result = tqm_ptr->evaluate( patch, *i, tvalue, err );
          if (MSQ_CHKERR(err) || !result)
            return false;    // inverted element detected
        }
      }
    }
  }

    // Get termination criteria
  outer_crit=this->get_outer_termination_criterion();
  inner_crit=this->get_inner_termination_criterion();
  if(outer_crit == 0){
    MSQ_SETERR(err)("Termination Criterion pointer is Null", MsqError::INVALID_STATE);
    return 0.;
  }
  if(inner_crit == 0){
    MSQ_SETERR(err)("Termination Criterion pointer for inner loop is Null", MsqError::INVALID_STATE);
    return 0.;
  }

    // Set Termination Criterion defaults if no other Criterion is set
  if ( !outer_crit->criterion_is_set() )
    outer_crit->add_iteration_limit(1);
  if ( !inner_crit->criterion_is_set() )
    inner_crit->add_iteration_limit(10);

    // If using a local patch, suppress output of inner termination criterion
  if (patch_list.size() > 1) 
    inner_crit->set_debug_output_level(3);
  else
    one_patch = true;
  
  if (jacobiOpt) {
    coord_tag = get_jacobi_coord_tag(mesh, err);
    MSQ_ERRZERO(err);
    coord_tag_ptr = &coord_tag;
  }
  
    // Initialize outer loop
    
  this->initialize(patch, err);        
  if (MSQ_CHKERR(err)) goto ERROR;
  
  valid = obj_func.initialize( mesh_and_domain, settings, patch_set, err ); 
  if (MSQ_CHKERR(err)) goto ERROR;
  if (!valid) {
    MSQ_SETERR(err)("ObjectiveFunction initialization failed.  Mesh "
                    "invalid at one or more sample points.", 
                    MsqError::INVALID_MESH);
    goto ERROR;
  }
  
  outer_crit->reset_outer(mesh, domain, obj_func, settings, err); 
  if (MSQ_CHKERR(err)) goto ERROR;
  
 
    // if only one patch, get the patch now
  if (one_patch) {
    patch_set->get_patch( patch_list[0], patch_elements, patch_vertices, err );
    if (MSQ_CHKERR(err)) goto ERROR;
    patch.set_mesh_entities( patch_elements, patch_vertices, err );
    if (MSQ_CHKERR(err)) goto ERROR;
  }
  
   // Loop until outer termination criterion is met
  inner_crit_terminated = false;
  while (!outer_crit->terminate())
  {
    cout << "\nEmpieza ITERACION EXTERNA (miVertexMover) numero: " << outer_crit->iterationCounter << "/" <<  outer_crit->iterationBound << ", ElementosInverted= " << outer_crit->globalInvertedCount << ", OFtodaMalla= " << outer_crit->currentOFValue << ", one_patch: " << one_patch << endl << endl;

    if (inner_crit_terminated) {
      MSQ_SETERR(err)("Inner termiation criterion satisfied for all patches "
                      "without meeting outer termination criterion.  This is "
                      "an infinite loop.  Aborting.", MsqError::INVALID_STATE);
      break;
    }
    inner_crit_terminated = true;
    all_culled = true;

        int num_patches=0;
      // Loop over each patch
    std::vector<PatchSet::PatchHandle>::iterator p_iter = patch_list.begin();
    while( p_iter != patch_list.end() )
    {
      if (!one_patch) { // if only one patch (global) re-use the previous one
          // loop until we get a non-empty patch.  patch will be empty
          // for culled vertices with element-on-vertex patches
        do {
          patch_set->get_patch( *p_iter, patch_elements, patch_vertices, err );
          if (MSQ_CHKERR(err)) goto ERROR;
          ++p_iter;
        } while (patch_elements.empty() && p_iter != patch_list.end()) ;
        
        if (patch_elements.empty()) { // no more non-culled vertices
          if (0)
             std::cout << "P[" << get_parallel_rank() << "] tmp srk all vertices culled."  << std::endl;
          break;
        }
      
        all_culled = false;
        patch.set_mesh_entities( patch_elements, patch_vertices, err );
        if (MSQ_CHKERR(err)) goto ERROR;
      } else {
        ++p_iter;
        all_culled = false;
      }
        
      ++num_patches;

      // Initialize for inner iteration
        
      this->initialize_mesh_iteration(patch, err);
      if (MSQ_CHKERR(err)) goto ERROR;
      
      obj_func.reset();
      
      outer_crit->reset_patch( patch, err );
      if (MSQ_CHKERR(err)) goto ERROR;
      
      inner_crit->reset_inner( patch, obj_func, err );
      if (MSQ_CHKERR(err)) goto ERROR;
      
      inner_crit->reset_patch( patch, err );
      if (MSQ_CHKERR(err)) goto ERROR;
      
        // Don't even call optimizer if inner termination 
        // criterion has already been met.
      // MIO inner_crit->add_iteration_limit(5);

      if (!inner_crit->terminate())
      {
        inner_crit_terminated = false;

          // Call optimizer - should loop on inner_crit->terminate()
        this->optimize_vertex_positions( patch, err );
        if (MSQ_CHKERR(err)) goto ERROR;
      
          // Update for changes during inner iteration 
          // (during optimizer loop)
        
        outer_crit->accumulate_patch( patch, err );
        if (MSQ_CHKERR(err)) goto ERROR;
        
        inner_crit->cull_vertices( patch, obj_func, err );
        if (MSQ_CHKERR(err)) goto ERROR;

                // FIXME
                if (0)
                  {
                    inner_crit->cull_vertices_global (patch, 
                                                      mesh, domain, settings,
                                                      obj_func, err);
                    if (MSQ_CHKERR(err)) goto ERROR;
                  }
        
        patch.update_mesh( err, coord_tag_ptr );
        if (MSQ_CHKERR(err)) goto ERROR;
      }
    } 

    if (jacobiOpt)
      commit_jacobi_coords( coord_tag, mesh, err );

    this->terminate_mesh_iteration(patch, err); 
    if (MSQ_CHKERR(err)) goto ERROR;
    
    outer_crit->accumulate_outer( mesh, domain, obj_func, settings, err );
    if (MSQ_CHKERR(err)) goto ERROR;
    
    if (all_culled)
      break;
  }

    cout << "Termina ITERACION EXTERNA (miVertexMover) numero: " << outer_crit->iterationCounter << "/" <<  outer_crit->iterationBound << ", maxMovementRealizado/Min= " << sqrt(outer_crit->maxSquaredMovement) << "/" << sqrt(outer_crit->vertexMovementAbsoluteEps) << ", ElementosInverted= " << outer_crit->globalInvertedCount << ", OFtodaMalla= " << outer_crit->currentOFValue << endl << endl;


ERROR:  
  if (jacobiOpt)
    mesh->tag_delete( coord_tag, err );

    //call the criteria's cleanup funtions.
  if (outer_crit) 
    outer_crit->cleanup(mesh,domain,err);
  if (inner_crit)
  inner_crit->cleanup(mesh,domain,err);
    //call the optimization cleanup function.
  this->cleanup();

  return 0.;
}
  

static void checkpoint_bytes( Mesh* mesh, std::vector<unsigned char>& saved_bytes, MsqError& err)
{
  std::vector<Mesh::VertexHandle> vertexHandlesArray;
  mesh->get_all_vertices(vertexHandlesArray, err); MSQ_ERRRTN(err);
  saved_bytes.resize(vertexHandlesArray.size());
  mesh->vertices_get_byte( &vertexHandlesArray[0],
                           &saved_bytes[0],
                           vertexHandlesArray.size(),
                           err ); MSQ_ERRRTN(err);
}

static void restore_bytes( Mesh* mesh, std::vector<unsigned char>& saved_bytes, MsqError& err)
{
  std::vector<Mesh::VertexHandle> vertexHandlesArray;
  mesh->get_all_vertices(vertexHandlesArray, err); MSQ_ERRRTN(err);
  mesh->vertices_set_byte( &vertexHandlesArray[0],
                           &saved_bytes[0],
                           vertexHandlesArray.size(),
                           err ); MSQ_ERRRTN(err);
}

  static void save_or_restore_debug_state(bool save)
  {
    static bool debug[3] = {false,false,false};
    if (save) 
      {
        debug[0] = MsqDebug::get(1);
        debug[1] = MsqDebug::get(2);
        debug[2] = MsqDebug::get(3);
      }
    else
      {
        if (debug[0]) MsqDebug::enable(1);
        if (debug[1]) MsqDebug::enable(2);
        if (debug[2]) MsqDebug::enable(3);
      }
  }

/*! \brief Improves the quality of the MeshSet, calling some
    methods specified in a class derived from VertexMover

    \param const MeshSet &: this MeshSet is looped over. Only the
    mutable data members are changed (such as currentVertexInd).
  */
double VertexMover::loop_over_mesh( ParallelMesh* mesh,
                                    MeshDomain* domain,
                                    const Settings* settings,
                                    MsqError& err )
{

  std::vector<size_t> junk;
  Mesh::VertexHandle vertex_handle;
  TagHandle coord_tag = 0; // store uncommitted coords for jacobi optimization 
  TagHandle* coord_tag_ptr = 0;
  int outer_iter=0;
  int inner_iter=0;
  bool one_patch = false;
  int iDumy1, iDumy2, icount;
  Timer c_timer;
  size_t NinteriorFreeVTX = 0;
  size_t NboundaryFreeVTX = 0;
  size_t Npatches_0, Npatches_1;
  int nF; // numero fases frontera
  size_t drank = get_parallel_rank();

  //std::cout << "P[" << get_parallel_rank() << "] miVertexMover::loop_over_mesh, EMPIEZA a iterar sobre la malla" << std::endl;


  // MIO - ejecuto qualityassesor en cada iteracion para ver los elem invalidos
  // porque a traves de miTerminationCriterion.cpp no me da el mismo valor
/*
  hSigmaMetric tm(1.2e-3, 1.0e-10);
  IdealShapeTarget target;
  ThSigmaMetric m( &target, &tm );
  ElementPMeanP untangle_metric(1, &m);
  InstructionQueue q1;
  QualityAssessor qa_untangle(&untangle_metric);
  q1.add_quality_assessor(&qa_untangle, err); //MSQ_ERRRTN(err);
*/

    // Clear culling flag, set hard fixed flag, etc on all vertices
  MeshDomainAssoc mesh_and_domain = MeshDomainAssoc((Mesh*)mesh, 0);
  initialize_vertex_byte( &mesh_and_domain, settings, err ); MSQ_ERRZERO(err);

    // Get the patch data to use for the first iteration
  OFEvaluator& obj_func = get_objective_function_evaluator();
  
  PatchData patch;
  patch.set_mesh( (Mesh*) mesh );
  patch.set_domain( domain );
  patch.attach_settings( settings );

  ParallelHelper* helper = mesh->get_parallel_helper();
  if (!helper) {
    MSQ_SETERR(err)("No ParallelHelper instance", MsqError::INVALID_STATE);
    return 0;
  }

  //ORIG: helper->smoothing_init(err);  MSQ_ERRZERO(err);

  bool inner_crit_terminated, all_culled;
  std::vector<Mesh::VertexHandle> patch_vertices;
  std::vector<Mesh::ElementHandle> patch_elements;
  std::vector<Mesh::VertexHandle> fixed_vertices;
  std::vector<Mesh::VertexHandle> free_vertices;
   
    // Get termination criteria
  TerminationCriterion* outer_crit=this->get_outer_termination_criterion();
  TerminationCriterion* inner_crit=this->get_inner_termination_criterion();
  if(outer_crit == 0){
    MSQ_SETERR(err)("Termination Criterion pointer is Null", MsqError::INVALID_STATE);
    return 0.;
  }
  if(inner_crit == 0){
    MSQ_SETERR(err)("Termination Criterion pointer for inner loop is Null", MsqError::INVALID_STATE);
    return 0.;
  }
  
  PatchSet* patch_set = get_patch_set();
  if (!patch_set) {
    MSQ_SETERR(err)("No PatchSet for QualityImprover!", MsqError::INVALID_STATE);
    return 0.0;
  }
  patch_set->set_mesh( (Mesh*)mesh );

  std::vector<PatchSet::PatchHandle> patch_list;
  patch_set->get_patch_handles( patch_list, err ); MSQ_ERRZERO(err);
  std::cout << "P[" << drank << "] miVertexMover::loop_over_mesh_parallel, EMPIEZA a iterar sobre la malla, NumPatchesMalla: " << patch_list.size() << std::endl;
 
  if (patch_list.size() > 1) 
      inner_crit->set_debug_output_level(3);
  else
      one_patch = true;

  if (jacobiOpt) {
    coord_tag = get_jacobi_coord_tag(mesh, err);
    MSQ_ERRZERO(err);
    coord_tag_ptr = &coord_tag;
  }
  
  // parallel error checking
  MsqError perr;
  bool pdone=false;

#define PERROR_COND continue

  // Initialize outer loop
  std::cout << "P[" << drank << "] miVertexMover::loop_over_mesh_parallel, one_patch: " <<
	one_patch << ", INICIALIZA ITERACION EXTERNA ... " << std::endl;
    
  //std::cout << "P[" << get_parallel_rank() << "] miVertexMover::loop_over_mesh_parallel, INICIALIZA ITERACION EXTERNA ... initializePatch" << std::endl;
  this->initialize(patch, err); 
  if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)("initialize patch", MsqError::INVALID_STATE); } //goto ERROR;
  
  //std::cout << "P[" << get_parallel_rank() << "] miVertexMover::loop_over_mesh_parallel, INICIALIZA ITERACION EXTERNA ... initializeFO" << std::endl;
  MeshDomainAssoc mesh_and_domain2 = MeshDomainAssoc((Mesh*)mesh, domain);
  obj_func.initialize( &mesh_and_domain2, settings, patch_set, err ); 
  if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)("initialize obj_func", MsqError::INVALID_STATE);} //goto ERROR;
  
  //std::cout << "P[" << get_parallel_rank() << "] miVertexMover::loop_over_mesh_parallel, INICIALIZA ITERACION EXTERNA ... initializeCriteria" << std::endl;
  outer_crit->reset_outer( (Mesh*)mesh, domain, obj_func, settings, err); 
  if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)("reset_outer", MsqError::INVALID_STATE);} //goto ERROR;

  std::cout << "P[" << drank << "] miVertexMover::loop_over_mesh_parallel, one_patch: " <<
	one_patch << ", INICIALIZA ITERACION EXTERNA ... DONE" << std::endl;
   
   // Loop until outer termination criterion is met
  inner_crit_terminated = false;
  all_culled = false;

  size_t elemInvIterExtIni;
  size_t elemInvIterExtFin,dumy, CRC;
  size_t elemInvIterIntIni, elemInvIterIntFin, dumyInt; 
  size_t acumulado, acumuladoTotal=0;
  size_t elemInvIni =  outer_crit->globalInvertedCount;
  PatchData global_patch;

  //IdealWeightMeanRatio metric;
  miIdealWeightMeanRatio metric;
  metric.set_averaging_method( QualityMetric::LINEAR );

/* otra metrica para dar calidades de la malla
  hSigmaMetric tm(1.2e-3, 1.0e-10);
  IdealShapeTarget target;
  ThSigmaMetric metric( &target, &tm );
*/

  InstructionQueue q1;
  QualityAssessor inv_check(&metric);
  inv_check.disable_printing_results();
  q1.add_quality_assessor(&inv_check, err); //MSQ_ERRRTN(err);
  //q1.run_common( &mesh_and_domain, mesh, (Settings*)settings, err );
  //q1.run_common( &mesh_and_domain, 0, (Settings*)settings, err );

  // se obtiene la metrica ThSigmaMetric para leer las medidas de prestaciones
  ObjectiveFunctionTemplate* OF = (ObjectiveFunctionTemplate *) obj_func.get_objective_function();
  QualityMetric* quaMetr = OF->get_quality_metric();
  ElemSampleQM* ESQM = (ElemSampleQM *) quaMetr;
  TMPQualityMetric* ESQM2 = (TMPQualityMetric *) ESQM;
  /* BUENO */ ThSigmaMetric * QM = (ThSigmaMetric *) ESQM2;
  //ThMixedQ* QM = (ThMixedQ *) ESQM2;
  //miUntangleBetaQualityMetric * QM = (miUntangleBetaQualityMetric *) ESQM2;

  // se obtienen los GLOBAL_ID de los vtx de toda la malla cuando NO estamos en 1-core
  std::vector< Mesh::VertexHandle > vertex;
  mesh->get_all_vertices (vertex, err) ;
  int num_vtx = vertex.size();
  size_t * tag_data_ID;
  tag_data_ID= (unsigned long*)malloc(num_vtx*sizeof(unsigned long));
  // tag > 0 si la malla tiene GLOBAL_ID que contiene la ID global de cada vertice
  TagHandle tag;
  if (get_parallel_size() > 1) { // version multi-core, el fichero .vtk de la particion si tiene GLOBAL_ID
	tag = mesh->tag_get( "GLOBAL_ID", err );
  	mesh->tag_get_vertex_data(tag,num_vtx,&vertex[0],tag_data_ID ,err);
  }
  else { // version 1-core, el fichero .vtk no tiene GLOBAL_ID, al obtener GLOBAL_ID da error
    for(int i=0; i<num_vtx; i++) tag_data_ID[i] = i;  
  }
//
  // lectura de fichero orden.bat
  // fichero generado en ejecucion paralela que define el orden de procesamiento de free vtx
  // cuando se ejecuta 1-core, se fuerza ese orden para establecer las prestaciones baseline
  // solo se ejcuta cuando 1-core
  size_t *orden;
  bool fuerzaOrden = false; // variable que indica si se fuerza el orden en 1-core, soloc uando exista el fichero orden.bat
  if (get_parallel_size() == 1) { // version multi-core, el fichero .vtk de la particion si tiene GLOBAL_ID
	  FILE *fileOrden;
	  char sdumy[30];
	  //sprintf(sdumy,"orden_P%i.log",get_parallel_rank());
	  fileOrden = fopen("orden.bat","r");
	  if(fileOrden != NULL) {
		printf("\nOK - miVertexMover!, File orden.bat opened for reading.\n");
	  	orden= (size_t*)malloc(num_vtx*sizeof(size_t));
	  	int i=0;
	  	while( fscanf(fileOrden,"%lu\n",&orden[i]) != EOF ){
		  cout << "miVertexMover, Orden forzado en 1-core " << i << ", leido desde fichero orden.bat: " <<  orden[i] << endl;
		  i++;
	  	}
	  	fclose(fileOrden);
		fuerzaOrden=true;
	  }
	  else {
		printf("\nERROR - miVertexMover!, Couldn't open reading file orden.bat, no se fuerza un orden de procesamiento de los vertices en 1-core\n");
		fuerzaOrden=false;
	  }
  }

#ifdef PAPI
  int EventSet= PAPI_NULL;
  //#define NUM_EVENTS 1
  #define NUM_EVENTS 1
  //#define NUM_EVENTS 4
  //int Events[NUM_EVENTS]={PAPI_LD_INS,PAPI_SR_INS};
  //int Events[NUM_EVENTS]={PAPI_FP_OPS,PAPI_L2_DCM,PAPI_L3_LDM};
  //int Events[NUM_EVENTS]={0x40000068};
  //int Events[NUM_EVENTS]={PAPI_TOT_INS,PAPI_LST_INS,PAPI_BR_INS};
  int Events[NUM_EVENTS]={PAPI_FP_INS};
  //int Events[NUM_EVENTS]={PAPI_TOT_INS};
  //int Events[NUM_EVENTS]={PAPI_RES_STL};
  //int Events[NUM_EVENTS]={PAPI_L1_TCM};
  //int Events[NUM_EVENTS]={PAPI_L3_TCM};
  //int Events[NUM_EVENTS]={PAPI_L1_DCM,PAPI_L3_LDM};
  //int Events[NUM_EVENTS]={PAPI_L2_DCM};
  //int Events[NUM_EVENTS]={PAPI_L1_DCM,PAPI_L3_LDM};
  //int Events[NUM_EVENTS]={PAPI_FP_INS,PAPI_L1_DCM,PAPI_L3_LDM};
  //int Events[NUM_EVENTS]={PAPI_FP_INS,PAPI_L1_DCM,PAPI_L2_DCM};
  //int Events[NUM_EVENTS]={PAPI_FP_INS,PAPI_L2_DCM,PAPI_L3_LDM};
  long long values[NUM_EVENTS], valor_tmp[NUM_EVENTS], valor_promedio[NUM_EVENTS];
  long long us, usPrev;
  int retval;
  char eventName[PAPI_MAX_STR_LEN];
  char errstring[PAPI_MAX_STR_LEN];
  float MFlops;
  
  miPapiInicializacion(); // inicializacion de PAPI

  retval = PAPI_library_init( PAPI_VER_CURRENT );
  if ( retval != PAPI_VER_CURRENT ) {
    	PAPI_perror((char *)"PAPI_library_init");
    	printf("%s:%d::PAPI_library init. %d %s\n", __FILE__,__LINE__,retval,errstring);
  }
  retval = PAPI_create_eventset(&EventSet);
  if (retval != PAPI_OK) { 
	printf("[Papi] Error inicializacion PAPI_create_eventset: %i\n", retval);
    	PAPI_perror((char *)"PAPI_create_eventset");
    	printf("%s:%d::PAPI_create eventset failed. %d %s\n", __FILE__,__LINE__,retval,errstring);
    	exit(1);
   }
  // Anade los contadores hardware
  retval = PAPI_add_events(EventSet,Events,NUM_EVENTS);
  if (retval != PAPI_OK) { printf("[Papi] Error inicializacion PAPI_add_events: %i\n", retval);}
  else {
 	printf("P[%i] miVertexMover - contadores PAPI activados: %i, nombres: ", get_parallel_rank(), NUM_EVENTS); 
	PAPI_event_info_t info1;
	for(int k=0; k<NUM_EVENTS; k++){
	  retval = PAPI_get_event_info( Events[k], &info1 );
	  printf (" %s -",info1.symbol);
	}
	printf("\n");
  }
  // Contadores empiezan a contar
  retval = PAPI_start(EventSet);
  if (retval != PAPI_OK) { printf("[Papi] Error inicializacion PAPI_start: %i\n", retval);}

#endif

// --- TMP: mide calidad minima de la malla y establece la calidad minima en la funcion log-barrier ---
// --- TMP: solo cuando la metrica es the tipo log-barrier + ThMixedQ ---
/*
  PatchData global_patch2;
  logBarrierObjFunc* OFr = (logBarrierObjFunc*) obj_func.get_objective_function();
  const char* Cdumy ;
  global_patch2.set_mesh( mesh );
*/
// --- TMP ---

  for (;;) // empieza iteracion externa de optimizacion de toda la malla
  {

    if (0)
      std::cout << "P[" << drank << "] tmp srk inner_iter= " << inner_iter << " outer_iter= " << outer_iter << std::endl;


    //PERROR:

    if (MSQ_CHKERR(perr))
      {
        std::cout << "P[" << get_parallel_rank() << "] VertexMover::loop_over_mesh found parallel error: " << perr << "\n quitting... pdone= " << pdone << std::endl;
        pdone = true;
      }

    /* PRUEBA */  helper->smoothing_init(err);  MSQ_ERRZERO(err);

    helper->communicate_any_true (pdone, err);

    if (0)
      std::cout << "P[" << get_parallel_rank() << "] tmp srk inner_iter= " << inner_iter << " outer_iter= " << outer_iter << " pdone= " << pdone << std::endl;

    if (pdone)
      {
        std::cout << "P[" << get_parallel_rank() << "] VertexMover::loop_over_mesh found parallel error, quitting... pdone= " << pdone << std::endl;
        MSQ_SETERR(err)("PARALLEL ERROR", MsqError::PARALLEL_ERROR);
        break;
      }

    ++outer_iter; // contador de iteraciones externas

    // MIO: iDumy1 registra el numero de nodos enredados
/*
    q1.run_common( &mesh_and_domain, mesh, (Settings*)settings, err );
    qa_untangle.get_inverted_element_count(iDumy1, iDumy2, err);
*/
    //std::cout << "\nP[" << get_parallel_rank() << "], miVertexMover, EMPIEZA iteracion externa: " << outer_iter << ", ElementosInverted= " << outer_crit->globalInvertedCount << std::endl;
    //usar iDumy1 como criterio de terminacion en este programa
    //std::cout << "\nP[" << get_parallel_rank() << "] miVertexMover, EMPIEZA iteracion externa: " << outer_iter << ", ElementosInverted= " << iDumy1 << std::endl;
    //outer_crit->globalInvertedCount = iDumy1; // MIO: fuerzo la variable globalInvertedCount ya que indica mas derendedos de los que realmente indica qa_untangle

    elemInvIterExtIni=  outer_crit->globalInvertedCount;

    // para obtener la calidad de la malla al inicio de la iteracion externa (MIO)
/*
    Mesh* mesh2 = mesh_and_domain.get_mesh();
    MeshDomain* domain2 = mesh_and_domain.get_domain();
    Mesh* mesh_ptr = mesh2;
    MeshDomainAssoc mesh_and_domain2 = MeshDomainAssoc(mesh_ptr, domain2);

    IdealWeightMeanRatio metric;
    metric.set_averaging_method( QualityMetric::LINEAR );
    InstructionQueue q1;
    QualityAssessor inv_check(&metric);
    //inv_check.disable_printing_results();
    q1.add_quality_assessor(&inv_check, err); //MSQ_ERRRTN(err);
    //q1.run_common( &mesh_and_domain, 0, (Settings*)settings, err );
*/
    q1.run_common( &mesh_and_domain2, 0, (Settings*)settings, err );
  
    Npatches_0 = this->count_Npatches();

#ifdef PAPI
  retval = PAPI_read( EventSet , values);
  for(int k=0; k < NUM_EVENTS; k++) valor_tmp[k] = values[k];
  us  = PAPI_get_real_usec(  ); // en uSegundos
  usPrev = us;
#endif

    size_t Nprev = QM->get_NtetraVisitados(); // marca inicial de tetraedros visitados para luego calcular en cada iteracion

    cout << "\nP[" << drank << "] miVertexMover::loop_over_mesh," << 
	" EMPIEZA Iteracion Externa Paralela numero: " << 
	1 + outer_crit->iterationCounter << "/" <<  outer_crit->iterationBound << 
	", \n\tElemInverted= " << (int)elemInvIterExtIni << 
	", \n\tElemInverted2= " << inv_check.get_results(&metric)->get_invalid_element_count() <<
	", \n\tOFtodaMalla= " << outer_crit->currentOFValue << 
	//", \n\tQmeanRatio, promedio= " << inv_check.get_results(&metric)->get_average() << 
	", \n\tQ(" << metric.get_name() << "), promedio= " << inv_check.get_results(&metric)->get_average() << 
	", maximo= " << inv_check.get_results(&metric)->get_maximum() << 
	", minimo= " << inv_check.get_results(&metric)->get_minimum() << 
	", desvEstan= " << inv_check.get_results(&metric)->get_stddev() << 
	", \n\tone_patch= " << one_patch << 
	", \n\tt= " << c_timer.since_birth() << " s" <<
        ", \n\tNtetraVisitados: " << Nprev << 
        ", \n\tNpatchesInteriorVisitados: " << NpatchesInterior << endl << endl;
        //", \n\tNtetraVisitados: " << QM->get_NtetraVisitados() << endl << endl;
    imprimeUsoMemoria();

/*
// --- TMP: mide calidad minima de la malla y establece la calidad minima en la funcion log-barrier ---
// --- TMP: solo cuando la metrica es the tipo log-barrier + ThMixedQ ---
  //Cdumy = OFr->get_name().c_str();
  Cdumy = OFr->get_quality_metric()->get_name().c_str();
  printf("miVertexMover::loop_over_mesh - Metrica= %s\n", Cdumy);

  //if(OFr->get_quality_metric()->get_name().compare("ThMixedQ")){
  if(strcmp(Cdumy,"ThMixedQ") == 0){
  //if(strcmp(Cdumy,"logBarrierObjectiveFunction") == 0){
  	  global_patch2.fill_global_patch( err );
	  double Ddumy = OFr->get_calidad_minima_patch(global_patch2);
	  double Dmu = OFr->get_mu();
	  printf("miVertexMover::loop_over_mesh - Calidad minima malla= %9.2e, mu= %9.2e\n", Ddumy, Dmu);
	  //Ddumy -= 3e0; // funciona para TangledCube: untangling + smoothing
	  //Ddumy -= 1e-3; 
	  //Ddumy -= (fabs(Ddumy) * 10.0); //1e1; //0.1;
	  //OFr->set_calidad_minima(Ddumy);
	  //Dmu *= 0.95;
	  //OFr->set_mu(Dmu);
	  //printf("miVertexMover::loop_over_mesh - Nueva barrera Calidad minima OF log-barrier= %9.2e\n", OFr->get_calidad_minima_ObjFunc());
	  //printf("miVertexMover::loop_over_mesh - Nueva mu OF log-barrier= %9.2e\n", OFr->get_mu());
  }
// --- TMP --
*/
// TMP
  //logBarrierObjFunc* OFdumy = dynamic_cast<logBarrierObjFunc*>(obj_func.get_objective_function());
  //if(OFdumy != NULL ){ // solo se activa si la funcion objetivo es de tipo log-barrier
  if( dynamic_cast<logBarrierObjFunc*>(obj_func.get_objective_function()) != NULL){
  //if( dynamic_cast<logBarrierObjFunc*>((logBarrierObjFunc*)obj_func.get_objective_function()) != NULL){
  	logBarrierObjFunc* OFr = (logBarrierObjFunc*) obj_func.get_objective_function();
	double Ddumy = OFr->get_mu();
	//OFr->set_mu(Ddumy*0.95);
	printf("miVertexMover - OF log-barrier - nuevo MU: %f\n", OFr->get_mu());
  } else if( dynamic_cast<logBarrierObjFunc2*>(obj_func.get_objective_function()) != NULL){
  	logBarrierObjFunc2* OFr = (logBarrierObjFunc2*) obj_func.get_objective_function();
	double Ddumy = OFr->get_mu();
	//OFr->set_mu(Ddumy*0.95);
	printf("miVertexMover - OF log-barrier2 - nuevo MU: %f\n", OFr->get_mu());
  } else if( dynamic_cast<hSigmaBarrierObjFunc*>(obj_func.get_objective_function()) != NULL){
  	hSigmaBarrierObjFunc* OFr = (hSigmaBarrierObjFunc*) obj_func.get_objective_function();
	double Ddumy = OFr->get_mu();
	//OFr->set_mu(Ddumy*0.95);
	printf("miVertexMover - OF hSigma-barrier - nuevo MU: %f\n", OFr->get_mu());
  }
// TMP
    /// srkenno@sandia.gov 1/19/12: the logic here was changed so that all proc's must agree
    ///   on the values used for outer and inner termination before the iteration is stopped.
    ///   Previously, the ParallelHelper::communicate_all_true method returned true if any
    ///   proc sent it a true value, which seems to be a bug, at least in the name of the method.
    ///   The method has been changed to return true only if all proc's values are true.  
    ///   In the previous version, this meant that if any proc hit its inner or outer
    ///   termination criterion, the loop was exited, and thus some parts of the mesh
    ///   are potentially left unconverged.  Also, if the outer criterion was satisfied on
    ///   part of the mesh (say a uniform part), the iterations were not executed at all.
    /// Also, changed name of "did_some" to "inner_crit_terminated", and flipped its boolean 
    ///   value to be consistent with the name - for readability and for correctness since
    ///   we want to communicate a true value through the helper.

    bool outer_crit_terminated = outer_crit->terminate();
    bool outer_crit_terminated_local = outer_crit_terminated;
    helper->communicate_all_true( outer_crit_terminated, err ); 

    bool inner_crit_terminated_local = inner_crit_terminated;
    helper->communicate_all_true( inner_crit_terminated, err ); 

    bool all_culled_local = all_culled;
    helper->communicate_all_true( all_culled, err ); 

    bool done = all_culled || outer_crit_terminated;
    if (inner_crit_terminated) {
      MSQ_SETERR(err)("Inner termination criterion satisfied for all patches "
                      "without meeting outer termination criterion.  This is "
                      "an infinite loop.  Aborting.", MsqError::INVALID_STATE);
      done = true;
      helper->communicate_any_true( done, err ); 
    }

    bool local_done=done;

    helper->communicate_all_true( done, err ); 

    if (0)
      std::cout << "P[" << get_parallel_rank() << "] tmp srk done= " << done << " local_done= " << local_done 
                << " all_culled= " << all_culled 
                << " outer_crit->terminate()= " << outer_crit->terminate()
                << " outer_term= " << outer_crit_terminated
                << " outer_term_local= " << outer_crit_terminated_local
                << " inner_crit_terminated = " << inner_crit_terminated
                << " inner_crit_terminated_local = " << inner_crit_terminated_local
                << " all_culled = " << all_culled
                << " all_culled_local = " << all_culled_local
                << std::endl;

    if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)("loop start", MsqError::INVALID_STATE);} //goto ERROR;


    if (done)
      break;
    
    inner_crit_terminated = true;
    all_culled = true;

    ///*** smooth the interior ***////

    // get the fixed vertices (i.e. the ones *not* part of the first independent set)
    helper->compute_first_independent_set(fixed_vertices); 
    //std::cout << "P[" << get_parallel_rank() << "]- miVertexMover, fixed_vtx.size= " << fixed_vertices.size() << std::endl;

    //std::cout << "\nP[" << get_parallel_rank() << "] miVertexMover, EMPIEZA iteracion INTERNA-Interior: " << std::endl;
    //for (size_t i = 0; i < fixed_vertices.size(); ++i) 
    //std::cout << "P[" << get_parallel_rank() << "]- miVertexMover, smoothInterior, fixed_vtx[" << i << "]= " << (size_t)fixed_vertices[i] << std::endl;

    // sort the fixed vertices
    std::sort(fixed_vertices.begin(), fixed_vertices.end());

    // Loop over each patch
    if (0 && MSQ_DBG(2))
        //if (1 )
          std::cout << "P[" << get_parallel_rank() << "] tmp srk number of patches = " << patch_list.size() 
                    << " inner_iter= " << inner_iter << " outer_iter= " << outer_iter 
                    << " inner.globalInvertedCount = " << inner_crit->globalInvertedCount
                    << " outer.globalInvertedCount = " << outer_crit->globalInvertedCount
                    << " inner.patchInvertedCount = " << inner_crit->patchInvertedCount
                    << " outer.patchInvertedCount = " << outer_crit->patchInvertedCount
                    << std::endl;

    save_or_restore_debug_state(true);
    //MsqDebug::disable_all();
      
    size_t num_patches=0;

    acumulado = 0;

    std::vector<PatchSet::PatchHandle>::iterator p_iter = patch_list.begin();
    while( p_iter != patch_list.end() )
    {

	//printf("num_patches= %lu/%lu\n",num_patches, patch_list.size());
	//std::cout << *p_iter << std::endl;

      // loop until we get a non-empty patch.  patch will be empty
      // for culled vertices with element-on-vertex patches
      do {
	//patch_set->get_patch((PatchSet::PatchHandle *)num_patches, patch_elements, patch_vertices, err );
	patch_set->get_patch( *p_iter, patch_elements, patch_vertices, err );
	//printf("patch_elements.empty()== %i\n",patch_elements.empty());
	//std::cout << *p_iter << std::endl;
	if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)("get_patch", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;
	++p_iter;
        ++num_patches;
	//std::cout << "P[" << get_parallel_rank() << "], patch_elements= " << patch_elements.size() << ", numero vertices= " << patch_vertices.size() << ", nodos fixed= " << fixed_vertices.size() << ", nodos free= " << patch.num_free_vertices() << std::endl;
      } while (patch_elements.empty() && p_iter != patch_list.end()) ;
      
        
      if (patch_elements.empty()) { // no more non-culled vertices
        if (0)
          std::cout << "P[" << get_parallel_rank() << "] tmp srk all vertices culled."  << std::endl;
      	break;
      }

      if (patch_vertices.empty()) // global patch hack (means all mesh vertices)
      {
	mesh->get_all_vertices(patch_vertices, err);
	//std::cout << "P[" << get_parallel_rank() << "], patch_elements= " << patch_elements.size() << ", numero vertices= " << patch_vertices.size() << ", nodos fixed= " << fixed_vertices.size() << ", nodos free= " << patch.num_free_vertices() << std::endl;
      }
    
      //std::sort(patch_vertices.begin(), patch_vertices.end()); // MIO

/*
      for(int i = 0; i < patch_vertices.size(); i++){
	  std::cout << "P[" << get_parallel_rank() << "] Patch: " << num_patches << ", freeVtxHandle[" << i << "]= " << (size_t)patch_vertices[i] << std::endl;
       }
      for(int i = 0; i < patch_elements.size(); i++){
	  std::cout << "P[" << get_parallel_rank() << "] Patch: " << num_patches << ", elemHandle[" << i << "]= " << (size_t)patch_elements[i] << std::endl;
       }
*/
      free_vertices.clear();

      for (size_t i = 0; i < patch_vertices.size(); ++i) 
	if (!std::binary_search(fixed_vertices.begin(), fixed_vertices.end(), patch_vertices[i]))
	  free_vertices.push_back(patch_vertices[i]);

      // se muestra informacion del nodo libre del patch
      // se usa para ver el orden de procesamiento de los free vtx cuando se activan n-core
      size_t dumy ;
      //if (outer_crit->iterationCounter >= 0) {
      if (outer_crit->iterationCounter == 0) {
        for (size_t i = 0; i < free_vertices.size(); ++i)  {
	  dumy = (size_t)free_vertices[i]; // ID local del free vtx
      	  //std::cout << "P[" << get_parallel_rank() << "] ORDEN patchNum: " << ((size_t)*p_iter)-1 << //num_patches << 
      	  //std::cout << "P[" << get_parallel_rank() << "] ORDENinterno " << NinteriorFreeVTX << " - " <<
      	  std::cout << "\nORDENinterno " << 
	    //"mesh.freeVtxLOCAL_ID[" << i << "]= " << dumy << 
	    //",  mesh.freeVtxGLOBAL_ID[" << i << "]= " << (size_t)tag_data_ID[dumy] << std::endl;
	    (size_t)tag_data_ID[dumy] << " (P" << drank << ")\n" << std::endl;
	  //  (size_t)tag_data_ID[dumy] << std::endl;
	  //fprintf(fileOrden, "%lu\n",(size_t)tag_data_ID[dumy]);
	  NinteriorFreeVTX++;
        }
      } // if

      if (free_vertices.empty()) { // all vertices were fixed -> skip patch
	continue;
      }

      all_culled = false;

// se fuerza un orden de procesamiento de los free_vertices en core-1
  if(!one_patch){
      if (get_parallel_size() == 1 && fuerzaOrden) { // comprueba que se ejecuta una version 1-core
	if (outer_crit->iterationCounter == 0)
	   cout << "NUEVO orden forzado en 1-core: " << num_patches-1 << ", en procesamiento: " <<  orden[num_patches-1] << endl << endl;
	dumy = orden[num_patches-1]; // se fuerza el orden
      } else {
	dumy = (size_t)free_vertices[0]; // ID local del free vtx
      }
      free_vertices.clear();
      free_vertices.push_back((Mesh::VertexHandle *)dumy);
      patch_elements.clear(); 
      mesh->vertices_get_attached_elements( &free_vertices[0],
                                              1,
                                              patch_elements, 
                                              junk, err );
   } // if !one_patch
// termina forzamiento
      


      patch.set_mesh_entities( patch_elements, free_vertices, err );
      if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)("set_mesh_entities", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;
        
#ifdef debugMensajes
      //std::cout << "\nP[" << get_parallel_rank() << "] miVertexMover, IteracionesInternas, Patch: " << num_patches << "/" << patch_list.size() << std::endl << std::endl;
      elemInvIterIntIni = inner_crit->count_inverted( patch, err );
      std::cout << "\nP[" << get_parallel_rank() << "] miVertexMover, EMPIEZA iteracionInterna, Patch: " << num_patches << "/" << patch_list.size() << ", elemInvertidosPatch: " << elemInvIterIntIni << std::endl;
#endif

      //std::cout << "\nP[" << get_parallel_rank() << "] miVertexMover, IteracionesInternas, Patch: " << num_patches << "/" << patch_list.size() << ", VerticesPatch: " << patch_vertices.size() << ", vertices libres: " << patch.num_free_vertices() << ", ElemementosPatch: " << patch_elements.size() << std::endl;

      // Initialize for inner iteration
      //std::cout << "P[" << get_parallel_rank() << "] miVertexMover, Inicia iteracion interna" << std::endl; 
      this->initialize_mesh_iteration(patch, err);
      if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)("initialize_mesh_iteration", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;
      
      obj_func.reset();
      
      outer_crit->reset_patch( patch, err );
      if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)("reset_patch outer", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;
      
      inner_crit->reset_inner( patch, obj_func, err );
      if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)("reset_inner", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;
      
      inner_crit->reset_patch( patch, err );
      if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)("inner reset_patch", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;
      
        // Don't even call optimizer if inner termination 
        // criterion has already been met.
      if (!inner_crit->terminate())
      {
        inner_crit_terminated = false;
        if (one_patch) ++inner_iter;

// Call optimizer - should loop on inner_crit->terminate()

        size_t num_vert=patch.num_free_vertices();
#ifdef debugMensajes
        std::cout << "P[" << get_parallel_rank() << "] VertexMover, num_free_vert= " << num_vert << ", patchNum: " << num_patches << ", patch_vertices.size= " << patch_vertices.size() << ", nodos fixed= " << fixed_vertices.size() << std::endl;
#endif
              
// MIO: transforma coordenadas patch en un bounding box
#ifdef debugBoundingBox
#ifdef debugMensajes
      std::cout << "P[" << get_parallel_rank() << "] miVertexMover, Transformamos el patch al Bounding Box" << std::endl;
#endif
#endif
#ifdef debugBoundingBox
	float  vectorTransformacion[4];
        this->patch_enBoundingBox(patch, vectorTransformacion, true, tag_data_ID);
#endif
/*
	for(int k=0; k<4; k++){
	   printf("vectorTransformacion[%i]: %f  ", k, vectorTransformacion[k]);
	}
	printf("\n\n");
*/
#ifdef debugMensajes
        std::cout << "P[" << get_parallel_rank() << "] miVertexMover, optimizando freeVTX ... " << std::endl;
#endif

// TMP
  //double Edumy ;
  //logBarrierObjFunc* OFdumy = dynamic_cast<logBarrierObjFunc*>(OF); // si OFdumy!=NULL, la funcion objetivo es log-barrier
#ifdef debugMensajes
  //printf("miVertexMover::loop_over_mesh - OF es: %s\n", OFdumy != NULL ? "logBarrier" : "null");
 // printf("miVertexMover::loop_over_mesh - OF es: %s\n", dynamic_cast<logBarrierObjFunc*>(obj_func.get_objective_function()) != NULL ? "logBarrier" : "null");
 std::string nombreOF;
 if	(dynamic_cast<logBarrierObjFunc*>(obj_func.get_objective_function()) != NULL ) nombreOF = "log-barrier ";
 else if(dynamic_cast<logBarrierObjFunc2*>(obj_func.get_objective_function()) != NULL ) nombreOF = "log-barrier2";
 else if(dynamic_cast<hSigmaBarrierObjFunc*>(obj_func.get_objective_function()) != NULL ) nombreOF = "hSigma-barrier";
 else nombreOF = "LPtoP";
 printf("miVertexMover::loop_over_mesh - OF es: " );
 std::cout << nombreOF << std::endl;

#endif
  //if((strcmp(Cdumy,"hSigmaMetric") == 0) || strcmp(Cdumy,"ThMixedQ") == 0) {
  //if(OFdumy != NULL ){ // solo se activa si la funcion objetivo es de tipo log-barrier
  if( dynamic_cast<logBarrierObjFunc*>(obj_func.get_objective_function()) != NULL){
  	  logBarrierObjFunc* OFr = (logBarrierObjFunc*) obj_func.get_objective_function();
  	  //Edumy = OFr->get_calidad_minima_ObjFunc();
	  //printf("miVertexMover::loop_over_mesh - Calculamos calidad minima patch en bounding box\n");
	  /* logBarrier_hSigma, Qmin tal que Dmax = Dmax_i * 1,01 */ double Ddumy = OFr->get_calidad_minima_patch(patch); Ddumy *= 1.001;
	  /* logBarrier_MixedQ, Qmin tal que dF/dQmin = 0 */ //double Ddumy = OFr->get_calidad_minima2_patch(patch);
	  /* logBarrier_MixedQ, prueba que funciona mejor */ //double Ddumy = OFr->get_calidad_minima_patch(patch);
	  /* logBarrier_MixedQ, prueba que funciona mejor */ //Ddumy -= (fabs(Ddumy) * 0.001); // desenreda TangledCube
	  /* BUENO */  OFr->set_calidad_minima(Ddumy); // inicializamos valor de Qmin de la funcion log-barrier
	  //printf("miVertexMover::loop_over_mesh - Calidad minima patch en bounding box= %9.2e\n", Ddumy);
#ifdef debugMensajes
  	  const char* Cdumy = OFr->get_quality_metric()->get_name().c_str();
  	  printf("miVertexMover::loop_over_mesh - logBarrierObjFunc - Metrica= %s\n", Cdumy);
	  double Dmu = OFr->get_mu();
	  printf("miVertexMover::loop_over_mesh - Calidad minima patch en bounding box= %9.2e, mu= %9.2e\n", Ddumy, Dmu);
	  printf("miVertexMover::loop_over_mesh - Nueva barrera Calidad minima OF log-barrier= %9.2e\n", OFr->get_calidad_minima_ObjFunc());
#endif
  } else if( dynamic_cast<logBarrierObjFunc2*>(obj_func.get_objective_function()) != NULL){
  	  logBarrierObjFunc2* OFr = (logBarrierObjFunc2*) obj_func.get_objective_function();
	  long int indice_max; // indice local del patch donde Q es maxima
	  /* Qmin actual del patch */ double Ddumy = OFr->get_calidad_minima_patch(patch, indice_max);
	  OFr->set_elemDistorcionMax(indice_max);
	  OFr->set_calidad_maxima(Ddumy);
#ifdef debugMensajes
  	  const char* Cdumy = OFr->get_quality_metric()->get_name().c_str();
  	  printf("miVertexMover::loop_over_mesh - logBarrierObjFunc2 - Metrica= %s\n", Cdumy);
	  double Dmu = OFr->get_mu();
	  printf("miVertexMover::loop_over_mesh - Calidad maxima patch en bounding box= %9.2e, mu= %9.2e\n", Ddumy, Dmu);
	  printf("miVertexMover::loop_over_mesh - Nuevo elemento con Qmax log-barrier2= %li, orden_max= %li\n", OFr->get_elemDistorcionMax(), indice_max);
#endif
  } else if( dynamic_cast<hSigmaBarrierObjFunc*>(obj_func.get_objective_function()) != NULL){
  	  hSigmaBarrierObjFunc* OFr = (hSigmaBarrierObjFunc*) obj_func.get_objective_function();
	  /* Se obtiene Qmin de la estrella */ 	double Ddumy = OFr->get_calidad_minima_patch(patch);
	  					Ddumy *= 1.0; // para 1_h, Qmin tal que Dmax = Dmax_i * 1.0
	  					//Ddumy *= 0.99; // para 1_hPrima, Qmin tal que Dmax = Dmax_i * 0.99
	  OFr->set_calidad_minima(Ddumy); // inicializamos valor de Qmin de la funcion log-barrier
#ifdef debugMensajes
  	  const char* Cdumy = OFr->get_quality_metric()->get_name().c_str();
  	  printf("miVertexMover::loop_over_mesh - hSigmaBarrierObjFunc - Metrica= %s\n", Cdumy);
	  double Dmu = OFr->get_mu();
	  //printf("miVertexMover::loop_over_mesh - Calidad max patch en bounding box= %9.4e, mu= %9.2e\n", Ddumy, Dmu);
	  printf("miVertexMover::loop_over_mesh - Nueva barrera Calidad max OF log-barrier= %9.4e\n", OFr->get_calidad_minima_ObjFunc());
#endif
  }
// TMP

        this->optimize_vertex_positions( patch, err );

// TMP
//
/*
  if(OFdumy != NULL ){ // solo se activa si la funcion objectivo es de tipo log-barrier
	OFr->set_calidad_minima(Edumy);
	//printf("miVertexMover::loop_over_mesh - Restauro calidad minima OF log-barrier= %9.2e\n", Edumy);
  }
*/
//
// TMP

#ifdef debugMensajes
        std::cout << "P[" << get_parallel_rank() << "] miVertexMover, optimizando freeVTX ... FIN" << std::endl;
#endif

#ifdef debugBoundingBox
#ifdef debugMensajes
        std::cout << "P[" << get_parallel_rank() << "] miVertexMover, Devolvemos el patch a sus dimensiones originales" << std::endl;
#endif
#endif

#ifdef debugBoundingBox
        this->patch_enBoundingBox(patch, vectorTransformacion, false, tag_data_ID);
#endif

        if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)("optimize_vertex_positions", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;
      
          // Update for changes during inner iteration 
          // (during optimizer loop)
        
        outer_crit->accumulate_patch( patch, err );
        if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)("outer accumulate_patch", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;
        
        inner_crit->cull_vertices( patch, obj_func, err );
        if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)("inner cull_vertices", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;

                // experimental...
                if (0)
                  {
                    inner_crit->cull_vertices_global (patch, 
                                                      mesh, domain, settings,
                                                      obj_func, err);
                    if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)("cull_vertices_global", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;
                  }
        
        patch.update_mesh( err, coord_tag_ptr );
        if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)("update_mesh", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;

// Calculo lo elementos invertidos del patch
 //icount=0;
 //for (size_t i = 0; i < patch_elements.size() ; i++) {
    //patch.element_by_index(i).check_element_orientation(patch, inverted, samples, err);
 //   patch.element_by_index(i).check_element_orientation(patch, iDumy1, iDumy2, err);
 //   if (iDumy1){
 //     ++icount;
 //   }
 //}	
 //std::cout << "P[" << get_parallel_rank() << "] miVertexMover - icount= " << icount << std::endl;

      } // if (!inner_crit->terminate()) --> termina las iteraciones internas sobre el patch

#ifdef debugMensajes
      elemInvIterIntFin = inner_crit->count_inverted( patch, err );
      dumyInt = elemInvIterIntFin - elemInvIterIntIni;
      acumulado += dumyInt;
      std::cout << "\nP[" << get_parallel_rank() << "] miVertexMover, TERMINA iteracionInterna, Patch: " << num_patches << "/" << patch_list.size() << ", elemInvertidosPatch: " << (int)elemInvIterIntFin << ", incrementoInvertidos: " << (int)dumyInt << ", incrInvAcumulado: " << (int)acumulado << std::endl;
/*
    global_patch.attach_settings( settings );
    global_patch.set_mesh( mesh );
    global_patch.set_domain( domain );
    global_patch.fill_global_patch( err );
    elemInvIterExtFin = outer_crit->count_inverted( global_patch, err ); // se cuenta el numero de elem invertidos contandolos
    CRC = elemInvIterExtFin - outer_crit->globalInvertedCount;
      std::cout << "\nP[" << get_parallel_rank() << "] miVertexMover, TERMINA iteracionInterna, Patch: " << num_patches << "/" << patch_list.size() << ", elemInvertidosPatch: " << elemInvIterIntFin << ", incrementoInvertidos: " << (int) dumyInt << ", incrInvAcumulado: " << (int)acumulado << ", CRC: " << (int)CRC << std::endl;
*/
#endif

    } // while(p_iter.... --> termina las iteraciones externas sobre todos los patch de la mañ

    global_patch.attach_settings( settings );
    global_patch.set_mesh( mesh );
    global_patch.set_domain( domain );
    global_patch.fill_global_patch( err );
    //elemInvIterExtFin = outer_crit->count_inverted( global_patch, err ); // se cuenta el numero de elem invertidos contandolos
    elemInvIterExtFin = outer_crit->globalInvertedCount;
    acumuladoTotal += acumulado;

    // CRC = - elementos invertidos al final de esta iteracion externa (elemInvIterExtFin) + elementos invertidos qu aumenta (si negativo: disminuyen, acumuladoTotal) desde principio de optimizacion + elementos invertidos antes de empezar toda la optimizacion (elemInvIni) -- CRC tiene que ser 0, si no, error que no reduce bien el numero de elementos invertidos
    CRC = elemInvIterExtFin - outer_crit->globalInvertedCount;
    dumy = elemInvIterExtFin - elemInvIterExtIni;

    float tiempo = c_timer.since_birth();
    size_t N = QM->get_NtetraVisitados() ;

#ifdef PAPI
  us  = PAPI_get_real_usec(  ) - usPrev; // en uSegundos
#endif

    float dumy11 =  ((float)N-(float)Nprev) / ((float)us * (float)1e-6) ;

    Npatches_1 = this->count_Npatches();
    NpatchesInterior += (Npatches_1-Npatches_0);
    //this->set_NpatchesInterior(NpatchesInterior);

    cout << "\nP[" << drank << 
	"] miVertexMover::loop_over_mesh, TERMINA Iteracion Externa Paralela numero: " << 
	1 + outer_crit->iterationCounter << "/" <<  outer_crit->iterationBound << 
	", \n\tmaxMovement/minMovPermitido= " << 
	sqrt(outer_crit->maxSquaredMovement) << "/" << sqrt(outer_crit->vertexMovementAbsoluteEps) << 
	", \n\tElemInverted(incremento)= " << (int)elemInvIterExtFin << 
	" (sumaExt: " << (int)dumy << ", sumaInt: " << (int)acumulado << 
	"), \n\tOFtodaMalla= " << outer_crit->currentOFValue << 
	", \n\tCRC: " << (int)CRC << 
	", \n\tt.since_birth= " << tiempo << " s, t_estaIterPAPI= " << us*1e-6 << "s" <<
        ", \n\tNtetraVisitados: " << N << 
        ", \n\tNpatchesInteriorVisitados: " << NpatchesInterior <<
        ", \n\tTetraPerSeg = " << (float)N / tiempo << 
        ", \n\tNtetraVisitados/estaIteracion: " << N-Nprev << 
      	", \n\tNumeroFreeVXTinterno: " << NinteriorFreeVTX << 
        ", \n\tTetraPerSeg/estaIteracion = " << dumy11 << endl << endl;

    save_or_restore_debug_state(false);

    if (1)
      {
        bool pdone_inner=false;

        if (MSQ_CHKERR(perr))
          {
            std::cout << "P[" << get_parallel_rank() << "] VertexMover::loop_over_mesh found parallel error: " << perr << "\n quitting... pdone_inner= " << pdone_inner << std::endl;
            pdone_inner = true;
          }

        helper->communicate_any_true (pdone_inner, err);

        if (0)
          std::cout << "P[" << get_parallel_rank() << "] tmp srk inner_iter= " << inner_iter << " outer_iter= " << outer_iter << " pdone_inner= " << pdone_inner << std::endl;

        if (pdone_inner)
          {
             if (0)
              std::cout << "P[" << get_parallel_rank() << "] tmp srk found parallel error, quitting... pdone_inner= " << pdone_inner << std::endl;
            MSQ_SETERR(err)("PARALLEL ERROR", MsqError::PARALLEL_ERROR);
            break;
          }
      }

    /// srkenno@sandia.gov save vertex bytes since boundary smoothing changes them
    std::vector<unsigned char> saved_bytes;
    checkpoint_bytes(mesh, saved_bytes, err); 
    if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)("checkpoint_bytes ", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;

    helper->communicate_first_independent_set(err); 
    if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)("communicate_first_independent_set ", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;


    ///*** smooth the boundary ***////

    Npatches_0 = this->count_Npatches();
    Npatches_1 = 0;

//#ifdef debugMensajes
    std::cout << "P[" << get_parallel_rank() << "] miVertexMover, EMPIEZA iteracion Interna-Boundary" << std::endl; 
    imprimeUsoMemoria();
//#endif
    acumulado = 0; // donde se van guardan el saldo de elementos invertidos desenredado/re-enredados en una iteracion externa en la parte del boundary

    save_or_restore_debug_state(true);
    MsqDebug::disable_all();


    // independent_set: vtx frontera de este procesador que tiene un numero aleatorio
    // mayor que los otros vtx de tetra que pertenecen a otros procesadores
    nF=0; // numero de fase boundary
    while (helper->compute_next_independent_set())
    {
      nF++;
      if (outer_crit->iterationCounter == 0) {
        std::cout << "P[" << drank << "] miVertexMover - EMPIEZA faseBoundary = " << nF << ", NboundaryFreeVTX: " << NboundaryFreeVTX << std::endl;
      }
      // Loop over all boundary elements
      while(helper->get_next_partition_boundary_vertex(vertex_handle))
      {
	//std::cout << "P[" << get_parallel_rank() << "] miVertexMover - Boundary freeVTXhandle= " << (size_t)vertex_handle << std::endl;

        // se muestra informacion del nodo libre del patch de la frontera
        // se usa para ver el orden de procesamiento de los free vtx
      //if (outer_crit->iterationCounter >= 0) {
      if (outer_crit->iterationCounter == 0) {
	size_t dumy = (size_t)vertex_handle; // ID local del free vtx
      	//std::cout << "P[" << get_parallel_rank() << "] ORDENboundary " << NboundaryFreeVTX << " - " <<
      	std::cout << "ORDENboundary " << 
	  //"mesh.freeVtxLOCAL_ID= " << dumy << 
	  //", mesh.freeVtxGLOBAL_ID= " << (size_t)tag_data_ID[dumy] << std::endl;
	  (size_t)tag_data_ID[dumy] << " (P" << drank << ") patchesAcu: " << this->count_Npatches()-Npatches_0 << " patchesDif: " << this->count_Npatches()-Npatches_0-Npatches_1 << std::endl;
	Npatches_1=this->count_Npatches()-Npatches_0;
	  //(size_t)tag_data_ID[dumy] << std::endl;
	//fprintf(fileOrden, "%lu\n",(size_t)tag_data_ID[dumy]);
	NboundaryFreeVTX++;
      }

	patch_vertices.clear();
	patch_vertices.push_back(vertex_handle);
	patch_elements.clear(); 
	mesh->vertices_get_attached_elements( &vertex_handle, 
                                              1,
                                              patch_elements, 
                                              junk, err );

	all_culled = false;
	patch.set_mesh_entities( patch_elements, patch_vertices, err );

	if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)("set_mesh_entities 2 ", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;
        
        // Initialize for inner iteration
#ifdef debugMensajes
	std::cout << "P[" << get_parallel_rank() << "] miVertexMover - Boundary freeVTXhandle= " << (size_t)vertex_handle << std::endl;
	//for(int i=0;i<patch_elements.size();i++){
	//  std::cout << "P[" << get_parallel_rank() << "] miVertexMover - Boundary pd.elem[" << i << "]= " << (size_t)patch_elements[i] << std::endl;
	//}
#endif
        
	this->initialize_mesh_iteration(patch, err);
	if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)(" initialize_mesh_iteration 2", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;
	
	obj_func.reset();
	
	outer_crit->reset_patch( patch, err );
	if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)("outer reset_patch 2 ", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;
	
	inner_crit->reset_inner( patch, obj_func, err );
	if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)(" inner reset_inner 2", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;
      
	inner_crit->reset_patch( patch, err );
	if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)(" inner_crit reset_patch 2", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;
      
        // Don't even call optimizer if inner termination 
        // criterion has already been met.
        elemInvIterIntIni = inner_crit->count_inverted( patch, err );

	if (!inner_crit->terminate())
	{
    	  //std::cout << "P[" << get_parallel_rank() << "] VertexMover, Boundary-Inner" << std::endl; 
	  inner_crit_terminated = false;
	  
          // Call optimizer - should loop on inner_crit->terminate()


	// MIO: transforma coordenadas patch en un bounding box
#ifdef debugBoundingBox
#ifdef debugMensajes
      std::cout << "P[" << get_parallel_rank() << "] miVertexMover-Boundary, Transformamos el patch al Bounding Box" << std::endl;
#endif
#endif
#ifdef debugBoundingBox
	float  vectorTransformacion[4];
        this->patch_enBoundingBox(patch, vectorTransformacion, true, tag_data_ID);
#endif

	  this->optimize_vertex_positions( patch, err );

#ifdef debugBoundingBox
#ifdef debugMensajes
        std::cout << "P[" << get_parallel_rank() << "] miVertexMover-Boundary, Devolvemos el patch a sus dimensiones originales" << std::endl;
#endif
#endif

#ifdef debugBoundingBox
        this->patch_enBoundingBox(patch, vectorTransformacion, false, tag_data_ID);
#endif

	  if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)(" optimize_vertex_positions 2", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;
      
          // Update for changes during inner iteration 
          // (during optimizer loop)
	  
	  outer_crit->accumulate_patch( patch, err );
	  if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)(" outer accumulate_patch 2", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;
	  
	  inner_crit->cull_vertices( patch, obj_func, err );
	  if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)(" inner cull_vertices", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;

                    // FIXME
                    if (0)
                      {
                        inner_crit->cull_vertices_global (patch, 
                                                          mesh, domain, settings,
                                                          obj_func, err);
                        if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)(" cull_vertices_global 2 ", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;
                      }
        
          patch.update_mesh( err, coord_tag_ptr );
	  if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)(" update_mesh 2", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;

	} // if (!inner_crit->terminate()) --> nodos del boundary

        elemInvIterIntFin = inner_crit->count_inverted( patch, err );
        dumyInt = elemInvIterIntFin - elemInvIterIntIni; // saldo de nodos desenredados/re-enredados en un patch del boundary
        acumulado += dumyInt;
#ifdef debugMensajes
        std::cout << "P[" << get_parallel_rank() << "] miVertexMover, TERMINA optimiza patch Boundary, saldo este patch: " << (int)dumyInt << ", acumulado: " << (int)acumulado << std::endl; 
#endif

      } // while(helper->get_next_partition_boundary_vertex(vertex_handle)) --> nodos del boundary

      helper->communicate_next_independent_set(err);
      if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)(" communicate_next_independent_set 2", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;

     } // termina smooth boundary, while(helper->compute_next_independent_set())
 
    save_or_restore_debug_state(false);
 
    //if (!get_parallel_rank())
    //std::cout << "P[" << get_parallel_rank() << "] tmp srk num_patches= " << num_patches << std::endl;

    /// srkenno@sandia.gov restore vertex bytes since boundary smoothing changes them
    restore_bytes(mesh, saved_bytes, err);
    if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)(" restore_bytes", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;

    if (jacobiOpt)
      commit_jacobi_coords( coord_tag, mesh, err );

    this->terminate_mesh_iteration(patch, err); 
    if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)(" terminate_mesh_iteration", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;
    
    outer_crit->accumulate_outer( mesh, domain, obj_func, settings, err );
    if (MSQ_CHKERR(err)) { MSQ_SETERR(perr)(" outer_crit accumulate_outer", MsqError::INVALID_STATE); PERROR_COND; } //goto ERROR;


#ifdef debugMensajes
    std::cout << "P[" << get_parallel_rank() << "] miVertexMover, TERMINA iteracion Interna-Boundary y la iteracion de la malla" << std::endl; 
#endif

// ejecuto qualityassesor al final de cada iteracion externa
    //std::vector< Mesh::ElementHandle > elements;
    //mesh->get_all_elements (elements, err);
    //q1.run_common( &mesh_and_domain, mesh, (Settings*)settings, err );
    // MIO q1.run_common( &mesh_and_domain, 0, (Settings*)settings, err );
    //qa_untangle.get_inverted_element_count(iDumy1, iDumy2, err);
    //std::cout << "\nP[" << get_parallel_rank() << "], miVertexMover, TERMINA iteracion externa: " << outer_iter << " - Elementos Invalidos: " << iDumy1 <<"/" << elements.size() << std::endl;

    /*PRUEBA*/ helper->smoothing_close(err); MSQ_CHKERR(err);

    global_patch.attach_settings( settings );
    global_patch.set_mesh( mesh );
    global_patch.set_domain( domain );
    global_patch.fill_global_patch( err );
  
    elemInvIterExtFin = count_inverted(global_patch, err);
    /*fuerzo ...*/ outer_crit->globalInvertedCount = elemInvIterExtFin;

    //elemInvIterExtFin=  outer_crit->globalInvertedCount;
    //elemInvIterExtFin = outer_crit->count_inverted( global_patch, err ); // se cuenta el numero de elem invertidos contandolos
    acumuladoTotal += acumulado;

    // CRC = - elementos invertidos al final de esta iteracion externa (elemInvIterExtFin) + elementos invertidos qu aumenta (si negativo: disminuyen, acumuladoTotal) desde principio de optimizacion + elementos invertidos antes de empezar toda la optimizacion (elemInvIni) -- CRC tiene que ser 0, si no, error que no reduce bien el numero de elementos invertidos
    CRC = elemInvIterExtFin - outer_crit->globalInvertedCount;
    dumy = elemInvIterExtFin - elemInvIterExtIni;
    tiempo = c_timer.since_birth();
    N = QM->get_NtetraVisitados();

#ifdef PAPI
  retval = PAPI_read( EventSet , values);
  for(int k=0; k < NUM_EVENTS; k++) valor_tmp[k] = values[k] - valor_tmp[k];
  us  = PAPI_get_real_usec(  ) - usPrev; // en uSegundos
#endif

    Npatches_1 = this->count_Npatches();
    Npatches_1 -= Npatches_0;
    NpatchesFrontera += Npatches_1;
    //NpatchesFrontera += (Npatches_1-Npatches_0);
    //this->set_NpatchesFrontera(NpatchesFrontera);

    dumy11 =  ((float)N-(float)Nprev) / ((float)us * (float)1e-6) ;
    cout << "\nP[" << drank << 
	"] miVertexMover::loop_over_mesh, TERMINA Iteracion Externa Paralela Particion+Boundary numero: " 
	<< outer_crit->iterationCounter << "/" <<  outer_crit->iterationBound << 
	", \n\tmaxMovement/minMovPermitido= " 
	<< sqrt(outer_crit->maxSquaredMovement) << "/" << sqrt(outer_crit->vertexMovementAbsoluteEps) << 
	", \n\tElemInverted(incremento)= " << (int)elemInvIterExtFin << 
	" (sumaExt: " << (int)dumy << ", sumaInt: " << (int)acumulado << 
	"), \n\tOFtodaMalla= " << outer_crit->currentOFValue << 
	", \n\tCRC: " << (int)CRC << 
	//", \n\tt= " << tiempo << " s" <<
	", \n\tt.since_birth= " << tiempo << " s, t_estaIterPAPI= " << us*1e-6 << "s, " <<
        ", \n\tNtetraVisitados: " << N << 
        ", \n\tNpatchesFronteraVisitados: " << NpatchesFrontera << 
        ", \n\tNtetraVisitados/estaIteracion: " << N-Nprev << 
        ", \n\tTetraPerSeg = " << (float)N / tiempo << 
        ", \n\tTetraPerSeg/estaIteracion = " << dumy11 << 
        ", \n\tNtetraVisitados/estaIteracion: " << N-Nprev << 
      	", \n\tNumeroFreeVXTboundary: " << NboundaryFreeVTX << 
      	", \n\tNumeroFasesFrontera: " << nF << 
        ", \n\tTetraPerSeg/estaIteracion = " << dumy11 << endl ;
#ifdef PAPI
  MFlops = (float)valor_tmp[0] / us ; // valor_tmp[0] = PAPI_FP_INS, us: tiempo en microsegundos
  printf("estaIteracion, PAPIseg: %4.2f seg, MFlops-DP/s: %5.2e ", (float)us/1.0e6, MFlops);

  //float fdumy2 = 100.0*(float)valor_tmp[1]/(float)valor_tmp[0]; // Frecuencia fallos L1 por instruccion flotante
  //printf(" L1missFlop(%%): %5.3f ",fdumy2);
  //printf(" L2missFlop(%%): %5.3f ",fdumy2);

  //fdumy2 = 100.0*(float)valor_tmp[2]/(float)valor_tmp[0]; // Frecuencia fallos L2 por instruccion flotante
  //printf(" L2missFlop(%%): %5.3f\n",fdumy2);
  //printf(" L3missFlop(%%): %6.4f",fdumy2);

  MFlops = (float)valor_tmp[0] / ((float)N-(float)Nprev) ; // valor_tmp[0] = PAPI_FP_INS, us: tiempo en microsegundos
  printf(" Flops-DP/tetra: %5.1f  TetraPerSeg/estaIteracion: %5.2e ", MFlops, dumy11);

  iteraciones++;

  for(int k=0; k < NUM_EVENTS; k++) {
        PAPI_event_code_to_name(Events[k],eventName);
        printf("\n %s: %llu ", eventName, valor_tmp[k] );
        valor_promedio[k]= valor_promedio[k]+valor_tmp[k];       
	printf("\n total: %llu ", valor_promedio[k]);
        printf("promedio %i: %llu \n", iteraciones, (valor_promedio[k]/iteraciones) );
  }
  printf("\n");
#endif
	printf("\n");

  } // termina iteracion externa de optimizacion: particion interna + boundary

//ERROR: 

  if (MSQ_CHKERR(err)) {
    std::cout << "P[" << get_parallel_rank() << "] VertexMover::loop_over_mesh error = " << err.error_message() << std::endl;
  }

  if (jacobiOpt)
    mesh->tag_delete( coord_tag, err );

    //call the criteria's cleanup funtions.
  outer_crit->cleanup(mesh,domain,err); MSQ_CHKERR(err);
  inner_crit->cleanup(mesh,domain,err); MSQ_CHKERR(err);
    //call the optimization cleanup function.
  this->cleanup();

    // close the helper
  // ORIG: helper->smoothing_close(err); MSQ_CHKERR(err);

    //q1.run_common( &mesh_and_domain2, mesh, (Settings*)settings, err );
    q1.run_common( &mesh_and_domain2, 0, (Settings*)settings, err );
    cout << "\nP[" << drank << "] AQUI ----------" << 
	", \n\tElemInverted2= " << inv_check.get_results(&metric)->get_invalid_element_count() << std::endl;

  free(tag_data_ID);
  //fclose(fileOrden);

#ifdef PAPI
  retval = PAPI_stop( EventSet , values);
#endif

    this->set_NpatchesInterior(NpatchesInterior);
    this->set_NpatchesFrontera(NpatchesFrontera);

  return 0.;
}

    
void VertexMover::initialize_queue( MeshDomainAssoc* mesh_and_domain,
                                    const Settings* settings,
                                    MsqError& err )
{
  QualityImprover::initialize_queue( mesh_and_domain, settings, err ); MSQ_ERRRTN(err);
  objFuncEval.initialize_queue( mesh_and_domain, settings, err ); MSQ_ERRRTN(err);
}

TagHandle VertexMover::get_jacobi_coord_tag( Mesh* mesh, MsqError& err )
{
    // Get tag handle
  const char tagname[] = "msq_jacobi_temp_coords";
  TagHandle tag = mesh->tag_create( tagname, Mesh::DOUBLE, 3, 0, err );  MSQ_ERRZERO(err);
    /* VertexMover will always delete the tag when it is done, so it is probably
       best not to accept an existing tag
  if (err.error_code() == TAG_ALREADY_EXISTS) {
    err.clear();
    tag = tag_get( tagname, err ); MSQ_ERRZERO(err);
    std::string name;
    Mesh::TagType type;
    unsigned length;
    mesh->tag_properties( tag, name, type, length, err ); MSQ_ERRZERO(err);
    if (type != Mesh::DOUBLE || length != 3) {
      MSQ_SETERR(err)(TAG_ALREADY_EXISTS,
                     "Tag \"%s\" already exists with invalid type", 
                     tagname);
      return 0;
    }
  }
    */
  
    // Initialize tag value with initial vertex coordinates so that
    // vertices that never get moved end up with the correct coords.
  std::vector<Mesh::VertexHandle> vertices;
  mesh->get_all_vertices( vertices, err ); MSQ_ERRZERO(err);
    // remove fixed vertices
    // to avoid really huge arrays (especially since we need to copy
    // coords out of annoying MsqVertex class to double array), work
    // in blocks
  const size_t blocksize = 4096;
  std::vector<bool> fixed(blocksize);
  MsqVertex coords[blocksize];
  double tag_data[3*blocksize];
  for (size_t i = 0; i < vertices.size(); i += blocksize) {
    size_t count = std::min( blocksize, vertices.size() - i );
      // remove fixed vertices
    mesh->vertices_get_fixed_flag( &vertices[i], fixed, count, err ); MSQ_ERRZERO(err);
    size_t w = 0;
    for (size_t j = 0; j < count; ++j)
      if (!fixed[j])
        vertices[i + w++] = vertices[i + j];
    count = w;
      // store tag data for free vertices
    mesh->vertices_get_coordinates( &vertices[i], coords, count, err ); MSQ_ERRZERO(err);
    for (size_t j = 0; j < count; ++j) {
      tag_data[3*j  ] = coords[j][0];
      tag_data[3*j+1] = coords[j][1];
      tag_data[3*j+2] = coords[j][2];
    }
    mesh->tag_set_vertex_data( tag, count, &vertices[i], tag_data, err ); MSQ_ERRZERO(err);
  }
  
  return tag;
}

void VertexMover::commit_jacobi_coords( TagHandle tag, Mesh* mesh, MsqError& err )
{
  Vector3D coords;
  std::vector<Mesh::VertexHandle> vertices;
  mesh->get_all_vertices( vertices, err ); MSQ_ERRRTN(err);
  std::vector<bool> fixed( vertices.size() );
  mesh->vertices_get_fixed_flag( &vertices[0], fixed, vertices.size(), err );
  for (size_t i = 0; i < vertices.size(); ++i) {
    if (!fixed[i]) {
      mesh->tag_get_vertex_data( tag, 1, &vertices[i], coords.to_array(), err ); MSQ_ERRRTN(err);
      mesh->vertex_set_coordinates( vertices[i], coords, err ); MSQ_ERRRTN(err);
    }
  }
}

// true: to bounding box
// false: from bounding box
#define anchoBoundingBox 2.0 // ancho del bounding box: maximo - minimo
//#define anchoBoundingBox 1.01 // prueba para hueso
void VertexMover::patch_enBoundingBox(PatchData &pd, float (&vectorTransformacion)[4], bool to_from_boundingBox, size_t * tag_data_ID)
//void VertexMover::patch_enBoundingBox(PatchData &pd, float (&vectorTransformacion)[4], bool to_from_boundingBox)
{
   MsqVertex * vertexArray;
   Mesh::VertexHandle * vertex_handle;
   double incremento, maximos[3], minimos[3];
   int N, i, j;

   size_t e2 = ElemSampleQM::elem(0); // indice del elemento 0 para ver si es 2D o 3D, supongo mallas uniformes
   MsqMeshEntity elem2 = pd.element_by_index( e2 ); // estructura asociada al elemento
   EntityTopology typeDelta = elem2.get_element_type(); // tipo de elemento
   unsigned edim = TopologyInfo::dimension( typeDelta ); // 3D: edim=3, 2D: edim=2

#ifdef debugMensajes
   std::cout << "P[" << get_parallel_rank() << "] miVertexMover, patch_enBoundingBox - numero vertices patch: " << pd.num_nodes() << std::endl;
#endif
   vertexArray = (MsqVertex*)pd.get_vertex_array(); // puntero al array de vertices del patch
   vertex_handle = pd.get_vertex_handles_array();
   // se buscan las coordenadas maxima y minimas en X,Y,Z
   N=pd.num_nodes(); // numero vertices del patch
#ifdef debugMensajes
   std::cout << "P[" << get_parallel_rank() << "] miVertexMover, patch_enBoundingBox - informacion de vertices patch y su bounding box INICIAL" << std::endl;
#endif
   // calcula el bounding box y devuelve sus parametros
   //verDatos(vertexArray, N, incremento, maximos, minimos); 
   verDatos(vertexArray, N, incremento, maximos, minimos,vertex_handle, tag_data_ID); 
   // to bounding box, to_from_boundingBox=TRUE
   if(to_from_boundingBox && incremento>0.0){
	// escalamos el patch
   	for(i=0; i<N; i++){
   	   for(j=0; j<edim; j++){
   	   //for(j=0; j<3; j++){
		vertexArray[i][j] *= (anchoBoundingBox/incremento);
	   }
	}
	// creamos vectorTransformacion
	vectorTransformacion[0] = incremento; // M
   	for(j=0; j<3; j++) vectorTransformacion[j+1]=0;

#ifdef debugMensajes
   std::cout << "P[" << get_parallel_rank() << "] miVertexMover, patch_enBoundingBox - informacion de vertices patch y su bounding box ESCALADO" << std::endl;
#endif
        // vuelve a calcular el bounding box ahora escalado y devuelve sus parametros
   	//verDatos(vertexArray, N, incremento, maximos, minimos);
        verDatos(vertexArray, N, incremento, maximos, minimos,vertex_handle, tag_data_ID); 
   	for(j=0; j<edim; j++){
   	//for(j=0; j<3; j++){
	 if(maximos[j] > 0.5*anchoBoundingBox) {
	   vectorTransformacion[j+1] = maximos[j]-1;
	 }
	 else if(minimos[j] < -0.5*anchoBoundingBox){
	   vectorTransformacion[j+1] = minimos[j]+1;
	 }
	}
	// desplazamos el patch
   	for(i=0; i<N; i++){
   	   for(j=0; j<edim; j++){
   	   //for(j=0; j<3; j++){
		vertexArray[i][j] -= vectorTransformacion[j+1];
	   }
	}
#ifdef debugMensajes
	// para comprobar que patch esta en bounding box
        std::cout << "P[" << get_parallel_rank() << "] miVertexMover, patch_enBoundingBox - informacion de vertices patch y su bounding box FINAL" << std::endl;
   	//verDatos(vertexArray, N, incremento, maximos, minimos);
        verDatos(vertexArray, N, incremento, maximos, minimos,vertex_handle, tag_data_ID); 
#endif
   }
   // FROM bounding box, to_from_boundingBox=FALSE
   else if(!to_from_boundingBox){
   	for(i=0; i<N; i++){
   	   for(j=0; j<edim; j++){
   	   //for(j=0; j<3; j++){
		vertexArray[i][j] += vectorTransformacion[j+1]; // se desplaza primero 
		vertexArray[i][j] *= (vectorTransformacion[0]/anchoBoundingBox); // se escala luego
	   }
	}
#ifdef debugMensajes
	// para comprobar que patch esta en bounding box original
   	std::cout << "\tinformacion de vertices y su bounding box ORIGINAL" << std::endl;
   	//verDatos(vertexArray, N, incremento, maximos, minimos);
        verDatos(vertexArray, N, incremento, maximos, minimos,vertex_handle, tag_data_ID); 
#endif
   }
}
void verDatos(MsqVertex* vertexArray, int N, double &incremento, double (&maximos)[3], double (&minimos)[3], Mesh::VertexHandle* vertex_handle, size_t * tag_data_ID){
//void verDatos(MsqVertex* vertexArray, int N, double &incremento, double (&maximos)[3], double (&minimos)[3]){
   double dumy;
   char etiquetas[3][6]={"eje-X","eje-Y","eje-Z"};
   for(int j=0; j<3; j++){
      maximos[j]=-1*MSQ_MAX;
      minimos[j]=MSQ_MAX;
   }
   incremento=-1*MSQ_MAX; 
   for(int i=0; i<N; i++){
#ifdef debugMensajes
	//std::cout << "\t" << i << ": " << vertexArray[i] << std::endl;
	size_t dumy = (size_t)vertex_handle[i];
	std::cout << "\tP[" << get_parallel_rank() << 
	"], lID: " << dumy << 
	//"], lID: " << printf("%3lu", dumy) << 
	", gID: " << (size_t)tag_data_ID[dumy] << 
	//", gID: " << printf("%3lu", (size_t)tag_data_ID[dumy]) << 
	", x-y-z: " << vertexArray[i] << std::endl ; //<< std::fflush(std::cout);
	//", x-y-z: " << printf("%5.1f",vertexArray[i][0]) << std::endl ;
	//" " << printf("%5.1f",vertexArray[i][1]) << 
	//" " << printf("%5.1f",vertexArray[i][2]) << std::endl ; //<< std::fflush(std::cout);
#endif
	for(int j=0; j<3; j++){
		if(vertexArray[i][j] > maximos[j]) maximos[j] = vertexArray[i][j];
		if(vertexArray[i][j] < minimos[j]) minimos[j] = vertexArray[i][j];
	}
   }
   for(int j=0; j<3; j++){
#ifdef debugMensajes
	std::cout << "\tMaximo " << etiquetas[j] << ": " << maximos[j] ;
	std::cout << "\tMinimo " << etiquetas[j] << ": " << minimos[j] ;
#endif
	dumy = maximos[j]-minimos[j];
#ifdef debugMensajes
	std::cout << "\tMaximo-Minimo " << etiquetas[j] << ": " << dumy << std::endl;
#endif
	if(dumy>incremento) incremento=dumy;
   }
#ifdef debugMensajes
   std::cout << "\tM: " << incremento << std::endl; // mayor diferencia maximo-minimo en X-Y-Z
#endif
}

size_t count_inverted( PatchData& pd, MsqError& err )
{
   size_t num_elem = pd.num_elements();
   size_t count=0;
   int inverted, samples;
   for (size_t i = 0; i < num_elem; i++) {
        pd.element_by_index(i).check_element_orientation(pd, inverted, samples, err);
        if (inverted)
        ++count;
   }


//printf("count_inverted, num_elem= %lu, invertidos= %lu\n",num_elem,count);
   return count;
}

} // namespace Mesquite

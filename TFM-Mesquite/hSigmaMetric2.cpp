/* ***************************************************************** 
    MESQUITE -- The Mesh Quality Improvement Toolkit

    Copyright 2007 Sandia National Laboratories.  Developed at the
    University of Wisconsin--Madison under SNL contract number
    624796.  The U.S. Government and the University of Wisconsin
    retain certain rights to this software.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License 
    (lgpl.txt) along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    (2008) kraftche@cae.wisc.edu    
    (2013) dbenitez@siani.es

  ***************************************************************** */


/** \file hSigmaMetric2.cpp
 *  \brief 
 *  \author Domingo Benitez
 *  Calcula la metrica SUS de un tetraedro
 */
//#define miDebug

#include "Mesquite.hpp"
#include "hSigmaMetric2.hpp"
#include "MsqMatrix.hpp"
#include "MsqError.hpp"
#include "Exponent.hpp"
#include <iostream>
#include <cfloat>
#include <limits>

using std::cout;
using std::endl;
using std::fixed;

typedef std::numeric_limits< double > dbl;

namespace MESQUITE_NS {

static inline double h(double sigma, double delta, double epsilon){

	long double sigma_epsilon = sigma - 2.0 * epsilon; 
	long double h = 0.5 * ( sigma + sqrt( (sigma_epsilon*sigma_epsilon) + (4*delta*delta) ) );
	if (h < epsilon) h = epsilon;

	//cout << "\thSigmaMetric2.cpp, sigma= " << sigma << "\tdelta= " << delta << ", h= " << h << endl;

#ifdef miDebug
  	cout << "\n\thSigma.cpp, sigma= " << sigma ;
	cout << ", delta= " << delta ;
	cout << ", delta/sigma= " << delta/sigma ;
	cout << ", hS= " << h << endl ;
	if(sigma < 0){
	  cout << "\thSigma.cpp, sigma= " << sigma ;
	  cout << "\tepsilon= " << epsilon ;
	  cout << "\th= " << h ;
	  cout << "\tdelta= " << delta << endl << endl;
	}
	if (h <= 0) cout << "\th es ZERO, sigma= " << sigma << "\tdelta= " << delta;
	if(sigma < 0){
	  cout << "\tsigma= " << sigma ;
	  cout << "\tsigma_epsilon= " << sigma_epsilon ;
	  cout << "\tepsilon_sigma= " << epsilon ;
	  cout << "\tDBL_EPSILON= " << DBL_EPSILON;
	  cout << "\th= " << h ;
	  cout << "\tdelta= " << delta << endl;
	}
#endif
	return (double) h;
  }

hSigmaMetric2::hSigmaMetric2(double del, double eps)
  : mDelta( del ), mEpsilon( eps )
  { }

hSigmaMetric2::hSigmaMetric2(size_t nodosMalla)
  : mDelta( 1.2e-3), mEpsilon( 1e-10)
  { Nvertices = nodosMalla; }

hSigmaMetric2::hSigmaMetric2(void)
  : mDelta( 1.2e-3), mEpsilon( 1e-10)
  { }

std::string hSigmaMetric2::get_name() const
  { return "hSigmaMetric2"; }

hSigmaMetric2::~hSigmaMetric2() { } //{free(handleVerticesCache);}

void hSigmaMetric2::setNumeroVertices(size_t nodosMalla) 
  { Nvertices = nodosMalla; }

bool hSigmaMetric2::evaluate( const MsqMatrix<2,2>& T, 
                                   double& result, 
                                   MsqError& err )
{
  double hS = h( det( T ), mDelta, mEpsilon );
  //result = sqr_Frobenius(T) / (2 * hS);
  result = hS;
#ifdef miDebug
  cout << "hSigmaMetric2::evaluate2D, result= " << result;
  cout << "\tT= " << T(0,0) << ", " << T(0,1) << ", " << T(1,0) << ", " << T(1,1) << endl;
#endif
  return true;
}


bool hSigmaMetric2::evaluate_with_grad( const MsqMatrix<2,2>& T,
                                             double& result,
                                             MsqMatrix<2,2>& deriv_wrt_T,
                                             MsqError& err )
{
  double determinante = det(T);
  double hS = h( determinante, mDelta, mEpsilon );
  //result = sqr_Frobenius(T) / (2 * hS);
  result = hS;
  deriv_wrt_T  = transpose_adj(T) * hS;
  deriv_wrt_T /= (2 * hS - determinante);
#ifdef miDebug
  cout << "hSigmaMetric2::evaluate_with_grad2D, result= " << result;
  cout << "\tT= " << T(0,0) << ", " << T(0,1) << ", " << T(1,0) << ", " << T(1,1);
  cout << "\tdT= " << deriv_wrt_T(0,0) << ", " << deriv_wrt_T(0,1) << ", " << deriv_wrt_T(1,0) << ", " << deriv_wrt_T(1,1) << endl;
#endif
  return true;
}

double componenteHesiano2D_2(int i, int j, int k, int l, const MsqMatrix<2,2>& T, const MsqMatrix<2,2>& AT, double mDelta, double epsilon){
  double resultado = 0.0;
  double determinante = det(T);
  double hS = h( determinante, mDelta, epsilon );
  double F2 = sqr_Frobenius(T);
  const double inv_hS = 1.0 / hS;
  const double p1 = 2 * hS - determinante;
  if (i==j && k==l) resultado += inv_hS;
  resultado -= ((inv_hS / p1) * (T(i,j) * AT(k,l) + T(k,l) * AT(i,j)));
  resultado += ((inv_hS * F2 / (2*determinante*p1)) * (AT(k,j) * AT(i,l) - AT(i,j) * AT(k,l)));
  resultado += ((inv_hS * F2 / (2*p1*p1)) * (AT(i,j) * AT(k,l)));
  resultado += ((inv_hS * F2 * determinante / (2*p1*p1*p1)) * (AT(i,j) * AT(k,l)));

  return resultado;
}
double componenteHesiano3D_(int i, int j, int k, int l, const MsqMatrix<3,3>& T, const MsqMatrix<3,3>& AT, double mDelta, double epsilon){
  double resultado = 0.0;
  double determinante = det(T);
  double inv_det= 1/determinante;
  double hS = h( determinante, mDelta, epsilon );
  const Exponent d23;
  double d = d23.powTwoThirds(hS);
  double inv_d = 1/d;
  double F2 = sqr_Frobenius(T);
  const double c2_3 = 2/3;
  const double c2_9 = 2/9;
  const double c4_9 = 4/9;
  const double c4_27 = 4/27;
  const double p1 = 2 * hS - determinante;
  const double inv_p1 = 1/p1;
  const double inv_p1_2 = inv_p1 * inv_p1;
  const double inv_p1_3 = inv_p1 * inv_p1 * inv_p1;
  if (i==j && k==l) resultado += (c2_3*inv_d);
  resultado -= (c4_9 * inv_d * inv_p1 * (T(i,j) * AT(k,l) + T(k,l) * AT(i,j)));
  resultado += (c2_9 * inv_d * inv_p1 * inv_det * F2 * (AT(k,j) * AT(i,l) - AT(i,j) * AT(k,l)));
  resultado += (c4_27* inv_d * inv_p1_2 * F2 * AT(i,j) * AT(k,l) );
  resultado += (c2_9 * inv_d * inv_p1_3 * determinante * F2 * AT(i,j) * AT(k,l) );

  return resultado;
}

bool hSigmaMetric2::evaluate_with_hess( const MsqMatrix<2,2>& T,
                                             double& result,
                                             MsqMatrix<2,2>& dA,
                                             MsqMatrix<2,2> d2A[3],
                                             MsqError& err )
{
    return true;
}


bool hSigmaMetric2::evaluate( const MsqMatrix<3,3>& T, 
                                   double& result, 
                                   MsqError& err )
{
  double determinante = det(T);
  double hS = h( determinante, mDelta, mEpsilon );
  //const Exponent d23;
  //double d = d23.powTwoThirds(hS);
  //result = sqr_Frobenius(T) / (3 * d);
  result = hS;

  //--->cout << "\thSigmaMetric2::evaluate3D, eta= " << result << "\tsigma= " << determinante << ", hS= " << hS << endl;

#ifdef miDebug
  cout << "hSigmaMetric2::evaluate3D, result= " << result << "\tdet(T)= " << determinante << "\thS= " << hS << endl;
  cout << "\tT= " << T(0,0) << ", " << T(0,1) << ", " << T(0,2) << ", " << T(1,0) << ", " << T(1,1) << ", " << T(1,2) << endl;
#endif

  return true;
}


bool hSigmaMetric2::evaluate_with_grad( const MsqMatrix<3,3>& T,
                                             double& result,
                                             MsqMatrix<3,3>& deriv_wrt_T,
                                             MsqError& err )
{  
  double determinante = det(T);
  double hS = h( determinante, mDelta, mEpsilon );

  result = hS;
  deriv_wrt_T  = transpose_adj(T) * hS;
  deriv_wrt_T /= (2 * hS - determinante);
#ifdef miDebug
  cout << "hSigmaMetric2::evaluate_with_grad3D, result= " << result;
  cout << "\tT= " << T(0,0) << ", " << T(0,1) << ", " << T(0,2) << ", " << T(1,0) << ", " << T(1,1) << ", " << T(1,2) ;
  cout << "\tdT= " << deriv_wrt_T(0,0) << ", " << deriv_wrt_T(0,1) << ", " << deriv_wrt_T(0,2) << ", " << deriv_wrt_T(1,0) << ", " << deriv_wrt_T(1,1) << ", " << deriv_wrt_T(1,2) << endl;
#endif

  return true;
}


bool hSigmaMetric2::evaluate_with_hess( const MsqMatrix<3,3>& T,
                                             double& result,
                                             MsqMatrix<3,3>& dA,
                                             MsqMatrix<3,3> d2A[6],
                                             MsqError& err )
{
  return true;
}

} // namespace Mesquite

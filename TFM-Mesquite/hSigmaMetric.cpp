/* ***************************************************************** 
    MESQUITE -- The Mesh Quality Improvement Toolkit

    Copyright 2007 Sandia National Laboratories.  Developed at the
    University of Wisconsin--Madison under SNL contract number
    624796.  The U.S. Government and the University of Wisconsin
    retain certain rights to this software.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License 
    (lgpl.txt) along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    (2008) kraftche@cae.wisc.edu    
    (2013) dbenitez@siani.es

  ***************************************************************** */


/** \file hSigmaMetric.cpp
 *  \brief 
 *  \author Domingo Benitez
 *  Calcula la metrica SUS de un tetraedro
 */
//#define miDebug

#include "Mesquite.hpp"
#include "hSigmaMetric.hpp"
#include "MsqMatrix.hpp"
#include "MsqError.hpp"
#include "Exponent.hpp"
#include <iostream>
#include <cfloat>
#include <limits>

using std::cout;
using std::endl;
using std::fixed;

typedef std::numeric_limits< double > dbl;

namespace MESQUITE_NS {

static inline double h(double sigma, double delta, double epsilon){

	long double sigma_epsilon = sigma - 2.0 * epsilon; 
	long double h = 0.5 * ( sigma + sqrt( (sigma_epsilon*sigma_epsilon) + (4*delta*delta) ) );
	if (h < epsilon) h = epsilon;

#ifdef miDebug
  	cout << "\thSigma.cpp, h(), sigma= " << sigma ;
	cout << ", delta= " << delta ;
	cout << ", delta/sigma= " << delta/sigma ;
	cout << ", hS= " << h << endl ;
/*
	if(sigma < 0){
	  cout << "\thSigma.cpp, sigma= " << sigma ;
	  cout << "\tepsilon= " << epsilon ;
	  cout << "\th= " << h ;
	  cout << "\tdelta= " << delta << endl << endl;
	}
	if (h <= 0) cout << "\th es ZERO, sigma= " << sigma << "\tdelta= " << delta;
	if(sigma < 0){
	  cout << "\tsigma= " << sigma ;
	  cout << "\tsigma_epsilon= " << sigma_epsilon ;
	  cout << "\tepsilon_sigma= " << epsilon ;
	  cout << "\tDBL_EPSILON= " << DBL_EPSILON;
	  cout << "\th= " << h ;
	  cout << "\tdelta= " << delta << endl;
	}
*/
#endif
	return (double) h;
  }

hSigmaMetric::hSigmaMetric(double del, double eps)
  : mDelta( del ), mEpsilon( eps )
  { }

hSigmaMetric::hSigmaMetric(size_t nodosMalla)
  : mDelta( 1.2e-3), mEpsilon( 1e-10)
  { Nvertices = nodosMalla; }

hSigmaMetric::hSigmaMetric(void)
  : mDelta( 1.2e-3), mEpsilon( 1e-10)
  { }

std::string hSigmaMetric::get_name() const
  { return "hSigmaMetric"; }

hSigmaMetric::~hSigmaMetric() { } //{free(handleVerticesCache);}

void hSigmaMetric::setNumeroVertices(size_t nodosMalla) 
  { Nvertices = nodosMalla; }

bool hSigmaMetric::evaluate( const MsqMatrix<2,2>& T, 
                                   double& result, 
                                   MsqError& err )
{
  double hS = h( det( T ), mDelta, mEpsilon );
  result = sqr_Frobenius(T) / (2 * hS);
#ifdef miDebug
  cout << "hSigmaMetric::evaluate2D, result= " << result;
  cout << "\tT= " << T(0,0) << ", " << T(0,1) << ", " << T(1,0) << ", " << T(1,1) << endl;
#endif
  return true;
}


bool hSigmaMetric::evaluate_with_grad( const MsqMatrix<2,2>& T,
                                             double& result,
                                             MsqMatrix<2,2>& deriv_wrt_T,
                                             MsqError& err )
{
  double determinante = det(T);
  double hS = h( determinante, mDelta, mEpsilon );
  result = sqr_Frobenius(T) / (2 * hS);
  deriv_wrt_T  = - transpose_adj(T) * sqr_Frobenius(T);
  deriv_wrt_T /= 2 * (2 * hS - determinante);
  deriv_wrt_T += T;
  deriv_wrt_T /= hS;
#ifdef miDebug
  cout << "hSigmaMetric::evaluate_with_grad2D, result= " << result;
  cout << "\tT= " << T(0,0) << ", " << T(0,1) << ", " << T(1,0) << ", " << T(1,1);
  cout << "\tdT= " << deriv_wrt_T(0,0) << ", " << deriv_wrt_T(0,1) << ", " << deriv_wrt_T(1,0) << ", " << deriv_wrt_T(1,1) << endl;
#endif
  return true;
}

double componenteHesiano2D(int i, int j, int k, int l, const MsqMatrix<2,2>& T, const MsqMatrix<2,2>& AT, double mDelta, double epsilon){
  double resultado = 0.0;
  double determinante = det(T);
  double hS = h( determinante, mDelta, epsilon );
  double F2 = sqr_Frobenius(T);
  const double inv_hS = 1.0 / hS;
  const double p1 = 2 * hS - determinante;
  if (i==j && k==l) resultado += inv_hS;
  resultado -= ((inv_hS / p1) * (T(i,j) * AT(k,l) + T(k,l) * AT(i,j)));
  resultado += ((inv_hS * F2 / (2*determinante*p1)) * (AT(k,j) * AT(i,l) - AT(i,j) * AT(k,l)));
  resultado += ((inv_hS * F2 / (2*p1*p1)) * (AT(i,j) * AT(k,l)));
  resultado += ((inv_hS * F2 * determinante / (2*p1*p1*p1)) * (AT(i,j) * AT(k,l)));

  return resultado;
}
double componenteHesiano3D(int i, int j, int k, int l, const MsqMatrix<3,3>& T, const MsqMatrix<3,3>& AT, double mDelta, double epsilon){
  double resultado = 0.0;
  double determinante = det(T);
  double inv_det= 1/determinante;
  double hS = h( determinante, mDelta, epsilon );
  const Exponent d23;
  double d = d23.powTwoThirds(hS);
  double inv_d = 1/d;
  double F2 = sqr_Frobenius(T);
  const double c2_3 = 2/3;
  const double c2_9 = 2/9;
  const double c4_9 = 4/9;
  const double c4_27 = 4/27;
  const double p1 = 2 * hS - determinante;
  const double inv_p1 = 1/p1;
  const double inv_p1_2 = inv_p1 * inv_p1;
  const double inv_p1_3 = inv_p1 * inv_p1 * inv_p1;
  if (i==j && k==l) resultado += (c2_3*inv_d);
  resultado -= (c4_9 * inv_d * inv_p1 * (T(i,j) * AT(k,l) + T(k,l) * AT(i,j)));
  resultado += (c2_9 * inv_d * inv_p1 * inv_det * F2 * (AT(k,j) * AT(i,l) - AT(i,j) * AT(k,l)));
  resultado += (c4_27* inv_d * inv_p1_2 * F2 * AT(i,j) * AT(k,l) );
  resultado += (c2_9 * inv_d * inv_p1_3 * determinante * F2 * AT(i,j) * AT(k,l) );

  return resultado;
}

bool hSigmaMetric::evaluate_with_hess( const MsqMatrix<2,2>& T,
                                             double& result,
                                             MsqMatrix<2,2>& dA,
                                             MsqMatrix<2,2> d2A[3],
                                             MsqError& err )
{
  double determinante = det(T);
  double hS = h( determinante, mDelta, mEpsilon );
  const double inv_hS = 1.0 / hS;
  result = sqr_Frobenius(T) * inv_hS / 2 ;
    
  const MsqMatrix<2,2> AT = transpose_adj(T);
  dA = - AT * sqr_Frobenius(T);
  dA /= (2 * (2 * hS - determinante));
  dA += T;
  dA *= inv_hS;
    
  d2A[0](0,0) = componenteHesiano2D(0,0,0,0,T,AT,mDelta,mEpsilon);
  d2A[0](0,1) = componenteHesiano2D(0,1,0,0,T,AT,mDelta,mEpsilon);
  d2A[0](1,0) = componenteHesiano2D(0,0,0,1,T,AT,mDelta,mEpsilon);
  d2A[0](1,1) = componenteHesiano2D(0,1,0,1,T,AT,mDelta,mEpsilon);

  d2A[1](0,0) = componenteHesiano2D(1,0,0,0,T,AT,mDelta,mEpsilon);
  d2A[1](0,1) = componenteHesiano2D(1,1,0,0,T,AT,mDelta,mEpsilon);
  d2A[1](1,0) = componenteHesiano2D(1,0,0,1,T,AT,mDelta,mEpsilon);
  d2A[1](1,1) = componenteHesiano2D(1,1,0,1,T,AT,mDelta,mEpsilon);

  d2A[2](0,0) = componenteHesiano2D(1,0,1,0,T,AT,mDelta,mEpsilon);
  d2A[2](0,1) = componenteHesiano2D(1,1,1,0,T,AT,mDelta,mEpsilon);
  d2A[2](1,0) = componenteHesiano2D(1,0,1,1,T,AT,mDelta,mEpsilon);
  d2A[2](1,1) = componenteHesiano2D(1,1,1,1,T,AT,mDelta,mEpsilon);
   
#ifdef miDebug
  cout << "hSigmaMetric::evaluate_with_hess2D, result= " << result;
  cout << "\tT= " << T(0,0) << ", " << T(0,1) << ", " << T(1,0) << ", " << T(1,1) << endl;
#endif
    return true;
}


bool hSigmaMetric::evaluate( const 	MsqMatrix<3,3>& T, 
                             double& 	result, 
                             MsqError& 	err )
{
#ifdef miDebug
  cout << "hSigmaMetric::evaluate3D" << endl;
#endif
  double determinante = det(T);
  double hS = h( determinante, mDelta, mEpsilon );
  const Exponent d23;
  double d = d23.powTwoThirds(hS);
  result = sqr_Frobenius(T) / (3 * d);

#ifdef miDebug
  cout << "\tresult= " << result << "\tdet(T)= " << determinante << "\thS= " << hS << endl;
  //cout << "\tT= " << T(0,0) << ", " << T(0,1) << ", " << T(0,2) << ", " << T(1,0) << ", " << T(1,1) << ", " << T(1,2) << endl;
#endif

  return true;
}


bool hSigmaMetric::evaluate_with_grad( const MsqMatrix<3,3>& T,
                                             double& result,
                                             MsqMatrix<3,3>& deriv_wrt_T,
                                             MsqError& err )
{  
  double determinante = det(T);
  double hS = h( determinante, mDelta, mEpsilon );
  const Exponent d23;
  double d = d23.powTwoThirds(hS);
  result = sqr_Frobenius(T) / (3 * d);
  deriv_wrt_T  = - 2 * transpose_adj(T) * sqr_Frobenius(T);
  deriv_wrt_T /= (2 * hS - determinante);
  deriv_wrt_T += (6 * T);
  deriv_wrt_T /= (9 * d);

#ifdef miDebug
  cout << "hSigmaMetric::evaluate_with_grad3D, result= " << result;
  //cout << "\tT= " << T(0,0) << ", " << T(0,1) << ", " << T(0,2) << ", " << T(1,0) << ", " << T(1,1) << ", " << T(1,2) ;
  cout << "\tdT= " << deriv_wrt_T(0,0) << ", " << deriv_wrt_T(0,1) << ", " << deriv_wrt_T(0,2) << ", " << deriv_wrt_T(1,0) << ", " << deriv_wrt_T(1,1) << ", " << deriv_wrt_T(1,2) << endl;
#endif

  return true;
}


bool hSigmaMetric::evaluate_with_hess( const MsqMatrix<3,3>& T,
                                             double& result,
                                             MsqMatrix<3,3>& dA,
                                             MsqMatrix<3,3> d2A[6],
                                             MsqError& err )
{
  double determinante = det(T);
  double hS = h( determinante, mDelta, mEpsilon );
  const Exponent d23;
  double d = d23.powTwoThirds(hS);
  result = sqr_Frobenius(T) / (3 * d);

  const double f0 = 1.0/hS;
  const double c = Mesquite::cbrt(f0);
  const double f1 = 1.0/3.0 * c * c;
  const double f2 = sqr_Frobenius(T);

  const double f3 = 2 * f1;
  const double f4 = result * (10.0/9.0) * f0 * f0;
  const double f5 = (1.0/3.0) * f0 * f3;
  const double f6 = 2 * f5;
  const double f7 = f2 * f5;

  const MsqMatrix<3,3> AT = transpose_adj(T);
  dA = - 2 * AT * f2;
  dA /= (2 * hS - determinante);
  dA += 6 * T;
  dA /= 9 * d;

  d2A[0](0,0) = componenteHesiano3D(0,0,0,0,T,AT,mDelta,mEpsilon);
  d2A[0](0,1) = componenteHesiano3D(0,1,0,0,T,AT,mDelta,mEpsilon);
  d2A[0](0,2) = componenteHesiano3D(0,2,0,0,T,AT,mDelta,mEpsilon);
  d2A[0](1,0) = componenteHesiano3D(0,0,0,1,T,AT,mDelta,mEpsilon);
  d2A[0](1,1) = componenteHesiano3D(0,1,0,1,T,AT,mDelta,mEpsilon);
  d2A[0](1,2) = componenteHesiano3D(0,2,0,1,T,AT,mDelta,mEpsilon);
  d2A[0](2,0) = componenteHesiano3D(0,0,0,2,T,AT,mDelta,mEpsilon);
  d2A[0](2,1) = componenteHesiano3D(0,1,0,2,T,AT,mDelta,mEpsilon);
  d2A[0](2,2) = componenteHesiano3D(0,2,0,2,T,AT,mDelta,mEpsilon);

  d2A[1](0,0) = componenteHesiano3D(1,0,0,0,T,AT,mDelta,mEpsilon);
  d2A[1](0,1) = componenteHesiano3D(1,1,0,0,T,AT,mDelta,mEpsilon);
  d2A[1](0,2) = componenteHesiano3D(1,2,0,0,T,AT,mDelta,mEpsilon);
  d2A[1](1,0) = componenteHesiano3D(1,0,0,1,T,AT,mDelta,mEpsilon);
  d2A[1](1,1) = componenteHesiano3D(1,1,0,1,T,AT,mDelta,mEpsilon);
  d2A[1](1,2) = componenteHesiano3D(1,2,0,1,T,AT,mDelta,mEpsilon);
  d2A[1](2,0) = componenteHesiano3D(1,0,0,2,T,AT,mDelta,mEpsilon);
  d2A[1](2,1) = componenteHesiano3D(1,1,0,2,T,AT,mDelta,mEpsilon);
  d2A[1](2,2) = componenteHesiano3D(1,2,0,2,T,AT,mDelta,mEpsilon);

  d2A[2](0,0) = componenteHesiano3D(2,0,0,0,T,AT,mDelta,mEpsilon);
  d2A[2](0,1) = componenteHesiano3D(2,1,0,0,T,AT,mDelta,mEpsilon);
  d2A[2](0,2) = componenteHesiano3D(2,2,0,0,T,AT,mDelta,mEpsilon);
  d2A[2](1,0) = componenteHesiano3D(2,0,0,1,T,AT,mDelta,mEpsilon);
  d2A[2](1,1) = componenteHesiano3D(2,1,0,1,T,AT,mDelta,mEpsilon);
  d2A[2](1,2) = componenteHesiano3D(2,2,0,1,T,AT,mDelta,mEpsilon);
  d2A[2](2,0) = componenteHesiano3D(2,0,0,2,T,AT,mDelta,mEpsilon);
  d2A[2](2,1) = componenteHesiano3D(2,1,0,2,T,AT,mDelta,mEpsilon);
  d2A[2](2,2) = componenteHesiano3D(2,2,0,2,T,AT,mDelta,mEpsilon);

  d2A[3](0,0) = componenteHesiano3D(1,0,1,0,T,AT,mDelta,mEpsilon);
  d2A[3](0,1) = componenteHesiano3D(1,1,1,0,T,AT,mDelta,mEpsilon);
  d2A[3](0,2) = componenteHesiano3D(1,2,1,0,T,AT,mDelta,mEpsilon);
  d2A[3](1,0) = componenteHesiano3D(1,0,1,1,T,AT,mDelta,mEpsilon);
  d2A[3](1,1) = componenteHesiano3D(1,1,1,1,T,AT,mDelta,mEpsilon);
  d2A[3](1,2) = componenteHesiano3D(1,2,1,1,T,AT,mDelta,mEpsilon);
  d2A[3](2,0) = componenteHesiano3D(1,0,1,2,T,AT,mDelta,mEpsilon);
  d2A[3](2,1) = componenteHesiano3D(1,1,1,2,T,AT,mDelta,mEpsilon);
  d2A[3](2,2) = componenteHesiano3D(1,2,1,2,T,AT,mDelta,mEpsilon);

  d2A[4](0,0) = componenteHesiano3D(2,0,1,0,T,AT,mDelta,mEpsilon);
  d2A[4](0,1) = componenteHesiano3D(2,1,1,0,T,AT,mDelta,mEpsilon);
  d2A[4](0,2) = componenteHesiano3D(2,2,1,0,T,AT,mDelta,mEpsilon);
  d2A[4](1,0) = componenteHesiano3D(2,0,1,1,T,AT,mDelta,mEpsilon);
  d2A[4](1,1) = componenteHesiano3D(2,1,1,1,T,AT,mDelta,mEpsilon);
  d2A[4](1,2) = componenteHesiano3D(2,2,1,1,T,AT,mDelta,mEpsilon);
  d2A[4](2,0) = componenteHesiano3D(2,0,1,2,T,AT,mDelta,mEpsilon);
  d2A[4](2,1) = componenteHesiano3D(2,1,1,2,T,AT,mDelta,mEpsilon);
  d2A[4](2,2) = componenteHesiano3D(2,2,1,2,T,AT,mDelta,mEpsilon);

  d2A[5](0,0) = componenteHesiano3D(2,0,2,0,T,AT,mDelta,mEpsilon);
  d2A[5](0,1) = componenteHesiano3D(2,1,2,0,T,AT,mDelta,mEpsilon);
  d2A[5](0,2) = componenteHesiano3D(2,2,2,0,T,AT,mDelta,mEpsilon);
  d2A[5](1,0) = componenteHesiano3D(2,0,2,1,T,AT,mDelta,mEpsilon);
  d2A[5](1,1) = componenteHesiano3D(2,1,2,1,T,AT,mDelta,mEpsilon);
  d2A[5](1,2) = componenteHesiano3D(2,2,2,1,T,AT,mDelta,mEpsilon);
  d2A[5](2,0) = componenteHesiano3D(2,0,2,2,T,AT,mDelta,mEpsilon);
  d2A[5](2,1) = componenteHesiano3D(2,1,2,2,T,AT,mDelta,mEpsilon);
  d2A[5](2,2) = componenteHesiano3D(2,2,2,2,T,AT,mDelta,mEpsilon);

#ifdef miDebug
  cout << "hSigmaMetric::evaluate_with_hess3D, result= " << result;
  cout << "\tT= " << T(0,0) << ", " << T(0,1) << ", " << T(0,2) << ", " << T(1,0) << ", " << T(1,1) << ", " << T(1,2) << endl;
#endif
  return true;
}

} // namespace Mesquite

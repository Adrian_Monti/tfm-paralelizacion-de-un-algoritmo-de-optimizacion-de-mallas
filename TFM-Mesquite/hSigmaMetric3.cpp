/* ***************************************************************** 
    MESQUITE -- The Mesh Quality Improvement Toolkit

    Copyright 2007 Sandia National Laboratories.  Developed at the
    University of Wisconsin--Madison under SNL contract number
    624796.  The U.S. Government and the University of Wisconsin
    retain certain rights to this software.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License 
    (lgpl.txt) along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    (2008) kraftche@cae.wisc.edu    
    (2013) dbenitez@siani.es

  ***************************************************************** */


/** \file hSigmaMetric3.cpp
 *  \brief 
 *  \author Domingo Benitez
 *  Calcula la metrica SUS de un tetraedro
 */
#define miDebug

#include "Mesquite.hpp"
#include "hSigmaMetric3.hpp"
#include "MsqMatrix.hpp"
#include "MsqError.hpp"
#include "Exponent.hpp"
#include <iostream>
#include <cfloat>
#include <limits>

using std::cout;
using std::endl;
using std::fixed;

typedef std::numeric_limits< double > dbl;

namespace MESQUITE_NS {

static inline double h(double sigma, double delta, double epsilon){

	long double sigma_epsilon = sigma - 2.0 * epsilon; 
	long double h = 0.5 * ( sigma + sqrt( (sigma_epsilon*sigma_epsilon) + (4*delta*delta) ) );
	if (h < epsilon) h = epsilon;

	//cout << "\thSigmaMetric3.cpp, sigma= " << sigma << "\tdelta= " << delta << ", h= " << h << endl;

#ifdef miDebug
  	cout << "\n\thSigmaMetric3.cpp, h(), sigma= " << sigma ;
	cout << ", delta= " << delta ;
	cout << ", delta/sigma= " << delta/sigma ;
	cout << ", hS= " << h << endl ;
	if(sigma < 0){
	  cout << "\thSigma.cpp, sigma= " << sigma ;
	  cout << "\tepsilon= " << epsilon ;
	  cout << "\th= " << h ;
	  cout << "\tdelta= " << delta << endl << endl;
	}
	if (h <= 0) cout << "\th es ZERO, sigma= " << sigma << "\tdelta= " << delta;
	if(sigma < 0){
	  cout << "\tsigma= " << sigma ;
	  cout << "\tsigma_epsilon= " << sigma_epsilon ;
	  cout << "\tepsilon_sigma= " << epsilon ;
	  cout << "\tDBL_EPSILON= " << DBL_EPSILON;
	  cout << "\th= " << h ;
	  cout << "\tdelta= " << delta << endl;
	}
#endif
	return (double) h;
  }

hSigmaMetric3::hSigmaMetric3(double del, double eps)
  : mDelta( del ), mEpsilon( eps )
  { }

hSigmaMetric3::hSigmaMetric3(size_t nodosMalla)
  : mDelta( 1.2e-3), mEpsilon( 1e-10)
  { Nvertices = nodosMalla; }

hSigmaMetric3::hSigmaMetric3(void)
  : mDelta( 1.2e-3), mEpsilon( 1e-10)
  { }

std::string hSigmaMetric3::get_name() const
  { return "hSigmaMetric3"; }

hSigmaMetric3::~hSigmaMetric3() { } //{free(handleVerticesCache);}

void hSigmaMetric3::setNumeroVertices(size_t nodosMalla) 
  { Nvertices = nodosMalla; }

bool hSigmaMetric3::evaluate( const MsqMatrix<2,2>& T, 
                                   double& result, 
                                   MsqError& err )
{
  return true;
}


bool hSigmaMetric3::evaluate_with_grad( const MsqMatrix<2,2>& T,
                                             double& result,
                                             MsqMatrix<2,2>& deriv_wrt_T,
                                             MsqError& err )
{
  return true;
}

bool hSigmaMetric3::evaluate_with_hess( const MsqMatrix<2,2>& T,
                                             double& result,
                                             MsqMatrix<2,2>& dA,
                                             MsqMatrix<2,2> d2A[3],
                                             MsqError& err )
{
    return true;
}


bool hSigmaMetric3::evaluate( const MsqMatrix<3,3>& T, 
                                   double& result, 
                                   MsqError& err )
{
  double determinante = det(T);
  double hS = h( determinante, mDelta, mEpsilon );
  const Exponent d23;
  double d = d23.powTwoThirds(hS);
  result = 3 * d / sqr_Frobenius(T) ;
  //result = sqr_Frobenius(T) / (3 * d);

  //--->cout << "\thSigmaMetric3::evaluate3D, eta= " << result << "\tsigma= " << determinante << ", hS= " << hS << endl;

#ifdef miDebug
  cout << "hSigmaMetric3::evaluate3D tetra, result= " << result << "\tdet(T)= " << determinante << "\thS= " << hS << endl;
  cout << "\tT= " << T(0,0) << ", " << T(0,1) << ", " << T(0,2) << ", \n\t   " << T(1,0) << ", " << T(1,1) << ", " << T(1,2) << ",\n\t   " << T(2,0) << ", " << T(2,1) << ", " << T(2,2) << endl;
#endif

  return true;
}


bool hSigmaMetric3::evaluate_with_grad( const MsqMatrix<3,3>& T,
                                             double& result,
                                             MsqMatrix<3,3>& deriv_wrt_T,
                                             MsqError& err )
{  
  double determinante = det(T);
  double hS = h( determinante, mDelta, mEpsilon );
  const Exponent d23;
  double d = d23.powTwoThirds(hS);
  result = 3 * d / sqr_Frobenius(T) ;
  //result = sqr_Frobenius(T) / (3 * d);
  deriv_wrt_T  = (transpose_adj(T) / 3);
  deriv_wrt_T /= (2 * hS - determinante);
  deriv_wrt_T -= (T / sqr_Frobenius(T));
  deriv_wrt_T *= (2 * result);

#ifdef miDebug
  cout << "hSigmaMetric3::evaluate_with_grad3D, result= " << result;
  cout << "\tT= " << T(0,0) << ", " << T(0,1) << ", " << T(0,2) << ", " << T(1,0) << ", " << T(1,1) << ", " << T(1,2) ;
  cout << "\tdT= " << deriv_wrt_T(0,0) << ", " << deriv_wrt_T(0,1) << ", " << deriv_wrt_T(0,2) << ", " << deriv_wrt_T(1,0) << ", " << deriv_wrt_T(1,1) << ", " << deriv_wrt_T(1,2) << endl;
#endif

  return true;
}


bool hSigmaMetric3::evaluate_with_hess( const MsqMatrix<3,3>& T,
                                             double& result,
                                             MsqMatrix<3,3>& dA,
                                             MsqMatrix<3,3> d2A[6],
                                             MsqError& err )
{
  return true;
}

} // namespace Mesquite

/* ***************************************************************** 
    MESQUITE -- The Mesh Quality Improvement Toolkit

    Copyright 2004 Sandia Corporation and Argonne National
    Laboratory.  Under the terms of Contract DE-AC04-94AL85000 
    with Sandia Corporation, the U.S. Government retains certain 
    rights in this software.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License 
    (lgpl.txt) along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 
    diachin2@llnl.gov, djmelan@sandia.gov, mbrewer@sandia.gov, 
    pknupp@sandia.gov, tleurent@mcs.anl.gov, tmunson@mcs.anl.gov      
   
  ***************************************************************** */
/*!
  \file   logBarrierObjFunc.cpp
  \brief  

  This Objective Function is evaluated using an L P norm to the pth power.
  total=(sum (x_i)^pVal)
  \author Michael Brewer
  \author Thomas Leurent
  \date   2002-01-23
*/
//#define debugPrintf
//#define logBarrierMIN // la barrera es: calidad minima patch, para metrica ThMixedQ
#define logBarrierMAX // la barrera logaritmica es: distorcion maxima patch, para metrica hSigmaMetric

#include <math.h>
#include <iostream>
#include "logBarrierObjFunc.hpp"
#include "MsqFreeVertexIndexIterator.hpp"
#include "MsqTimer.hpp"
#include "MsqHessian.hpp"
#include "MsqDebug.hpp"
#include "QualityMetric.hpp"
#include "ThMixedQ.hpp"

using  namespace Mesquite;  

logBarrierObjFunc::logBarrierObjFunc( 
	QualityMetric* qm, 
	double Qmin, 
	double mu 
  )
  : 
	ObjectiveFunctionTemplate(qm), 
	mCalidadMinima(Qmin), 
	mMU(mu)
  { 
	clear(); 
  }
  
void logBarrierObjFunc::clear()
{
  mCount = 0;
  mPowSum = 0;
  saveCount = 0;
  savePowSum = 0;
}

//Michael:  need to clean up here
logBarrierObjFunc::~logBarrierObjFunc(){

}

ObjectiveFunction* logBarrierObjFunc::clone() const
  { return new logBarrierObjFunc(*this); }

double logBarrierObjFunc::get_value( double power_sum, size_t count, EvalType type,
                                 size_t& global_count, MsqError& err )
{
  double result = 0;
  return result;
}

// esta funcion se utiliza exclusivamente para la metrica ThMixedQ
// proporciona las medias de la calidad mixta (Q), volumen de tetra (V), longitudes cuadrado de aristas (L)
// luego se utiliza para dar valores a los parametros alfa y beta de la metrica mista Q de ThMixedQ.<hpp,cpp>
void logBarrierObjFunc::get_parametros_patch(PatchData& pd, double& Qmed, double& Vmed, double& Lmed, double& Qstd)
{
	size_t num_elem = pd.num_elements();
	MsqError err;
	//double QminActual= get_calidad_minima_patch(pd);
	ThMixedQ* MP = (ThMixedQ*) get_quality_metric();
	//double mLAMBDA = MP->mLAMBDA;
	MP->get_evaluations( pd, qmHandles, free, err );  
	// calculate Q value for just the patch
	std::vector<size_t>::const_iterator i;
	Qmed=0.0; Vmed=0.0; Lmed=0.0;
	double value, Qmin=MSQ_MAX; //, Vmin, Lmin; // 1e100;
	size_t orden=0;
	double Q[num_elem];
	for (i = qmHandles.begin(); i != qmHandles.end(); ++i)
	{
	  // Qi
	  MP->evaluate( pd, *i, value, err );
	  Qmed += value;
	  Vmed += MP->ultimoVol;
	  Lmed += MP->ultimoL;
	  if(value < Qmin) {
		Qmin = value;
		//Vmin = MP->ultimoVol;
		//Lmin = MP->ultimoL;
	  }
	  Q[orden] = value;
	  orden++;
	}
	Qmed /= num_elem; //pd.num_elements();
	Vmed /= num_elem; //pd.num_elements();
	Lmed /= num_elem; //pd.num_elements();
	Qstd=0.0;
	double dumy;
	for (int j=0; j<orden; j++){
	  dumy = Q[j]-Qmed;
	  Qstd += (dumy*dumy);
	}
	Qstd /= (num_elem-1); //(pd.num_elements()-1);
	Qstd = sqrt(Qstd);
}

// encuentra el valor de Qmin que hace minima la funcion objetivo: dF / dQmin = 0
// se utiliza para cualquier metrica, solo que en caso de ThMixedQ se usa tambien
// para asignar valores a los parametros alfa y beta
double logBarrierObjFunc::get_calidad_minima2_patch(PatchData& pd)
{
   size_t num_elem = pd.num_elements();
   QualityMetric* MP = get_quality_metric();
// calculamos alfa y beta de la metrica Q mista de ThMixedQ
// y lo inicializamos en la clase ThMixedW
   double Qmed, Vmed, Lmed, Qstd;
   if(strcmp(MP->get_name().c_str(), "ThMixedQ") == 0){
	get_parametros_patch(pd, Qmed, Vmed, Lmed, Qstd);
	//((ThMixedQ*) MP)->mALFA = 1e+0 / fabs(Vmed);
	((ThMixedQ*) MP)->mALFA = 1e+1 / fabs(Vmed);
	((ThMixedQ*) MP)->mBETA = sqrt(Lmed * Lmed * Lmed) / fabs(Vmed);
	//((ThMixedQ*) MP)->mBETA = 1e+1 * sqrt(Lmed * Lmed * Lmed) / fabs(Vmed);
   } else MP = get_quality_metric();
//
  	float log10e = log10(exp(1.0)); 
	double QminActual= get_calidad_minima_patch(pd); // obteine la Q minima del patch
	MsqError err;
	MP->get_evaluations( pd, qmHandles, free, err );  
	// calculate Q value for just the patch
	std::vector<size_t>::const_iterator i;
	double value, Qmin=MSQ_MAX; // 1e100;
	size_t orden=0;
	//double Q[10000];
   	double Q[num_elem];
	double suma=0.0;
// calculamos Qmin tal que dF / dQmin = 0
// primera vez que se calcula suma = SUMA(mu x log10e / (Qi - Qmin))
	for (i = qmHandles.begin(); i != qmHandles.end(); ++i) {
	  // obtenemos Qi = value
	  bool result = MP->evaluate( pd, *i, value, err );
	  if (MSQ_CHKERR(err) || !result) return false;
	  Q[orden] = value;
#ifdef logBarrierMIN
 	  if(Q[orden] > QminActual) suma += (1/(Q[orden]-QminActual));
 	  else suma += (1/(Q[orden]+1e-3-QminActual));
#endif
#ifdef logBarrierMAX
 	  if(Q[orden] < QminActual) suma += (1/(-Q[orden]+QminActual));
 	  else suma += (1/(-Q[orden]+1e-3+QminActual));
	  //printf("Elem: %lu, Qi = %8.2e, Qmax= %8.2e, sumaParcial= %8.2e\n",orden,Q[orden],QminActual,suma);
#endif
	  orden++; // se registran los valores de Qi para usarlos luego
	}
	//suma *= log10e ;
	suma *= (log10e * mMU); // prueba

	double QminPrueba=QminActual; // primer valor de la nueva Qmin que iniciliza la log-barrier
	size_t iter=0;
	double factor = log10(suma); // primer valor del factor que se usa para calcular la nueva Qmin
	double sumaPrevio=suma;

#ifdef debugPrintf
	printf("logBarrierObjFunc::get_calidad_minima2_patch_IN, elems: %lu, factor= %9.2e, QminInicial= %9.2e QminPatch= %9.2e suma= %9.3e MU= %8.2e\n", orden, factor, QminPrueba , QminActual, suma, mMU);
#endif

	// bucle que obtiene el valor final de la nueva Qmin a inicializar en la funcion objetivo log-barrier
	// se mantiene en el bucle mientras suma no se aproxime a 1 en un umbral=1e-3
	while(fabs(suma - 1.0)>1e-3){
	//while(suma > 1.001){ // && fabs(suma - 1.0)>1e-3){
	   sumaPrevio=suma;
#ifdef logBarrierMIN
	   QminPrueba -= fabs(QminActual * factor); // nueva Qmin se va reduciendo en cada iteracion
#endif
#ifdef logBarrierMAX
	   QminPrueba += fabs(QminActual * factor ); // nueva Qmin se va reduciendo en cada iteracion
#endif
	   suma = 0.0;
	   iter++;
	   // se vuelve a calcular suma
	   for(int j=0; j<orden; j++){
#ifdef logBarrierMIN
		suma += (1/(Q[j]-QminPrueba));
#endif
#ifdef logBarrierMAX
		suma += (1/(-Q[j]+QminPrueba));
#endif
	   }
	   //suma *= log10e ;
	   //suma *= log10e * mMU;
	   suma *= (log10e * mMU); // prueba
	   if ( (suma > sumaPrevio && iter > 1)  || suma < 1.0-1e-3 ){ // nos hemos pasado en factor
#ifdef logBarrierMIN
	   	QminPrueba += fabs(QminActual * factor);
#endif
#ifdef logBarrierMAX
	   	QminPrueba -= fabs(QminActual * factor );
#endif
		factor /= 2;
		//printf("get_calidad_minima2_patch, nuevo factor/2: %e, suma= %9.2e, sumaPrevio= %9.2e, iterRealizadas: %lu\n", factor, suma, sumaPrevio, iter);
		suma=sumaPrevio;
	   } else if ((sumaPrevio-suma) < 0.001*sumaPrevio || sumaPrevio < suma) {
		factor *= 2;
		//printf("get_calidad_minima2_patch, nuevo factor*2: %e, suma= %9.2e, sumaPrevio= %9.2e, iterRealizadas: %lu\n", factor, suma, sumaPrevio, iter);
	   }
	   //printf("logBarrierObjFunc::get_calidad_minima2_patch, elems: %lu, iter= %lu, QminPrueba= %8.1e QminActual= %8.1e suma: %9.4e, fabs(suma - 1.0)= %9.4e\n", orden, iter,QminPrueba , QminActual, suma, fabs(suma - 1.0));
	} // end while

#ifdef debugPrintf
	printf("logBarrierObjFunc::get_calidad_minima2_patch_OUT, elems: %lu, iter= %lu, QminBarreraFinal= %9.2e QminPatch= %9.2e suma: %9.3e MU= %8.2e\n", orden, iter,QminPrueba , QminActual, suma, mMU);
   	if(strcmp(MP->get_name().c_str(), "ThMixedQ") == 0){
	    printf("logBarrierObjFunc::get_calidad_minima2_patch_OUT, Qmin= %9.3e Qmed= %9.3e Qstd= %9.3e Vmed= %9.3e Lmed: %9.3e Lambda: %7.1e Alfa: %9.3e Beta: %9.3e\n", QminActual, Qmed, Qstd, Vmed,Lmed, ((ThMixedQ*) MP)->mLAMBDA, ((ThMixedQ*) MP)->mALFA, ((ThMixedQ*) MP)->mBETA);
	}
#endif

	return QminPrueba;
}

// obtiene la calidad minima del patch
double logBarrierObjFunc::get_calidad_minima_patch(PatchData& pd)
{
	MsqError err;
	QualityMetric* MP = get_quality_metric();
	//MP = get_quality_metric();
	MP->get_evaluations( pd, qmHandles, free, err );  
	// calculate Q value for just the patch
	std::vector<size_t>::const_iterator i;
#ifdef logBarrierMIN
	double value, Qmin=MSQ_MAX; // 1e100;
#endif
#ifdef logBarrierMAX
	double value, Qmin=-MSQ_MAX; // 1e100;
#endif
	for (i = qmHandles.begin(); i != qmHandles.end(); ++i)
	{
	  // Qi
	  bool result = MP->evaluate( pd, *i, value, err );
	  if (MSQ_CHKERR(err) || !result) return false;
#ifdef logBarrierMIN
	  if(value < Qmin) Qmin = value;
#endif
#ifdef logBarrierMAX
	  if(value > Qmin) Qmin = value;
#endif
	}

#ifdef debugPrintf
  	std::cout << "logBarrierObjFunc::get_calidad_minima_patch, pd.size= " << qmHandles.size() << "\tQmin= " << Qmin << std::endl;
#endif

	return Qmin;
}

bool logBarrierObjFunc::evaluate( EvalType 	type, 
                              	  PatchData& 	pd,
                              	  double& 	value_out,
                              	  bool 		free,
                              	  MsqError& 	err )
{
#ifdef debugPrintf
	std::cout << "logBarrierObjFunc::evaluate" << std::endl;
#endif

	QualityMetric* MP = get_quality_metric();
	//MP = get_quality_metric();
	MP->get_evaluations( pd, qmHandles, free, err );  

	// calculate OF value for just the patch
	std::vector<size_t>::const_iterator i;
	double value, working_sum = 0.0;
	for (i = qmHandles.begin(); i != qmHandles.end(); ++i)
	{
	  // calcula result = log(Qi - Qmin)
	  bool result = MP->evaluate( pd, *i, value, err );
	  if (MSQ_CHKERR(err) || !result) return false;

	// calcula working_sum = suma(log(Qi - mCalidadMinima))
#ifdef logBarrierMIN
	  if(value > mCalidadMinima + MSQ_MIN)
 	    working_sum += log10(value - mCalidadMinima);
#endif
#ifdef logBarrierMAX
	  if(value < mCalidadMinima - MSQ_MIN)
 	    working_sum += log10(-value + mCalidadMinima);
#endif
	  else {
#ifdef debugPrintf
	    printf("logBarrierObjFunc::evaluate, ERROR: supera la barrera log-barrier Q= %8.1e Qmin= %8.1e \n: ", value, mCalidadMinima);
#endif
	    /* BUENO */ return false;
	  }
	}
#ifdef logBarrierMIN
	value_out = working_sum * mMU + mCalidadMinima;
#endif
#ifdef logBarrierMAX
	value_out = -1 * working_sum * mMU + mCalidadMinima;
#endif
	/* BUENO */  value_out *= MP->get_negate_flag();

#ifdef debugPrintf
  printf("logBarrierObjFunc::evaluate, pd.size= %2lu  OF= %9.4e sum_log= %9.4e QminPatch= %9.2e QminObjFun= %9.2e\n", qmHandles.size() , value_out , working_sum , get_calidad_minima_patch(pd), mCalidadMinima );
#endif

  return true;
}

// calcula la funcion objetivo log-barrier y sus derivadas respecto a S
// siguiendo la clase LPtoPTemplate 
bool logBarrierObjFunc::evaluate_with_gradient( EvalType 		type, 
                                            	PatchData& 		pd,
                                            	double& 		value_out,
                                            	std::vector<Vector3D>& 	grad_out,
                                            	MsqError& 		err )
{
  bool retorno=true;

#ifdef debugPrintf
	std::cout << "logBarrierObjFunc::evaluate_with_gradient" << std::endl;
#endif
	QualityMetric* MP = get_quality_metric();
	//MP = get_quality_metric();
	MP->get_evaluations( pd, qmHandles, OF_FREE_EVALS_ONLY, err );  

// zero gradient
	grad_out.clear();
	grad_out.resize( pd.num_free_vertices(), Vector3D(0.0,0.0,0.0) );
	bool qm_bool=true;
	double QM_val;
	value_out = 0.;
  	float log10e = log10(exp(1.0)); 

// calculate OF value and gradient for just the patch
// dF/dS = mMU * log10e * ...
	std::vector<size_t>::const_iterator i;
	for (i = qmHandles.begin(); i != qmHandles.end(); ++i)
	{
	  qm_bool = MP->evaluate_with_gradient( pd, *i, QM_val, mIndices, mGradient, err );
	  if (MSQ_CHKERR(err) || !qm_bool) return false;
#ifdef logBarrierMIN
	  if(QM_val > mCalidadMinima + MSQ_MIN)
 	    value_out += log10(QM_val - mCalidadMinima);
#endif
#ifdef logBarrierMAX
	  if(QM_val < mCalidadMinima - MSQ_MIN)
 	    value_out += log10(-QM_val + mCalidadMinima);
#endif
	  else {
#ifdef debugPrintf
	    printf("logBarrierObjFunc::evaluate_with_gradient, ERROR: supera la barrera log-barrier Q= %8.1e Qmin= %8.1e \n", QM_val, mCalidadMinima);
#endif
	    retorno=false;
	  }

     	  std::vector<Vector3D> mGradientDumy = mGradient;
#ifdef logBarrierMIN
	  /* BUENO */ double localDumy2 = mMU * log10e * MP->get_negate_flag() / (QM_val - mCalidadMinima);
#endif
#ifdef logBarrierMAX
	  double localDumy2 = mMU * log10e * MP->get_negate_flag() / (-QM_val + mCalidadMinima);
#endif
	  for (size_t j = 0; j < mIndices.size(); ++j) {
  	  	// deriv_F_wrt_T  = deriv_Q_wrt_T / (value - mCalidadMinima); 
		// double localDumy2 = mMU * log10e * get_negate_flag() / (QM_val - mCalidadMinima);
		// mGradient[j] *= (mMU * log10e * get_negate_flag() / (QM_val - mCalidadMinima));
		mGradient[j] *= localDumy2;
		for(int k=0; k<3; k++) {
			//double localDumy = mGradient[j][k];
#ifdef logBarrierMIN
	  		if(QM_val < mCalidadMinima){
#endif
#ifdef logBarrierMAX
	  		if(QM_val > mCalidadMinima){
#endif
#ifdef debugPrintf
			 std::cout << *i << "  ";
			 /*if(localDumy != localDumy)*/ printf("logBarrierObjFunc.cpp - coor= %i, Q= %e, mCalidadMin= %f, localDumy2= %e, mGD= %f\n", k,QM_val, mCalidadMinima, localDumy2, mGradientDumy[j][k]);
#endif
			}
		}
		grad_out[mIndices[j]] += mGradient[j];
	  }
	}
	value_out *= mMU ;
#ifdef logBarrierMIN
	value_out += mCalidadMinima;
#endif
#ifdef logBarrierMAX
	value_out = mCalidadMinima - value_out;
#endif
	/* BUENO */  value_out *= MP->get_negate_flag();

#ifdef debugPrintf
  	printf("logBarrierObjFunc::evaluate_with_gradient, pd.size= %2lu OF= %9.4e QminPatch= %9.2e QminObjFun= %9.2e\n", qmHandles.size() , value_out , get_calidad_minima_patch(pd), mCalidadMinima );
#endif

  return retorno;
}
  
bool logBarrierObjFunc::evaluate_with_Hessian_diagonal( EvalType type, 
                                        PatchData& pd,
                                        double& OF_val,
                                        std::vector<Vector3D>& grad,
                                        std::vector<SymMatrix3D>& hess_diag,
                                        MsqError& err )
{
 
  return true;
}
	
bool logBarrierObjFunc::evaluate_with_Hessian( EvalType type, 
                                           PatchData& pd,
                                           double& OF_val,
                                           std::vector<Vector3D>& grad,
                                           MsqHessian& hessian,
                                           MsqError& err )
{
  
  return true;
}

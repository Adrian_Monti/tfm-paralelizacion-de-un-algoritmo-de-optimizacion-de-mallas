# Makefile para compilar la aplicacion mesquite "main"
# comando: make -f compila.bat

CC = mpicxx
LINK = libtool

OBJECTS = ParallelHelper.o hSigmaMetric.o ThSigmaMetric.o miTerminationCriterion.o miVertexMover.o miQualityImprover.o miConjugateGradient.o miFeasibleNewton.o miSteepestDescent.o miTrustRegion.o miIdealWeightMeanRatio.o miVertexMover_papi.o miTUntangle1.o miTUntangleBeta.o miTUntangleMu.o miUntangleBetaQualityMetric.o miAWUntangleBeta.o miQuasiNewton.o ThTMetric.o logBarrierObjFunc.o ThMixedQ.o hSigmaMetric2.o ThSigmaMetric2.o hSigmaMetric3.o ThSigmaMetric3.o logBarrierObjFunc2.o hSigmaBarrierObjFunc.o

LA = /opt/Mesquite/lib/libmesquite.la
DFLAGS1 = -DHAVE_CONFIG_H 
INCLUDE = -I. -I/opt/Mesquite/include -I./src/include -I./src/Mesh -I./includeFaltanOLD -I./src/TargetMetric
DFLAGS2 = -DNDEBUG -DHAVE_TERMIOS_H -DHAVE_SYS_IOCTL_H -DHAVE_VSNPRINTF -DHAVE_VSPRINTF -DHAVE_CLOCK -DHAVE_TIMES -DHAVE_CBRT -DHAVE_FEENABLEEXCEPT
FLAGS1 = -O2 -MT 
FLAGS2 = -MD -MP -MF
FLAGS3 = -lm -fopenmp -L/opt/PAPI/lib -lpapi -Wl,-rpath,/opt/PAPI/lib

main : $(OBJECTS) main.o
	$(LINK) --tag=CXX --mode=link mpicxx -O2 -o main main.o $(OBJECTS) $(LA) $(FLAGS3)


hSigmaBarrierObjFunc.o : hSigmaBarrierObjFunc.cpp hSigmaBarrierObjFunc.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) hSigmaBarrierObjFunc.o $(FLAGS2) .deps/hSigmaBarrierObjFunc.Tpo -c -o hSigmaBarrierObjFunc.o hSigmaBarrierObjFunc.cpp 
	mv -f .deps/hSigmaBarrierObjFunc.Tpo .deps/hSigmaBarrierObjFunc.Po

logBarrierObjFunc2.o : logBarrierObjFunc2.cpp logBarrierObjFunc2.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) logBarrierObjFunc2.o $(FLAGS2) .deps/logBarrierObjFunc2.Tpo -c -o logBarrierObjFunc2.o logBarrierObjFunc2.cpp 
	mv -f .deps/logBarrierObjFunc2.Tpo .deps/logBarrierObjFunc2.Po

logBarrierObjFunc.o : logBarrierObjFunc.cpp logBarrierObjFunc.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) logBarrierObjFunc.o $(FLAGS2) .deps/logBarrierObjFunc.Tpo -c -o logBarrierObjFunc.o logBarrierObjFunc.cpp 
	mv -f .deps/logBarrierObjFunc.Tpo .deps/logBarrierObjFunc.Po

ParallelHelper.o : ParallelHelper.cpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) ParallelHelper.o $(FLAGS2) .deps/ParallelHelper.Tpo -c -o ParallelHelper.o ParallelHelper.cpp 
	mv -f .deps/ParallelHelper.Tpo .deps/ParallelHelper.Po

miIdealWeightMeanRatio.o : miIdealWeightMeanRatio.cpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) miIdealWeightMeanRatio.o $(FLAGS2) .deps/miIdealWeightMeanRatio.Tpo -c -o miIdealWeightMeanRatio.o miIdealWeightMeanRatio.cpp 
	mv -f .deps/miIdealWeightMeanRatio.Tpo .deps/miIdealWeightMeanRatio.Po

hSigmaMetric.o : hSigmaMetric.cpp hSigmaMetric.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) hSigmaMetric.o $(FLAGS2) .deps/hSigmaMetric.Tpo -c -o hSigmaMetric.o hSigmaMetric.cpp 
	mv -f .deps/hSigmaMetric.Tpo .deps/hSigmaMetric.Po

hSigmaMetric2.o : hSigmaMetric2.cpp hSigmaMetric2.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) hSigmaMetric2.o $(FLAGS2) .deps/hSigmaMetric2.Tpo -c -o hSigmaMetric2.o hSigmaMetric2.cpp 
	mv -f .deps/hSigmaMetric2.Tpo .deps/hSigmaMetric2.Po

hSigmaMetric3.o : hSigmaMetric3.cpp hSigmaMetric3.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) hSigmaMetric3.o $(FLAGS2) .deps/hSigmaMetric3.Tpo -c -o hSigmaMetric3.o hSigmaMetric3.cpp 
	mv -f .deps/hSigmaMetric3.Tpo .deps/hSigmaMetric3.Po

ThSigmaMetric.o : ThSigmaMetric.cpp ThSigmaMetric.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) ThSigmaMetric.o $(FLAGS2) .deps/ThSigmaMetric.Tpo -c -o ThSigmaMetric.o ThSigmaMetric.cpp 
	mv -f .deps/ThSigmaMetric.Tpo .deps/ThSigmaMetric.Po

ThSigmaMetric2.o : ThSigmaMetric2.cpp ThSigmaMetric2.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) ThSigmaMetric2.o $(FLAGS2) .deps/ThSigmaMetric2.Tpo -c -o ThSigmaMetric2.o ThSigmaMetric2.cpp 
	mv -f .deps/ThSigmaMetric2.Tpo .deps/ThSigmaMetric2.Po

ThSigmaMetric3.o : ThSigmaMetric3.cpp ThSigmaMetric3.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) ThSigmaMetric3.o $(FLAGS2) .deps/ThSigmaMetric3.Tpo -c -o ThSigmaMetric3.o ThSigmaMetric3.cpp 
	mv -f .deps/ThSigmaMetric3.Tpo .deps/ThSigmaMetric3.Po

ThTMetric.o : ThTMetric.cpp ThTMetric.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) ThTMetric.o $(FLAGS2) .deps/ThTMetric.Tpo -c -o ThTMetric.o ThTMetric.cpp 
	mv -f .deps/ThTMetric.Tpo .deps/ThTMetric.Po

miTerminationCriterion.o : miTerminationCriterion.cpp miTerminationCriterion.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) miTerminationCriterion.o $(FLAGS2) .deps/miTerminationCriterion.Tpo -c -o miTerminationCriterion.o miTerminationCriterion.cpp 
	mv -f .deps/miTerminationCriterion.Tpo .deps/miTerminationCriterion.Po


miVertexMover.o : miVertexMover.cpp miVertexMover.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) miVertexMover.o $(FLAGS2) .deps/miVertexMover.Tpo -c -o miVertexMover.o miVertexMover.cpp 
	mv -f .deps/miVertexMover.Tpo .deps/miVertexMover.Po

miVertexMover_papi.o : miVertexMover_papi.c miVertexMover_papi.h
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) miVertexMover_papi.o $(FLAGS2) .deps/miVertexMover_papi.Tpo -c -o miVertexMover_papi.o miVertexMover_papi.c 
	mv -f .deps/miVertexMover_papi.Tpo .deps/miVertexMover_papi.Po

miQualityImprover.o : miQualityImprover.cpp miQualityImprover.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) miQualityImprover.o $(FLAGS2) .deps/miQualityImprover.Tpo -c -o miQualityImprover.o miQualityImprover.cpp 
	mv -f .deps/miQualityImprover.Tpo .deps/miQualityImprover.Po


miTUntangle1.o : miTUntangle1.cpp miTUntangle1.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) miTUntangle1.o $(FLAGS2) .deps/miTUntangle1.Tpo -c -o miTUntangle1.o miTUntangle1.cpp 
	mv -f .deps/miTUntangle1.Tpo .deps/miTUntangle1.Po

miTUntangleBeta.o : miTUntangleBeta.cpp miTUntangleBeta.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) miTUntangleBeta.o $(FLAGS2) .deps/miTUntangleBeta.Tpo -c -o miTUntangleBeta.o miTUntangleBeta.cpp 
	mv -f .deps/miTUntangleBeta.Tpo .deps/miTUntangleBeta.Po

miAWUntangleBeta.o : miAWUntangleBeta.cpp miAWUntangleBeta.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) miAWUntangleBeta.o $(FLAGS2) .deps/miAWUntangleBeta.Tpo -c -o miAWUntangleBeta.o miAWUntangleBeta.cpp 
	mv -f .deps/miAWUntangleBeta.Tpo .deps/miAWUntangleBeta.Po

miTUntangleMu.o : miTUntangleMu.cpp miTUntangleMu.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) miTUntangleMu.o $(FLAGS2) .deps/miTUntangleMu.Tpo -c -o miTUntangleMu.o miTUntangleMu.cpp 
	mv -f .deps/miTUntangleMu.Tpo .deps/miTUntangleMu.Po

miUntangleBetaQualityMetric.o : miUntangleBetaQualityMetric.cpp miUntangleBetaQualityMetric.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) miUntangleBetaQualityMetric.o $(FLAGS2) .deps/miUntangleBetaQualityMetric.Tpo -c -o miUntangleBetaQualityMetric.o miUntangleBetaQualityMetric.cpp 
	mv -f .deps/miUntangleBetaQualityMetric.Tpo .deps/miUntangleBetaQualityMetric.Po

miConjugateGradient.o : miConjugateGradient.cpp miConjugateGradient.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) miConjugateGradient.o $(FLAGS2) .deps/miConjugateGradient.Tpo -c -o miConjugateGradient.o miConjugateGradient.cpp 
	mv -f .deps/miConjugateGradient.Tpo .deps/miConjugateGradient.Po

miQuasiNewton.o : miQuasiNewton.cpp miQuasiNewton.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) miQuasiNewton.o $(FLAGS2) .deps/miQuasiNewton.Tpo -c -o miQuasiNewton.o miQuasiNewton.cpp 
	mv -f .deps/miQuasiNewton.Tpo .deps/miQuasiNewton.Po

miFeasibleNewton.o : miFeasibleNewton.cpp miFeasibleNewton.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) miFeasibleNewton.o $(FLAGS2) .deps/miFeasibleNewton.Tpo -c -o miFeasibleNewton.o miFeasibleNewton.cpp 
	mv -f .deps/miFeasibleNewton.Tpo .deps/miFeasibleNewton.Po

miSteepestDescent.o : miSteepestDescent.cpp miSteepestDescent.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) miSteepestDescent.o $(FLAGS2) .deps/miSteepestDescent.Tpo -c -o miSteepestDescent.o miSteepestDescent.cpp 
	mv -f .deps/miSteepestDescent.Tpo .deps/miSteepestDescent.Po

miTrustRegion.o : miTrustRegion.cpp miTrustRegion.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) miTrustRegion.o $(FLAGS2) .deps/miTrustRegion.Tpo -c -o miTrustRegion.o miTrustRegion.cpp 
	mv -f .deps/miTrustRegion.Tpo .deps/miTrustRegion.Po

main.o : *.cpp *.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) main.o $(FLAGS2) .deps/main.Tpo -c -o main.o main.cpp 
	mv -f .deps/main.Tpo .deps/main.Po

ThMixedQ.o : ThMixedQ.cpp ThMixedQ.hpp
	$(CC) $(DFLAGS1) $(INCLUDE) $(DFLAGS2) $(FLAGS1) ThMixedQ.o $(FLAGS2) .deps/ThMixedQ.Tpo -c -o ThMixedQ.o ThMixedQ.cpp 
	mv -f .deps/ThMixedQ.Tpo .deps/ThMixedQ.Po

clean:
	rm -f main *.o

/* ***************************************************************** 
    MESQUITE -- The Mesh Quality Improvement Toolkit

    Copyright 2004 Sandia Corporation and Argonne National
    Laboratory.  Under the terms of Contract DE-AC04-94AL85000 
    with Sandia Corporation, the U.S. Government retains certain 
    rights in this software.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License 
    (lgpl.txt) along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 
    diachin2@llnl.gov, djmelan@sandia.gov, mbrewer@sandia.gov, 
    pknupp@sandia.gov, tleurent@mcs.anl.gov, tmunson@mcs.anl.gov      
   
  ***************************************************************** */
/*!
  \file   hSigmaBarrierObjFunc.cpp
  \brief  

  This Objective Function is evaluated using an L P norm to the pth power.
  total=(sum (x_i)^pVal)
  \author Michael Brewer
  \author Thomas Leurent
  \date   2002-01-23
*/
//#define debugPrintf
#define BARRERA_1_h
//#define BARRERA_1_hPrima

#include <math.h>
#include <iostream>
#include "hSigmaBarrierObjFunc.hpp"
#include "MsqFreeVertexIndexIterator.hpp"
#include "MsqTimer.hpp"
#include "MsqHessian.hpp"
#include "MsqDebug.hpp"
#include "QualityMetric.hpp"
#include "ThMixedQ.hpp"
#include "ThSigmaMetric.hpp"

using  namespace Mesquite;  

static inline double h(double sigma, double delta, double epsilon){

	long double sigma_epsilon = sigma - 2.0 * epsilon; 
	long double h = 0.5 * ( sigma + sqrt( (sigma_epsilon*sigma_epsilon) + (4*delta*delta) ) );
	if (h < epsilon) h = epsilon;
	return (double) h;
  }

hSigmaBarrierObjFunc::hSigmaBarrierObjFunc( 
	QualityMetric* qm, 
	double Qmin, 
	double mu 
  )
  : 
	ObjectiveFunctionTemplate(qm), 
	mCalidadMinima(Qmin), 
	mMU(mu)
  { 
	clear(); 
  }
  
void hSigmaBarrierObjFunc::clear()
{
  mCount = 0;
  mPowSum = 0;
  saveCount = 0;
  savePowSum = 0;
}

//Michael:  need to clean up here
hSigmaBarrierObjFunc::~hSigmaBarrierObjFunc(){

}

ObjectiveFunction* hSigmaBarrierObjFunc::clone() const
  { return new hSigmaBarrierObjFunc(*this); }

double hSigmaBarrierObjFunc::get_value( double power_sum, size_t count, EvalType type,
                                 size_t& global_count, MsqError& err )
{
  double result = 0;
  return result;
}

// esta funcion se utiliza exclusivamente para la metrica ThMixedQ
// proporciona las medias de la calidad mixta (Q), volumen de tetra (V), longitudes cuadrado de aristas (L)
// luego se utiliza para dar valores a los parametros alfa y beta de la metrica mista Q de ThMixedQ.<hpp,cpp>
void hSigmaBarrierObjFunc::get_parametros_patch(PatchData& pd, double& Qmed, double& Vmed, double& Lmed, double& Qstd)
{
	size_t num_elem = pd.num_elements();
	MsqError err;
	//double QminActual= get_calidad_minima_patch(pd);
	ThMixedQ* MP = (ThMixedQ*) get_quality_metric();
	//double mLAMBDA = MP->mLAMBDA;
	MP->get_evaluations( pd, qmHandles, free, err );  
	// calculate Q value for just the patch
	std::vector<size_t>::const_iterator i;
	Qmed=0.0; Vmed=0.0; Lmed=0.0;
	double value, Qmin=MSQ_MAX; //, Vmin, Lmin; // 1e100;
	size_t orden=0;
	double Q[num_elem];
	for (i = qmHandles.begin(); i != qmHandles.end(); ++i)
	{
	  // Qi
	  MP->evaluate( pd, *i, value, err );
	  Qmed += value;
	  Vmed += MP->ultimoVol;
	  Lmed += MP->ultimoL;
	  if(value < Qmin) {
		Qmin = value;
		//Vmin = MP->ultimoVol;
		//Lmin = MP->ultimoL;
	  }
	  Q[orden] = value;
	  orden++;
	}
/* probamos a dar los Q V y L del tetra con Q minima */
/*
	Qmed = Qmin;
	Vmed = Vmin;
	Lmed = Lmin;
	Qstd = 0.0;
*/
	Qmed /= num_elem; //pd.num_elements();
	Vmed /= num_elem; //pd.num_elements();
	Lmed /= num_elem; //pd.num_elements();
	Qstd=0.0;
	double dumy;
	for (int j=0; j<orden; j++){
	  dumy = Q[j]-Qmed;
	  Qstd += (dumy*dumy);
	}
	Qstd /= (num_elem-1); //(pd.num_elements()-1);
	Qstd = sqrt(Qstd);
}

// encuentra el valor de Qmin que hace minima la funcion objetivo: dF / dQmin = 0
// se utiliza para cualquier metrica, solo que en caso de ThMixedQ se usa tambien
// para asignar valores a los parametros alfa y beta
double hSigmaBarrierObjFunc::get_calidad_minima2_patch(PatchData& pd)
{
   size_t num_elem = pd.num_elements();
   QualityMetric* MP = get_quality_metric();
// calculamos alfa y beta de la metrica Q mista de ThMixedQ
// y lo inicializamos en la clase ThMixedW
   double Qmed, Vmed, Lmed, Qstd;
   if(strcmp(MP->get_name().c_str(), "ThMixedQ") == 0){
	get_parametros_patch(pd, Qmed, Vmed, Lmed, Qstd);
	//((ThMixedQ*) MP)->mALFA = 1e+0 / fabs(Vmed);
	((ThMixedQ*) MP)->mALFA = 1e+1 / fabs(Vmed);
	((ThMixedQ*) MP)->mBETA = sqrt(Lmed * Lmed * Lmed) / fabs(Vmed);
	//((ThMixedQ*) MP)->mBETA = 1e+1 * sqrt(Lmed * Lmed * Lmed) / fabs(Vmed);
   } else MP = get_quality_metric();
//
  	float log10e = log10(exp(1.0)); 
	double QminActual= get_calidad_minima_patch(pd); // obteine la Q minima del patch
	MsqError err;
	MP->get_evaluations( pd, qmHandles, free, err );  
	// calculate Q value for just the patch
	std::vector<size_t>::const_iterator i;
	double value, Qmin=MSQ_MAX; // 1e100;
	size_t orden=0;
	//double Q[10000];
   	double Q[num_elem];
	double suma=0.0;
// calculamos Qmin tal que dF / dQmin = 0
// primera vez que se calcula suma = SUMA(mu x log10e / (Qi - Qmin))
	for (i = qmHandles.begin(); i != qmHandles.end(); ++i) {
	  // obtenemos Qi = value
	  bool result = MP->evaluate( pd, *i, value, err );
	  if (MSQ_CHKERR(err) || !result) return false;
	  Q[orden] = value;
 	  if(Q[orden] < QminActual) suma += (1/(-Q[orden]+QminActual));
 	  else suma += (1/(-Q[orden]+1e-3+QminActual));
	  //printf("Elem: %lu, Qi = %8.2e, Qmax= %8.2e, sumaParcial= %8.2e\n",orden,Q[orden],QminActual,suma);
	  orden++; // se registran los valores de Qi para usarlos luego
	}
	//suma *= log10e ;
	suma *= (log10e * mMU); // prueba

	double QminPrueba=QminActual; // primer valor de la nueva Qmin que iniciliza la log-barrier
	size_t iter=0;
	double factor = log10(suma); // primer valor del factor que se usa para calcular la nueva Qmin
	double sumaPrevio=suma;

#ifdef debugPrintf
	printf("hSigmaBarrierObjFunc::get_calidad_minima2_patch_IN, elems: %lu, factor= %9.2e, QminInicial= %9.2e QminPatch= %9.2e suma= %9.3e MU= %8.2e\n", orden, factor, QminPrueba , QminActual, suma, mMU);
#endif

	// bucle que obtiene el valor final de la nueva Qmin a inicializar en la funcion objetivo log-barrier
	// se mantiene en el bucle mientras suma no se aproxime a 1 en un umbral=1e-3
	while(fabs(suma - 1.0)>1e-3){
	//while(suma > 1.001){ // && fabs(suma - 1.0)>1e-3){
	   sumaPrevio=suma;
	   QminPrueba += fabs(QminActual * factor ); // nueva Qmin se va reduciendo en cada iteracion
	   suma = 0.0;
	   iter++;
	   // se vuelve a calcular suma
	   for(int j=0; j<orden; j++){
		suma += (1/(-Q[j]+QminPrueba));
	   }
	   //suma *= log10e ;
	   //suma *= log10e * mMU;
	   suma *= (log10e * mMU); // prueba
	   if ( (suma > sumaPrevio && iter > 1)  || suma < 1.0-1e-3 ){ // nos hemos pasado en factor
	   	QminPrueba -= fabs(QminActual * factor );
		factor /= 2;
		//printf("get_calidad_minima2_patch, nuevo factor/2: %e, suma= %9.2e, sumaPrevio= %9.2e, iterRealizadas: %lu\n", factor, suma, sumaPrevio, iter);
		suma=sumaPrevio;
	   } else if ((sumaPrevio-suma) < 0.001*sumaPrevio || sumaPrevio < suma) {
		factor *= 2;
		//printf("get_calidad_minima2_patch, nuevo factor*2: %e, suma= %9.2e, sumaPrevio= %9.2e, iterRealizadas: %lu\n", factor, suma, sumaPrevio, iter);
	   }
	   //printf("hSigmaBarrierObjFunc::get_calidad_minima2_patch, elems: %lu, iter= %lu, QminPrueba= %8.1e QminActual= %8.1e suma: %9.4e, fabs(suma - 1.0)= %9.4e\n", orden, iter,QminPrueba , QminActual, suma, fabs(suma - 1.0));
	} // end while

#ifdef debugPrintf
	printf("hSigmaBarrierObjFunc::get_calidad_minima2_patch_OUT, elems: %lu, iter= %lu, QminBarreraFinal= %9.2e QminPatch= %9.2e suma: %9.3e MU= %8.2e\n", orden, iter,QminPrueba , QminActual, suma, mMU);
   	if(strcmp(MP->get_name().c_str(), "ThMixedQ") == 0){
	    printf("hSigmaBarrierObjFunc::get_calidad_minima2_patch_OUT, Qmin= %9.3e Qmed= %9.3e Qstd= %9.3e Vmed= %9.3e Lmed: %9.3e Lambda: %7.1e Alfa: %9.3e Beta: %9.3e\n", QminActual, Qmed, Qstd, Vmed,Lmed, ((ThMixedQ*) MP)->mLAMBDA, ((ThMixedQ*) MP)->mALFA, ((ThMixedQ*) MP)->mBETA);
	}
#endif

	return QminPrueba;
}

// obtiene la calidad minima del patch
double hSigmaBarrierObjFunc::get_calidad_minima_patch(PatchData& pd)
{
	MsqError err;
	ThSigmaMetric* MP = (ThSigmaMetric*) get_quality_metric();
	MP->get_evaluations( pd, qmHandles, free, err );  
	// calculate Q value for just the patch
	std::vector<size_t>::const_iterator i;
	double value, Qmin=-MSQ_MAX; // 1e100;
	for (i = qmHandles.begin(); i != qmHandles.end(); ++i)
	{
	  // Qi
	  bool result = MP->evaluate( pd, *i, value, err );
	  if (MSQ_CHKERR(err) || !result) return false;
	  // calcula working_sum = suma(Ni + 1/h(Nmax - Ni))
	  if(value > Qmin) Qmin = value;
	}

#ifdef debugPrintf
  	std::cout << "hSigmaBarrierObjFunc::get_calidad_minima_patch, pd.size= " << qmHandles.size() << "\tNmax= " << Qmin << std::endl;
#endif

	return Qmin;
}

bool hSigmaBarrierObjFunc::evaluate( EvalType type, 
                              PatchData& pd,
                              double& value_out,
                              bool free,
                              MsqError& err )
{
#ifdef debugPrintf
#ifdef BARRERA_1_h
	std::cout << "hSigmaBarrierObjFunc::evaluate con barrera= 1/h" << std::endl;
#endif
#ifdef BARRERA_1_hPrima
	std::cout << "hSigmaBarrierObjFunc::evaluate con barrera= 1/hPrima" << std::endl;
#endif
#endif
	//QualityMetric* MP = get_quality_metric();
	ThSigmaMetric* MP = (ThSigmaMetric*) get_quality_metric();
	MP->get_evaluations( pd, qmHandles, free, err );  
	// calculate OF value for just the patch
	std::vector<size_t>::const_iterator i;
	double value, working_sum = 0.0;
	for (i = qmHandles.begin(); i != qmHandles.end(); ++i)
	{
	  // calcula result = Ni + 1/h(Nmax - Ni)
	  bool result = MP->evaluate( pd, *i, value, err );
	  if (MSQ_CHKERR(err) || !result) return false;
	  //double delta = MP->calculaDelta(pd);
	  double delta = calculaDeltaParaHSigmaBarrier(pd);
	  double sigma = mCalidadMinima - value;
	  double epsilon = 1e-10;
	  double hvalor = h(sigma , delta, epsilon);
	  // calcula working_sum = suma(Ni + 1/h(Nmax - Ni))
#ifdef BARRERA_1_h
 	  /* BUENO */ //working_sum += (value + (1/hvalor));
 	  working_sum += (value + (mMU/hvalor));
#endif
#ifdef BARRERA_1_hPrima
 	  working_sum += (value - (mMU * sigma/hvalor));
#endif
#ifdef debugPrintf
	  printf("hSigmaBarrierObjFunc::evaluate, Q= %8.1e Qmin= %8.1e delta= %8.2e sigma= %8.2e hvalor= %8.2e\n", value+(1/hvalor), mCalidadMinima, delta, sigma, hvalor);
#endif
	}
	value_out = working_sum;
	//value_out *= MP->get_negate_flag();

#ifdef debugPrintf
  printf("hSigmaBarrierObjFunc::evaluate, pd.size= %2lu  OF= %9.4e NmaxPatch= %9.2e NmaxObjFun= %9.2e\n", qmHandles.size() , value_out, get_calidad_minima_patch(pd), mCalidadMinima );
#endif

  return true;
}

double hSigmaBarrierObjFunc::calculaDeltaParaHSigmaBarrier(PatchData& pd){
#ifdef debugPrintf
	std::cout << "hSigmaBarrierObjFunc::calculaDeltaParaHSigmaBarrier" << std::endl;
#endif

// BEGIN: cache de la metrica
// devuelve ultimo valor si se ha calculado previamente para el mismo patch
//
// 1: lee coordenadas de los nodos del patch
	size_t numNodosPatch = pd.num_nodes(), nodo=0, coincide=0;
	Mesh::VertexHandle dumy5;
	Mesh::VertexHandle * vtxHandle;
	vtxHandle  = (Mesh::VertexHandle *) pd.get_vertex_handles_array();
// 2: cuenta el numero de coincidencias de los vertices del patch
	int limiteCuentaCoincidencias = (numNodosPatch > 100 ? 100 : numNodosPatch);
	for (int i=0; i<limiteCuentaCoincidencias; i++) {
	  dumy5 = handleVerticesCache[i];
	  if (vtxHandle[i] == dumy5) coincide++; // si coincide los handles
	  handleVerticesCache[i] = vtxHandle[i];
	  nodo++;
	}
// 3: si coinciden los handle, devuelve delta que tiene en cache
	if(coincide == nodo) {
	  return getDeltaParaHSigmaBarrier();
	}
// END: cache del calculo de delta

// Calculo exhaustivo, accediendo a la estrella
	double delta, delta_min;
        MsqError err;
	ThSigmaMetric* MP = (ThSigmaMetric*) get_quality_metric();
	MP->get_evaluations( pd, qmHandles, free, err );  
	std::vector<size_t>::const_iterator i;
	double value, working_sum = 0.0;
	double sigma_min=MSQ_MAX, sigma_media=0;
	size_t numElemPatch = pd.num_elements();
	double EpsilonEfectivo, EpsSafetyFactor=1e11;
	for (i = qmHandles.begin(); i != qmHandles.end(); ++i)
	{
	  // calcula result = Ni + 1/h(Nmax - Ni)
	  bool result = MP->evaluate( pd, *i, value, err );
	  double sigma = mCalidadMinima - value;
	  if(sigma < sigma_min) sigma_min = sigma;
	  sigma_media += sigma;
#ifdef debugPrintf
	   printf("sigma= %8.1e, sigma_min= %8.1e, sigma_media= %8.1e \n", sigma, sigma_min, sigma_media) ;
#endif
	}
	sigma_media /= numElemPatch;
	EpsilonEfectivo = DBL_EPSILON * EpsSafetyFactor;
	delta_min = EpsilonEfectivo * (EpsilonEfectivo - sigma_min);
	if(delta_min > 0) delta_min = sqrt(delta_min);
	else delta_min = 0.0; 
	delta = (delta_min > 1e-4*sigma_media)? delta_min : 1e-4*sigma_media;
	setDeltaParaHSigmaBarrier(delta);

#ifdef debugPrintf
printf("calculaDeltaParaHSigmaBarrier - sigma_media= %8.1e, ", sigma_media) ;
printf("sigma_min= %8.1e, ", sigma_min) ;
printf("deltaMin= %8.1e, ", delta_min) ;
printf("deltaVariable= %8.1e, ", delta) ;
printf("numElemPatch= %lu, ", numElemPatch) ;
printf("qmHandles= %lu\n", qmHandles.size()) ;
#endif

	return delta;
}

// calcula la funcion objetivo log-barrier y sus derivadas respecto a S
// siguiendo la clase LPtoPTemplate 
bool hSigmaBarrierObjFunc::evaluate_with_gradient( EvalType type, 
                                            PatchData& pd,
                                            double& value_out,
                                            std::vector<Vector3D>& grad_out,
                                            MsqError& err )
{
#ifdef debugPrintf
#ifdef BARRERA_1_h
	std::cout << "hSigmaBarrierObjFunc::evaluate_with_gradient con barrera= 1/h" << std::endl;
#endif
#ifdef BARRERA_1_hPrima
	std::cout << "hSigmaBarrierObjFunc::evaluate_with_gradient con barrera= 1/hPrima" << std::endl;
#endif
#endif

  bool retorno=true;

//std::cout << "hSigmaBarrierObjFunc::evaluate_with_gradient" << std::endl;
	ThSigmaMetric* MP = (ThSigmaMetric*) get_quality_metric();
	//QualityMetric* MP = get_quality_metric();
	MP->get_evaluations( pd, qmHandles, OF_FREE_EVALS_ONLY, err );  

// zero gradient
	grad_out.clear();
	grad_out.resize( pd.num_free_vertices(), Vector3D(0.0,0.0,0.0) );
	bool qm_bool=true;
	double QM_val;
	value_out = 0.;
  	float log10e = log10(exp(1.0)); 

// calculate OF value and gradient for just the patch
	std::vector<size_t>::const_iterator i;
	//double factor = get_negate_flag(); //-1.0: la funcion objetivo log se maximiza
	for (i = qmHandles.begin(); i != qmHandles.end(); ++i)
	{
	  qm_bool = MP->evaluate_with_gradient( pd, *i, QM_val, mIndices, mGradient, err );
	  if (MSQ_CHKERR(err) || !qm_bool) return false;


	  //double delta = MP->calculaDelta(pd);
	  double delta = calculaDeltaParaHSigmaBarrier(pd);
	  double sigma = mCalidadMinima - QM_val;
	  double epsilon = 1e-10;
	  double hvalor = h(sigma , delta, epsilon);
	  // calcula working_sum = suma(Ni + 1/h(Nmax - Ni))
#ifdef BARRERA_1_h
 	  /* BUENO */ //value_out += (QM_val + (1/hvalor));
 	  value_out += (QM_val + (mMU/hvalor));
#endif
#ifdef BARRERA_1_hPrima
 	  value_out += (QM_val - (mMU * sigma / hvalor));
#endif

#ifdef debugPrintf
	  //printf("hSigmaBarrierObjFunc::evaluate_with_gradient, Ni= %8.1e NmaxObjFunc= %8.1e \n", QM_val+(1/hvalor), mCalidadMinima);
	  printf("hSigmaBarrierObjFunc::evaluate_with_gradient, Ni= %8.1e Nmax= %8.1e delta= %8.2e sigma= %8.2e hvalor= %8.2e\n", QM_val+(1/hvalor), mCalidadMinima, delta, sigma, hvalor);
#endif

     	  std::vector<Vector3D> mGradientDumy = mGradient;
#ifdef BARRERA_1_h
	  /* BUENO */ //double localDumy2 = 1 + ( 1 / ( hvalor * ( 2 * hvalor - (-QM_val + mCalidadMinima))));
	  //double localDumy2 = 1 + ( mMU / ( hvalor * ( 2 * hvalor - (-QM_val + mCalidadMinima))));
	  double localDumy2 = 1 + ( mMU / ( hvalor * ( 2 * hvalor - sigma)));
#endif
#ifdef BARRERA_1_hPrima
	  double localDumy2 = 1 - ( 2 * mMU * (sigma - hvalor) / ( hvalor * ( 2 * hvalor - sigma )));
#endif
	  for (size_t j = 0; j < mIndices.size(); ++j) {
		mGradient[j] *= localDumy2;
		for(int k=0; k<3; k++) {
#ifdef debugPrintf
		  //std::cout << *i << "  ";
		  //printf("hSigmaBarrierObjFunc.cpp - coor= %i, Q= %e, mCalidadMin= %f, localDumy2= %e, mGD= %f\n", k,QM_val, mCalidadMinima, localDumy2, mGradientDumy[j][k]);
#endif
		}
		grad_out[mIndices[j]] += mGradient[j];
	  }
	}
	//value_out *= mMU ;
	//value_out *= MP->get_negate_flag();

#ifdef debugPrintf
  	printf("hSigmaBarrierObjFunc::evaluate_with_gradient, pd.size= %2lu OF= %9.4e NmaxPatch= %9.2e QmaxObjFun= %9.2e\n", qmHandles.size() , value_out , get_calidad_minima_patch(pd), mCalidadMinima );
#endif

  return retorno;
}
  
bool hSigmaBarrierObjFunc::evaluate_with_Hessian_diagonal( EvalType type, 
                                        PatchData& pd,
                                        double& OF_val,
                                        std::vector<Vector3D>& grad,
                                        std::vector<SymMatrix3D>& hess_diag,
                                        MsqError& err )
{
 
  return true;
}
	
bool hSigmaBarrierObjFunc::evaluate_with_Hessian( EvalType type, 
                                           PatchData& pd,
                                           double& OF_val,
                                           std::vector<Vector3D>& grad,
                                           MsqHessian& hessian,
                                           MsqError& err )
{
  
  return true;
}

/* ***************************************************************** 
    MESQUITE -- The Mesh Quality Improvement Toolkit

    Copyright 2004 Sandia Corporation and Argonne National
    Laboratory.  Under the terms of Contract DE-AC04-94AL85000 
    with Sandia Corporation, the U.S. Government retains certain 
    rights in this software.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License 
    (lgpl.txt) along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 
    diachin2@llnl.gov, djmelan@sandia.gov, mbrewer@sandia.gov, 
    pknupp@sandia.gov, tleurent@mcs.anl.gov, tmunson@mcs.anl.gov      
   
  ***************************************************************** */
/*!
  \file   logBarrierObjFunc2.cpp
  \brief  

  This Objective Function is evaluated using an L P norm to the pth power.
  total=(sum (x_i)^pVal)
  \author Michael Brewer
  \author Thomas Leurent
  \date   2002-01-23
*/
#define debugPrintf
#define logBarrierMAX // la barrera logaritmica es: distorcion maxima patch

#include <math.h>
#include <iostream>
#include "logBarrierObjFunc2.hpp"
#include "MsqFreeVertexIndexIterator.hpp"
#include "MsqTimer.hpp"
#include "MsqHessian.hpp"
#include "MsqDebug.hpp"
#include "QualityMetric.hpp"
#include "ThMixedQ.hpp"

using  namespace Mesquite;  

logBarrierObjFunc2::logBarrierObjFunc2( 
	QualityMetric* qm, 
	double Qmax, 
	double mu 
  )
  : 
	ObjectiveFunctionTemplate(qm), 
	mCalidadMaxima(Qmax), 
        elemDistorcionMax(-1),
	mMU(mu)
  { 
	clear(); 
  }
  
void logBarrierObjFunc2::clear()
{
  mCount = 0;
  mPowSum = 0;
  saveCount = 0;
  savePowSum = 0;
}

//Michael:  need to clean up here
logBarrierObjFunc2::~logBarrierObjFunc2(){

}

ObjectiveFunction* logBarrierObjFunc2::clone() const
  { return new logBarrierObjFunc2(*this); }

double logBarrierObjFunc2::get_value( double power_sum, size_t count, EvalType type,
                                 size_t& global_count, MsqError& err )
{
  double result = 0;
  return result;
}

// esta funcion se utiliza exclusivamente para la metrica ThMixedQ
// proporciona las medias de la calidad mixta (Q), volumen de tetra (V), longitudes cuadrado de aristas (L)
// luego se utiliza para dar valores a los parametros alfa y beta de la metrica mista Q de ThMixedQ.<hpp,cpp>
void logBarrierObjFunc2::get_parametros_patch(PatchData& pd, double& Qmed, double& Vmed, double& Lmed, double& Qstd)
{
/*
	size_t num_elem = pd.num_elements();
	MsqError err;
	//double QminActual= get_calidad_minima_patch(pd);
	ThMixedQ* MP = (ThMixedQ*) get_quality_metric();
	//double mLAMBDA = MP->mLAMBDA;
	MP->get_evaluations( pd, qmHandles, free, err );  
	// calculate Q value for just the patch
	std::vector<size_t>::const_iterator i;
	Qmed=0.0; Vmed=0.0; Lmed=0.0;
	double value, Qmin=MSQ_MAX; // 1e100;
	size_t orden=0;
	double Q[num_elem];
	for (i = qmHandles.begin(); i != qmHandles.end(); ++i)
	{
	  // Qi
	  MP->evaluate( pd, *i, value, err );
	  Qmed += value;
	  Vmed += MP->ultimoVol;
	  Lmed += MP->ultimoL;
	  if(value < Qmin) Qmin = value;
	  Q[orden] = value;
	  orden++;
	}
	Qmed /= num_elem; //pd.num_elements();
	Vmed /= num_elem; //pd.num_elements();
	Lmed /= num_elem; //pd.num_elements();
	Qstd=0.0;
	double dumy;
	for (int j=0; j<orden; j++){
	  dumy = Q[j]-Qmed;
	  Qstd += (dumy*dumy);
	}
	Qstd /= (num_elem-1); //(pd.num_elements()-1);
	Qstd = sqrt(Qstd);
*/
}

// encuentra el valor de Qmin que hace minima la funcion objetivo: dF / dQmin = 0
// se utiliza para cualquier metrica, solo que en caso de ThMixedQ se usa tambien
// para asignar valores a los parametros alfa y beta
double logBarrierObjFunc2::get_calidad_minima2_patch(PatchData& pd)
{
/*
   size_t num_elem = pd.num_elements();
   QualityMetric* MP = get_quality_metric();
// calculamos alfa y beta de la metrica Q mista de ThMixedQ
// y lo inicializamos en la clase ThMixedW
   double Qmed, Vmed, Lmed, Qstd;
   if(strcmp(MP->get_name().c_str(), "ThMixedQ") == 0){
	get_parametros_patch(pd, Qmed, Vmed, Lmed, Qstd);
	((ThMixedQ*) MP)->mALFA = 1e+0 / fabs(Vmed);
	// desenreda TangledCube ((ThMixedQ*) MP)->mALFA = 1e+0 / fabs(Vmed);
	//((ThMixedQ*) MP)->mBETA = ((ThMixedQ*) MP)->mALFA;
	((ThMixedQ*) MP)->mBETA = sqrt(Lmed * Lmed * fabs(Lmed)) / fabs(Vmed);
   } else MP = get_quality_metric();
//
  	float log10e = log10(exp(1.0)); 
	double QminActual= get_calidad_minima_patch(pd); // obteine la Q minima del patch
	MsqError err;
	MP->get_evaluations( pd, qmHandles, free, err );  
	// calculate Q value for just the patch
	std::vector<size_t>::const_iterator i;
	double value, Qmin=MSQ_MAX; // 1e100;
	size_t orden=0;
	//double Q[10000];
   	double Q[num_elem];
	double suma=0.0;
// calculamos Qmin tal que dF / dQmin = 0
// primera vez que se calcula suma = SUMA(mu x log10e / (Qi - Qmin))
	for (i = qmHandles.begin(); i != qmHandles.end(); ++i) {
	  // obtenemos Qi = value
	  bool result = MP->evaluate( pd, *i, value, err );
	  if (MSQ_CHKERR(err) || !result) return false;
	  Q[orden] = value;
#ifdef logBarrierMIN
 	  if(Q[orden] > QminActual) suma += (1/(Q[orden]-QminActual));
 	  else suma += (1/(Q[orden]+1e-3-QminActual));
#endif
#ifdef logBarrierMAX
 	  if(Q[orden] < QminActual) suma += (1/(-Q[orden]+QminActual));
 	  else suma += (1/(-Q[orden]+1e-3+QminActual));
	  //printf("Elem: %lu, Qi = %8.2e, Qmax= %8.2e, sumaParcial= %8.2e\n",orden,Q[orden],QminActual,suma);
#endif
	  orden++; // se registran los valores de Qi para usarlos luego
	}
	//suma *= log10e ;
	suma *= (log10e * mMU); // prueba

	double QminPrueba=QminActual; // primer valor de la nueva Qmin que iniciliza la log-barrier
	size_t iter=0;
	double factor = log10(suma); // primer valor del factor que se usa para calcular la nueva Qmin
	double sumaPrevio=suma;

#ifdef debugPrintf
	printf("logBarrierObjFunc2::get_calidad_minima2_patch_IN, elems: %lu, factor= %9.2e, QminPrueba= %9.2e QminActual= %9.2e suma: %9.3e\n", orden, factor, QminPrueba , QminActual, suma);
#endif

	// bucle que obtiene el valor final de la nueva Qmin a inicializar en la funcion objetivo log-barrier
	// se mantiene en el bucle mientras suma no se aproxime a 1 en un umbral=1e-3
	while(fabs(suma - 1.0)>1e-3){
	//while(suma > 1.001){ // && fabs(suma - 1.0)>1e-3){
	   sumaPrevio=suma;
#ifdef logBarrierMIN
	   QminPrueba -= fabs(QminActual * factor); // nueva Qmin se va reduciendo en cada iteracion
#endif
#ifdef logBarrierMAX
	   QminPrueba += fabs(QminActual * factor ); // nueva Qmin se va reduciendo en cada iteracion
#endif
	   suma = 0.0;
	   iter++;
	   // se vuelve a calcular suma
	   for(int j=0; j<orden; j++){
#ifdef logBarrierMIN
		suma += (1/(Q[j]-QminPrueba));
#endif
#ifdef logBarrierMAX
		suma += (1/(-Q[j]+QminPrueba));
#endif
	   }
	   //suma *= log10e ;
	   //suma *= log10e * mMU;
	   suma *= (log10e * mMU); // prueba
	   if ( (suma > sumaPrevio && iter > 1)  || suma < 1.0-1e-3 ){ // nos hemos pasado en factor
#ifdef logBarrierMIN
	   	QminPrueba += fabs(QminActual * factor);
#endif
#ifdef logBarrierMAX
	   	QminPrueba -= fabs(QminActual * factor );
#endif
		factor /= 2;
		//printf("get_calidad_minima2_patch, nuevo factor/2: %e, suma= %9.2e, sumaPrevio= %9.2e, iterRealizadas: %lu\n", factor, suma, sumaPrevio, iter);
		suma=sumaPrevio;
	   } else if ((sumaPrevio-suma) < 0.001*sumaPrevio || sumaPrevio < suma) {
		factor *= 2;
		//printf("get_calidad_minima2_patch, nuevo factor*2: %e, suma= %9.2e, sumaPrevio= %9.2e, iterRealizadas: %lu\n", factor, suma, sumaPrevio, iter);
	   }
	   //printf("logBarrierObjFunc2::get_calidad_minima2_patch, elems: %lu, iter= %lu, QminPrueba= %8.1e QminActual= %8.1e suma: %9.4e, fabs(suma - 1.0)= %9.4e\n", orden, iter,QminPrueba , QminActual, suma, fabs(suma - 1.0));
	} // end while

#ifdef debugPrintf
	printf("logBarrierObjFunc2::get_calidad_minima2_patch_OUT, elems: %lu, iter= %lu, QminPrueba= %9.2e QminActual= %9.2e suma: %9.3e\n", orden, iter,QminPrueba , QminActual, suma);
   	if(strcmp(MP->get_name().c_str(), "ThMixedQ") == 0){
	    printf("logBarrierObjFunc2::get_calidad_minima2_patch_OUT, Qmin= %9.3e Qmed= %9.3e Qstd= %9.3e Vmed= %9.3e Lmed: %9.3e Lambda: %7.1e Alfa: %9.3e Beta: %9.3e\n", QminActual, Qmed, Qstd, Vmed,Lmed, ((ThMixedQ*) MP)->mLAMBDA, ((ThMixedQ*) MP)->mALFA, ((ThMixedQ*) MP)->mBETA);
	}
#endif

	return QminPrueba;
*/
	return 0.0;
}

// obtiene la calidad minima del patch
double logBarrierObjFunc2::get_calidad_minima_patch(PatchData& pd, long int& orden_max)
{
//
	MsqError err;
	QualityMetric* MP = get_quality_metric();
	MP = get_quality_metric();
	MP->get_evaluations( pd, qmHandles, free, err );  
	// calculate Q value for just the patch
	std::vector<size_t>::const_iterator i;
	double value, Qmin=-MSQ_MAX; // 1e100;
	size_t orden=0;
	for (i = qmHandles.begin(); i != qmHandles.end(); ++i)
	{
	  // Qi
	  bool result = MP->evaluate( pd, *i, value, err );
	  if (MSQ_CHKERR(err) || !result) return false;
	  if(value > Qmin) {
		Qmin = value;
		orden_max = (long int) orden;
	  }
	  orden++;
	}

#ifdef debugPrintf
  	std::cout << "logBarrierObjFunc2::get_calidad_minima_patch, pd.size= " << qmHandles.size() << "\tQmin= " << Qmin << "\torden_max: " << orden_max << std::endl;
#endif

	return Qmin;
//
}

bool logBarrierObjFunc2::evaluate( EvalType type, 
                              PatchData& pd,
                              double& value_out,
                              bool free,
                              MsqError& err )
{
std::cout << "logBarrierObjFunc2::evaluate - ENTRADA " << get_elemDistorcionMax() << std::endl;

	QualityMetric* MP = get_quality_metric();
	MP = get_quality_metric();
	MP->get_evaluations( pd, qmHandles, free, err );  

	std::vector<size_t>::const_iterator i;
   	size_t num_elem = pd.num_elements();
	double value, Qmax=-MSQ_MAX; // 1e100;
	size_t orden=0; //, orden_max;
	long int orden_max = get_elemDistorcionMax();
	double Q[num_elem];

	// registramos Qi y Qmax y el ID del elemento (orden)
	for (i = qmHandles.begin(); i != qmHandles.end(); ++i)
	{
	  // Qi
	  MP->evaluate( pd, *i, value, err );
	  if(get_elemDistorcionMax() < 0){ // cuando es negativo es que no se ha inicializado
	    if(value > Qmax) {
		Qmax = value;
		orden_max=(long int)orden;
	    }
	  }
	  Q[orden] = value;

#ifdef debugPrintf
	  //printf("logBarrierObjFunc2::evaluate - Elem: %lu, Qi= %8.2e, Qmax (%lu)= %8.2e\n", orden, Q[orden], orden_max, Qmax);
	  printf("logBarrierObjFunc2::evaluate - Elem: %lu, Qi= %8.2e, orden_max= %li\n", orden, Q[orden], orden_max);
#endif

	  orden++;
	}

	// calculate OF value for just the patch
	double working_sum = 0.0;
	orden=0;
	for (i = qmHandles.begin(); i != qmHandles.end(); ++i)
	{
	    // calcula working_sum = suma(log(Qmax - Qi))
	  if(Q[orden_max] >= Q[orden]){
 	    working_sum += log10(1 - Q[orden] + Q[orden_max]);
	  } else {
#ifdef debugPrintf
	    printf("logBarrierObjFunc2::evaluate ERROR Qi > Qmax - Elem: %lu, Qi= %8.2e, Qmax= %8.2e\n", orden, Q[orden], Q[orden_max]);
#endif
	    return false;
	  }

#ifdef debugPrintf
	    printf("logBarrierObjFunc2::evaluate - Elem: %lu, OF_suma_parcial= %8.2e\n", orden, working_sum);
	    //std::cout << *i << std::endl;
#endif
	    orden++;
	}
	value_out = -1 * working_sum * mMU + Q[orden_max];
	value_out *= MP->get_negate_flag();

	// si la Qmax es superior a la anteriormente registrada, devolvemos FALSE
	if (Q[orden_max] > get_calidad_maxima_ObjFunc()){
#ifdef debugPrintf
	    printf("logBarrierObjFunc2::evaluate ERROR Qmax > QmaxANTERIOR - Elem_max: %lu, Qmax= %8.2e, QmaxAnterior= %8.2e\n", orden_max, Q[orden_max], get_calidad_maxima_ObjFunc());
#endif
	  return false;
	}
/*
	} else {
	  set_calidad_maxima(Q[orden_max]);
#ifdef debugPrintf
	    printf("logBarrierObjFunc2::evaluate NUEVA Qmax - Elem_max: %lu, Qmax= %8.2e\n", orden_max, get_calidad_maxima_ObjFunc());
#endif
	}
*/

#ifdef debugPrintf
  	printf("logBarrierObjFunc2::evaluate, pd.size= %2lu OF= %9.4e sum_log= %9.4e QmaxPatch(%lu)= %9.2e MU= %9.2e\n", qmHandles.size() , value_out , working_sum , orden_max, Q[orden_max], mMU);
	std::cout << "logBarrierObjFunc2::evaluate - SALIDA" << std::endl;
#endif

  return true;
}

// calcula la funcion objetivo log-barrier y sus derivadas respecto a S
// siguiendo la clase LPtoPTemplate 
bool logBarrierObjFunc2::evaluate_with_gradient( EvalType type, 
                                            PatchData& pd,
                                            double& value_out,
                                            std::vector<Vector3D>& grad_out,
                                            MsqError& err )
{
  std::cout << "logBarrierObjFunc2::evaluate with gradient - ENTRADA" << std::endl;
  bool retorno=true;

  QualityMetric* MP = get_quality_metric();
  MP = get_quality_metric();
  MP->get_evaluations( pd, qmHandles, OF_FREE_EVALS_ONLY, err );  
  size_t num_elem = pd.num_elements();

// zero gradient
  grad_out.clear();
  grad_out.resize( pd.num_free_vertices(), Vector3D(0.0,0.0,0.0) );

// calculate OF value and gradient for just the patch
  bool qm_bool=true;
  double QM_val;
  float log10e = log10(exp(1.0)); 
  std::vector<size_t>::const_iterator i;

  double Qmax=-MSQ_MAX; // 1e100;
  size_t orden=0; //, orden_max;
  long int orden_max = get_elemDistorcionMax();
  double Q[num_elem];
  std::vector<Vector3D> mGradientArray[num_elem] ;

  // registramos Qi y dQi/dS
  for (i = qmHandles.begin(); i != qmHandles.end(); ++i)
  {
	qm_bool = MP->evaluate_with_gradient( pd, *i, QM_val, mIndices, mGradient, err );

	if(get_elemDistorcionMax() < 0){ // cuando es negativo es que no se ha inicializado
	    if(QM_val > Qmax) {
		Qmax = QM_val;
		orden_max=(long int)orden;
	    }
	}

	Q[orden] = QM_val;
	mGradientArray[orden] = mGradient;
#ifdef debugPrintf
	  //printf("logBarrierObjFunc2::evaluate with gradient - Elem: %lu, Qi= %8.2e, Qmax (%lu)= %8.2e\n", orden, Q[orden], orden_max, Qmax);
	printf("logBarrierObjFunc2::evaluate with gradient - Elem: %lu, Qi= %8.2e, orden_max= %li\n", orden, Q[orden], orden_max);
#endif
	orden++;
  } // for i


// F = Nmax - mMU ...
// dF/dS = dNmax/dS - mMU * log10e * ...
	orden = 0;
	value_out = 0.;
	for (i = qmHandles.begin(); i != qmHandles.end(); ++i)
	{
	  if(Q[orden_max] >= Q[orden]){
 	    value_out += log10(1 - Q[orden] + Q[orden_max]);
	  } else {
#ifdef debugPrintf
	  printf("logBarrierObjFunc2::evaluate_with_gradient ERROR Qi > Qmax - Elem: %lu, Qi= %8.2e, Qmax= %8.2e\n", orden, Q[orden], Q[orden_max]);
#endif
	  return false;
	  }

     	  std::vector<Vector3D> mGradientDumy = mGradient;
	  double localDumy2 = -1 * mMU * log10e * MP->get_negate_flag() / (1 - Q[orden] + Q[orden_max]);
	  for (size_t j = 0; j < mIndices.size(); ++j) {
		mGradient[j] = mGradientArray[orden_max][j] - mGradientArray[orden][j];
		mGradient[j] *= localDumy2 ;
/*
#ifdef debugPrintf
	for(int k=0; k<3; k++) {
		 std::cout << *i << "  ";
		 printf("logBarrierObjFunc2.cpp - coor= %i, Q= %e, mCalidadMin= %f, localDumy2= %e, mGD= %f\n", k,QM_val, mCalidadMinima, localDumy2, mGradientDumy[j][k]);
	}
#endif
*/
		grad_out[mIndices[j]] += mGradient[j];
	  }
	  orden++;
	}
	value_out *= mMU ;
	value_out = Q[orden_max]- value_out;
	value_out *= MP->get_negate_flag();
	for (size_t j = 0; j < mIndices.size(); ++j) {
		grad_out[mIndices[j]] += mGradientArray[orden_max][j];
	}

#ifdef debugPrintf
  	printf("logBarrierObjFunc2::evaluate_with_gradient, pd.size= %2lu OF= %9.4e QmaxPatch(%lu)= %9.2e\n", qmHandles.size(), value_out, orden_max, Q[orden_max]);
  	std::cout << "logBarrierObjFunc2::evaluate with gradient - SALIDA" << std::endl;
#endif

  return retorno;
}
  
bool logBarrierObjFunc2::evaluate_with_Hessian_diagonal( EvalType type, 
                                        PatchData& pd,
                                        double& OF_val,
                                        std::vector<Vector3D>& grad,
                                        std::vector<SymMatrix3D>& hess_diag,
                                        MsqError& err )
{
 
  return true;
}
	
bool logBarrierObjFunc2::evaluate_with_Hessian( EvalType type, 
                                           PatchData& pd,
                                           double& OF_val,
                                           std::vector<Vector3D>& grad,
                                           MsqHessian& hessian,
                                           MsqError& err )
{
  
  return true;
}

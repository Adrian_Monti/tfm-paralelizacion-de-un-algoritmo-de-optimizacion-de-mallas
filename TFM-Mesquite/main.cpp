/* ***************************************************************** 
   MESQUITE -- The Mesh Quality Improvement Toolkit

   Copyright 2004 Sandia Corporation and Argonne National
   Laboratory.  Under the terms of Contract DE-AC04-94AL85000 
   with Sandia Corporation, the U.S. Government retains certain 
/bin/bash: nterfa: orden no encontrada

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License 
   (lgpl.txt) along with this library; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 
   diachin2@llnl.gov, djmelan@sandia.gov, mbrewer@sandia.gov, 
   pknupp@sandia.gov, tleurent@mcs.anl.gov, tmunson@mcs.anl.gov      
   
   ***************************************************************** */
// -*- Mode : c++; tab-width: 2; c-tab-always-indent: t; indent-tabs-mode: nil; c-basic-offset: 2 -*-
//
//   SUMMARY: 
//     USAGE:
//
// ORIG-DATE: 24-Jan-12
//  LAST-MOD: 26-Jan-1 by Stephen Kennon
//
//
// DESCRIPTION:
// ============
/*! \file par_hex.cpp

A test of Mesquite's parallel capabilities.  Reads a split vtk file, smooths in parallel using Laplace
smoothing, writes out the result (which can be compared with the "gold" copy of the same name in the
meshFiles VTK directory).

See the Mesquite User's Guide, section "Using Mesquite in Parallel" - this code is very similar to the
example code shown therein.

*/
// DESCRIP-END.
//

char nombreMalla[50]; // variable global para nombre de ficheros

#include <omp.h>

#include "meshfiles.h"

#include "MeshImpl.hpp"
#include "MeshImplData.hpp"
#include "MeshImplTags.hpp"
#include "MeshUtil.hpp"
#include "MsqTimer.hpp"
#include "Mesquite.hpp"
#include "MsqError.hpp"
#include "Vector3D.hpp"
#include "InstructionQueue.hpp"
#include "LaplaceWrapper.hpp"
#include "ShapeImprovementWrapper.hpp"
#include "UntangleWrapper.hpp"
#include "PatchData.hpp"
#include "miTerminationCriterion.hpp"
#include "QualityAssessor.hpp"
#include "VtkTypeInfo.hpp"

/* Mesquite includes */
#include "ParallelMeshImpl.hpp"
#include "ParallelHelper.hpp"
#include "MsqDebug.hpp"
#include "Settings.hpp"
//#include "ShapeImprovementWrapper.hpp"
//#include "UntangleWrapper.hpp"

#include "IdealWeightInverseMeanRatio.hpp" 
#include "miIdealWeightMeanRatio.hpp" 
#include "ConditionNumberQualityMetric.hpp" 

#include "miUntangleBetaQualityMetric.hpp"
/* BUENO */  #include "miConjugateGradient.hpp"
#include "miSteepestDescent.hpp"
#include "miQuasiNewton.hpp"
#include "miFeasibleNewton.hpp"
#include "miTrustRegion.hpp"
#include "miTUntangle1.hpp"
#include "miTUntangleBeta.hpp"
#include "miTUntangleMu.hpp"
#include "TQualityMetric.hpp"
#include "AWQualityMetric.hpp"
#include "miAWUntangleBeta.hpp"
#include "ThTMetric.hpp"
#include "logBarrierObjFunc.hpp"
#include "logBarrierObjFunc2.hpp"
#include "ThMixedQ.hpp"
#include "hSigmaBarrierObjFunc.hpp"

// algorithms
#include "Randomize.hpp"
#include "ConditionNumberQualityMetric.hpp"
#include "LPtoPTemplate.hpp"
#include "LInfTemplate.hpp"
//#include "SteepestDescent.hpp"
//#include "ConjugateGradient.hpp"
#include "PlanarDomain.hpp"

#include "hSigmaMetric.hpp"
#include "hSigmaMetric2.hpp"
#include "hSigmaMetric3.hpp"
#include "ThSigmaMetric.hpp"
#include "ThSigmaMetric2.hpp"
#include "ThSigmaMetric3.hpp"
#include "IdealShapeTarget.hpp"
#include "ElementPMeanP.hpp"
#include "SimpleStats.hpp"

#include <iostream>
using std::cout;
using std::endl;
#include <cstdlib>

#include <mpi.h>
#include <sstream>
#include <time.h>

using namespace Mesquite;

#define VTK_3D_DIR MESH_FILES_DIR "3D/vtk/hexes/tangled/"
#define VTK_2D_DIR MESH_FILES_DIR "2D/vtk/quads/tangled/"

#define MSQ_USE_FUNCTION_TIMERS

#define FRONTERA // activa el analisis de la frontera entre particiones, tarda para mallas grandas

using namespace std;
  
const double DEF_UNT_BETA = 1e-8;
const double DEF_SUC_EPS = 1e-7;

static long int PE(double dumy) ;
static long int PD(double dumy, unsigned int numDecimales) ;

float promedioMedida(float *receptorDatosEvalPres, int Nmetricas, int nprocs, int ordenMetrica){
// calcula el promedio de las metricas de los cores
    float dumy=0;
    for(int i = ordenMetrica; i < Nmetricas*nprocs; i=i+Nmetricas){
	dumy += receptorDatosEvalPres[i];
    }
    dumy /= nprocs;
    return dumy;
}
float desbalanceoSuperiorMedida(float *receptorDatosEvalPres, int Nmetricas, int nprocs, int ordenMetrica){
// calcula el porcentaje de la diferencia del valor maximo - valor medio respecto al valor medio
    float dumy=-1e30, media;

    media = promedioMedida(receptorDatosEvalPres, Nmetricas, nprocs, ordenMetrica);

    for(int i = ordenMetrica; i < Nmetricas*nprocs; i=i+Nmetricas){
     if(receptorDatosEvalPres[i] >= media && receptorDatosEvalPres[i] > dumy){
	dumy = receptorDatosEvalPres[i];
     }
    }
    if (media > 0) dumy = 100.0 * abs((dumy - media) / media) ;
    else dumy = 0;
    return dumy;
}
float desbalanceoInferiorMedida(float *receptorDatosEvalPres, int Nmetricas, int nprocs, int ordenMetrica){
// calcula el porcentaje de la diferencia del valor maximo - valor medio respecto al valor medio
    float dumy=1e30, media;

    media = promedioMedida(receptorDatosEvalPres, Nmetricas, nprocs, ordenMetrica);

    for(int i = ordenMetrica; i < Nmetricas*nprocs; i=i+Nmetricas){
     if(receptorDatosEvalPres[i] <= media && receptorDatosEvalPres[i] < dumy){
	dumy = receptorDatosEvalPres[i];
     }
    }
    if (media > 0) dumy = 100.0 * abs((media - dumy)/ media) ;
    else dumy = 0;
    return dumy;
}

int* analizaFrontera(ParallelMeshImpl parallel_mesh); 
//void analizaFrontera(ParallelMeshImpl parallel_mesh); 

void guardaFrontera(size_t num_vtx_fron, size_t num_elem_fron, bool * vtx_fixed_fron, double * coord_array_fron, EntityTopology * elem_topo_fron, int * vtx2_handle_fron, int* tag_data_PROC_fron, unsigned long * tag_data_ID_fron); 

void mi_write_vtk(MeshImplData * myMesh, int * tag_data_PROC, unsigned long * tag_data_ID, const char* out_filename, MsqError &err);

static void get_field_names( const TagDescription& tag,
                             std::string& field_out,
                             std::string& member_out,
                             MsqError& err ) ;

void rellenaMesh(MeshImplData * myMesh, 
		size_t num_vtx,
		size_t num_elem,
  		bool * fixed_array,
  		double * coord_array,
		EntityTopology * element_topologies,
		int * vertex2_array);

class ParShapeImprover
{
  int innerIter;
  double gradNorm;

public:

  ParShapeImprover(int inner_iterations=100, double grad_norm=1.e-8) : innerIter(inner_iterations),gradNorm(grad_norm) 
{
}

  class ParShapeImprovementWrapper : public Wrapper {
     
  public:  
        
    //Constructor sets the instructions in the queue.
    ParShapeImprovementWrapper(int inner_iterations = 100,
                               double cpu_time = 0.0, 
                               double grad_norm =1.e-8,
                               int parallel_iterations = 100)
      : innerIter(inner_iterations),
        maxTime(cpu_time), 
        gradNorm(grad_norm),
        untBeta(DEF_UNT_BETA),
        successiveEps(DEF_SUC_EPS),
        parallelIterations(parallel_iterations),
        m_do_untangle_only(false)
    {}


  protected:

    void run_wrapper( MeshDomainAssoc* mesh_and_domain,
			      ParallelMesh* pmesh,
			      Settings* settings,
			      QualityAssessor* qa,
			      MsqError& err );
	      
  private:

    int innerIter;
    double maxTime, gradNorm;
    // constants
    const double untBeta;
    const double successiveEps;
    int parallelIterations;

  public:

    bool m_do_untangle_only;

  }; // class ParShapeImprovementWrapper

 static int count_invalid_elements(Mesh &mesh, MeshDomain *domain=0);

 static float* checkQuality(Mesh &mesh, MeshDomain *domain=0);
 
 //void run(Mesh &mesh, MeshDomain *domain, MsqError& err, bool always_smooth=true, int debug=0, const char* vtk_name);
 void run(Mesh &mesh, MeshDomain *domain, MsqError& err, bool always_smooth=true, int debug=0);

}; // class ParShapeImprover

void ParShapeImprover::ParShapeImprovementWrapper::run_wrapper( MeshDomainAssoc* mesh_and_domain,
								ParallelMesh* pmesh,
								Settings* settings,
								QualityAssessor* qa,
								MsqError& err )
{
// extrae la submalla que corresponde a la frontera de esta particion y sus vecinas
// los resultados de la extraccion se guardan en array para recogerlos por el master

  int rank, nprocs;
  char sdumy[30];
  int iDumy1, iDumy2;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

#ifdef FRONTERA
	int* resultadosFrontera;
	if (nprocs > 1) resultadosFrontera = analizaFrontera(pmesh); 
	//resultadosFrontera = analizaFrontera(parallel_mesh); 
#else
	printf("P[%i] main.cpp - No se analiza frontera porque esta desactivado\n\n", rank);
#endif

  // Define an UNTANGLER

  // Parte 1: definicion de medida calidad/distorcion + funcion objetivo

  // Metodo: ThMixedQ + logBarrier 
/*
  IdealShapeTarget 	target;
  ThMixedQ 		m( &target);			// Metrica Mixta: Q = vol + (vol/L)
  // logBarrier para MixedQ 
  // maximizando calidad, activarlo en logBarrierObjFunc.cpp
  // desenreda TangledCube, ThMixedQ.hpp: mALFA=1.0e3; mBETA=1.0e3; mLAMBDA=1.0e+0; + BarreraNew (miVertexmover.cpp)= BarreraActual - 1e-3 * Qmin
  logBarrierObjFunc 	untangle_func(&m, -5e8, 10); 	
*/
  // Metodo: hSigmaMetric + logBarrier 
/*
  hSigmaMetric 		tm(1.2e-3, 1.0e-10);
  IdealShapeTarget 	target;
  ThSigmaMetric 	m( &target, &tm );
  // logBarrier para hSigmaMetric
  // minimizando distorcion, activarlo en logBarrierObjFunc.cpp
  // desenreda todas las mallas, BarreraNew (miVertexmover.cpp)= BarreraActual * 1e3 
  logBarrierObjFunc 	untangle_func(&m, +5.0e8, 1); 	
*/

  // Metodo: hSigmaMetric + LPtoP
//
  hSigmaMetric 		tm(1.2e-3, 1.0e-10);
  IdealShapeTarget 	target;
  ThSigmaMetric 	m( &target, &tm );
  // LPtoP para hSigmaMetric, minimizando distorcion, desenreda todas las mallas
  LPtoPTemplate 	untangle_func( 1, &m); 		// norma L1 
  //LPtoPTemplate 	untangle_func( 2, &m); 		// norma L2 
//

  // Metodo: TUntangle1 + LPtoP
/*
  IdealShapeTarget 	target;
  miTUntangle1 		tm(1e-6,1.0); // 
  ThTMetric 		m( &target, &tm );
  LPtoPTemplate 	untangle_func( 1, &m); 		// norma L1 
*/

  // Metodo: TUntangleBeta + LPtoP
/*
  IdealShapeTarget 	target;
  miTUntangleBeta 	tm(1e-5); // 
  TQualityMetric 	m( &target, &tm );
  LPtoPTemplate 	untangle_func( 1, &m); 		// norma L1 
*/

  // Metodo: hSigmaMetric + barreraJoseMaria 
/*
  hSigmaMetric 		tm(1.2e-3, 1.0e-10);
  IdealShapeTarget 	target;
  ThSigmaMetric 	m( &target, &tm );
  // funcion objetivo con Barrera, minimizando distorcion, desenreda todas las mallas
  // 2 posibilidades de barrera: 1/h, 1/hPrima; activarlo en hSigmaBarrierObjFunc
  // en miVertexMover.cpp se establece el factor de la nueva barrera: *1.0 (1/h), *0.99 (1/hPrima)
  hSigmaBarrierObjFunc 	untangle_func(&m, +5e8, 1); 
*/

  // Parte 2: definicion del solver y sus caracteristicas
  miConjugateGradient untangle_solver( &untangle_func );// minizacion de funcion objetivo por Gradiente Conjugato, necesita derivadas
  untangle_solver.use_element_on_vertex_patch();	// optimizacion local: estrella a estrella
  //untangle_solver.use_global_patch();			// optimizacion global: toda la malla a la vez

  // Parte 3: definicion criterios de parada del solver
  TerminationCriterion untangle_inner("-TermCrit:untan_inner-"), untangle_outer("-TermCrit:untan_outer-");

  // criterios para el line search
  untangle_inner.add_iteration_limit( 150 );		// numero maximo de interaciones internas (line search)
  untangle_inner.add_absolute_vertex_movement(1e-3); 	// la distancia se compara con el cuadrado de 1e-3
  //untangle_inner.add_absolute_vertex_movement(1e-6); 	// la distancia se compara con el cuadrado de 1e-3

  // criterios para toda la malla
  untangle_outer.add_iteration_limit(20000);		// numero maxximo de mesh sweeps 
  sprintf(sdumy,"untangle_%5s_P%i.gpt",nombreMalla,rank);
  untangle_outer.write_iterations(sdumy, err);
  untangle_outer.add_untangled_mesh();			// la malla esta desenredada completamente
  untangle_outer.add_absolute_vertex_movement(1e-6);	// esto solo influye cuando no desenreda, para salir

  untangle_solver.set_inner_termination_criterion( &untangle_inner );
  untangle_solver.set_outer_termination_criterion( &untangle_outer );

  /* BUENO */    //hSigmaMetric tm(1.2e-3, 1.0e-10);
  //hSigmaMetric3 tm(1.2e-3, 1.0e-10);
  //hSigmaMetric2 tm(1.2e-3, 1.0e-10);

  /* desenreda con parametros variables todas las mallas menos armadillo y ej_10  */   //miTUntangle1 tm(1e-4,1.0); // 
  /* desenreda TangledCube y ej9 con parametros ctes */ //miTUntangle1 tm(1e-6,1.0); // 

  /* desenreda TangledCube */ //miTUntangleBeta tm(1e-5); // 
  //miTUntangleBeta tm(7e-3); // 

  /* desenreda TangledCube */ //miTUntangle1 tmpre(1e-6,1.0); // 
  /* desenreda ej9 */  //miTUntangle1 tmpre(1e-4,1.0); // 
  /* desenreda TangledCube */ //miTUntangleBeta tmpre(1e-5); // 
  /* desenreda TangledCube */ //miTUntangleMu tm(&tmpre,1e-7); // 
  /* desenreda ej9 */  //miTUntangleMu tm(&tmpre,1e-7); // 

  /* desenreda TangledCube */ //miAWUntangleBeta tm(0.5); // TangledCube: 0.5
  //miAWUntangleBeta tm(0.080); // TangledCube: 0.5


  //std::vector< Mesh::VertexHandle > vertex;
  //pmesh->get_all_vertices (vertex, err) ;
  //size_t num_vtx = vertex.size();
  //tm.setNumeroVertices(num_vtx);
  //std::cout << "P[" << rank << "] main.cpp, metrica hSigma se ha incializado su cache para " << num_vtx << " vertices" << std::endl;

  //IdealShapeTarget target;
  /* BUENO */  //ThSigmaMetric m( &target, &tm );
  //ThSigmaMetric2 m( &target, &tm );
  //ThSigmaMetric3 m( &target, &tm );

 //Metrica Mixta: Q = vol + (vol/L)
  //ThMixedQ m( &target);

  /* acompanya a metricas miTU* para params constantes */  //TQualityMetric m( &target, &tm );
  /* acompanya a metricas miTUntangle1* para params variables  */  //ThTMetric m( &target, &tm );
  /* acompanya a metricas miAWUntangleBeta  */  //AWQualityMetric m( &target, &tm );
  /* desenreda Tangled Cube */ //miUntangleBetaQualityMetric m(4e-4); // UntangleBetaQualityMetric (double bet=0.05)

  // siempre - ElementPMeanP untangle_metric(1, &m);

  bool check_untangle = true;
  //bool check_untangle = false; // desactivamos la medida de calidad previo de la malla para "ahorrar"
  float* QmallaInicioUntangling;
  int NtetraInvalidIniUntan;

  if (check_untangle) {
/*
     std::cout << "\nP[" << rank << "]  ParShapeImproverWrapper.... running QA with untangle_metric before... " << std::endl << std::flush;
     InstructionQueue q1;
     QualityAssessor qa_untangle(&untangle_metric);
     q1.add_quality_assessor(&qa_untangle, err); MSQ_ERRRTN(err);
     q1.run_common( mesh_and_domain, pmesh, settings, err ); 
     std::cout << std::flush;
     std::cout << "\nP[" << rank << "]  ParShapeImproverWrapper.... running QA with untangle_metric before... done " << std::endl << std::flush;
   qa_untangle.get_inverted_element_count(iDumy1, iDumy2, err);
*/
      Mesh* mesh = mesh_and_domain->get_mesh();
      MeshDomain* domain = mesh_and_domain->get_domain();
      int num_invalid = count_invalid_elements(*mesh, domain);
      QmallaInicioUntangling = checkQuality(*mesh, domain); // procedimiento que calcula la calidad mean-ratio
      NtetraInvalidIniUntan = num_invalid;
      std::cout << "\nP[" << rank << "] main.cpp - ParShapeImproverWrapper - antes untangling... \n\telementos Invalidos: " << 
		num_invalid << ", \n\tcalidad promedio mean-ratio inicial malla: " << QmallaInicioUntangling[0] << 
		num_invalid << ", \n\tcalidad maxima   mean-ratio inicial malla: " << QmallaInicioUntangling[1] << 
		num_invalid << ", \n\tcalidad minima   mean-ratio inicial malla: " << QmallaInicioUntangling[2] << std::endl << std::flush;
  } // if
//
  // siempre - LPtoPTemplate untangle_func( 1, &untangle_metric );
  /* BUENO */  // LPtoPTemplate untangle_func( 1, &m);  
   //LPtoPTemplate untangle_func( 2, &m);  

// logBarrier funcion objetivo
   //--> logBarrierObjFunc untangle_func(&m, +5.0e8, 1); 	// logBarrier_hSigma, minimizando

   //   logBarrierObjFunc untangle_func(&m, -5e8, 10); 	// logBarrier_MixedQ, maximizando, desenreda TangledCube
   //logBarrierObjFunc untangle_func(&m, -0.4e0, 1); 	// desenreda ej9
   // logBarrierObjFunc untangle_func(&m, -3.1, 1);	// desenreda conejo
   //logBarrierObjFunc untangle_func(&m, -0.3, 1); 	// desenreda hueso
   //logBarrierObjFunc untangle_func(&m, -9400.0, 1); 	// prueba Screwdriver

   //logBarrierObjFunc2 untangle_func(&m, +5.0e8, 1.0); 	// 

      //hSigmaBarrierObjFunc untangle_func(&m, +5e8, 1); // funcion objetivo con Barrera de Jose Maria	

  /* BUENO */   //miConjugateGradient untangle_solver( &untangle_func );
  //ConjugateGradient untangle_solver( &untangle_func );
  //miQuasiNewton untangle_solver( &untangle_func );
  /* desenreda TangledCube*/ //miSteepestDescent untangle_solver( &untangle_func );
  //miFeasibleNewton untangle_solver( &untangle_func );
  /* desenreda TangledCube*/    //miTrustRegion untangle_solver( &untangle_func );
  //untangle_solver.set_debugging_level(3);
  //untangle_solver.use_element_on_vertex_patch(1);
  //untangle_solver.use_global_patch();
  /* BUENO optimizacion local */  //untangle_solver.use_element_on_vertex_patch();

  //TerminationCriterion untangle_inner("-TermCrit:untan_inner-"), untangle_outer("-TermCrit:untan_outer-");

  //untangle_inner.add_absolute_gradient_L2_norm( gradNorm );
  //untangle_inner.add_absolute_successive_improvement( successiveEps );
  //untangle_inner.add_relative_successive_improvement( 1.e-6 );
  //untangle_inner.write_iterations("untangle.gpt", err);

  // For parallel runs, we generally need to have the inner and outer TerminationCriterion
  //   have the same criteria else we can get an infinite loop (see VertexMover::loop_over_mesh)
  //untangle_inner.add_absolute_quality_improvement( 0.0 );
  /* experimentos con log-barrier */ //untangle_inner.add_iteration_limit( 50 ); // funciono para todas las mallas excepto armadillo
  /* prueba con ThMixedQ */ //untangle_inner.add_iteration_limit( 50 );
  /* BUENO */ //untangle_inner.add_iteration_limit( 150 );
  //untangle_inner.add_untangled_mesh();
  //--> untangle_inner.add_absolute_vertex_movement(1e-3); // la distancia se compara con el cuadrado de 1e-3
  /* BUENO */ //untangle_inner.add_absolute_vertex_movement(1e-6); // la distancia se compara con el cuadrado de 1e-3
  //untangle_inner.add_absolute_vertex_movement(1e-9); // prueba para hueso
  //untangle_inner.add_absolute_gradient_L2_norm( 1e-3 );
  //untangle_inner.add_relative_quality_improvement( 1.0e-6 );

  //untangle_outer.add_absolute_gradient_L2_norm( gradNorm );
  //untangle_outer.add_absolute_quality_improvement( 0.8 );
  //untangle_outer.add_relative_quality_improvement( 1.0e-8 );
  //untangle_outer.add_iteration_limit( pmesh ? parallelIterations : 1 );
  //untangle_outer.add_iteration_limit(20000); 
  //--> untangle_outer.add_iteration_limit(200); 
  //sprintf(sdumy,"untangleP%i.gpt",rank);
  //sprintf(sdumy,"untangle_%5s_P%i.gpt",nombreMalla,rank);
  //sprintf(sdumy,"untangle_hSigma_%5s_P%i.gpt",nombreMalla,rank);
  //untangle_outer.write_iterations(sdumy, err);
  //untangle_outer.write_mesh_steps(sdumy, untangle_outer.GNUPLOT);
  //untangle_outer.write_iterations("untangle.gpt", err);
  /* BUENO */ //untangle_outer.add_untangled_mesh();
  /* BUENO */ //untangle_outer.add_absolute_vertex_movement(1e-6);
  //untangle_outer.add_absolute_vertex_movement(1e-3);
  //untangle_outer.add_absolute_vertex_movement(1e-9); // prueba para hueso

  //untangle_solver.set_inner_termination_criterion( &untangle_inner );
  //untangle_solver.set_outer_termination_criterion( &untangle_outer );

  // define SHAPE IMPROVER

  // Parte 1: definicion de medida calidad/distorcion + funcion objetivo

  // Metodo: Metrica Mixta ThMixedQ: Q = vol + (vol/L) + logBarrier_maximizando
/*
  IdealShapeTarget 	target_shape;
  ThMixedQ 		inverse_mean_ratio( &target_shape);
  logBarrierObjFunc 	obj_func(&inverse_mean_ratio, -5e8, 10);	// logBarrier para ThMixedQ 
*/

  // Metodo: hSigmaMetric + logBarrier 
/*
  hSigmaMetric 		tm2(1.2e-3, 1.0e-10);
  IdealShapeTarget 	target_shape;
  ThSigmaMetric 	inverse_mean_ratio( &target_shape, &tm );
  // logBarrier para hSigmaMetric
  // minimizando distorcion, activarlo en logBarrierObjFunc.cpp
  // BarreraNew (miVertexmover.cpp)= BarreraActual * 1e3 
  logBarrierObjFunc 	obj_func(&inverse_mean_ratio, +5.0e8, 1); 	
*/

  // Metodo: hSigmaMetric + LPtoP
//
  hSigmaMetric 		tm2(1.2e-3, 1.0e-10);
  IdealShapeTarget 	target_shape;
  ThSigmaMetric 	inverse_mean_ratio( &target_shape, &tm2 );
  // LPtoP para hSigmaMetric, minimizando distorcion, desenreda todas las mallas
  LPtoPTemplate 	obj_func( 1, &inverse_mean_ratio); 		// norma L1 
  //LPtoPTemplate 	obj_func( 2, &inverse_mean_ratio); 		// norma L2, mejor calidad minima 
//

  // Metodo: TUntangle1 + LPtoP
/*
  IdealShapeTarget 	target_shape;
  miTUntangle1 		tm2(1e-6,1.0); // 
  ThTMetric 		inverse_mean_ratio( &target_shape, &tm2 );
  LPtoPTemplate 	obj_func( 1, &inverse_mean_ratio); 		// norma L1 
*/

  // Metodo: TUntangleBeta + LPtoP
/*
  IdealShapeTarget 	target_shape;
  miTUntangleBeta 	tm2(1e-5); // 
  TQualityMetric 	inverse_mean_ratio( &target_shape, &tm2 );
  LPtoPTemplate 	obj_func( 1, &inverse_mean_ratio);		// norma L1 
*/

  // Metodo: hSigmaMetric + barreraJoseMaria 
/*
  hSigmaMetric 		tm2(1.2e-3, 1.0e-10);
  IdealShapeTarget 	target_shape;
  ThSigmaMetric 	inverse_mean_ratio( &target_shape, &tm2 );
  // funcion objetivo con Barrera, minimizando distorcion, desenreda todas las mallas
  // 2 posibilidades de barrera: 1/h, 1/hPrima; activarlo en hSigmaBarrierObjFunc
  // en miVertexMover.cpp se establece el factor de la nueva barrera: *1.0 (1/h), *0.99 (1/hPrima)
  hSigmaBarrierObjFunc 	obj_func(&inverse_mean_ratio, +5e8, 1); 
*/

  // Parte 2: definicion del solver y sus caracteristicas
  miConjugateGradient shape_solver( &obj_func );	// minizacion de funcion objetivo por Gradiente Conjugato, necesita derivadas
  shape_solver.use_element_on_vertex_patch();		// optimizacion local: estrella a estrella
  //shape_solver.use_global_patch();			// optimizacion global: toda la malla a la vez

  // Parte 3: definicion criterios de parada del solver
  TerminationCriterion term_inner("-TermCrit:shape_inner-"), term_outer("-TermCrit:shape_outer-");

  // criterios para el line search
  term_inner.add_iteration_limit( 150 );		// numero maximo de interaciones internas (line search)
  term_inner.add_absolute_vertex_movement(1e-3); 	// la distancia se compara con el cuadrado de 1e-3
  //term_inner.add_absolute_vertex_movement(1e-6); 	// la distancia se compara con el cuadrado de 1e-3

  // criterios para toda la malla
  sprintf(sdumy,"shape_%5s_P%i.gpt",nombreMalla,rank);
  term_outer.write_iterations(sdumy, err);
  term_outer.add_iteration_limit(20000);		// numero maximo de mesh sweeps 
  term_outer.add_absolute_vertex_movement(1e-3); 	// la distancia se compara con el cuadrado de 1e-3

  shape_solver.set_inner_termination_criterion( &term_inner );
  shape_solver.set_outer_termination_criterion( &term_outer );

  /* BUENO */ //--> hSigmaMetric tm2(1.2e-3, 1.0e-10);
  /* BUENO */ //--> ThSigmaMetric inverse_mean_ratio( &target, &tm2 );
  //IdealWeightInverseMeanRatio inverse_mean_ratio;
  //inverse_mean_ratio.set_averaging_method( QualityMetric::LINEAR );
  //miUntangleBetaQualityMetric inverse_mean_ratio(0.05); // UntangleBetaQualityMetric (double bet=0.05)
  //inverse_mean_ratio.set_NtetraVisitados(0);

  // Metrica TUntangle1 + LPtoP
  /* desenreda con parametros variables todas las mallas menos armadillo y ej_10  */   //miTUntangle1 tm2(1e-4,1.0); // 
  /* acompanya a metricas miTUntangle1* para params variables  */  //ThTMetric inverse_mean_ratio( &target, &tm2 );
  /* BUENO */   //LPtoPTemplate obj_func( 1, &inverse_mean_ratio );
  
  //Metrica Mixta: Q = vol + (vol/L) + logBarrier_maximizando
  //ThMixedQ inverse_mean_ratio( &target);
  //logBarrierObjFunc obj_func(&inverse_mean_ratio, -5e8, 10); 	// para ThMixedQ 

  /* BUENO */   //LPtoPTemplate obj_func( 1, &inverse_mean_ratio );
   //LPtoPTemplate obj_func( 2, &inverse_mean_ratio );

  //--> logBarrierObjFunc obj_func(&inverse_mean_ratio, +5.0e8, 1); 	// 

    //hSigmaBarrierObjFunc obj_func(&inverse_mean_ratio, +5e8, 1); 	// funcion objetivo con barrera de Jose Maria 

  /* BUENO */   //miConjugateGradient shape_solver( &obj_func );
  //miFeasibleNewton shape_solver( &obj_func );
  //ConjugateGradient shape_solver( &obj_func );
  //term_inner.write_iterations("shape.gpt", err);
  //miSteepestDescent shape_solver( &obj_func );
  /* desenreda TangledCube*/  //miTrustRegion shape_solver( &obj_func );

  //shape_solver.use_global_patch();
  //shape_solver.use_element_on_vertex_patch();
  // siempre - qa->add_quality_assessment( &inverse_mean_ratio );

  // For parallel runs, we generally need to have the inner and outer TerminationCriterion
  //   have the same criteria else we can get an infinite loop (see VertexMover::loop_over_mesh)
  //TerminationCriterion term_inner("-TermCrit:shape_inner-"), term_outer("-TermCrit:shape_outer-");
  //term_inner.add_absolute_gradient_L2_norm( gradNorm );
  /* BUENO */  //term_inner.add_absolute_vertex_movement(1.0e-6);
  //--> term_inner.add_absolute_vertex_movement(1.0e-3);
  //term_inner.add_iteration_limit( innerIter );
  /* BUENO */ //term_inner.add_iteration_limit( 50 );
  /* BUENO */ //term_inner.add_iteration_limit( 150 );

  //term_outer.add_relative_gradient_L2_norm( 1e-3);
  //term_outer.add_absolute_gradient_inf_norm( gradNorm );
  //term_outer.add_absolute_gradient_L2_norm( gradNorm );
  /* BUENO */  //term_outer.add_absolute_vertex_movement(1.0e-6);
  //yy-->term_outer.add_absolute_vertex_movement(1.0e-3);
  //term_outer.add_absolute_vertex_movement(3.0e-3);
  //term_outer.add_absolute_vertex_movement(5.0e-1);
  /* BUENO */ //term_outer.add_iteration_limit( 10 );
  //term_outer.write_iterations("shape.gpt", err);
  //sprintf(sdumy,"shape_P%i.gpt",rank);
  //sprintf(sdumy,"shape_%5s_P%i.gpt",nombreMalla,rank);
  //sprintf(sdumy,"shape_hSigma_%5s_P%i.gpt",nombreMalla,rank);
  //term_outer.write_iterations(sdumy, err);

  //term_outer.add_absolute_quality_improvement( 1.e-2 );
  //term_outer.add_relative_successive_improvement( successiveEps );
  //term_outer.add_relative_successive_improvement( 1e-4 );
  //term_outer.add_absolute_successive_improvement( 1e-9 );
  //term_outer.add_relative_minimum_quality_improvement( 1e-9 ); // nuevo critero que sale cuando no cambia la calidad minima mas que un umbral
  /* BUENO */ //term_outer.add_relative_minimum_quality_improvement( 1e-6 ); // nuevo critero que sale cuando no cambia la calidad minima mas que un umbral
  //term_outer.add_relative_vertex_movement(1.0e-6);

  //shape_solver.set_inner_termination_criterion( &term_inner );
  //shape_solver.set_outer_termination_criterion( &term_outer );

  // Apply CPU time limit to untangler
  if (maxTime > 0.0) untangle_inner.add_cpu_time( maxTime );
  
  // Run untangler
  Timer totalTimer;

  std::string nombreOF;

/*
  if( (logBarrierObjFunc*)(&obj_func) != NULL){nombreOF = "log-barrier "; }
  else if( (logBarrierObjFunc2*)(&obj_func) != NULL){nombreOF = "log-barrier2"; }
  else nombreOF = "LPtoP       ";
*/

  if	 (dynamic_cast<logBarrierObjFunc*>(&untangle_func) != NULL ) nombreOF = "log-barrier ";
  else if(dynamic_cast<logBarrierObjFunc2*>(&untangle_func) != NULL ) nombreOF = "log-barrier2";
  else if(dynamic_cast<hSigmaBarrierObjFunc*>(&untangle_func) != NULL ) nombreOF = "hSigma-barrier";
  else 	 nombreOF = "LPtoP";

  std::cout << "\nP[" << rank << "] " << 
    "main.cpp - ParShapeImproverWrapper - EMPIEZA running untangler... " << std::endl;
  //std::cout << "\t ... funcion objetivo: " << (((dynamic_cast<logBarrierObjFunc*>(&untangle_func) != NULL )) ? "log-barrier" : "LPtoP");
  std::cout << "\t ... funcion objetivo: " << nombreOF; 
  std::cout << ", metrica: " << m.get_name() << ", minimizacion: " << untangle_solver.get_name() << std::endl <<  std::endl;

  bool use_untangle_wrapper = false;
  if (use_untangle_wrapper) { 
      UntangleWrapper uw;
      //uw.set_untangle_metric(UntangleWrapper::BETA);
      uw.run_instructions(mesh_and_domain, err);
  } else {
      InstructionQueue q1;
      //QualityAssessor qa_untangle(&m);
      //q1.add_quality_assessor(&qa_untangle, err); MSQ_ERRRTN(err);
      //q1.add_quality_assessor(&qa_untangle, err); MSQ_ERRRTN(err);
      q1.set_master_quality_improver( &untangle_solver, err ); MSQ_ERRRTN(err);
      q1.run_common( mesh_and_domain, pmesh, settings, err ); 
      //iDumy1 = count_invalid_elements(*pmesh, NULL);
      //qa_untangle.get_inverted_element_count(iDumy1, iDumy2, err);
  }

  // Evaluacion de prestaciones
  //size_t NtetraUntangling   = tm.get_NtetraVisitados() ;  // para metricas TU* y miAWUntangleBeta
  size_t NtetraUntangling   = m.get_NtetraVisitados() ; // para hSigma y UBQM 
  //size_t NtetraUntangling   = 0; 
  float  tiempoUntangling   = totalTimer.since_birth();
  size_t NpatchesUntangling = untangle_solver.Npatches; 
  //size_t NpatchesUntangling = m.get_NpatchesVisitados() ; 
  size_t NpatchesInteriorUntangling = untangle_solver.NpatchesInterior;
  size_t NpatchesFronteraUntangling = untangle_solver.NpatchesFrontera;

  std::cout << "\nP[" << rank << "] " << 
    " ParShapeImprovementWrapper: running untangler... done\n " << std::endl << std::flush;
  std::cout << "\nP[" << rank << "] " << 
    " ParShapeImprovementWrapper: MsqError after untangler: " << err << std::endl << std::flush;

  bool check_quality_after_untangler = true;
  float* QmallaInicioSmooth ;
  if (check_quality_after_untangler) {
      Mesh* mesh = mesh_and_domain->get_mesh();
      MeshDomain* domain = mesh_and_domain->get_domain();
      int num_invalid = count_invalid_elements(*mesh, domain);
      std::cout << "\nP[" << rank << "] " << 
	" ParShapeImprover num_invalid after untangler= " << num_invalid << " " 
	<< (num_invalid ? " ERROR still have invalid elements after Mesquite untangle" : 
	" SUCCESS: untangled invalid elements ") << std::endl;
      QmallaInicioSmooth = checkQuality(*mesh, domain);

/*
      if (check_untangle) {
	  std::cout << "\nP[" << rank << 
	    "]  ParShapeImprover(linea 321).... running QA with untangle_metric " << 
	    std::endl << std::flush;
	  InstructionQueue q1;
	  QualityAssessor qa_untangle(&untangle_metric);
	  q1.add_quality_assessor(&qa_untangle, err); MSQ_ERRRTN(err);
	  q1.run_common( mesh_and_domain, pmesh, settings, err ); 
	  std::cout << "\nP[" << rank << 
	    "]  ParShapeImprover.... running QA with untangle_metric... done " 
	    << std::endl << std::flush;
	}
*/
      if (num_invalid) return;
  }
  if (m_do_untangle_only) return;
  MSQ_ERRRTN(err);

  // If limited by CPU time, limit next step to remaning time
  if (maxTime > 0.0) {
    double remaining = maxTime - totalTimer.since_birth();
    if (remaining <= 0.0 ){
      MSQ_DBGOUT(2) << "Optimization is terminating without perfoming shape improvement." << std::endl;
      remaining = 0.0;
    }
    term_inner.add_cpu_time( remaining );
  }
	  
  // Run shape improver
  Timer totalTimerSmooth;
  InstructionQueue q2;
  std::cout << "\nP[" << rank << "] " << 
    " ParShapeImprovementWrapper: running shape improver... \n" << std::endl;

/*
  if( (logBarrierObjFunc*)(&obj_func) != NULL){nombreOF = "log-barrier "; }
  else if( (logBarrierObjFunc2*)(&obj_func) != NULL){nombreOF = "log-barrier2"; }
  else nombreOF = "LPtoP       ";
*/
  if(dynamic_cast<logBarrierObjFunc*>(&obj_func) != NULL ) nombreOF = "log-barrier ";
  else if(dynamic_cast<logBarrierObjFunc2*>(&obj_func) != NULL ) nombreOF = "log-barrier2";
  else if(dynamic_cast<hSigmaBarrierObjFunc*>(&obj_func) != NULL ) nombreOF = "hSigma-barrier";
  else nombreOF = "LPtoP";
  //std::cout << "\t ... funcion objetivo: " << (((dynamic_cast<LPtoPTemplate*>(&obj_func) == NULL )) ? "log-barrier" : "LPtoP");
  std::cout << "\t ... funcion objetivo: " << nombreOF << ", metrica: " << inverse_mean_ratio.get_name() << ", minimizacion: " << shape_solver.get_name() << std::endl <<  std::endl;

  //q2.add_quality_assessor( qa, err ); MSQ_ERRRTN(err);
  q2.set_master_quality_improver( &shape_solver, err ); MSQ_ERRRTN(err);
  //q2.add_quality_assessor( qa, err ); MSQ_ERRRTN(err);
  q2.run_common( mesh_and_domain, pmesh, settings, err ); 

  // Evaluacion de prestaciones de la parte de Smoothing
  //size_t NtetraSmooth = tm2.get_NtetraVisitados() ;  // para metricas TU* y miAWUntangleBeta
  size_t NtetraSmooth = inverse_mean_ratio.get_NtetraVisitados() ;  // para metricas hSigma
  float tiempoSmooth = totalTimerSmooth.since_birth();
  size_t NpatchesSmooth= shape_solver.Npatches;
  //size_t NpatchesSmooth= inverse_mean_ratio.get_NpatchesVisitados() ; 
  size_t NpatchesInteriorSmooth = shape_solver.NpatchesInterior;
  size_t NpatchesFronteraSmooth = shape_solver.NpatchesFrontera;

  std::cout << "\nP[" << rank << "] " << 
    " ParShapeImprovementWrapper: running shape improver... done \n" << std::endl;
  MSQ_ERRRTN(err);

  // se comprueba la calidad de la malla y si existen elementos enredados
  bool check_quality_after_shape_improver = true;
  float* QmallaFinal ;
  if (check_quality_after_shape_improver) {
      Mesh* mesh = mesh_and_domain->get_mesh();
      MeshDomain* domain = mesh_and_domain->get_domain();
      int num_invalid = count_invalid_elements(*mesh, domain);
      std::cout << "\nP[" << rank << "] " << 
	"ParShapeImprovementWrapper: num_invalid after shape improver= " << num_invalid << " " 
	<< (num_invalid ? " ERROR still have invalid elements after Mesquite shape improver" : 
    	" SUCCESS: no tangled invalid elements ") << std::endl;
      QmallaFinal = checkQuality(*mesh, domain);
   }

  // Evaluacion de prestaciones
#ifdef FRONTERA
  int Nmetricas;  // numero de medidas de prestaciones que se visualizan
  if(nprocs > 1){
	Nmetricas=31;
  } else {
	Nmetricas=22;
  }
#else
  int Nmetricas=22;
#endif

  // se inicializa una array de datos de prestaciones
  float enviadorDatosEvalPres[Nmetricas]; // vector donde cada cores pone sus datos de prestaciones
  enviadorDatosEvalPres[0] = tiempoUntangling;
  enviadorDatosEvalPres[1] = tiempoSmooth;
  enviadorDatosEvalPres[2] = tiempoUntangling + tiempoSmooth;
  enviadorDatosEvalPres[3] = (float)NtetraUntangling;
  enviadorDatosEvalPres[4] = (float)NtetraSmooth;
  enviadorDatosEvalPres[5] = (float)NtetraUntangling + (float)NtetraSmooth;
  enviadorDatosEvalPres[6] = enviadorDatosEvalPres[3]/enviadorDatosEvalPres[0];
  enviadorDatosEvalPres[7] = enviadorDatosEvalPres[4]/enviadorDatosEvalPres[1];
  enviadorDatosEvalPres[8] = enviadorDatosEvalPres[5]/enviadorDatosEvalPres[2];
  enviadorDatosEvalPres[9] = QmallaInicioUntangling[0];
  enviadorDatosEvalPres[10] = QmallaInicioUntangling[1];
  enviadorDatosEvalPres[11] = QmallaInicioUntangling[2];
  enviadorDatosEvalPres[12] = QmallaInicioSmooth[0] ;
  enviadorDatosEvalPres[13] = QmallaInicioSmooth[1] ;
  enviadorDatosEvalPres[14] = QmallaInicioSmooth[2] ;
  enviadorDatosEvalPres[15] = QmallaFinal[0];
  enviadorDatosEvalPres[16] = QmallaFinal[1];
  enviadorDatosEvalPres[17] = QmallaFinal[2];

  enviadorDatosEvalPres[18] = NpatchesUntangling ;
  enviadorDatosEvalPres[19] = NpatchesSmooth;
  enviadorDatosEvalPres[20] = NpatchesUntangling+NpatchesSmooth;
  enviadorDatosEvalPres[21] = NtetraInvalidIniUntan;

#ifdef FRONTERA
if(nprocs > 1){
  enviadorDatosEvalPres[22] = resultadosFrontera[0];
  enviadorDatosEvalPres[23] = resultadosFrontera[1];
  enviadorDatosEvalPres[24] = resultadosFrontera[2];
  enviadorDatosEvalPres[25] = resultadosFrontera[3];
  enviadorDatosEvalPres[26] = resultadosFrontera[4];
  enviadorDatosEvalPres[27] = NpatchesInteriorUntangling; 
  enviadorDatosEvalPres[28] = NpatchesFronteraUntangling; 
  enviadorDatosEvalPres[29] = NpatchesInteriorSmooth; 
  enviadorDatosEvalPres[30] = NpatchesFronteraSmooth; 
}
#endif

  const char *stringMetrica[Nmetricas]; // vector donde se inscriben los nombres de las metricas
  stringMetrica[0] = "tiempoUntangling";
  stringMetrica[1] = "tiempoSmoothing";
  stringMetrica[2] = "tiempoTotal";
  stringMetrica[3] = "NtetraUntangling";
  stringMetrica[4] = "NtetraSmooth";
  stringMetrica[5] = "NtetraTotal";
  stringMetrica[6] = "NtetraPerSegUntangling";
  stringMetrica[7] = "NtetraPerSegSmooth";
  stringMetrica[8] = "NtetraPerSegTotal";
  stringMetrica[9] = "QmallaPromedioInicioUntangling";
  stringMetrica[10] = "QmallaMaximoInicioUntangling";
  stringMetrica[11] = "QmallaMinimoInicioUntangling";
  stringMetrica[12] = "QmallaPromedioInicioSmoothing";
  stringMetrica[13] = "QmallaMaximoInicioSmoothing";
  stringMetrica[14] = "QmallaMinimoInicioSmoothing";
  stringMetrica[15] = "QmallaPromedioFinal";
  stringMetrica[16] = "QmallaMaximoFinal";
  stringMetrica[17] = "QmallaMinimoFinal";

  stringMetrica[18] = "NpatchesUntangling"; // num elem del fichero frontera
  stringMetrica[19] = "NpatchesSmooth"; // num elem del fichero frontera
  stringMetrica[20] = "NpatchesTotal"; // num elem del fichero frontera
  stringMetrica[21] = "NtetraInvalidIniUntan"; // num elem enredados de particion antes de optimizar

#ifdef FRONTERA
if(nprocs > 1){
  stringMetrica[22] = "NelementosParticion";
  stringMetrica[23] = "NverticesParticion";
  stringMetrica[24] = "NelemInternosParticion"; // numero elementos internos de la particion
  stringMetrica[25] = "NelemFronteraParticion"; // numero elementos frontera de la particion
  stringMetrica[26] = "NelemTotalFichFrontera"; // num elem del fichero frontera
  stringMetrica[27] = "NpatchesInteriorUntangling"; // num patches procesados en el interior de las particiones
  stringMetrica[28] = "NpatchesFronteraUntangling"; // num patches procesados en frontera entre particiones
  stringMetrica[29] = "NpatchesInteriorSmoothing"; // num patches procesados en el interior de las particiones
  stringMetrica[30] = "NpatchesFronteraSmoothing"; // num patches procesados en la frontera entre particiones
}
#endif

  // se recogen los resultados de evaluacion prestaciones
  double dumy, dumy2, dumy3;
  float receptorDatosEvalPres[Nmetricas*nprocs]; // array donde el maestro recoge los datos de los cores
  //MPI_Gather(&tiempoUntangling, 1, MPI_FLOAT, receptorDatosEvalPres, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
  MPI_Gather(enviadorDatosEvalPres, Nmetricas, MPI_FLOAT, receptorDatosEvalPres, Nmetricas, MPI_FLOAT, 0, MPI_COMM_WORLD);
  // se printa los resultados
  if (rank == 0){
    printf("\nResultados de evaluacion de prestaciones recogido por nodo maestro\n\t\t\t\t");
    for(int i = 0; i < nprocs; i++){
	printf("         P[%03i]",i);
    }
    printf("\n");
    for(int j = 0; j < Nmetricas; j++){
      printf("%31s ",stringMetrica[j]);
      for(int i = 0; i < nprocs; i++){
	//printf("%15.2f",receptorDatosEvalPres[j+i*Nmetricas]);
	dumy2 = (double)receptorDatosEvalPres[j+i*Nmetricas];
 	printf("%15ld,%02ld ",  PE(dumy2), PD(dumy2,2));
      }
      printf("\n");
    }

    //long int parteEntera, parteDecimal, signo;
    for(int i = 0; i < Nmetricas; i++){
	dumy = promedioMedida(receptorDatosEvalPres, Nmetricas, nprocs, i);
	dumy2 = desbalanceoSuperiorMedida(receptorDatosEvalPres, Nmetricas, nprocs, i);
	dumy3 = desbalanceoInferiorMedida(receptorDatosEvalPres, Nmetricas, nprocs, i);
/*
 	signo = ((dumy < 0) ? -1 : 1);
 	parteEntera = (long int)dumy; // se separa parte entera y parte decimal para luego poner coma separadora
 	dumy = (dumy - (double)parteEntera) * 100; // se sacan 2 digitos de decimales
 	parteDecimal = (long int)dumy * signo;
*/
 	printf("PROMEDIO %02i-cores %s %31s = %15ld,%04ld ",nprocs,nombreMalla,stringMetrica[i],PE(dumy),PD(dumy,4));
 	//printf("PROMEDIO %i-cores %s %31s = %15ld,%02ld ",nprocs,nombreMalla,stringMetrica[i],parteEntera,parteDecimal);
 	//printf(" +: %5.1f%% -: %5.1f%%\n", dumy2, dumy3);
 	printf(" +: %5ld,%02ld%% ",  PE(dumy2), PD(dumy2,2));
 	printf(" -: %5ld,%02ld%% \n",PE(dumy3), PD(dumy3,2));
    }

  }

  free( QmallaFinal );
  free(QmallaInicioUntangling);
  free(QmallaInicioSmooth );
#ifdef FRONTERA
 //if (nprocs>1) free(resultadosFrontera);
#endif

// Fin de evaluacion de prestaciones

}

// se separa parte entera y parte decimal para luego poner coma separadora
// devuelve la parte entera de un numero decimal
static long int PE(double dumy) {
 	return (long int)dumy; 
}

// devuelve la parte decimal de un numero decimal
static long int PD(double dumy, unsigned int numDecimales) {
    long int parteEntera, parteDecimal, signo;
    unsigned int factor=1;
    for(int i=0; i< numDecimales; i++) factor*=10;
    signo = ((dumy < 0) ? -1 : 1);
    parteEntera = (long int)dumy; // se separa parte entera 
    dumy = (dumy - (double)parteEntera) * factor; // se sacan 2 digitos de decimales
    parteDecimal = (long int)dumy * signo;
    return parteDecimal;
}

int ParShapeImprover::count_invalid_elements(Mesh &mesh, MeshDomain *domain)
{
  MsqError err;
  InstructionQueue q;
  int num_invalid, iDumy2;

/* probamos cambio de metrica de alidad cuando contamos elementos enredados
  hSigmaMetric tm(1.2e-3, 1.0e-10);
  IdealShapeTarget target;
  ThSigmaMetric metric( &target, &tm );
*/

/* Para medir la calidad inicial de la malla
*/
  //IdealWeightInverseMeanRatio metric;
  //IdealWeightMeanRatio metric;
  miIdealWeightMeanRatio metric;
  metric.set_averaging_method( QualityMetric::LINEAR );
  // Check for inverted elements in the mesh
  QualityAssessor inv_check( &metric );
  inv_check.disable_printing_results();
  q.add_quality_assessor( &inv_check, err );  MSQ_ERRZERO(err);
  Settings settings;
  // bug?  should we pass in pmesh?
  Mesh* mesh_ptr = &mesh; 
  MeshDomainAssoc mesh_and_domain = MeshDomainAssoc(mesh_ptr, domain);
  q.run_common( &mesh_and_domain, 0, &settings, err ); MSQ_ERRZERO(err);
  //const QualityAssessor::Assessor* inv_b = inv_check.get_results( &metric );
  //int num_invalid = inv_b->get_invalid_element_count();
  inv_check.get_inverted_element_count(num_invalid, iDumy2, err); // MIO y quite las 2 anteriores

  return num_invalid;
}

float* ParShapeImprover::checkQuality(Mesh &mesh, MeshDomain *domain)
// devuelve las calidades promedio, maxima y minima de una malla
{
  float *quality;
  MsqError err;
  InstructionQueue q;

/* probamos cambio de metrica de alidad cuando contamos elementos enredados
  hSigmaMetric tm(1.2e-3, 1.0e-10);
  IdealShapeTarget target;
  ThSigmaMetric metric( &target, &tm );
*/

/* Metrica original MeanRatio
*/
  //IdealWeightMeanRatio metric;
  /* BUENO */  miIdealWeightMeanRatio metric;
  /* BUENO */  //metric.set_averaging_method( QualityMetric::LINEAR );

  //ConditionNumberQualityMetric metric;

  QualityAssessor inv_check( &metric );
  inv_check.disable_printing_results();
  q.add_quality_assessor( &inv_check, err );  MSQ_ERRZERO(err);
  Settings settings;
  Mesh* mesh_ptr = &mesh; 
  MeshDomainAssoc mesh_and_domain = MeshDomainAssoc(mesh_ptr, domain);
  q.run_common( &mesh_and_domain, 0, &settings, err ); MSQ_ERRZERO(err);

  quality = (float *)malloc(5*sizeof(float));
  quality[0] = inv_check.get_results(&metric)->get_average() ; // Quality promedio
  quality[1] = inv_check.get_results(&metric)->get_maximum() ; // Quality maxima
  quality[2] = inv_check.get_results(&metric)->get_minimum() ; // Quality minima
  quality[3] = inv_check.get_results(&metric)->get_stddev() ; // Quality desviacion estandar
  quality[4] = inv_check.get_results(&metric)->get_rms() ; // Quality rms

  //printf("Qmalla= %f\n",quality[0]);
  return quality;
}

//void ParShapeImprover::run(Mesh &mesh, MeshDomain *domain, MsqError& err, bool always_smooth, int debug, const char* vtk_name)
void ParShapeImprover::run(Mesh &mesh, MeshDomain *domain, MsqError& err, bool always_smooth, int debug)
{
  int rank, nprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  if (debug)
    {
      MsqDebug::enable(1);
      if (debug > 1) MsqDebug::enable(2);
      if (debug > 2) MsqDebug::enable(3);
    }

  ParallelMesh *pmesh = dynamic_cast<ParallelMesh *>(&mesh);
  //std::cout << "P[" << rank << "] main.cpp-ParShapeImprover::run, pMESH= " << pmesh << std::endl;

  MsqError mErr;
  int num_invalid = 0;
  bool check_quality=true;
  //bool check_quality=false; // desactivamos el analisis previo de la calidad de la malla "para ahorrar"
  if (check_quality)
    {
      num_invalid = count_invalid_elements(mesh, domain);
      std::cout << "\nP[" << rank << "] " << "main.cpp - ParShapeImprover::run, num_invalid before= " << num_invalid 
	<< (num_invalid ? " WARNING: invalid elements exist before Mesquite smoothing" : 
	    (!always_smooth ? "WARNING: no smoothing requested since always_smooth=false" : " "))
	<< std::endl;
      std::cout << std::flush;
    }

  if (num_invalid || always_smooth)
    {
      bool use_canned_wrapper = false;
      if (use_canned_wrapper)
	{
	  ShapeImprovementWrapper siw(mErr);
	  if (pmesh)
	    siw.run_instructions(pmesh, domain, mErr);
	  else
          {
            MeshDomainAssoc mesh_and_domain = MeshDomainAssoc(&mesh, domain);
	    siw.run_instructions(&mesh_and_domain, mErr);
          }
	}
      else
	{
	  int  msq_debug             = debug; // 1,2,3 for more debug info
	  bool always_smooth_local   = false;
	  bool do_untangle_only      = false;
	  //ParShapeImprover::ParShapeImprovementWrapper siw(innerIter,0.0,gradNorm,10000,vtk_name);
	  ParShapeImprover::ParShapeImprovementWrapper siw(innerIter,0.0,gradNorm,10000);
	  siw.m_do_untangle_only = do_untangle_only;
	  if (pmesh){
	    std::cout << "P[" << rank << "] main.cpp - ParShapeImprover, empieza la optimizacion de la malla en 2 fasesuntangling + smoothing" << std::endl;
	    //siw.run_instructions(pmesh, 0, mErr);
	    siw.run_instructions(pmesh, domain, mErr);
	  }
	  else
          {
            MeshDomainAssoc mesh_and_domain = MeshDomainAssoc(&mesh, domain);
            siw.run_instructions(&mesh_and_domain, mErr);
          }

	}

	std::cout << "\nP[" << rank << "] " << " main.cpp-ParShapeImprover::run, MsqError after ShapeImprovementWrapper: " << mErr << std::endl;

	if (check_quality)
	{
	  num_invalid = count_invalid_elements(mesh, domain);
	  std::cout << "\nP[" << rank << "] " << " main.cpp-ParShapeImprover::run, num_invalid after= " << num_invalid << " " 
	    << (num_invalid ? " ERROR still have invalid elements after Mesquite smoothing" : 
		" SUCCESS: smoothed and removed invalid elements ")
	    << std::endl;
	}
	MSQ_ERRRTN(mErr);
  }
}

//static int test(std::string filename_prefix, std::string mesh_topology_name, MeshDomain *domain=0)
//static int test(std::string filename_prefix, std::string filename, MeshDomain *domain=0)
static int test(std::string filename_prefix, std::string filename, MeshDomain *domain)
{
  int rank, nprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  //if (nprocs > 2) { cerr << "parallel_untangle_shape::test(" << mesh_topology_name << " can only be run with 1 or 2 processors" << std::endl; return 0; }
  if (nprocs > 520) { cerr << "main::test(" << filename << 
	" can only be run with 1 or 2 processors" << std::endl; return 0; }

  /* create processor-specific file names */
  ostringstream in_name, out_name, gold_name;

  in_name   << filename_prefix << filename << "." << nprocs << "." << rank << ".vtk";
  cout << "P[" << rank << "] Ficheros de mallas - in_name=   " << in_name.str() << std::endl;

  gold_name << filename_prefix << filename << "." << nprocs << "." << rank << ".vtk";
  cout << "P[" << rank << "] Ficheros de mallas - gold_name= " << gold_name.str() << std::endl;

  out_name  <<                    filename << "." << nprocs << "." << rank << ".out.vtk";
  cout << "P[" << rank << "] Ficheros de mallas - out_name=  " << out_name.str() << std::endl << std::endl;
  std::cout << std::flush;

  /* load different mesh files on each processor */
  MsqError err;
  MeshImpl mesh;
  Timer readVtkTimer;
  mesh.read_vtk(in_name.str().c_str(), err);
  if (err) {cerr << err << endl; return 1;}


  /* create parallel mesh instance, specifying tags 
   * containing parallel data */
  ParallelMeshImpl parallel_mesh(&mesh, "GLOBAL_ID", "PROCESSOR_ID");
  ParallelHelperImpl helper;
  helper.set_communicator(MPI_COMM_WORLD);
  helper.set_parallel_mesh(&parallel_mesh);
  parallel_mesh.set_parallel_helper(&helper);

  std::vector< Mesh::ElementHandle > elements;
  std::vector< Mesh::VertexHandle > vertex, vertex2;
  std::vector<size_t> offsets;
  std::vector<bool> fixed;
  int i, fijos;

  mesh.get_all_elements (elements, err) ;
  mesh.get_all_vertices (vertex, err) ;
  int num_vtx = vertex.size();
  int num_elem = elements.size();
  mesh.vertices_get_fixed_flag(&vertex[0], fixed, num_vtx,err);

  parallel_mesh.get_all_elements (elements, err) ;
  parallel_mesh.get_all_vertices (vertex, err) ;
  num_vtx = vertex.size();
  num_elem = elements.size();

  //MsqVertex coord[num_vtx];
  //MsqVertex *coord;
  //parallel_mesh.vertices_get_coordinates (&vertex[0], coord, num_vtx, err) ;  MSQ_CHKERR(err);
  //EntityTopology element_topologies[num_elem];
  //parallel_mesh.elements_get_topologies(&elements[0],element_topologies,num_elem, err);

// calcular numero vertices fijos
  parallel_mesh.vertices_get_fixed_flag(&vertex[0], fixed, num_vtx,err);
  fijos=0;
  //for(i = 0;i < num_vtx;i++){
  //	if(fixed[i]) fijos++;
  //}

  // para mirar el numero medio de elementos por vtx libre 
  std::vector<size_t> offsets2;
  std::vector< Mesh::ElementHandle> elements2;
  float valencia = 0, inicio2, fin2, dumy, desviacion;
  parallel_mesh.vertices_get_attached_elements(&vertex[0],num_vtx,elements2,offsets2,err); 
  for(i = 0;i < num_vtx;i++){
	if(fixed[i]) fijos++;
	else {
	      // inicio y final del rango de elementos attached a vtx i
	      inicio2 = (float)offsets2[i], fin2 = (float)offsets2[i+1];
	      valencia += (fin2 - inicio2);
	}
  }
  valencia /= (num_vtx-fijos);

  // para calcular la desviacion estandar de la valencia
  for(i = 0;i < num_vtx;i++){
	if(!fixed[i]) {
	      // inicio y final del rango de elementos attached a vtx i
	      inicio2 = (float)offsets2[i], fin2 = (float)offsets2[i+1];
	      dumy = valencia - (fin2 - inicio2);
	      dumy *= dumy;
	      desviacion += dumy;
	}
  }
  dumy = valencia / (num_vtx-fijos-1);
  desviacion = sqrt(dumy);


  //printf("Vertices: %lu, elementos: %i, Vertices fijos: %lu, tiempoReadVTK: %4.1f\n", (size_t)num_vtx, (int)num_elem, (size_t)fijos, tiempoReadVTK);
  //printf("Vertices fijos: %lu\n", (size_t)fijos);
  //printf("Valencia: %f\n", valencia);


// calcular numero elementos libres (con al menos un nodo libre), resto: elementos fijos
// vertex es un array donde aparecen los vertices de la malla, ordenados por vertices
// vertex2 es un array donde aparecen los vertices de los elementos de la malla, ordenados por elemento
  parallel_mesh.elements_get_attached_vertices(&elements[0],num_elem,vertex2,offsets,err);
  int fijos2=0;
//  omp_set_num_threads(2); // se activan 4 cores por particion
//#pragma omp parallel for shared(fijos2, fixed, vertex, vertex2, num_vtx, num_elem)
  for(i = 0;i < num_elem;i++){
	continue; // desactivado este bucle por ahora
	if(i%1000 == 0) printf("Analizando elementos fijos, elementos explorados: %i/%i (%5.2f%%)\r", i+1,num_elem,100.0*(float(i)+1.0)/float(num_elem));
	else if(i == num_elem-1) printf("Analizando elementos fijos, elementos explorados: %i/%i (%5.2f%%)\n", i+1,num_elem,100.0*(float(i)+1.0)/float(num_elem));

	// punteros inicial y final donde se encuentran en vertex2 los handles de los vertices del elemento i
	int inicio = (int)offsets[i], fin= (int)offsets[i+1];
	int cuentaFijos=0;
	//printf("HandleElemento: %lu, offsets: %i, HandleVtx: ", (size_t)elements[i], (int)offsets[i]);
	for(int j=inicio; j<fin; j++) {
	   bool dumy;
	   for(int k=0; k<num_vtx; k++){
	      // vertex es el array de vertices de la malla, sin repetir vertices
	      // vertex2 tiene vertices repetidos porque varios elementos comparten vertices
	      if(vertex2[j] == vertex[k]) {
		dumy = fixed[k]; 
		cuentaFijos+=(int)dumy; 
		break;
	      }
	   }
	   //printf("%lu (%i) ", (size_t)vertex2[j],dumy);
	}
	if(cuentaFijos == 4) fijos2++;
	//if(cuentaFijos == 4) {printf(" elemento fijo \n"); fijos2++;}
	//else printf("\n");
  }
  //printf("Elementos fijos: %i, Elementos free: %i\n", fijos2, num_elem-fijos2);

  float tiempoReadVTK = readVtkTimer.since_birth();
  
  cout << "P[" << rank << "] main.cpp-test, leida particion pMESH: \n\tNelementos= " << 
        num_elem << ", \n\tNvertices= " << num_vtx << ", \n\tNfixed= " << fijos << 
	", \n\tElementos fijos: " << fijos2 << ", \n\tElementos free: " << 
	num_elem-fijos2 << ", \n\ttiempoReadVTK (s): " << tiempoReadVTK <<
	", \n\telementos por vtx libre (valencia): " << valencia << " +/- " << desviacion << " (" << 100*desviacion/valencia << "%)" <<
	std::endl << std::endl ;

// en los ficheros de particiones paralelas se analizan las CPU asignadas a cada vertice
// luego se obtiene informacion de las fronteras entre particiones paralelas
  TagHandle tag; 
  //int tag_data_PROC[num_vtx];
  //unsigned long tag_data_ID[num_vtx];
  //int cpu[Ncpu];

  // tag > 0 si la malla tiene data que contiene la cpu de cada vertice
  //tag = parallel_mesh.tag_get( "PROCESSOR_ID", err );

  if( nprocs == 1  ) { 
  //if( (size_t)tag == 0  ) { 
        // esta opcion es cuando solo se activa 1 core, y no se analiza frontera porque no existe
	printf("P[%i] main.cpp - Este fichero es para 1 procesador "
		"y no tiene el campo PROCESSOR_ID\n", rank);
  }
  else { 
        // ficheros de varias particiones paralelas, se analizan las fronteras
  	tag = parallel_mesh.tag_get( "PROCESSOR_ID", err );
	printf("P[%i] main.cpp - Este fichero tiene Tag-Processor_ID: %lu, "
		"por tanto, es una particion para computo paralelo", rank, (size_t)tag);
	cout << ", err= " << err << endl;
/*
#ifdef FRONTERA
	int* resultadosFrontera;
	resultadosFrontera = analizaFrontera(parallel_mesh); 
#else
	printf("P[%i] main.cpp - No se analiza frontera porque esta desactivado\n\n", rank);
#endif
*/

  } // else

  /* do Laplacian smooth */
  //LaplaceWrapper optimizer;
  //optimizer.run_instructions(&parallel_mesh, err);

  int    msq_debug      = 0; // 1,2,3 for more debug info
  bool   always_smooth  = true;
  int    innerIter 	= 100;
  double gradNorm 	= 1.e-9;

  ParShapeImprover si(innerIter, gradNorm);
  //si.run(parallel_mesh, domain, err, always_smooth, msq_debug, in_name.str().c_str());
  si.run(parallel_mesh, domain, err, always_smooth, msq_debug);
  if (err) {cout << err << endl; return 1; }
  //if (err) {cerr << err << endl; return 1; }

  /* write mesh */
  tiempoReadVTK = readVtkTimer.since_birth();
  mesh.write_vtk(out_name.str().c_str(),err);
  if (err) {cerr << err << endl; return 1;}
  tiempoReadVTK = readVtkTimer.since_birth() - tiempoReadVTK;
  cout << "P[" << rank << "] main.cpp-test, escrita malla resultado, tiempoWriteVTK (s): " << 
    tiempoReadVTK << std::endl << std::endl ;

  //std::cout << "P[ " << rank <<"] reading gold..." << std::endl;

  /* compare mesh with gold copy */
  MeshImpl gold;
  gold.read_vtk(gold_name.str().c_str(),err);
  if (err) {cerr << err << endl; return 1;}

  // Estadisticas de los lados de los elementos
  Settings settings;
  MeshUtil MU(&mesh, &settings);
  SimpleStats lados, lados2, lambda, lambda2;
  MeshUtil MU2(&gold, &settings);
  MU2.edge_length_distribution(lados,err);
  printf("P[%i] Estadistica de lados malla input - media: %5.2e, STD: %5.2e, max: %5.2e, min: %5.2e\n", rank, lados.average(), lados.standard_deviation(), lados.maximum(), lados.minimum());
  MU.edge_length_distribution(lados2,err);
  printf("P[%i] Estadistica de lados malla final - media: %5.2e, STD: %5.2e, max: %5.2e, min: %5.2e\n", rank, lados2.average(), lados2.standard_deviation(), lados2.maximum(), lados2.minimum());
  MU2.lambda_distribution(lambda,err);
  printf("P[%i] Estadistica de lambda malla input - media: %5.2e, STD: %5.2e, max: %5.2e, min: %5.2e\n", rank, lambda.average(), lambda.standard_deviation(), lambda.maximum(), lambda.minimum());
  MU.lambda_distribution(lambda2,err);
  printf("P[%i] Estadistica de lambda malla final - media: %5.2e, STD: %5.2e, max: %5.2e, min: %5.2e\n", rank, lambda2.average(), lambda2.standard_deviation(), lambda2.maximum(), lambda2.minimum());

// Histograma + calidades de elementos

  float *quality;
  InstructionQueue q;

/* Metrica original MeanRatio */
  /* BUENO */  miIdealWeightMeanRatio metric;
  //ConditionNumberQualityMetric metric;

  QualityAssessor inv_check( &metric );
  const QualityAssessor::Assessor *inv_check_ass;
  Mesh* mesh_ptr = &mesh; 
  MeshDomainAssoc mesh_and_domain = MeshDomainAssoc(mesh_ptr, domain);
  //inv_check.disable_printing_results();
  inv_check.add_histogram_assessment(&metric, 0.0, 1.0, 20, 0, NULL, NULL );
  //inv_check.loop_over_mesh(&mesh_and_domain,&settings, err );
  q.add_quality_assessor( &inv_check, err );  MSQ_ERRZERO(err);
  q.run_common( &mesh_and_domain, 0, &settings, err ); MSQ_ERRZERO(err);

  double lb, ub;
  std::vector< int > counts_out;
  inv_check_ass = inv_check.get_results(&metric);
  inv_check_ass->get_histogram(lb, ub, counts_out, err);
  printf("Histograma - lower_bound= %5.2f, upper_bound= %5.2f, Nmuestras= %lu\n", lb, ub, counts_out.size());
  char sdumy[30];
  sprintf(sdumy,"histograma_%5s_P%i.log",nombreMalla,rank);
  ofstream file(sdumy);
  for (size_t i = 1; i < counts_out.size()-1; i++) {
  	file << "H[" << i << "]= " << counts_out[i] << endl;
  }
  file.close();

  PatchData global_patch2;
  global_patch2.set_mesh( &mesh );
  global_patch2.attach_settings( &settings );
  global_patch2.set_domain( domain );
  global_patch2.fill_global_patch( err );
  num_elem = global_patch2.num_elements();
  int num_elem_free = 0;
  double Q[num_elem];
  std::vector< std::size_t > vertex_list;
  for (size_t i = 0; i < num_elem; i++) {
	MsqMeshEntity elem = global_patch2.element_by_index(i);
	elem.get_vertex_indices(vertex_list);
	int not_free = 0;
	for (size_t j = 0; j< elem.vertex_count(); j++) {
	  if( global_patch2.is_vertex_not_free( vertex_list[j] ))  not_free++;
	}
	if (not_free != 4)  { // solo se obtiene medida calidad de elementos free
	  metric.evaluate(global_patch2,i,Q[num_elem_free],err);
	  num_elem_free++;
	}
	//else Q[i] = 0;
  	//printf("Q[%lu]= %f\n",i,Q[i]);
  }
  sort(Q,Q+num_elem_free); // se ordenan calidades de menor a mayor, funcion de C++
  sprintf(sdumy,"calidades_%5s_P%i.log",nombreMalla,rank);
  ofstream file2(sdumy);
  if (!file2)
  {
    MSQ_SETERR(err)( MsqError::FILE_ACCESS );
    return 1;
  }
  // se guarda en fichero las calidades de los elementos free
  for (size_t i = 0; i < num_elem_free; i++) {
  	//printf("Q[%lu]= %f\n",i,Q[i]);
  	//file2 << "Q[" << i << "]= " << Q[i] << endl;
	long int iDumy = PD(Q[i],4);	
 	file2 << "Q[" << i << "]= " << PE(Q[i]) << "," << (iDumy<1000 ? "0":"") << iDumy << endl;
  }
  file2.close();

// Fin: Histograma + calidades de elementos

  print_timing_diagnostics(cout);

  //std::cout << "P[ " << rank <<"] read gold, checking mesh diff..." << std::endl;
  bool   do_print = true;
  double tol 	  = 1.e-4;
  bool   diff	  = MeshUtil::meshes_are_different(mesh, gold, err, tol, do_print);
  if (err) {cerr << err << endl; return 1;}
  std::cout << "P[ " << rank <<"] read gold, checking mesh diff...done" << std::endl;
  if (diff) {cerr << "Error, computed mesh different from gold copy" << std::endl; return 1;}
  

  return 0;
}

void split_path_file(char** p, char** f, char *pf) {
    char *slash = pf, *next;
    while ((next = strpbrk(slash + 1, "\\/"))) slash = next;
    if (pf != slash) slash++;
    *p = strndup(pf, slash - pf);
    *f = strdup(slash);
}

#include <unistd.h>

int main( int argc, char* argv[] )
{
  time_t curtime = time(NULL);
  char *path=NULL, *file=NULL;
  size_t size;
  int nprocs;
  int rank;

  if (argc != 3) {
    printf("error de comando: ");
    for(int i=0; i<argc; i++) printf("%s ", argv[i]);
    printf(", tiene que ser: ./main <directorioVTK>/<ficheroVTK> <2D/3D>\n");
    return 2;
  }

  std::cout << "--------------\nPrograma MPI SUS-Mesquite\nFecha: " << ctime(&curtime) << "--------------\n";
  std::cout << "--------------\nPrograma MPI SUS-Mesquite\nFase INIT\n--------------\n";
  std::cout << "Directorio de ejecucion: " <<  getcwd(path,size) << std::endl;
  //std::cout << "DBL_EPSILON: " <<  DBL_EPSILON << endl;

  // separa la ruta de la raiz del fichero introducidos conjuntamente en la linea de comandos
  split_path_file(&path, &file, argv[1]); 

  std::cout << "Directorio de fichero malla VTK: " << path << std::endl;
  std::cout << "Raiz de ficheros malla VTK: " << file << std::endl;
  std::cout << std::flush << std::endl;

  // se actualiza variable global para nombre ficheros
  strncpy(nombreMalla, file, 5); 

  /* init MPI */
  if (MPI_SUCCESS != MPI_Init(&argc, &argv)) {
    cerr << "MPI_Init failed." << endl;
    return 2;
  } else {
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    std::cout << "P[" << rank << "] MPI activado con Procesadores= " << nprocs << std::endl;
  }

  // se obtiene la cpu asignada al proceso
  int cpuActual = sched_getcpu();
 
  /*
  cpu_set_t cpuset;
  CPU_ZERO(&cpuset);
  CPU_SET(cpuActual, &cpuset);
  pthread_t current_thread = pthread_self();
  // se asigna una cpu al proceso/thread
  pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
  */

  std::cout << "P[" << rank << "] Asignada cpuActual= " << cpuActual << std::endl;

  std::cout << "--------------\nPrograma MPI SUS-Mesquite\nFase SUS\n--------------\n";
  int t1;
  if (strcmp ("3D", argv[2]) == 0)
	t1 = test(path /*directorioVTK*/, file /*ficheroVTK*/, 0 );
  else if (strcmp ("2D", argv[2]) == 0){
  	Vector3D pnt(0,0,0); // para las mallas miCuadrado y JMcuadrado
  	//Vector3D pnt(5,5,5);
  	//Vector3D s_norm(0,0,+1);
  	Vector3D s_norm(0,0,+0.1);
  	PlanarDomain planar_domain(s_norm, pnt);
	t1 = test(path /*directorioVTK*/, file /*ficheroVTK*/, &planar_domain );
	//t1 = test(path /*directorioVTK*/, file /*ficheroVTK*/, 0 );
  }
  else
    	printf("ERROR linea de comando, tiene que ser: ./main <directorioVTK>/<ficheroVTK> <2D/3D>\n");

  //int t1 = test(path /*directorioVTK*/, file /*ficheroVTK*/ );

  if (t1) return t1;

  MPI_Finalize();
  return 0;
}

// escribe un fichero de mallas en formato VTK
void mi_write_vtk(MeshImplData * myMesh, int * tag_data_PROC, unsigned long * tag_data_ID, const char* out_filename, MsqError &err){
  int i;

  ofstream file(out_filename);
  if (!file)
  {
    MSQ_SETERR(err)( MsqError::FILE_ACCESS );
    return;
  }

  file.precision( 10 );

    // Write a header
  file << "# vtk DataFile Version 3.0\n";
  file << "Mesquite Mesh\n";
  file << "ASCII\n";
  file << "DATASET UNSTRUCTURED_GRID\n";

  file << "POINTS " << myMesh->num_vertices() << " double\n";
  //std::cout << "POINTS " << myMesh->num_vertices() << " double\n";

  std::vector<size_t> vertex_indices( myMesh->max_vertex_index() );
  size_t count = 0;
  for (i = 0; i < myMesh->max_vertex_index(); ++i)
  {
    if (myMesh->is_vertex_valid(i))
    {
      Vector3D coords = myMesh->get_vertex_coords( i, err ); 
      file << coords[0] << ' ' << coords[1] << ' ' << coords[2] << '\n';
      //std::cout << out_filename << ": " << coords[0] << ' ' << coords[1] << ' ' << coords[2] << '\n';
      vertex_indices[i] = count++;
    }
    else
    {
      vertex_indices[i] = myMesh->max_vertex_index();
    }
  }
    // Write out the connectivity table
  size_t elem_idx;
  size_t connectivity_size = myMesh->num_elements() + myMesh->num_vertex_uses();
  file << "CELLS " << myMesh->num_elements() << ' ' << connectivity_size << '\n';
  //std::cout << "CELLS " << myMesh->num_elements() << ' ' << connectivity_size << '\n';
  for (elem_idx = 0; elem_idx < myMesh->max_element_index(); ++elem_idx)
  {
    if (!myMesh->is_element_valid(elem_idx)){
      printf("CELLS-Elem %lu NO es valido\n", elem_idx);
      continue;
    }
    //else  printf("CELLS-Elem %lu SI es valido\n", elem_idx);

    std::vector<size_t> conn = myMesh->element_connectivity( elem_idx, err ); MSQ_ERRRTN(err);
    EntityTopology topo = myMesh->element_topology( elem_idx, err ); MSQ_ERRRTN(err);

      // If necessary, convert from Exodus to VTK node-ordering.
    const VtkTypeInfo* info = VtkTypeInfo::find_type( topo, conn.size(), err ); MSQ_ERRRTN(err);
    if (info->msqType != POLYGON)
      info->mesquiteToVtkOrder( conn );

    file << conn.size();
    //std::cout << conn.size();
    for (i = 0; i < conn.size(); ++i)
      file << ' ' << vertex_indices[(size_t)conn[i]];
      //std::cout << ' ' << vertex_indices[(size_t)conn[i]];
    file << '\n';
    //std::cout << '\n';
  }

    // Write out the element types
  file << "CELL_TYPES " << myMesh->num_elements() << '\n';
  //std::cout << "CELL_TYPES " << myMesh->num_elements() << '\n';
  for (elem_idx = 0; elem_idx < myMesh->max_element_index(); ++elem_idx)
  {
    if (!myMesh->is_element_valid(elem_idx)){
      printf("Elem %lu NO es valido\n", elem_idx);
      continue;
    }

    EntityTopology topo = myMesh->element_topology( elem_idx, err ); MSQ_ERRRTN(err);
    count = myMesh->element_connectivity( elem_idx, err ).size(); MSQ_ERRRTN(err);
    const VtkTypeInfo* info = VtkTypeInfo::find_type( topo, count, err ); MSQ_ERRRTN(err);
    file << info->vtkType << '\n';
    //std::cout << info->vtkType << '\n';
  }

    // Write out which points are fixed.
  file << "POINT_DATA " << myMesh->num_vertices()
  //std::cout << "POINT_DATA " << myMesh->num_vertices()
       << "\nSCALARS fixed int\nLOOKUP_TABLE default\n";
  for (i = 0; i < myMesh->max_vertex_index(); ++i)
    if (myMesh->is_vertex_valid( i ))
      file <<( myMesh->vertex_is_fixed( i, err ) ? "1" : "0") << "\n";
      //std::cout <<( myMesh->vertex_is_fixed( i, err ) ? "1" : "0") << "\n";

  if (myMesh->have_slaved_flags()) {
    file << "SCALARS slaved int\nLOOKUP_TABLE default\n";
    //std::cout << "SCALARS slaved int\nLOOKUP_TABLE default\n";
    for (i = 0; i < myMesh->max_vertex_index(); ++i)
      if (myMesh->is_vertex_valid( i ))
        file <<( myMesh->vertex_is_slaved( i, err ) ? "1" : "0") << "\n";
        //std::cout <<( myMesh->vertex_is_slaved( i, err ) ? "1" : "0") << "\n";
  }

    // core asignado a cada vertice
  file << "SCALARS GLOBAL_ID unsigned_long 1" << "\nLOOKUP_TABLE default\n";
    for (i = 0; i < myMesh->max_vertex_index(); ++i)
      if (myMesh->is_vertex_valid( i ))
        file << tag_data_ID[i] << "\n";

    // core asignado a cada vertice
  file << "SCALARS PROCESSOR_ID int 1" << "\nLOOKUP_TABLE default\n";
    for (i = 0; i < myMesh->max_vertex_index(); ++i)
      if (myMesh->is_vertex_valid( i ))
        file << tag_data_PROC[i] << "\n";

  file.close();
}

void rellenaMesh(MeshImplData * myMesh, 
		size_t num_vtx,
		size_t num_elem,
  		bool * fixed_array,
  		double * coord_array,
		EntityTopology * topo,
		int * vertex2_array){
  MsqError err;

  myMesh->allocate_vertices( num_vtx, err ); 
  myMesh->allocate_elements( num_elem, err ); 

  for (size_t i = 0; i < num_vtx; ++i) {
	myMesh->reset_vertex(i, Vector3D(coord_array + 3*i), fixed_array[i], err);
  }

  int num_indices = 0;
  std::vector<long> connect;

  // Count the number of indices
  const int* conn_iter = vertex2_array;
  for (size_t i = 0; i < num_elem; ++i) {
    num_indices = TopologyInfo::corners( topo[i] );
    connect.resize( num_indices );
    std::copy( conn_iter, conn_iter + num_indices, connect.begin() );
    myMesh->reset_element((size_t)i, connect, topo[i], err );
    conn_iter += num_indices;
    //printf("P[%i] elem: %i, num_indices: %i, vtx: ",rank,i,num_indices);
    //for(int j=0; j<num_indices; j++) printf("%lu ",connect[j]);
    //printf(", err: %i\n",(int)err);
  }


}

// se guarda la malla de frontera entre particiones
void guardaFrontera(size_t num_vtx_fron, size_t num_elem_fron, bool * vtx_fixed_fron, double * coord_array_fron, EntityTopology * elem_topo_fron, int * vtx2_handle_fron, int* tag_data_PROC_fron, unsigned long * tag_data_ID_fron){ 

  MsqError err;
  int rank,nprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

  // se crea una malla, se rellenan sus vertices, elementos y tags, y luego sse guarda en fichero vtk
  MeshImplData *  myMesh= new MeshImplData();

  //rellenaMesh(myMesh,num_vtx,num_elem,fixed_array,coord_array,element_topologies,vertex2_array); // malla de particion completa
  rellenaMesh(myMesh,num_vtx_fron,num_elem_fron,vtx_fixed_fron,coord_array_fron,elem_topo_fron,vtx2_handle_fron); // malla de frontera entre particiones

  printf("P[%i] Frontera vertices reservados, max_vertex_index: %lu, err: %s\n",
        rank,myMesh->max_vertex_index(), err.error_message());
  printf("P[%i] Frontera elementos reservados, max_elements_index: %lu, err: %s\n",
        rank,myMesh->max_element_index(), err.error_message());

  char nombreFile[50];
  sprintf(nombreFile,"%s_%icores_frontera_P%i.vtk",nombreMalla,nprocs,rank);
  //sprintf(nombreFile,"malla_frontera_P%i.vtk",rank);
  //mesh_frontera.write_vtk(nombreFile, err);

  //mi_write_vtk(myMesh,tag_data_PROC,tag_data_ID,nombreFile,err); // malla de la particion completa
  mi_write_vtk(myMesh,tag_data_PROC_fron,tag_data_ID_fron,nombreFile,err); // malla de frontera

}

int* analizaFrontera(ParallelMeshImpl parallel_mesh) 
//void analizaFrontera(ParallelMeshImpl parallel_mesh) 
// extrae la frontera de una particion paralela con las vecinas
{
  printf("analizaFrontera - inicio\n");

  MsqError err;
  int rank, i, Ncpu;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &Ncpu);
  TagHandle tag; 
  int cpu[Ncpu];
  bool cpu_total[Ncpu]; // todas las cpus/particiones que toca una frontera
  static int resultados[5];
  //int *resultados;

  std::vector< Mesh::VertexHandle > vertex; 
  std::vector< Mesh::ElementHandle> elements;
  std::vector< Mesh::ElementHandle> elements2;
  std::vector< Mesh::VertexHandle > vertex2;
  std::vector<size_t> offsets;
  std::vector<size_t> offsets2;
  std::vector<bool>   fixed;

  //resultados = (int *)malloc(5*sizeof(int)); // donde se devuelven resultados de esta rutina

// para ver cuantos hilos OMP estan activos
/*
  int th_id, nthreads;
  omp_set_num_threads(Nhilos); // se activan 4 cores por particion
  #pragma omp parallel private(th_id)
  {
    th_id = omp_get_thread_num();
    printf("P[%i] Hello from thread %d\n", rank,th_id);
    #pragma omp barrier
    if ( th_id == 0 ) {
      nthreads = omp_get_num_threads();
      printf("P[%i] There are %d threads\n",rank,nthreads);
    }
  }
*/
  parallel_mesh.get_all_vertices (vertex, err) ;
  parallel_mesh.get_all_elements (elements, err) ;

  int num_vtx = vertex.size();
  int num_elem = elements.size();

  //EntityTopology element_topologies[num_elem];
  EntityTopology* element_topologies;
  element_topologies= (EntityTopology *)malloc(num_elem*sizeof(EntityTopology)); 

  //MsqVertex coord[num_vtx];
  MsqVertex* coord;
  coord= (MsqVertex*)malloc(num_vtx*sizeof(MsqVertex)); 

  parallel_mesh.elements_get_attached_vertices(&elements[0],num_elem,vertex2,offsets,err);
  parallel_mesh.elements_get_topologies(&elements[0],element_topologies,num_elem, err);

  parallel_mesh.vertices_get_coordinates (&vertex[0], coord, num_vtx, err) ;
  parallel_mesh.vertices_get_fixed_flag(&vertex[0], fixed, num_vtx,err);

  int    elemInternos    = 0;
  int    elemIntFrontera = 0;
  int    elemGhost       = 0;
  size_t num_vtx_fron    = 0;
  size_t num_elem_fron   = 0;

  //int vtx_handle_fron[num_vtx];
  int* vtx_handle_fron;
  vtx_handle_fron= (int *)malloc(num_vtx*sizeof(int)); 

  //double coord_array_fron[3*num_vtx];
  double* coord_array_fron;
  coord_array_fron= (double *)malloc(3*num_vtx*sizeof(double)); 

  //bool vtx_fixed_fron[num_vtx];
  bool* vtx_fixed_fron;
  vtx_fixed_fron= (bool *)malloc(num_elem*sizeof(bool)); 

  //int elem_handle_fron[num_elem];
  int* elem_handle_fron;
  elem_handle_fron= (int *)malloc(num_elem*sizeof(int)); 

  //EntityTopology  elem_topo_fron[num_elem];
  EntityTopology*  elem_topo_fron;
  elem_topo_fron= (EntityTopology *)malloc(num_elem*sizeof(EntityTopology)); 

  //int vtx2_handle_fron[4*num_elem];
  int* vtx2_handle_fron;
  vtx2_handle_fron= (int *)malloc(4*num_elem*sizeof(int)); 

  //int tag_data_PROC_fron[num_vtx];
  int* tag_data_PROC_fron;
  tag_data_PROC_fron= (int *)malloc(num_vtx*sizeof(int)); 

  //unsigned long tag_data_ID_fron[num_vtx];
  unsigned long* tag_data_ID_fron;
  tag_data_ID_fron= (unsigned long*)malloc(num_vtx*sizeof(unsigned long)); 

  //int tag_data_PROC[num_vtx];
  int* tag_data_PROC;
  tag_data_PROC= (int *)malloc(num_vtx*sizeof(int)); 

  //unsigned long tag_data_ID[num_vtx];
  unsigned long* tag_data_ID;
  tag_data_ID= (unsigned long*)malloc(num_vtx*sizeof(unsigned long)); 

  int* vtxOriginalVTXfrontera;
  vtxOriginalVTXfrontera= (int *)malloc(num_vtx*sizeof(int)); 
  for(i = 0; i < num_vtx; i++) vtxOriginalVTXfrontera[i] = 0; // se incializa a 0


  // tag > 0 si la malla tiene data que contiene la cpu de cada vertice
  tag = parallel_mesh.tag_get( "PROCESSOR_ID", err );
  // tag_data_PROC contiene la cpu de cada vertice
  parallel_mesh.tag_get_vertex_data(tag,num_vtx,&vertex[0],tag_data_PROC ,err); 
  // tag_data_ID contiene el ID de cada vertice
  tag = parallel_mesh.tag_get( "GLOBAL_ID", err );
  parallel_mesh.tag_get_vertex_data(tag,num_vtx,&vertex[0],tag_data_ID ,err); 

  // get elements adjacent to vertices
  parallel_mesh.vertices_get_attached_elements(&vertex[0],num_vtx,elements2,offsets2,err); 

  size_t nPartVTXfronAcumula , nPartVTXfron; // num promedio de particiones por vertice frontera

  for(int j=0; j<Ncpu; j++) cpu_total[j]=0; // resetar el array cpu[]=0
  // en este bucle se cuentan los distintos tipos de elementos: frontera, particion, interiores
  for(i = 0; i < num_elem; i++){ // se visitan los distintos elementos de la particion de la malla

   //printf("P[%i] se visita oldElemento: %i\n",rank,i);
   //if( i%1 == 0 ) {
   if( i%1000000 == 0 ) {
	printf("P[%i] se visita oldElemento: %i/%i (%5.1f%%)\n",
     	  //rank,i,num_elem,100.0*(float)(i+1)/(float)num_elem); 
     	  rank,i+1,num_elem,100.0*(float)(i+1)/(float)num_elem); 
	fflush(stdout);
   } else if( i == num_elem-1 ) {
	printf("P[%i] se visita oldElemento: %i/%i (%5.1f%%)\n",
     	  rank,i+1,num_elem,100.0*(float)(i+1)/(float)num_elem); 
	fflush(stdout);
   }

   // punteros inicio y fin donde se encuentran en vertex2 
   // los handles de los vertices del elemento i; en un tetraedro: fin-inicio=4
   int inicio = (int)offsets[i], fin= (int)offsets[i+1];
   int dumy, dumy2, dumy3;
   for(int j=0; j<Ncpu; j++) cpu[j]=0; // resetar el array cpu[]=0

   // se registran las cpus de los vertices de cada elemento
   for(int j=inicio; j<fin; j++) { // se analizan las cpus de los 4 vertices del elemento i
      dumy = (size_t)vertex2[j]; // ID del vtx del elem j
      dumy2 = tag_data_PROC[dumy]; // cpu de vtx
      cpu[dumy2]++; // cuenta las distintas cpus de los vtx de un elemento
/*
      for(int k=0; k<num_vtx; k++){
	 if(vertex2[j] == vertex[k]) { // coincide el corner vertex2 con el handle del vertice vertex
	   dumy = tag_data_PROC[k]; // cpu de vertex[k]
	   cpu[dumy]++; // cuenta las distintas cpus de los vtx de un elemento
//printf("Elem: %i, vtx: %i, vertex2: %lu, vertex: %lu, tagID: %i, k: %i\n", i,j,(size_t)vertex2[j],(size_t)vertex[k],(int)tag_data_ID[k],k);
	   break;
	 }
      }
*/
   } // for j

   // dumy = procesador asignado a todos los vertices del elemento i si coinciden
   // dumy = -1 si no coinciden todas las cpus
   dumy = -1;
   nPartVTXfron= 0;
   for(int j=0; j<Ncpu; j++){
	if(cpu[j] != 0) {
	  cpu_total[j]=1;
	  nPartVTXfron++; // indica el número de particiones al que pertenece un elemento frontera
	} 
	if(cpu[j] == 4) {
	  dumy=j; 
	  break; 
	} 
   }
   if( dumy != -1 && dumy == rank ){ // todas cpus igual e igual a rank, i es un elemento interno
	//printf("P[%i] Elemento %i es interno, cpu: %i\n",rank,i,dumy);
	elemInternos++;
//
	// se identifican ahora los elementos internos que tocan con elementos frontera
	// se mira que alguno de sus nodos estan conectados a un elemento frontera
	dumy3=0; // indica que elemento i toca a un elemento frontera

	//printf("P[%i]\t elem es interno: %i,  se visita sus vertices\n", rank, elemInternos);

	for(int j=inicio; j<fin; j++) { // se visitan lo vertices del elemento i
	   // vertex2[j] es uno de los vertices del elemento i			
	   // se obtienen los elementos que estan conectados a vertex2[j]
	   // se recorren otra vez los elementos de la particion para encontrar los frontera

  	   //parallel_mesh.vertices_get_attached_elements(&vertex[0],num_vtx,elements,offsets2,err); 
	   int k = (size_t)vertex2[j];
	
	   //printf("P[%i]\t elem es interno: %i, suVertice: %i\n", rank, elemInternos, k);


	   //for(k = 0; k < num_elem; k++){ // se visitan los distintos elementos de la particion de la malla
	    if (dumy3!=1) { // no se sigue iterando en k si ya se ha encontrado un elemento de la forntera que comparte vertice j de elemento i


	      int inicio2 = (int)offsets2[k], fin2 = (int)offsets2[k+1];
	      //printf("P[%i]\t buscamos elementos del vtx que sean frontera entre %i .. %i\n", rank, inicio2, fin2);
	      //for(int m=0; m<Ncpu; m++) cpu[m]=0;
	      for(int m=inicio2; m<fin2; m++) { // se visitan los elementos del vtx k
		// se mira si es un elemento frontera
		// se obtiene la cpu de cada vertice2 del elemento k

	   	size_t kelem = (size_t)elements2[m];
	        //printf("P[%i]\t\t elem es interno: %i, vertice: %i, elementoAestudiar: %lu\n", rank, elemInternos, k, kelem);
	        int inicio3 = (int)offsets[kelem], fin3 = (int)offsets[kelem+1];
	        for(int p=0; p<Ncpu; p++) cpu[p]=0;
	        for(int p=inicio3; p<fin3; p++) {

			//dumy = (size_t)vertex2[m]; // ID del vtx del elem j
			dumy = (size_t)vertex2[p]; // ID del vtx del elem kelem
			//printf("P[%i]\t\t\t elem es interno: %i, vertice: %i, elementoAestudiar: %lu, suVTX: %i\n", rank, elemInternos, k, kelem, p);
			dumy2 = tag_data_PROC[dumy]; // cpu de vtx
			cpu[dumy2]++; // cuenta las distintas cpus de los vtx de un elemento

		}

	        // indica la cpu que son iguales en los 4 vertices, si queda dumy=-1 el elemento kelem es frontera
	        dumy2=-1; 
	        for(int p=0; p<Ncpu; p++){
		  if(cpu[p] == 4) dumy2=p; // dumy2 = procesador asignado a todos los nodos del elemento k
	        }


	        // dumy2 == -1: no todas cpus son iguales, por lo tanto kelem es elemento frontera
		// hemos encontrado la condicion para que elemento i sea elemIntFrontera
	        if(dumy2 == -1){ 
		     elemIntFrontera++;
		     dumy3 = 1;
		     //printf("P[%i]\t\t\t elem es interno: %i es elemIntFrontera: %i, suVertice: %i, suElem: %lu es frontera \n", rank, elemInternos, elemIntFrontera, k , kelem);
		     break; // no se sigue iterando en m porque hemos encontrado un elemento frontera pegado al elemento i
	        } // if dumy2

	      } // for m 

	    } // if dumy3

	   if (dumy3==1) break; // no se sigue iterando en j si ya se ha encontrado un vertice j del elemento i que comparte vertice con un elemento de la forntera 
	} // for j
//
   } // if
   // vertices Ghost: todas cpus iguales (dumy!=-1) pero distinta a rank, 
   // es decir, es un elemento interno de otra particion
   else if(dumy != -1 && dumy != rank ){ 
	//printf("P[%i] Elemento %i es ghost, cpu: %i\n",rank,i,dumy);
	elemGhost++;
   }
   // elementos frontera: no todas las cpus de los vertices son iguales
   else { 

	/*printf("P[%i] Elemento %i es frontera, con nodos de distintas cpus: ",rank,i);
	for(int j=0; j<Ncpu; j++){
	  if (cpu[j] !=0) printf("%i(%i) ",j,cpu[j]);
	}	
	printf("\n");*/

	// se van formando los array de vertices, elementos, fixed, cpu, ID de la malla forntera
/*
	printf("P[%i] Elemento %3i es frontera, vertices: ",rank,i);
	for(int j=inicio; j<fin; j++) { // se visitan lo vertices del elemento i
	  dumy = (size_t)vertex2[j]; // handle original del vertice
	  printf("%3lu(fix:%1i,cpu:%1i,ID:%3lu) ", (size_t)dumy, (int)fixed[dumy], tag_data_PROC[dumy], tag_data_ID[dumy]);
	}
	printf("\n");
	printf("P[%i] Creamos NuevoElemenFrontera %3lu\n",rank,num_elem_fron);
*/
	for(int j=0; j<4; j++){ // handle y coordenadas de los 4 vertices del tetra
	     dumy  = (size_t)vertex2[inicio+j]; // handle del vertice

	     //  vemos si el vtx esta registrado como vertice frontera usando el ID que no cambia
	     dumy2 = -1;

 	     if (vtxOriginalVTXfrontera[dumy] != 0) dumy2 = 1; // el vtx ya se registro como frontera 
/*
	     for(int k = 0; k < num_vtx_fron; k++){ 
		if(tag_data_ID_fron[k] == tag_data_ID[dumy] )  {
		   dumy2 = 1; // el vtx ya se registro como frontera 
		   break;
		} 
	     }
*/
	     if(dumy2 == -1){ // vtx no esta registrado como frontera y lo registramos
		vtx_handle_fron[num_vtx_fron] = num_vtx_fron; // nuevo handle del vtx frontera
		dumy = (size_t)vertex2[inicio+j]; // handle antiguo del vtx frontera
		vtx_fixed_fron[num_vtx_fron]=fixed[dumy];
		for (int k=0; k<3; k++) // coordenadas 3D del vtx frontera 
		   coord_array_fron[3*num_vtx_fron+k] = (double)coord[dumy][k];
		tag_data_ID_fron[num_vtx_fron] = tag_data_ID[dumy]; // tags del vtx frontera
		tag_data_PROC_fron[num_vtx_fron] = tag_data_PROC[dumy];

		//printf("P[%i] \tnuevoVTXfron: %3lu, newID: %3lu, antiguoVTX: %3i, antiguoID: %3lu, newFixed: %1i, antiguoFixed: %1i, newCpu: %1i, antiguoCpu: %1i\n",rank,num_vtx_fron,tag_data_ID_fron[num_vtx_fron],dumy,tag_data_ID[dumy],vtx_fixed_fron[num_vtx_fron],(int)fixed[dumy],tag_data_PROC_fron[num_vtx_fron],tag_data_PROC[dumy]);

		num_vtx_fron++; // 1 nuevos vertices

		vtxOriginalVTXfrontera[dumy]= num_vtx_fron; // registro del vtx_frontera al que pertenece un vtx
	     }

	     // registrar los corners del elemento frontera num_elem_fron 

 	     dumy2 = vtxOriginalVTXfrontera[dumy]; // orden en el array de la frontera si != 0
 	     if (dumy2 != 0) vtx2_handle_fron[4*num_elem_fron+j] = dumy2-1; // porque cuando se registro se anyadio 1 mas

/*
	     dumy2 = tag_data_ID[dumy]; // ID antiguo del vtx frontera
	     for(int k = 0; k < num_vtx_fron; k++){
	       if(dumy2 == tag_data_ID_fron[k]){
		 vtx2_handle_fron[4*num_elem_fron+j] = (size_t)vtx_handle_fron[k];
		 break;
	       }
	     }
*/
	}	
	elem_handle_fron[num_elem_fron]=num_elem_fron;
	elem_topo_fron[num_elem_fron]=element_topologies[i];
/*
	printf("P[%i] ElemOld %3i es frontera %3lu, newVTX: ",rank,i,num_elem_fron);
	for(int j=0; j<4; j++) { // se visitan lo vertices del elemento i
	  dumy = (size_t)vtx2_handle_fron[4*num_elem_fron+j]; // handle original del vertice
	  printf("%3lu ", (size_t)dumy);
	}
	printf("\n");
*/
	num_elem_fron++; // nuevo elemento frontera
	nPartVTXfronAcumula += nPartVTXfron; // se acumula el num de particiones al que pertenece el elemento frontera
   } // else	
  } // for i 

  float res = (float)nPartVTXfronAcumula / (float)num_elem_fron; // num promedio de particiones por vertice frontera

  nPartVTXfron=0;
  char grafo[10000];
  sprintf(grafo,"P[%i] GrafoConexionesFrontera: ",rank); 
  for(int j=0; j<Ncpu; j++){
	if(cpu_total[j] != 0) {
	  nPartVTXfron++; // indica el número de particiones al que pertenece un elemento frontera
	  if (j != rank) sprintf(grafo,"%s %i",grafo,j);
	}
  }

  printf("\nP[%i] \tElementosInternos: %i, \n\tElementosGhost: %i, \n\tElemFrontera: %lu, \n\ttotalElemFichero: %lu, \n\telemIntFrontera: %i, \n\telemTotFicheroFrontera: %lu, \n\tnPartVTXfron: %f, \n\tnPartcionesCompartenFrontera(P%i): %lu - : %s\n",rank,elemInternos,elemGhost,num_elem_fron,elemInternos+elemGhost+num_elem_fron,elemIntFrontera, num_elem_fron+elemGhost+elemIntFrontera,res,rank,nPartVTXfron,grafo);
  fflush(stdout);

  // se guarda la malla de frontera entre particiones
  guardaFrontera(num_vtx_fron,num_elem_fron,vtx_fixed_fron,coord_array_fron,elem_topo_fron,vtx2_handle_fron,tag_data_PROC_fron,tag_data_ID_fron); 

//
  resultados[0] = num_elem; // numero elementos de la particion
  resultados[1] = num_vtx; // numero vtx de la particion
  resultados[2] = elemInternos; // numero elementos internos de la particion
  resultados[3] = num_elem_fron; // numero elementos frontera de la particion
  resultados[4] = num_elem_fron+elemGhost+elemIntFrontera; // numero elementos fichero frontera 
//
  free(coord);
  free(coord_array_fron);
  free(element_topologies);
  free(elem_topo_fron);
  free(elem_handle_fron);
  free(vtx_fixed_fron);
  free(vtx_handle_fron);
  free(vtxOriginalVTXfrontera);
  free(vtx2_handle_fron);
  free(tag_data_ID);
  free(tag_data_PROC);
  free(tag_data_ID_fron);
  free(tag_data_PROC_fron);

  return resultados;
}

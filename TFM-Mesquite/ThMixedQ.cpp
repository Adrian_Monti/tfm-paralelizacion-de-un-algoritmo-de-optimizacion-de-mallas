/* ***************************************************************** 
    MESQUITE -- The Mesh Quality Improvement Toolkit

    Copyright 2007 Sandia National Laboratories.  Developed at the
    University of Wisconsin--Madison under SNL contract number
    624796.  The U.S. Government and the University of Wisconsin
    retain certain rights to this software.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License 
    (lgpl.txt) along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    (2008) kraftche@cae.wisc.edu    
    (2013) dbenitez@siani.es

  ***************************************************************** */


/** \file ThMixedQ.cpp
 *  \brief 
 *  \author Domingo Benitez
 *  Calcula la metrica log barrier de un tetraedro
 */

//#define debugPrintf

#include "Mesquite.hpp"
#include "ThMixedQ.hpp"
#include "MsqMatrix.hpp"
#include "MsqError.hpp"
#include "Exponent.hpp"
#include <iostream>
#include <cfloat>
#include <limits>
#include <math.h>
#include "MsqMeshEntity.hpp"
#include "IdealShapeTarget.hpp"
#include <stdio.h>
#include "TargetMetricUtil.hpp"

#define umbralArgumentoExponencial 1e2

using std::cout;
using std::endl;
using std::fixed;

typedef std::numeric_limits< double > dbl;

namespace MESQUITE_NS {

std::string ThMixedQ::get_name() const
  { return "ThMixedQ"; }

ThMixedQ::~ThMixedQ() { } //{free(handleVerticesCache);}

  // Calcula la calidad de un elemento
  bool ThMixedQ::evaluate_internal( PatchData& pd,
                  size_t handle,
                  double& value,
		  size_t* indices,
		  size_t& num_indices,
                  MsqError& err )
{
#ifdef debugPrintf
 cout << "ThMixedQ::evaluate_internal " << endl;
#endif
 NtetraVisitados++;

 double vol; // = det(T);
 double w_vol, w_q ; //= 1 / (1+exp(vol));
 double fval;
 double q, Q, LOG;
 double alfa=mALFA, beta=mBETA, lambda=mLAMBDA;
 std::vector<Vector3D> vert;
 double dumy1, dumy4;

 const size_t e = ElemSampleQM::  elem( handle );
 MsqMeshEntity& elem = pd.element_by_index( e );
 EntityTopology type = elem.get_element_type();
 unsigned edim = TopologyInfo::dimension( type ); // dimension del elemento: 2D=2, 3D=3

 vert.clear();
 // obtiene vertices del elemento
 pd.get_element_vertex_coordinates(handle, vert, err);  MSQ_ERRZERO(err); 
 // volumen del elemento
 if (edim == 3)
 	vol = (vert[1] - vert[0]) % ((vert[2] - vert[0]) * (vert[3] - vert[0])) / 6.0; 
 // area del triandulo
 else if (edim == 2) {
 	//vert[3] = (vert[1] - vert[0]) * (vert[2] - vert[0]); 
 	vert[0] = (vert[1] - vert[0]) * (vert[2] - vert[0]); 
	vol = 0.5 * sqrt(vert[0].length_squared());
 }


 dumy1 = alfa * lambda * vol;
 if (dumy1 < umbralArgumentoExponencial) {
 //if (vol < umbralArgumentoExponencial) {
     if (dumy1 < -1.0 * umbralArgumentoExponencial) w_vol = 1.0;
     //if (vol < -1.0 * umbralArgumentoExponencial) w_vol = 1.0;
     else {
	//dumy1 = alfa * lambda * vol;
  	w_vol = 1 / (1 + exp(dumy1));
     }
 }
 else w_vol = 0;

 // sum squares of edge lengths
 if (edim == 3)
   fval = (vert[1] - vert[0]).length_squared()
 	+ (vert[2] - vert[0]).length_squared()
 	+ (vert[3] - vert[0]).length_squared()
 	+ (vert[2] - vert[1]).length_squared()
 	+ (vert[3] - vert[1]).length_squared()
 	+ (vert[3] - vert[2]).length_squared();
 else if (edim == 2)
   fval = (vert[1] - vert[0]).length_squared()
 	+ (vert[2] - vert[0]).length_squared()
 	+ (vert[2] - vert[1]).length_squared();

 ultimoVol = vol;
 ultimoL = fval;

 //if (fval > 0) q = vol / d23.powThreeHalves(fval);
 if (fval > 0) {
    if (edim == 3)
	q = vol / sqrt(fval * fval * fval);
    else if (edim == 2)
	q = vol / fval;
 }
 else q = 0;

  //printf("vol= %8.1e, L= %8.1e q= %8.1e, L32= %8.3e\n", vol, fval, q, sqrt(fval * fval * fval));
/*
 if (fabs(q) > 1/umbralArgumentoExponencial) {
	dumy4 = -1.0 * beta / q;
  	w_q = 1 / (1 + exp(dumy4));
 }
 else if (q >= 0) w_q = 1;
 else if (q < 0)  w_q = 0;
*/

 //dumy4 = -1.0 * beta * q;
 dumy4 = -1.0 * beta * q * lambda;
 if (dumy4 < umbralArgumentoExponencial) {
 //if (q < umbralArgumentoExponencial) {
     if (dumy4 < -1.0 * umbralArgumentoExponencial) {
     //if (q < -1.0 * umbralArgumentoExponencial) {
	w_q = 1.0;
	//w_q = 0.0;
	//dumy4 = umbralArgumentoExponencial;
     }
     else {
	//dumy4 = -1.0 * beta * q;
  	w_q = 1 / (1 + exp(dumy4));
    }
 }
 else {
	w_q = 0;
	//w_q = 1;
	//dumy4 = -1.0 * umbralArgumentoExponencial;
 }

 Q = w_vol * vol * lambda + w_q * q;
 //Q = w_vol * vol * lambda + w_q * q * lambda;
 value = Q;

 if(vol != vol || fval != fval) printf("ThMixedQ.cpp - evaluate - NaN - vol= %8.1e, w_vol= %8.1e, L= %8.1e q= %8.1e, w_q= %8.1e, Q= %8.1e, dumy1= %8.1e, dumy4= %8.1e \n", vol, w_vol, fval, q, w_q, Q, dumy1, dumy4);
//printf("ThMixedQ::evaluate, handle= %3lu \tQ= %8.1e \tvol= %8.1e \tq= %8.1e \n",  handle,value,vol,q );
#ifdef debugPrintf
  //printf("ThMixedQ.cpp - evaluate - handle: %lu, vol= %8.1e, w_vol= %8.1e, L= %8.1e q= %8.1e, w_q= %8.1e, Q= %8.1e, dumy1= %8.1e, dumy4= %8.1e \n", handle, vol, w_vol, fval, q, w_q, Q, dumy1, dumy4);

  Mesh::ElementHandle* elem_array = pd.get_element_handles_array(); // todos handles de elem de patch
  //unsigned num_ele = pd.num_elements();
  //Mesh::VertexHandle* vtx_array = pd.get_vertex_handles_array(); // todos handles de vtx de patch

  //MsqMeshEntity*  elem = pd.get_element_array(err); // todos elem del patch

/* printa los IDs de los elementos del patch
  int j = handle;
  size_t *dumy=elem[j].get_vertex_index_array(); // handles de cada vtx de cada elem
 	printf("patch, elemHandle: %3lu, vtxIDs: ", (long unsigned int) elem_array[j]);
  for(int k=0; k<4;k++) {
	size_t dumy2 = (size_t) vtx_array[dumy[k]]; // ID del vtx del elem
  	printf("%3lu ", dumy2);
  }
  printf("\n");
*/

  //printf("patch, elementos: %u, IDs: ", num_ele);
  //if (num_ele < 100) for(int j=0; j<num_ele; j++) printf("%lu ", (long unsigned int) elem_array[j]);
  //printf("\n");
  printf("ThMixedQ.cpp-evaluate_internal- elemHandle: %4lu, vol= %8.1e, w_vol= %8.1e, L= %8.1e q= %8.1e, w_q= %8.1e, Q= %10.3e, dumy1= %8.1e, dumy4= %8.1e alfa= %9.3e beta= %9.3e\n", (long unsigned int) elem_array[handle], vol, w_vol, fval, q, w_q, Q, dumy1, dumy4, alfa, beta);

#endif

  return true;
}

  // identifica los nodos libres de una estrella/patch
  void ThMixedQ::get_evaluations( PatchData& pd,
	std::vector<size_t>& handles,
	bool free_only,
	MsqError& err )
  {
	size_t num_elem = pd.num_elements();
	if (!free_only) {
	  handles.resize( num_elem );
	  for (size_t i = 0; i < num_elem; ++i) handles[i] = i;
	  return;
	}

	handles.clear();
	for (size_t i = 0; i < num_elem; ++i) {
	  // check if element has any free vertices
	  MsqMeshEntity& elem = pd.element_by_index(i);
	  unsigned num_vtx = elem.node_count();
	  size_t* vtx = elem.get_vertex_index_array();
	  unsigned j;

	  for (j = 0; j < num_vtx && vtx[j] >= pd.num_free_vertices(); ++j);
	  if (j < num_vtx)
	    handles.push_back(i);
	  }
  }


bool ThMixedQ::evaluate_with_gradient( PatchData& pd,
                                             size_t handle,
                                             double& value,
                                             std::vector<size_t>& indices,
                                             std::vector<Vector3D>& grad,
                                             MsqError& err )
{
#ifdef debugPrintf
  cout << "ThMixedQ::evaluate_with_gradient " << endl;
#endif
  NtetraVisitados++;

  double alfa=mALFA, beta=mBETA, lambda=mLAMBDA;
  const Sample s = ElemSampleQM::sample( handle );
  const size_t e = ElemSampleQM::  elem( handle );
  MsqMeshEntity& elem = pd.element_by_index( e );
  EntityTopology type = elem.get_element_type();
  const MappingFunction3D* mf = pd.get_mapping_function_3D( type );
  const NodeSet bits = pd.non_slave_node_set( e );
  size_t num_idx = 0;

/*
  MsqMatrix<3,3> A, W;
  mf->jacobian( pd, e, bits, s, mIndices, mDerivs3D, num_idx, A, err );
  MSQ_ERRZERO(err);
  targetCalc->get_3D_target( pd, e, s, W, err ); MSQ_ERRZERO(err);
  const MsqMatrix<3,3> Winv = inverse(W);
  const MsqMatrix<3,3> T = A*Winv;
*/

  double vol, q, L;
  std::vector<Vector3D> vert;
  vert.clear();
  pd.get_element_vertex_coordinates(handle, vert, err);  MSQ_ERRZERO(err); // obtiene vertices del elemento

 //const size_t e = ElemSampleQM::  elem( handle );
 //MsqMeshEntity& elem = pd.element_by_index( e );
 //EntityTopology type = elem.get_element_type();
 unsigned edim = TopologyInfo::dimension( type ); // dimension del elemento: 2D=2, 3D=3


 // volumen del elemento
 if (edim == 3)
 	vol = (vert[1] - vert[0]) % ((vert[2] - vert[0]) * (vert[3] - vert[0])) / 6.0; 
 // area del triandulo
 else if (edim == 2) {
 	vert[0] = (vert[1] - vert[0]) * (vert[2] - vert[0]); 
	vol = 0.5 * sqrt(vert[0].length_squared());
 }
 //vol = (vert[1] - vert[0]) % ((vert[2] - vert[0]) * (vert[3] - vert[0])) / 6.0; // volumen de elemento


 // sum squares of edge lengths
 if (edim == 3)
   L = (vert[1] - vert[0]).length_squared()
 	+ (vert[2] - vert[0]).length_squared()
 	+ (vert[3] - vert[0]).length_squared()
 	+ (vert[2] - vert[1]).length_squared()
 	+ (vert[3] - vert[1]).length_squared()
 	+ (vert[3] - vert[2]).length_squared();
 else if (edim == 2)
   L = (vert[1] - vert[0]).length_squared()
 	+ (vert[2] - vert[0]).length_squared()
 	+ (vert[2] - vert[1]).length_squared();
/*
  L = (vert[1] - vert[0]).length_squared()
  + (vert[2] - vert[0]).length_squared()
  + (vert[3] - vert[0]).length_squared()
  + (vert[2] - vert[1]).length_squared()
  + (vert[3] - vert[1]).length_squared()
  + (vert[3] - vert[2]).length_squared();
*/

  double dumy1; // = alfa * vol * lambda;
  double dumy4; // = -1.0 * beta * q;
  double w_vol, w_q ; //= 1 / (1+exp(vol));
  //Exponent d23;

//
 dumy1 = alfa * lambda * vol;
 double dumy2 ; //= exp(dumy1);
 if (dumy1 < umbralArgumentoExponencial) {
 //if (vol < umbralArgumentoExponencial) {
     if (dumy1 < -1.0 * umbralArgumentoExponencial) {
     //if (vol < -1.0 * umbralArgumentoExponencial) {
	w_vol = 1.0;
	//dumy1 = -1.0 * umbralArgumentoExponencial;
 	dumy2  = 0.0;
     }
     else {
	//dumy1 = alfa * lambda * vol;
  	w_vol = 1 / (1 + exp(dumy1));
 	dumy2  = exp(dumy1);
     }
 }
 else {
	w_vol = 0;
	//dumy1 = umbralArgumentoExponencial;
 	dumy2  = MSQ_MAX - 1.0;
 }
//


 if (L > 0) {
    if (edim == 3)
	q = vol / sqrt(L * L * L);
    else if (edim == 2)
	q = vol / L;
 }
 else q = 0;

 //if (L > 0) q = vol / d23.powThreeHalves(L);
 //if (L > 0) q = vol / sqrt(L * L * L);
 //else q = 0;

 //dumy4 = -1.0 * beta * q;
 dumy4 = -1.0 * beta * q * lambda;
 double dumy5 ; //= exp(dumy4);
 if (dumy4 < umbralArgumentoExponencial) {
 //if (q < umbralArgumentoExponencial) {
     if (dumy4 < -1.0 * umbralArgumentoExponencial) {
     //if (q < -1.0 * umbralArgumentoExponencial) {
	w_q = 1.0;
	//w_q = 0.0;
	//dumy4 = umbralArgumentoExponencial;
	dumy5 = 0.0;
     }
     else {
	//dumy4 = -1.0 * beta * q;
  	w_q = 1 / (1 + exp(dumy4));
 	dumy5  = exp(dumy4);
    }
 }
 else {
	w_q = 0.;
	//w_q = 1;
	//dumy4 = -1.0 * umbralArgumentoExponencial;
	dumy5 = MSQ_MAX - 1.0;
 }
//

  double Q = w_vol * vol * lambda + w_q * q;
  //double Q = w_vol * vol * lambda + w_q * q * lambda;
  value = Q;

  double dumy3 = 1 + dumy2;
  double dumy6 = 1 + dumy5;

if(edim ==3) {
  MsqMatrix<3,3> A, W;
  mf->jacobian( pd, e, bits, s, mIndices, mDerivs3D, num_idx, A, err );
  MSQ_ERRZERO(err);
  targetCalc->get_3D_target( pd, e, s, W, err ); MSQ_ERRZERO(err);
  const MsqMatrix<3,3> Winv = inverse(W);
  const MsqMatrix<3,3> T = A*Winv;

  MsqMatrix<3,3> deriv_vol_wrt_T;
  deriv_vol_wrt_T  = (1.0/6.0) * det(W) * transpose_adj(T); // d(vol)/dS

  MsqMatrix<3,3> deriv_q_wrt_T;
  //d23.set_exponent(5.0/2.0);
  double dd = sqrt(L*L*L*L*L);
  //deriv_q_wrt_T = ( det(W) * transpose_adj(T) / (6 * d23.powThreeHalves(L)) ) - (4.0 * vol * T / d23.std_pow(L));
  //deriv_q_wrt_T = lambda * (( det(W) * transpose_adj(T) / (6 * d23.powThreeHalves(L)) ) - (4.0 * vol * T / d23.std_pow(L)));
  //deriv_q_wrt_T = lambda * (( det(W) * transpose_adj(T) / (6 * d23.powThreeHalves(L)) ) - (4.0 * vol * T / dd));
  //deriv_q_wrt_T = lambda * (( det(W) * transpose_adj(T) / (6 * sqrt(L*L*L)) ) - (4.0 * vol * T / dd));
  deriv_q_wrt_T = ( det(W) * transpose_adj(T) / (6 * sqrt(L*L*L)) ) - (4.0 * vol * T / dd);

  // ... dQ/dS ...

  MsqMatrix<3,3> deriv_Q_wrt_T, deriv_Q_wrt_T1, deriv_Q_wrt_T2;
  //deriv_Q_wrt_T1 = lambda * (1.0 + dumy2 * (1.0 - dumy1)) * deriv_vol_wrt_T / (dumy3 * dumy3);

 if (dumy1 < umbralArgumentoExponencial) {
     if (dumy1 < -1.0 * umbralArgumentoExponencial) {
  	deriv_Q_wrt_T1 = lambda * deriv_vol_wrt_T; 
     }
     else {
  	deriv_Q_wrt_T1 = lambda * (1.0 + dumy2 * (1.0 - dumy1)) * deriv_vol_wrt_T / (dumy3 * dumy3);
     }
 }
 else {
	deriv_Q_wrt_T1.zero();
 }


  //deriv_Q_wrt_T2 = (1.0 + dumy5 * (1.0 - dumy4)) * deriv_q_wrt_T / (dumy6 * dumy6);

 if (dumy4 < umbralArgumentoExponencial) {
     if (dumy4 < -1.0 * umbralArgumentoExponencial) {
	deriv_Q_wrt_T2 = deriv_q_wrt_T; 
     }
     else {
  	deriv_Q_wrt_T2 = (1.0 + dumy5 * (1.0 - dumy4)) * deriv_q_wrt_T / (dumy6 * dumy6);
     }
 }
 else {
	deriv_Q_wrt_T2.zero();
 }

  deriv_Q_wrt_T = deriv_Q_wrt_T1 + deriv_Q_wrt_T2;

  // ... dF/dS ... termino del gradiente de la funcion objetivo log-barrier (Fi)
  //MsqMatrix<3,3> deriv_F_wrt_T;
  //deriv_F_wrt_T  = deriv_Q_wrt_T / (value - mCalidadMinima); 

  // operador que calcula el gradiente, en TargetMetric.hpp
  //gradient<3>( num_idx, mDerivs3D, deriv_F_wrt_T * transpose(Winv), grad );
  gradient<3>( num_idx, mDerivs3D, deriv_Q_wrt_T * transpose(Winv), grad );


  //std::vector<Vector3D> mGradientDumy = mGradient;
  for (size_t j = 0; j < num_idx; ++j) {
	for(int i=0; i<3; i++) {
		double localDumy = grad[j][i];
		double localDumy2 = deriv_Q_wrt_T(j,i);
		double localDumy3 = deriv_Q_wrt_T1(j,i);
		double localDumy4 = deriv_Q_wrt_T2(j,i);
		double localDumy5 = deriv_vol_wrt_T(j,i);
		//if(localDumy != localDumy) printf("ThMixedQ.cpp - NaN - deriv_Q_wrt - T= %e, T1= %e, T2= %e deriv_vol_wrt_T= %e, dumy2= %e, dumy3= %e, vol= %e, dumy1= %e \n", localDumy2, localDumy3, localDumy4, localDumy5, dumy2, dumy3, vol, dumy1);
		if(localDumy != localDumy) printf("ThMixedQ.cpp - evaluate_with_gradient - NaN - vol= %8.1e, w_vol= %8.1e, L= %8.1e q= %8.1e, w_q= %8.1e, Q= %8.1e, dumy1= %8.1e, dumy4= %8.1e \n", vol, w_vol, L, q, w_q, Q, dumy1, dumy4);
	}
  }

#ifdef debugPrintf
  //printf("ThMixedQ.cpp - evaluate_with_gradient - handle: %lu, vol= %8.1e, w_vol= %8.1e, L= %8.1e q= %8.1e, w_q= %8.1e, Q= %8.1e, dumy1= %8.1e, dumy4= %8.1e \n", handle, vol, w_vol, L, q, w_q, Q, dumy1, dumy4);
  Mesh::ElementHandle* elem_array = pd.get_element_handles_array();
  //unsigned num_ele = pd.num_elements();
  //printf("patch, elementos: %u, IDs: ", num_ele);
  //if (num_ele < 100) for(int j=0; j<num_ele; j++) printf("%lu ", (long unsigned int) elem_array[j]);
  //printf("\n");
  printf("ThMixedQ.cpp - evaluate_with_gradient - handle: %4lu, vol= %8.1e, w_vol= %8.1e, L= %8.1e q= %8.1e, w_q= %8.1e, Q= %10.3e, dumy1= %8.1e, dumy4= %8.1e alfa= %9.3e beta= %9.3e\n", (long unsigned int) elem_array[handle], vol, w_vol, L, q, w_q, Q, dumy1, dumy4, alfa, beta);

#endif

} // edim = 3
else if (edim == 2) {
  MsqMatrix<2,2> A, W;
  MsqMatrix<3,2> S_a_transpose_Theta;

  bool rval = evaluate_surface_common( pd, s, e, bits, mIndices, num_idx,
                                 mDerivs2D, W, A, S_a_transpose_Theta, err ); 
  const MsqMatrix<2,2> Winv = inverse(W);
  const MsqMatrix<2,2> T = A*Winv;
  MsqMatrix<2,2> deriv_vol_wrt_T;
  deriv_vol_wrt_T  = 0.5 * det(W) * transpose_adj(T); // d(vol)/dS
  MsqMatrix<2,2> deriv_q_wrt_T;
  double dd = L*L;
  deriv_q_wrt_T = (L * deriv_vol_wrt_T - vol * T) / dd;

  // ... dQ/dS ...

  MsqMatrix<2,2> deriv_Q_wrt_T, deriv_Q_wrt_T1, deriv_Q_wrt_T2;

  if (dumy1 < umbralArgumentoExponencial) {
     if (dumy1 < -1.0 * umbralArgumentoExponencial) {
  	deriv_Q_wrt_T1 = lambda * deriv_vol_wrt_T; 
     }
     else {
  	deriv_Q_wrt_T1 = lambda * (1.0 + dumy2 * (1.0 - dumy1)) * deriv_vol_wrt_T / (dumy3 * dumy3);
     }
  }
  else {
	deriv_Q_wrt_T1.zero();
  }
  if (dumy4 < umbralArgumentoExponencial) {
     if (dumy4 < -1.0 * umbralArgumentoExponencial) {
	deriv_Q_wrt_T2 = deriv_q_wrt_T; 
     }
     else {
  	deriv_Q_wrt_T2 = (1.0 + dumy5 * (1.0 - dumy4)) * deriv_q_wrt_T / (dumy6 * dumy6);
     }
  }
  else {
	deriv_Q_wrt_T2.zero();
  }

  deriv_Q_wrt_T = deriv_Q_wrt_T1 + deriv_Q_wrt_T2;
  gradient<2>( num_idx, mDerivs2D, S_a_transpose_Theta * deriv_Q_wrt_T * transpose(Winv), grad );
} // edim = 2

    // pass back index list
  indices.resize( num_idx );
  std::copy( mIndices, mIndices+num_idx, indices.begin() );
  
    // apply target weight to value
  weight( pd, s, e, num_idx, value, grad.empty() ? 0 : arrptr(grad), 0, 0, err ); MSQ_ERRZERO(err);

  return true;
} // evaluate_with_grad()

} // namespace Mesquite
